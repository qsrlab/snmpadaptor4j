package net.sf.snmpadaptor4j.example.mbean;

/**
 * Example standard MBean.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public interface ExampleMBean {

	/**
	 * Returns the value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#integer32 integer32} as integer.
	 * @return Value as integer.
	 */
	int getInteger32AsInteger ();

	/**
	 * Sets the value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#integer32 integer32} as integer.
	 * @param value Value as integer.
	 */
	void setInteger32AsInteger (int value);

	/**
	 * Returns the value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#integer32 integer32} as byte.
	 * @return Value as byte.
	 */
	byte getInteger32AsByte ();

	/**
	 * Sets the value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#integer32 integer32} as byte.
	 * @param value Value as byte.
	 */
	void setInteger32AsByte (byte value);

	/**
	 * Returns the value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#integer32 integer32} as short.
	 * @return Value as short.
	 */
	short getInteger32AsShort ();

	/**
	 * Sets the value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#integer32 integer32} as short.
	 * @param value Value as short.
	 */
	void setInteger32AsShort (short value);

	/**
	 * Returns the value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#integer32 integer32} as boolean.
	 * @return Value as boolean.
	 */
	boolean isInteger32AsBoolean ();

	/**
	 * Sets the value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#integer32 integer32} as boolean.
	 * @param value Value as boolean.
	 */
	void setInteger32AsBoolean (boolean value);

	/**
	 * Returns the value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#unsigned32 unsigned32} as long.
	 * @return Value as long.
	 */
	long getUnsigned32AsLong ();

	/**
	 * Sets the value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#unsigned32 unsigned32} as long.
	 * @param value Value as long.
	 */
	void setUnsigned32AsLong (long value);

	/**
	 * Returns the value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#unsigned32 unsigned32} as integer.
	 * @return Value as integer.
	 */
	int getUnsigned32AsInteger ();

	/**
	 * Sets the value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#unsigned32 unsigned32} as integer.
	 * @param value Value as integer.
	 */
	void setUnsigned32AsInteger (int value);

	/**
	 * Returns the value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#unsigned32 unsigned32} as byte.
	 * @return Value as byte.
	 */
	byte getUnsigned32AsByte ();

	/**
	 * Sets the value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#unsigned32 unsigned32} as byte.
	 * @param value Value as byte.
	 */
	void setUnsigned32AsByte (byte value);

	/**
	 * Returns the value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#unsigned32 unsigned32} as short.
	 * @return Value as short.
	 */
	short getUnsigned32AsShort ();

	/**
	 * Sets the value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#unsigned32 unsigned32} as short.
	 * @param value Value as short.
	 */
	void setUnsigned32AsShort (short value);

}