package net.sf.snmpadaptor4j.example;

/**
 * Startup class for run the example.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class Startup {

	/**
	 * Main function.
	 * @param args Arguments.
	 * @throws Exception Exception if an error occurred.
	 */
	public static void main (final String[] args) throws Exception {

		if(args.length==0){
			System.out.println("you need to give one parameter which is  the port where to listen to");
			System.exit(1);
		}
		int port = 162;
		try{
			port = Integer.parseInt(args[0]);
		}catch(NumberFormatException ex){
			System.out.println("first parameter must be an integer");
			System.exit(2);
		}
		if(port<0 || port > 65535){
			System.out.println("first parameter must be an integer between 0 and 65535");
			System.exit(3);
		}
		final SnmpTrapManager snmpManager = new SnmpTrapManager(port);
		snmpManager.start();

		try {
			System.out.println("This  will receive traps and display on screen the trap content");
			int key;
			System.out.println();
			System.out.println("Q - Quit");
			do {

				while (System.in.available() == 0) {
					Thread.sleep(500);
				}
				key = System.in.read();
				while (System.in.available() > 0) {
					System.in.read();
				}
			}
			while ((key != 113) && (key != 81));
			System.out.println("Bye.");
		}
		finally {

			snmpManager.stop();
		}
	}

}