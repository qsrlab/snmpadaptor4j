package net.sf.snmpadaptor4j.example;

import java.lang.management.ManagementFactory;
import java.util.HashMap;
import java.util.Map;
import javax.management.MBeanServer;
import javax.management.Notification;
import javax.management.ObjectName;
import net.sf.snmpadaptor4j.SnmpAdaptor;
import net.sf.snmpadaptor4j.example.mbean.Example;

/**
 * JMX manager.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class Jmx {

	/**
	 * MBean server.
	 */
	private final MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();

	/**
	 * SNMP adaptor.
	 */
	private final SnmpAdaptor snmpAdaptor = new SnmpAdaptor(getClass().getResource("/snmp.xml"), true,null);

	/**
	 * Name for the SNMP adaptor.
	 */
	private ObjectName snmpAdaptorMBeanName = null;

	/**
	 * Name for the system informations MBean.
	 */
	private ObjectName systemInfoMBeanName = null;

	/**
	 * Name for the read only MBean.
	 */
	private ObjectName exampleReadOnlyMBeanName = null;

	/**
	 * Name for the writable MBean.
	 */
	private ObjectName exampleWritableMBeanName = null;

	/**
	 * Instance of writable MBean.
	 */
	private Example exampleWritable = null;

	/**
	 * <code>sequenceNumber</code> counter.
	 */
	private long sequenceNumberCounter = 0;

	/**
	 * Constructor.
	 * @throws Exception Exception if an error occurred.
	 */
	public Jmx () throws Exception {
		super();
	}

	/**
	 * Starts JMX.
	 * @throws Exception Exception if an error occurred.
	 */
	public void start () throws Exception {
		this.exampleReadOnlyMBeanName = new ObjectName("net.sf.snmpadaptor4j.example:type=Example,name=readOnly");
		this.mBeanServer.registerMBean(new Example(), this.exampleReadOnlyMBeanName);
		this.snmpAdaptorMBeanName = new ObjectName("net.sf.snmpadaptor4j.example:adaptor=SnmpAdaptor");
		this.mBeanServer.registerMBean(this.snmpAdaptor, this.snmpAdaptorMBeanName);
		this.snmpAdaptor.start();
		this.systemInfoMBeanName = new ObjectName("net.sf.snmpadaptor4j.example:type=SystemInfo");
		this.mBeanServer.registerMBean(this.snmpAdaptor.getSystemInfo(), this.systemInfoMBeanName);
		this.exampleWritable = new Example();
		this.exampleWritableMBeanName = new ObjectName("net.sf.snmpadaptor4j.example:type=Example,name=writable");
		this.mBeanServer.registerMBean(this.exampleWritable, this.exampleWritableMBeanName);
	}

	/**
	 * Stops JMX.
	 * @throws Exception Exception if an error occurred.
	 */
	public void stop () throws Exception {
		this.mBeanServer.unregisterMBean(this.exampleWritableMBeanName);
		this.mBeanServer.unregisterMBean(this.systemInfoMBeanName);
		this.mBeanServer.unregisterMBean(this.snmpAdaptorMBeanName);
		this.mBeanServer.unregisterMBean(this.exampleReadOnlyMBeanName);
		this.snmpAdaptor.stop();
	}

	/**
	 * Sends a notification "integer32AsInteger.down".
	 */
	public void sendNotificationOnInteger32AsIntegerDown () {
		System.out.println();
		System.out.println("\"integer32AsInteger.down\" notification sending...");
		final Notification notification = new Notification("integer32AsInteger.down", this.exampleWritableMBeanName, this.sequenceNumberCounter++, this.snmpAdaptor
				.getSystemInfo().getSysUpTime(), "integer32AsInteger is DOWN");
		notification.setUserData(new Integer(this.exampleWritable.getInteger32AsInteger()));
		this.exampleWritable.sendNotification(notification);
		System.out.println("\"integer32AsInteger.down\" notification sent...");
	}

	/**
	 * Sends a notification "integer32AsInteger.up".
	 */
	public void sendNotificationOnInteger32AsIntegerUp () {
		System.out.println();
		System.out.println("\"integer32AsInteger.up\" notification sending...");
		final Notification notification = new Notification("integer32AsInteger.up", this.exampleWritableMBeanName, this.sequenceNumberCounter++, this.snmpAdaptor
				.getSystemInfo().getSysUpTime(), "integer32AsInteger is UP");
		this.exampleWritable.sendNotification(notification);
		System.out.println("\"integer32AsInteger.up\" notification sent...");
	}

	/**
	 * Sends a notification "integer32AsByte.low".
	 */
	public void sendNotificationOnInteger32AsByteLow () {
		System.out.println();
		System.out.println("\"integer32AsByte.low\" notification sending...");
		final Notification notification = new Notification("integer32AsByte.low", this.exampleWritableMBeanName, this.sequenceNumberCounter++, this.snmpAdaptor
				.getSystemInfo().getSysUpTime(), "integer32AsByte is LOW");
		final Map<String, Object> userDataMap = new HashMap<String, Object>();
		userDataMap.put("userData.1", new Byte(this.exampleWritable.getInteger32AsByte()));
		userDataMap.put("userData.2", "Data 2");
		userDataMap.put("userData.3", "Data 3");
		userDataMap.put("userData.4", "Hidden data 4");
		userDataMap.put("userData.5", "Hidden data 5");
		notification.setUserData(userDataMap);
		this.exampleWritable.sendNotification(notification);
		System.out.println("\"integer32AsByte.low\" notification sent...");
	}

	/**
	 * Sends a notification "integer32AsByte.middle".
	 */
	public void sendNotificationOnInteger32AsByteMiddle () {
		System.out.println();
		System.out.println("\"integer32AsByte.middle\" notification sending...");
		final Notification notification = new Notification("integer32AsByte.middle", this.exampleWritableMBeanName, this.sequenceNumberCounter++, this.snmpAdaptor
				.getSystemInfo().getSysUpTime(), "integer32AsByte is MIDDLE");
		final Map<String, Object> userDataMap = new HashMap<String, Object>();
		userDataMap.put("userData.1", null);
		userDataMap.put("userData.2", "Data 2");
		userDataMap.put("userData.3", null);
		notification.setUserData(userDataMap);
		this.exampleWritable.sendNotification(notification);
		System.out.println("\"integer32AsByte.middle\" notification sent...");
	}

	/**
	 * Sends a notification "integer32AsByte.high".
	 */
	public void sendNotificationOnInteger32AsByteHigh () {
		System.out.println();
		System.out.println("\"integer32AsByte.high\" notification sending...");
		final Notification notification = new Notification("integer32AsByte.high", this.exampleWritableMBeanName, this.sequenceNumberCounter++, this.snmpAdaptor
				.getSystemInfo().getSysUpTime(), "integer32AsByte is HIGH");
		final Map<String, Object> userDataMap = new HashMap<String, Object>();
		userDataMap.put("userData.2", "Data 2");
		notification.setUserData(userDataMap);
		this.exampleWritable.sendNotification(notification);
		System.out.println("\"integer32AsByte.high\" notification sent...");
	}

}