package net.sf.snmpadaptor4j.example;

import java.net.InetAddress;
import java.net.UnknownHostException;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import net.sf.snmpadaptor4j.object.SnmpTrapData;
import org.opennms.protocols.snmp.SnmpCounter32;
import org.opennms.protocols.snmp.SnmpCounter64;
import org.opennms.protocols.snmp.SnmpGauge32;
import org.opennms.protocols.snmp.SnmpIPAddress;
import org.opennms.protocols.snmp.SnmpInt32;
import org.opennms.protocols.snmp.SnmpObjectId;
import org.opennms.protocols.snmp.SnmpOctetString;
import org.opennms.protocols.snmp.SnmpOpaque;
import org.opennms.protocols.snmp.SnmpPduPacket;
import org.opennms.protocols.snmp.SnmpPduRequest;
import org.opennms.protocols.snmp.SnmpPduTrap;
import org.opennms.protocols.snmp.SnmpSyntax;
import org.opennms.protocols.snmp.SnmpTimeTicks;
import org.opennms.protocols.snmp.SnmpTrapHandler;
import org.opennms.protocols.snmp.SnmpTrapSession;
import org.opennms.protocols.snmp.SnmpUInt32;
import org.opennms.protocols.snmp.SnmpVarBind;

/**
 * SNMP manager for the trap receiving. Listen on UDP 1162 port.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SnmpTrapManager
		implements SnmpTrapHandler {

	private int port;

	/**
	 * Daemon session.
	 */
	private SnmpTrapSession daemonSession = null;

	public SnmpTrapManager(int port){
		this.port = port;
	}

	/**
	 * Starts the manager.
	 * @throws Exception Exception if an error has occurred.
	 */
	public synchronized void start () throws Exception {
		if (this.daemonSession == null) {
			System.out.println("SNMP trap manager starting...");
			this.daemonSession = new SnmpTrapSession(this, port);
		}
	}

	/**
	 * Stops the manager.
	 */
	public synchronized void stop () {
		if (this.daemonSession != null) {
			System.out.println("SNMP trap manager stopping...");
			this.daemonSession.close();
		}
	}

	/**
	 * Returns <code>TRUE</code> if the manager is started.
	 * @return <code>TRUE</code> if the manager is started.
	 */
	public boolean isStarted () {
		return (this.daemonSession != null);
	}

	/*
	 * {@inheritDoc}
	 * @see org.opennms.protocols.snmp.SnmpTrapHandler#snmpReceivedTrap(org.opennms.protocols.snmp.SnmpTrapSession, java.net.InetAddress, int,
	 * org.opennms.protocols.snmp.SnmpOctetString, org.opennms.protocols.snmp.SnmpPduPacket)
	 */
	public void snmpReceivedTrap (final SnmpTrapSession session, final InetAddress agent, final int port, final SnmpOctetString community, final SnmpPduPacket pdu) {
		System.out.println("=============================================================================");
		if (pdu.getCommand() == SnmpPduPacket.V2TRAP) {
			System.out.println("SNMP manager says: V2 trap received from agent " + agent + " on port " + port);
			if ((pdu instanceof SnmpPduRequest) && (((SnmpPduRequest) pdu).getErrorStatus() != 0)) {
				System.out.println("SNMP manager says: V2 trap PDU error " + ((SnmpPduRequest) pdu).getErrorStatus() + " at index "
						+ ((SnmpPduRequest) pdu).getErrorIndex());
			}
			else {
				SnmpOid enterprise = null;
				for (final SnmpVarBind varBind : pdu.toVarBindArray()) {
					final SnmpTrapData value = toSnmpTrapData(varBind.getValue());
					final SnmpOid oid = SnmpOid.newInstance(varBind.getName().getIdentifiers());
					if (SnmpOid.newInstance("1.3.6.1.6.3.1.1.4.3.0").equals(oid) && (value.getType() == SnmpDataType.objectIdentifier)) {
						enterprise = (SnmpOid) (value.getValue());
					}
				}
				for (final SnmpVarBind varBind : pdu.toVarBindArray()) {
					final SnmpTrapData value = toSnmpTrapData(varBind.getValue());
					final SnmpOid oid = SnmpOid.newInstance(varBind.getName().getIdentifiers());
					if (SnmpOid.newInstance("1.3.6.1.2.1.1.3.0").equals(oid) && (value.getType() == SnmpDataType.timeTicks)) {
						System.out.println("SNMP manager says: " + oid + " = " + value + " (time-stamp)");
					}
					else if (SnmpOid.newInstance("1.3.6.1.6.3.1.1.4.1.0").equals(oid) && (value.getType() == SnmpDataType.objectIdentifier)) {
						final SnmpOid oidTrapType = (SnmpOid) (value.getValue());
						if (SnmpOid.newInstance("1.3.6.1.6.3.1.1.5.1").equals(oidTrapType)) {
							System.out.println("SNMP manager says: " + oid + " = " + value + " (generic-trap = coldStart)");
						}
						else if (SnmpOid.newInstance("1.3.6.1.6.3.1.1.5.2").equals(oidTrapType)) {
							System.out.println("SNMP manager says: " + oid + " = " + value + " (generic-trap = warmStart)");
						}
						else if (SnmpOid.newInstance("1.3.6.1.6.3.1.1.5.3").equals(oidTrapType)) {
							System.out.println("SNMP manager says: " + oid + " = " + value + " (generic-trap = linkDown)");
						}
						else if (SnmpOid.newInstance("1.3.6.1.6.3.1.1.5.4").equals(oidTrapType)) {
							System.out.println("SNMP manager says: " + oid + " = " + value + " (generic-trap = linkUp)");
						}
						else if (SnmpOid.newInstance("1.3.6.1.6.3.1.1.5.5").equals(oidTrapType)) {
							System.out.println("SNMP manager says: " + oid + " = " + value + " (generic-trap = authenticationFailure)");
						}
						else if (SnmpOid.newInstance("1.3.6.1.6.3.1.1.5.6").equals(oidTrapType)) {
							System.out.println("SNMP manager says: " + oid + " = " + value + " (generic-trap = egpNeighborLoss)");
						}
						else {
							Integer specific = null;
							if (enterprise != null) {
								if (((enterprise.getOid().length + 2) == oidTrapType.getOid().length) && enterprise.isRootOf(oidTrapType)) {
									if (oidTrapType.getOid()[oidTrapType.getOid().length - 2] == 0) {
										specific = new Integer(oidTrapType.getOid()[oidTrapType.getOid().length - 1] - 1);
									}
								}
							}
							if (specific != null) {
								System.out.println("SNMP manager says: " + oid + " = " + value + " (specific-trap = " + specific.intValue() + ")");
							}
							else {
								System.out.println("SNMP manager says: " + oid + " = " + value + " (specific-trap)");
							}
						}
					}
					else if (SnmpOid.newInstance("1.3.6.1.6.3.18.1.3.0").equals(oid) && (value.getType() == SnmpDataType.ipAddress)) {
						System.out.println("SNMP manager says: " + oid + " = " + value + " (agent-addr)");
					}
					else if (SnmpOid.newInstance("1.3.6.1.6.3.18.1.4.0").equals(oid) && (value.getType() == SnmpDataType.octetString)) {
						System.out.println("SNMP manager says: " + oid + " = " + value + " (community)");
					}
					else if (SnmpOid.newInstance("1.3.6.1.6.3.1.1.4.3.0").equals(oid) && (value.getType() == SnmpDataType.objectIdentifier)) {
						System.out.println("SNMP manager says: " + oid + " = " + value + " (enterprise)");
					}
					else {
						System.out.println("SNMP manager says: " + oid + " = " + value);
					}
				}
			}
		}
		else {
			System.out.println("SNMP manager says: another command received from agent " + agent + " on port " + port);
		}
	}

	/*
	 * {@inheritDoc}
	 * @see org.opennms.protocols.snmp.SnmpTrapHandler#snmpReceivedTrap(org.opennms.protocols.snmp.SnmpTrapSession, java.net.InetAddress, int,
	 * org.opennms.protocols.snmp.SnmpOctetString, org.opennms.protocols.snmp.SnmpPduTrap)
	 */
	public void snmpReceivedTrap (final SnmpTrapSession session, final InetAddress agent, final int port, final SnmpOctetString community, final SnmpPduTrap pdu) {
		System.out.println("=============================================================================");
		System.out.println("SNMP manager says: V1 trap received from agent " + agent + " on port " + port);
		System.out.println("SNMP manager says:     enterprise = " + SnmpOid.newInstance(pdu.getEnterprise().getIdentifiers()));
		try {
			System.out.println("SNMP manager says:     agent-addr = " + InetAddress.getByAddress(pdu.getAgentAddress().getString()));
		}
		catch (UnknownHostException e) {
			System.out.println("SNMP manager says:     agent-addr = ERROR");
		}
		if (pdu.getGeneric() == SnmpPduTrap.GenericColdStart) {
			System.out.println("SNMP manager says:   generic-trap = coldStart");
		}
		else if (pdu.getGeneric() == SnmpPduTrap.GenericWarmStart) {
			System.out.println("SNMP manager says:   generic-trap = warmStart");
		}
		else if (pdu.getGeneric() == SnmpPduTrap.GenericLinkDown) {
			System.out.println("SNMP manager says:   generic-trap = linkDown");
		}
		else if (pdu.getGeneric() == SnmpPduTrap.GenericLinkUp) {
			System.out.println("SNMP manager says:   generic-trap = linkUp");
		}
		else if (pdu.getGeneric() == SnmpPduTrap.GenericAuthenticationFailure) {
			System.out.println("SNMP manager says:   generic-trap = authenticationFailure");
		}
		else if (pdu.getGeneric() == SnmpPduTrap.GenericEgpNeighborLoss) {
			System.out.println("SNMP manager says:   generic-trap = egpNeighborLoss");
		}
		else {
			System.out.println("SNMP manager says:   generic-trap = enterpriseSpecific");
		}
		System.out.println("SNMP manager says:  specific-trap = " + pdu.getSpecific());
		System.out.println("SNMP manager says:     time-stamp = " + pdu.getTimeStamp());
		for (final SnmpVarBind varBind : pdu.toVarBindArray()) {
			final SnmpTrapData value = toSnmpTrapData(varBind.getValue());
			final SnmpOid oid = SnmpOid.newInstance(varBind.getName().getIdentifiers());
			System.out.println("SNMP manager says: " + oid + " = " + value);
		}
	}

	/*
	 * {@inheritDoc}
	 * @see org.opennms.protocols.snmp.SnmpTrapHandler#snmpTrapSessionError(org.opennms.protocols.snmp.SnmpTrapSession, int, java.lang.Object)
	 */
	public void snmpTrapSessionError (final SnmpTrapSession session, final int error, final Object ref) {
		System.out.println("SNMP manager says: an error n°" + error + " occurred" + (ref != null ? " (" + ref + ")" : ""));
	}

	/**
	 * Converts a {@link SnmpSyntax} to a {@link SnmpTrapData}.
	 * @param syntax Value as {@link SnmpSyntax}.
	 * @return Value as {@link SnmpTrapData}.
	 */
	private SnmpTrapData toSnmpTrapData (final SnmpSyntax syntax) {
		Object value;
		SnmpDataType type;
		if (syntax instanceof SnmpInt32) {
			value = new Integer(((SnmpInt32) syntax).getValue());
			type = SnmpDataType.integer32;
		}
		else if (syntax instanceof SnmpGauge32) {
			value = new Long(((SnmpGauge32) syntax).getValue());
			type = SnmpDataType.gauge32;
		}
		else if (syntax instanceof SnmpCounter32) {
			value = new Long(((SnmpCounter32) syntax).getValue());
			type = SnmpDataType.counter32;
		}
		else if (syntax instanceof SnmpTimeTicks) {
			value = new Long(((SnmpTimeTicks) syntax).getValue());
			type = SnmpDataType.timeTicks;
		}
		else if (syntax instanceof SnmpUInt32) {
			value = new Long(((SnmpUInt32) syntax).getValue());
			type = SnmpDataType.unsigned32;
		}
		else if (syntax instanceof SnmpCounter64) {
			value = ((SnmpCounter64) syntax).getValue();
			type = SnmpDataType.counter64;
		}
		else if (syntax instanceof SnmpIPAddress) {
			try {
				value = InetAddress.getByAddress(((SnmpIPAddress) syntax).getString());
			}
			catch (final UnknownHostException e) {
				e.printStackTrace();
				value = null;
			}
			type = SnmpDataType.ipAddress;
		}
		else if (syntax instanceof SnmpOpaque) {
			value = new String(((SnmpOpaque) syntax).getString());
			type = SnmpDataType.opaque;
		}
		else if (syntax instanceof SnmpOctetString) {
			value = new String(((SnmpOctetString) syntax).getString());
			type = SnmpDataType.octetString;
		}
		else if (syntax instanceof SnmpObjectId) {
			value = SnmpOid.newInstance(((SnmpObjectId) syntax).getIdentifiers());
			type = SnmpDataType.objectIdentifier;
		}
		else {
			if (syntax != null) {
				System.out.println("SNMP manager says: " + syntax.getClass().getName() + " isn't handled");
			}
			value = null;
			type = null;
		}
		return new SnmpTrapData(type, value);
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "SnmpTrapManager";
	}

}