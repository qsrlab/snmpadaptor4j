package net.sf.snmpadaptor4j.example.mbean;

import javax.management.NotificationBroadcasterSupport;

/**
 * Example standard MBean implementation.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class Example
		extends NotificationBroadcasterSupport
		implements ExampleMBean {

	/**
	 * Value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#integer32 integer32} as integer.
	 */
	private int integer32AsInteger = 0;

	/**
	 * Value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#integer32 integer32} as byte.
	 */
	private byte integer32AsByte = 0;

	/**
	 * Value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#integer32 integer32} as short.
	 */
	private short integer32AsShort = 0;

	/**
	 * Value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#integer32 integer32} as boolean.
	 */
	private boolean integer32AsBoolean = false;

	/**
	 * Value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#unsigned32 unsigned32} as long.
	 */
	private long unsigned32AsLong = 0;

	/**
	 * Value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#unsigned32 unsigned32} as integer.
	 */
	private int unsigned32AsInteger = 0;

	/**
	 * Value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#unsigned32 unsigned32} as byte.
	 */
	private byte unsigned32AsByte = 0;

	/**
	 * Value for {@link net.sf.snmpadaptor4j.object.SnmpDataType#unsigned32 unsigned32} as short.
	 */
	private short unsigned32AsShort = 0;

	/**
	 * Constructor.
	 */
	public Example () {
		super();
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.example.mbean.ExampleMBean#getInteger32AsInteger()
	 */
	public int getInteger32AsInteger () {
		return this.integer32AsInteger;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.example.mbean.ExampleMBean#setInteger32AsInteger(int)
	 */
	public void setInteger32AsInteger (final int value) {
		this.integer32AsInteger = value;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.example.mbean.ExampleMBean#getInteger32AsByte()
	 */
	public byte getInteger32AsByte () {
		return this.integer32AsByte;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.example.mbean.ExampleMBean#setInteger32AsByte(byte)
	 */
	public void setInteger32AsByte (final byte value) {
		this.integer32AsByte = value;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.example.mbean.ExampleMBean#getInteger32AsShort()
	 */
	public short getInteger32AsShort () {
		return this.integer32AsShort;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.example.mbean.ExampleMBean#setInteger32AsShort(short)
	 */
	public void setInteger32AsShort (final short value) {
		this.integer32AsShort = value;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.example.mbean.ExampleMBean#isInteger32AsBoolean()
	 */
	public boolean isInteger32AsBoolean () {
		return this.integer32AsBoolean;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.example.mbean.ExampleMBean#setInteger32AsBoolean(boolean)
	 */
	public void setInteger32AsBoolean (final boolean integer32AsBoolean) {
		this.integer32AsBoolean = integer32AsBoolean;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.example.mbean.ExampleMBean#getUnsigned32AsLong()
	 */
	public long getUnsigned32AsLong () {
		return this.unsigned32AsLong;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.example.mbean.ExampleMBean#setUnsigned32AsLong(long)
	 */
	public void setUnsigned32AsLong (final long value) {
		this.unsigned32AsLong = value;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.example.mbean.ExampleMBean#getUnsigned32AsInteger()
	 */
	public int getUnsigned32AsInteger () {
		return this.unsigned32AsInteger;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.example.mbean.ExampleMBean#setUnsigned32AsInteger(int)
	 */
	public void setUnsigned32AsInteger (final int value) {
		this.unsigned32AsInteger = value;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.example.mbean.ExampleMBean#getUnsigned32AsByte()
	 */
	public byte getUnsigned32AsByte () {
		return this.unsigned32AsByte;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.example.mbean.ExampleMBean#setUnsigned32AsByte(byte)
	 */
	public void setUnsigned32AsByte (final byte value) {
		this.unsigned32AsByte = value;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.example.mbean.ExampleMBean#getUnsigned32AsShort()
	 */
	public short getUnsigned32AsShort () {
		return this.unsigned32AsShort;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.example.mbean.ExampleMBean#setUnsigned32AsShort(short)
	 */
	public void setUnsigned32AsShort (final short value) {
		this.unsigned32AsShort = value;
	}

}