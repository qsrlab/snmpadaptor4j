package net.sf.snmpadaptor4j.core.mapping;

import java.io.Serializable;
import net.sf.snmpadaptor4j.object.SnmpOid;

/**
 * Object containing the mapping to build the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} from a JMX notification.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public abstract class DataMapTrapMapping
		implements Serializable {

	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = 2276163678262962818L;

	/**
	 * OID of <code>sequenceNumber</code> field of JMX notification.
	 */
	private final SnmpOid sequenceNumberOid;

	/**
	 * OID of <code>message</code> field of JMX notification.
	 */
	private final SnmpOid messageOid;

	/**
	 * <code>TRUE</code> for put all system information attributes in the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 */
	private final boolean hasSystemInfo;

	/**
	 * Hidden constructor (abstract class).
	 * @param sequenceNumberOid OID of <code>sequenceNumber</code> field of JMX notification. Is NULL if the <code>sequenceNumber</code> should not be present in
	 *            <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 * @param messageOid OID of <code>message</code> field of JMX notification. Is NULL if the <code>message</code> should not be present in <code>dataMap</code>
	 *            field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 * @param hasSystemInfo <code>TRUE</code> for put all system information attributes in the <code>dataMap</code> field of
	 *            {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 */
	protected DataMapTrapMapping (final SnmpOid sequenceNumberOid, final SnmpOid messageOid, final boolean hasSystemInfo) {
		super();
		this.sequenceNumberOid = sequenceNumberOid;
		this.messageOid = messageOid;
		this.hasSystemInfo = hasSystemInfo;
	}

	/**
	 * Returns the OID of <code>sequenceNumber</code> field of JMX notification.
	 * @return OID of <code>sequenceNumber</code> field of JMX notification. Is NULL if the <code>sequenceNumber</code> should not be present in <code>dataMap</code>
	 *         field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 */
	public final SnmpOid getSequenceNumberOid () {
		return this.sequenceNumberOid;
	}

	/**
	 * Returns the OID of <code>message</code> field of JMX notification.
	 * @return OID of <code>message</code> field of JMX notification. Is NULL if the <code>message</code> should not be present in <code>dataMap</code> field of
	 *         {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 */
	public final SnmpOid getMessageOid () {
		return this.messageOid;
	}

	/**
	 * Returns <code>TRUE</code> for put all system information attributes in the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}
	 * .
	 * @return <code>TRUE</code> for put all system information attributes in the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}
	 *         .
	 */
	public final boolean isHasSystemInfo () {
		return this.hasSystemInfo;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.hasSystemInfo ? 1231 : 1237);
		result = prime * result + ((this.messageOid == null) ? 0 : this.messageOid.hashCode());
		result = prime * result + ((this.sequenceNumberOid == null) ? 0 : this.sequenceNumberOid.hashCode());
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean result = false;
		if ((obj != null) && getClass().equals(obj.getClass())) {
			final DataMapTrapMapping other = (DataMapTrapMapping) obj;
			result = (this.sequenceNumberOid != null ? this.sequenceNumberOid.equals(other.sequenceNumberOid) : (other.sequenceNumberOid == null));
			if (result) {
				result = (this.messageOid != null ? this.messageOid.equals(other.messageOid) : (other.messageOid == null));
			}
			if (result) {
				result = (this.hasSystemInfo == other.hasSystemInfo);
			}
		}
		return result;
	}

}