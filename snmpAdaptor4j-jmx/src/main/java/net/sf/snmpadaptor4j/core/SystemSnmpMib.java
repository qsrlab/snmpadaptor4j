package net.sf.snmpadaptor4j.core;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;
import net.sf.snmpadaptor4j.SnmpAppContext;
import net.sf.snmpadaptor4j.api.SnmpMib;
import net.sf.snmpadaptor4j.api.AttributeAccessor;
import net.sf.snmpadaptor4j.mbean.SystemInfo;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;

/**
 * Object representing the <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB) for system attributes.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public class SystemSnmpMib
		implements SnmpMib {

	/**
	 * External node of the SNMP <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 */
	protected final class ExternalNode
			implements AttributeAccessor {

		/**
		 * Index of attribute.
		 */
		private final int index;

		/**
		 * <b>O</b>bject <b>ID</b>entifier (OID) of MIB node.
		 */
		private final SnmpOid oid;

		/**
		 * Constructor.
		 * @param index Index of attribute.
		 * @param oid <b>O</b>bject <b>ID</b>entifier (OID) of MIB node.
		 */
		protected ExternalNode (final int index, final SnmpOid oid) {
			super();
			this.index = index;
			this.oid = oid;
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getOid()
		 */
		public SnmpOid getOid () {
			return this.oid;
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getSnmpDataType()
		 */
		public SnmpDataType getSnmpDataType () {
			return SystemSnmpMib.this.getSnmpDataType(this.index);
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getJmxDataType()
		 */
		public Class<?> getJmxDataType () {
			return SystemSnmpMib.this.getJmxDataType(this.index);
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getValue()
		 */
		public Object getValue () {
			return SystemSnmpMib.this.getValue(this.index);
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#setValue(java.lang.Object)
		 */
		public void setValue (final Object value) {
			// NOP
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#isReadable()
		 */
		public boolean isReadable () {
			return true;
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#isWritable()
		 */
		public boolean isWritable () {
			return false;
		}

		/*
		 * {@inheritDoc}
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString () {
			return getOid() + ": (" + getSnmpDataType() + ") " + getValue();
		}

	}

	/**
	 * External node of the SNMP <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB) for <code>system.sysObjectID</code> attribute.
	 */
	protected final class SysObjectIDExternalNode
			implements AttributeAccessor {

		/**
		 * <b>O</b>bject <b>ID</b>entifier (OID) of MIB node.
		 */
		private final SnmpOid oid;

		/**
		 * Value of MIB node.
		 */
		private final SnmpOid value;

		/**
		 * Constructor.
		 * @param oid <b>O</b>bject <b>ID</b>entifier (OID) of MIB node.
		 * @param value Value of MIB node.
		 */
		protected SysObjectIDExternalNode (final SnmpOid oid, final SnmpOid value) {
			super();
			this.oid = oid;
			this.value = value;
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getOid()
		 */
		public SnmpOid getOid () {
			return this.oid;
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getSnmpDataType()
		 */
		public SnmpDataType getSnmpDataType () {
			return SnmpDataType.objectIdentifier;
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getJmxDataType()
		 */
		public Class<?> getJmxDataType () {
			return SnmpOid.class;
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getValue()
		 */
		public Object getValue () {
			return this.value;
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#setValue(java.lang.Object)
		 */
		public void setValue (final Object value) {
			// NOP
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#isReadable()
		 */
		public boolean isReadable () {
			return true;
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#isWritable()
		 */
		public boolean isWritable () {
			return false;
		}

		/*
		 * {@inheritDoc}
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString () {
			return getOid() + ": (" + getSnmpDataType() + ") " + getValue();
		}

	}

	/**
	 * Context of main application.
	 */
	private final SnmpAppContext mainAppContext;

	/**
	 * Application context map.
	 */
	private final Map<ClassLoader, SnmpAppContext> appContextMap;

	/**
	 * Informations on the system.
	 */
	private final SystemInfo systemInfo;

	/**
	 * <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 */
	private final SortedMap<SnmpOid, AttributeAccessor> mib = new TreeMap<SnmpOid, AttributeAccessor>();

	/**
	 * Constructor (only used by tests).
	 * @param systemInfo Informations on the system.
	 */
	protected SystemSnmpMib (final SystemInfo systemInfo) {
		super();
		this.mainAppContext = null;
		this.appContextMap = null;
		this.systemInfo = systemInfo;
	}

	/**
	 * Constructor.
	 * @param mainAppContext Context of main application (must not be <code>NULL</code>).
	 * @param appContextMap Application context map.
	 * @param systemInfo Informations on the system.
	 */
	public SystemSnmpMib (final SnmpAppContext mainAppContext, final Map<ClassLoader, SnmpAppContext> appContextMap, final SystemInfo systemInfo) {
		super();
		this.mainAppContext = mainAppContext;
		this.appContextMap = appContextMap;
		this.systemInfo = systemInfo;
		initSysObjectIDSet();
	}

	/**
	 * Returns the <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 * @return <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 */
	protected final SortedMap<SnmpOid, AttributeAccessor> getMib () {
		return this.mib;
	}

	/**
	 * Initializes the set of <code>system.sysObjectID</code> values with their OID.
	 */
	public void initSysObjectIDSet () {
		SnmpOid oid;
		final Set<SnmpOid> sysObjectIDSet = new TreeSet<SnmpOid>();
		if (this.mainAppContext.getDefaultRootOid() != null) {
			sysObjectIDSet.add(SnmpOid.newInstance(this.mainAppContext.getDefaultRootOid()));
		}
		for (final String rootOid : this.mainAppContext.getRootOidMap().values()) {
			oid = SnmpOid.newInstance(rootOid);
			if (!sysObjectIDSet.contains(oid)) {
				sysObjectIDSet.add(oid);
			}
		}
		for (final SnmpAppContext appContext : this.appContextMap.values()) {
			if (appContext.getDefaultRootOid() != null) {
				oid = SnmpOid.newInstance(appContext.getDefaultRootOid());
				if (!sysObjectIDSet.contains(oid)) {
					sysObjectIDSet.add(oid);
				}
			}
			for (final String rootOid : appContext.getRootOidMap().values()) {
				oid = SnmpOid.newInstance(rootOid);
				if (!sysObjectIDSet.contains(oid)) {
					sysObjectIDSet.add(oid);
				}
			}
		}
		synchronized (this.mib) {
			this.mib.clear();

			// system.sysName.0
			oid = SnmpOid.SYSNAME_OID;
			putExternalNode(0, oid);

			// system.sysDescr.0
			oid = SnmpOid.SYSDESCR_OID;
			putExternalNode(1, oid);

			// system.sysLocation.0
			oid = SnmpOid.SYSLOCATION_OID;
			putExternalNode(2, oid);

			// system.sysContact.0
			oid = SnmpOid.SYSCONTACT_OID;
			putExternalNode(3, oid);

			// system.sysUpTime.0
			oid = SnmpOid.SYSUPTIME_OID;
			putExternalNode(4, oid);

			// system.sysObjectID.0
			int index = 1;
			for (final SnmpOid value : sysObjectIDSet) {
				if (sysObjectIDSet.size() == 1) {
					oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 2, 0 });
					putExternalNode(oid, value);
				}
				else {
					oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 2, index });
					putExternalNode(oid, value);
				}
				index++;
			}

		}
	}

	/**
	 * Puts an external node of an attribute in the MIB.
	 * @param index Index of attribute.
	 * @param oid <b>O</b>bject <b>ID</b>entifier (OID) of MIB node.
	 */
	protected final void putExternalNode (final int index, final SnmpOid oid) {
		this.mib.put(oid, new ExternalNode(index, oid));
	}

	/**
	 * Puts an external node of an attribute in the MIB.
	 * @param oid <b>O</b>bject <b>ID</b>entifier (OID) of MIB node.
	 * @param value Value of MIB node.
	 */
	protected final void putExternalNode (final SnmpOid oid, final SnmpOid value) {
		this.mib.put(oid, new SysObjectIDExternalNode(oid, value));
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpMib#find(net.sf.snmpadaptor4j.object.SnmpOid)
	 */
	public final AttributeAccessor find (final SnmpOid oid) {
		AttributeAccessor node;
		synchronized (this.mib) {
			node = this.mib.get(oid);
		}
		return node;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpMib#next(net.sf.snmpadaptor4j.object.SnmpOid)
	 */
	public final AttributeAccessor next (final SnmpOid oid) {
		AttributeAccessor node;
		final Iterator<Entry<SnmpOid, AttributeAccessor>> entryIterator = nextSet(oid).entrySet().iterator();
		if (entryIterator.hasNext()) {
			node = entryIterator.next().getValue();
		}
		else {
			node = null;
		}
		return node;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpMib#nextSet(net.sf.snmpadaptor4j.object.SnmpOid)
	 */
	public final SortedMap<SnmpOid, AttributeAccessor> nextSet (final SnmpOid oid) {
		SortedMap<SnmpOid, AttributeAccessor> nodeMap;
		synchronized (this.mib) {
			nodeMap = this.mib.tailMap(SnmpOid.newInstance(oid.getOid(), 0));
		}
		return nodeMap;
	}

	/**
	 * Returns the SNMP data type of attribute.
	 * @param index Index of attribute.
	 * @return SNMP data type of attribute.
	 */
	protected final SnmpDataType getSnmpDataType (final int index) {
		SnmpDataType type;
		switch (index) {
			case 0:		// system.sysName.0
				type = SnmpDataType.octetString;
				break;
			case 1:		// system.sysDescr.0
				type = SnmpDataType.octetString;
				break;
			case 2:		// system.sysLocation.0
				type = SnmpDataType.octetString;
				break;
			case 3:		// system.sysContact.0
				type = SnmpDataType.octetString;
				break;
			case 4:		// system.sysUpTime.0
				type = SnmpDataType.timeTicks;
				break;
			default:
				type = null;
				break;
		}
		return type;
	}

	/**
	 * Returns the data type of JMX attribute.
	 * @param index Index of attribute.
	 * @return Data type of JMX attribute.
	 */
	protected final Class<?> getJmxDataType (final int index) {
		Class<?> type;
		switch (index) {
			case 0:		// system.sysName.0
				type = String.class;
				break;
			case 1:		// system.sysDescr.0
				type = String.class;
				break;
			case 2:		// system.sysLocation.0
				type = String.class;
				break;
			case 3:		// system.sysContact.0
				type = String.class;
				break;
			case 4:		// system.sysUpTime.0
				type = Long.class;
				break;
			default:
				type = null;
				break;
		}
		return type;
	}

	/**
	 * Returns the value of attribute.
	 * @param index Index of attribute.
	 * @return Value of attribute.
	 */
	public final Object getValue (final int index) {
		Object value;
		switch (index) {
			case 0:		// system.sysName.0
				value = this.systemInfo.getSysName();
				break;
			case 1:		// system.sysDescr.0
				value = this.systemInfo.getSysDescr();
				break;
			case 2:		// system.sysLocation.0
				value = this.systemInfo.getSysLocation();
				break;
			case 3:		// system.sysContact.0
				value = this.systemInfo.getSysContact();
				break;
			case 4:		// system.sysUpTime.0
				value = new Long(this.systemInfo.getSysUpTime());
				break;
			default:
				value = null;
				break;
		}
		return value;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public final String toString () {
		String values;
		synchronized (this.mib) {
			values = this.mib.values().toString();
		}
		return "SystemSnmpMib" + values;
	}

}