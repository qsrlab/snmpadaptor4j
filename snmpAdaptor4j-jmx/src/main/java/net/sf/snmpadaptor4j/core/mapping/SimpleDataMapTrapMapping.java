package net.sf.snmpadaptor4j.core.mapping;

import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;

/**
 * Object containing the mapping to build the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} from a <code>userData</code> field
 * of JMX notification and as <b>simple object</b>.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SimpleDataMapTrapMapping
		extends DataMapTrapMapping {

	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = 2806381975871178286L;

	/**
	 * SNMP data type of <code>userData</code> field of JMX notification.
	 */
	private final SnmpDataType userDataType;

	/**
	 * OID of <code>userData</code> field of JMX notification.
	 */
	private final SnmpOid userDataOid;

	/**
	 * Hidden constructor.
	 * @param sequenceNumberOid OID of <code>sequenceNumber</code> field of JMX notification. Is NULL if the <code>sequenceNumber</code> should not be present in
	 *            <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 * @param messageOid OID of <code>message</code> field of JMX notification. Is NULL if the <code>message</code> should not be present in <code>dataMap</code>
	 *            field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 * @param hasSystemInfo <code>TRUE</code> for put all system information attributes in the <code>dataMap</code> field of
	 *            {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 * @param userDataType SNMP data type of <code>userData</code> field of JMX notification (must not be <code>NULL</code>).
	 * @param userDataOid OID of <code>userData</code> field of JMX notification (must not be <code>NULL</code>).
	 * @see XmlMappingParser#newSnmpTrapMappingMap(String)
	 */
	SimpleDataMapTrapMapping (final SnmpOid sequenceNumberOid, final SnmpOid messageOid, final boolean hasSystemInfo, final SnmpDataType userDataType,
			final SnmpOid userDataOid) {
		super(sequenceNumberOid, messageOid, hasSystemInfo);
		this.userDataType = userDataType;
		this.userDataOid = userDataOid;
	}

	/**
	 * Returns the SNMP data type of <code>userData</code> field of JMX notification.
	 * @return SNMP data type of <code>userData</code> field of JMX notification (never <code>NULL</code>).
	 */
	public SnmpDataType getUserDataType () {
		return this.userDataType;
	}

	/**
	 * Returns the OID of <code>userData</code> field of JMX notification.
	 * @return OID of <code>userData</code> field of JMX notification (never <code>NULL</code>).
	 */
	public SnmpOid getUserDataOid () {
		return this.userDataOid;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.core.mapping.DataMapTrapMapping#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.userDataOid == null) ? 0 : this.userDataOid.hashCode());
		result = prime * result + ((this.userDataType == null) ? 0 : this.userDataType.hashCode());
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.core.mapping.DataMapTrapMapping#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean result = false;
		if (obj == this) {
			result = true;
		}
		else {
			result = super.equals(obj);
			if (result) {
				final SimpleDataMapTrapMapping other = (SimpleDataMapTrapMapping) obj;
				result = (this.userDataType == other.userDataType);
				if (result) {
					result = (this.userDataOid != null ? this.userDataOid.equals(other.userDataOid) : other.userDataOid == null);
				}
			}
		}
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "SimpleDataMapTrapMapping[sequenceNumberOid=" + getSequenceNumberOid() + "; messageOid=" + getMessageOid() + "; hasSystemInfo=" + isHasSystemInfo()
				+ "; userDataType=" + this.userDataType + "; userDataOid=" + this.userDataOid + "]";
	}

}