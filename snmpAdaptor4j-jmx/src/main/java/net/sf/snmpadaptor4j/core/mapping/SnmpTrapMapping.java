package net.sf.snmpadaptor4j.core.mapping;

import java.io.Serializable;
import net.sf.snmpadaptor4j.object.SnmpOid;

/**
 * Object containing the mapping to build SNMP traps from JMX notifications.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public abstract class SnmpTrapMapping
		implements Serializable {

	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = -3245663684853609245L;

	/**
	 * <code>source</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 */
	private final SnmpOid source;

	/**
	 * Mapping to build the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} from a JMX notification.
	 */
	private final DataMapTrapMapping dataMap;

	/**
	 * Hidden constructor (abstract class).
	 * @param source <code>source</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 * @param dataMap Mapping to build the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} from a JMX notification.
	 */
	protected SnmpTrapMapping (final SnmpOid source, final DataMapTrapMapping dataMap) {
		super();
		this.source = source;
		this.dataMap = dataMap;
	}

	/**
	 * Returns the <code>source</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 * @return <code>source</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 */
	public final SnmpOid getSource () {
		return this.source;
	}

	/**
	 * Returns the mapping to build the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} from a JMX notification.
	 * @return Mapping to build the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} from a JMX notification.
	 */
	public final DataMapTrapMapping getDataMap () {
		return this.dataMap;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.dataMap == null) ? 0 : this.dataMap.hashCode());
		result = prime * result + ((this.source == null) ? 0 : this.source.hashCode());
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean result = false;
		if ((obj != null) && getClass().equals(obj.getClass())) {
			final SnmpTrapMapping other = (SnmpTrapMapping) obj;
			result = (this.source != null ? this.source.equals(other.source) : (other.source == null));
			if (result) {
				result = (this.dataMap != null ? this.dataMap.equals(other.dataMap) : (other.dataMap == null));
			}
		}
		return result;
	}

}