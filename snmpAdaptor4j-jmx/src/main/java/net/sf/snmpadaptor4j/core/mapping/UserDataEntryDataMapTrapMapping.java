package net.sf.snmpadaptor4j.core.mapping;

import java.io.Serializable;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;

/**
 * Mapping to build an entry of the map in <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} from an entry of map in
 * <code>userData</code> field of JMX notification.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 * @see MapDataMapTrapMapping
 */
public final class UserDataEntryDataMapTrapMapping
		implements Serializable {

	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = -520862990459987242L;

	/**
	 * Key for find the entry in the map of <code>userData</code> field of JMX notification.
	 */
	private final String key;

	/**
	 * SNMP data type of entry in the map of <code>userData</code> field of JMX notification.
	 */
	private final SnmpDataType type;

	/**
	 * OID of entry in the map of <code>userData</code> field of JMX notification.
	 */
	private final SnmpOid oid;

	/**
	 * Hidden constructor.
	 * @param key Key for find the entry in the map of <code>userData</code> field of JMX notification (must not be <code>NULL</code>).
	 * @param type SNMP data type of entry in the map of <code>userData</code> field of JMX notification (must not be <code>NULL</code>).
	 * @param oid OID of entry in the map of <code>userData</code> field of JMX notification (must not be <code>NULL</code>).
	 * @see XmlMappingParser#newSnmpTrapMappingMap(String)
	 * @see MapDataMapTrapMapping#addUserDataEntry(String, SnmpDataType, SnmpOid)
	 */
	UserDataEntryDataMapTrapMapping (final String key, final SnmpDataType type, final SnmpOid oid) {
		super();
		this.key = key;
		this.type = type;
		this.oid = oid;
	}

	/**
	 * Returns the key for find the entry in the map of <code>userData</code> field of JMX notification.
	 * @return Key for find the entry in the map of <code>userData</code> field of JMX notification.
	 */
	public String getKey () {
		return this.key;
	}

	/**
	 * Returns the SNMP data type of entry in the map of <code>userData</code> field of JMX notification.
	 * @return SNMP data type of entry in the map of <code>userData</code> field of JMX notification.
	 */
	public SnmpDataType getType () {
		return this.type;
	}

	/**
	 * Returns the OID of entry in the map of <code>userData</code> field of JMX notification.
	 * @return OID of entry in the map of <code>userData</code> field of JMX notification.
	 */
	public SnmpOid getOid () {
		return this.oid;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.key == null) ? 0 : this.key.hashCode());
		result = prime * result + ((this.oid == null) ? 0 : this.oid.hashCode());
		result = prime * result + ((this.type == null) ? 0 : this.type.hashCode());
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean result = false;
		if (obj == this) {
			result = true;
		}
		else if ((obj != null) && getClass().equals(obj.getClass())) {
			final UserDataEntryDataMapTrapMapping other = (UserDataEntryDataMapTrapMapping) obj;
			result = (this.type == other.type);
			if (result) {
				result = (this.oid != null ? this.oid.equals(other.oid) : other.oid == null);
			}
			if (result) {
				result = (this.key != null ? this.key.equals(other.key) : other.key == null);
			}
		}
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "UserDataEntryDataMapTrapMapping[key=" + this.key + "; type=" + this.type + "; oid=" + this.oid + "]";
	}

}