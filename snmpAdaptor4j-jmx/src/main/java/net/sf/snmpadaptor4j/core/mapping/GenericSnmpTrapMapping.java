package net.sf.snmpadaptor4j.core.mapping;

import net.sf.snmpadaptor4j.object.SnmpOid;
import net.sf.snmpadaptor4j.object.GenericSnmpTrapType;

/**
 * Object containing the mapping to build <b>generic</b> SNMP traps from JMX notifications.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class GenericSnmpTrapMapping
		extends SnmpTrapMapping {

	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = -3515283428235197553L;

	/**
	 * <code>type</code> field of {@link net.sf.snmpadaptor4j.object.GenericSnmpTrap GenericSnmpTrap}.
	 */
	private final GenericSnmpTrapType type;

	/**
	 * Hidden constructor.
	 * @param source <code>source</code> field of {@link net.sf.snmpadaptor4j.object.GenericSnmpTrap GenericSnmpTrap}.
	 * @param dataMap Mapping to build the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.GenericSnmpTrap GenericSnmpTrap} from a JMX notification.
	 * @param type <code>type</code> field of {@link net.sf.snmpadaptor4j.object.GenericSnmpTrap GenericSnmpTrap}.
	 * @see XmlMappingParser#newSnmpTrapMappingMap(String)
	 */
	GenericSnmpTrapMapping (final SnmpOid source, final DataMapTrapMapping dataMap, final GenericSnmpTrapType type) {
		super(source, dataMap);
		this.type = type;
	}

	/**
	 * Returns the <code>type</code> field of {@link net.sf.snmpadaptor4j.object.GenericSnmpTrap GenericSnmpTrap}.
	 * @return <code>type</code> field of {@link net.sf.snmpadaptor4j.object.GenericSnmpTrap GenericSnmpTrap}.
	 */
	public GenericSnmpTrapType getType () {
		return this.type;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.core.mapping.SnmpTrapMapping#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.type == null) ? 0 : this.type.hashCode());
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.core.mapping.SnmpTrapMapping#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean result = false;
		if (obj == this) {
			result = true;
		}
		else {
			result = super.equals(obj);
			if (result) {
				final GenericSnmpTrapMapping other = (GenericSnmpTrapMapping) obj;
				result = (this.type == other.type);
			}
		}
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "SNMP trap " + getSource() + " - Type " + this.type + "/0";
	}

}