package net.sf.snmpadaptor4j.core.mapping;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;

/**
 * Object containing the mapping to build the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} from a <code>userData</code> field
 * of JMX notification and as <b>map object</b>.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class MapDataMapTrapMapping
		extends DataMapTrapMapping {

	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = -6097834534568446556L;

	/**
	 * List of mappings to build entries of the map in <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} from all entries of map in
	 * <code>userData</code> field of JMX notification.
	 */
	private final List<UserDataEntryDataMapTrapMapping> userDataEntryList = new ArrayList<UserDataEntryDataMapTrapMapping>();

	/**
	 * Hidden constructor.
	 * @param sequenceNumberOid OID of <code>sequenceNumber</code> field of JMX notification. Is NULL if the <code>sequenceNumber</code> should not be present in
	 *            <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 * @param messageOid OID of <code>message</code> field of JMX notification. Is NULL if the <code>message</code> should not be present in <code>dataMap</code>
	 *            field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 * @param hasSystemInfo <code>TRUE</code> for put all system information attributes in the <code>dataMap</code> field of
	 *            {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 * @see XmlMappingParser#newSnmpTrapMappingMap(String)
	 */
	MapDataMapTrapMapping (final SnmpOid sequenceNumberOid, final SnmpOid messageOid, final boolean hasSystemInfo) {
		super(sequenceNumberOid, messageOid, hasSystemInfo);
	}

	/**
	 * Adds a mapping to build an entry of the map in <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} from an entry of map in
	 * <code>userData</code> field of JMX notification.
	 * @param userDataKey Key for find the entry in the map of <code>userData</code> field of JMX notification (must not be <code>NULL</code>).
	 * @param userDataType SNMP data type of entry in the map of <code>userData</code> field of JMX notification (must not be <code>NULL</code>).
	 * @param userDataOid OID of entry in the map of <code>userData</code> field of JMX notification (must not be <code>NULL</code>).
	 * @see XmlMappingParser#newSnmpTrapMappingMap(String)
	 */
	void addUserDataEntry (final String userDataKey, final SnmpDataType userDataType, final SnmpOid userDataOid) {
		this.userDataEntryList.add(new UserDataEntryDataMapTrapMapping(userDataKey, userDataType, userDataOid));
	}

	/**
	 * Returns the list of mappings to build entries of the map in <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} from all
	 * entries of map in <code>userData</code> field of JMX notification.
	 * @return List of mappings to build entries of the map in <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} from all entries
	 *         of map in <code>userData</code> field of JMX notification (never <code>NULL</code>).
	 */
	public List<UserDataEntryDataMapTrapMapping> getUserDataEntryList () {
		return Collections.unmodifiableList(this.userDataEntryList);
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.core.mapping.DataMapTrapMapping#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + this.userDataEntryList.hashCode();
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.core.mapping.DataMapTrapMapping#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean result = false;
		if (obj == this) {
			result = true;
		}
		else {
			result = super.equals(obj);
			if (result) {
				final MapDataMapTrapMapping other = (MapDataMapTrapMapping) obj;
				result = this.userDataEntryList.equals(other.userDataEntryList);
			}
		}
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "MapDataMapTrapMapping[sequenceNumberOid=" + getSequenceNumberOid() + "; messageOid=" + getMessageOid() + "; hasSystemInfo=" + isHasSystemInfo()
				+ "; userDataEntryList=" + this.userDataEntryList + "]";
	}

}