/**
 * Package containing all classes used to build SNMP traps from JMX notifications and to send them to all SNMP managers.
 */
package net.sf.snmpadaptor4j.core.trap;