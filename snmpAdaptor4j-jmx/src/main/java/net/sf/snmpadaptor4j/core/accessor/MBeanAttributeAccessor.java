package net.sf.snmpadaptor4j.core.accessor;

import javax.management.Attribute;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import net.sf.snmpadaptor4j.api.AttributeAccessor;
import net.sf.snmpadaptor4j.core.mapping.MBeanAttributeMapping;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;

/**
 * Object representing an accessor to an attribute of MBean.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class MBeanAttributeAccessor
		implements AttributeAccessor {

	/**
	 * JMX agent where the MBean was registered.
	 */
	private final MBeanServer server;

	/**
	 * MBean name.
	 */
	private final ObjectName mBeanName;

	/**
	 * Mapping for access to a MBean attribute.
	 */
	private final MBeanAttributeMapping mapping;

	/**
	 * Constructor.
	 * @param server JMX agent where the MBean was registered.
	 * @param mBeanName MBean name.
	 * @param mapping Mapping for access to a MBean attribute.
	 */
	public MBeanAttributeAccessor (final MBeanServer server, final ObjectName mBeanName, final MBeanAttributeMapping mapping) {
		super();
		this.server = server;
		this.mBeanName = mBeanName;
		this.mapping = mapping;
	}

	/**
	 * Returns the MBean name.
	 * @return MBean name.
	 */
	public ObjectName getMBeanName () {
		return this.mBeanName;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getOid()
	 */
	public SnmpOid getOid () {
		return this.mapping.getOid();
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getSnmpDataType()
	 */
	public SnmpDataType getSnmpDataType () {
		return this.mapping.getSnmpDataType();
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getJmxDataType()
	 */
	public Class<?> getJmxDataType () {
		return this.mapping.getJmxDataType();
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getValue()
	 */
	public Object getValue () throws Exception {
		return this.server.getAttribute(this.mBeanName, this.mapping.getAttributeName());
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#setValue(java.lang.Object)
	 */
	public void setValue (final Object value) throws Exception {
		this.server.setAttribute(this.mBeanName, new Attribute(this.mapping.getAttributeName(), value));
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#isReadable()
	 */
	public boolean isReadable () {
		return this.mapping.isReadable();
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#isWritable()
	 */
	public boolean isWritable () {
		return this.mapping.isWritable();
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		Object value;
		try {
			value = getValue();
		}
		catch (final Throwable e) {
			value = "ERROR: " + (e.getMessage() != null ? e.getMessage() : e.getClass().getName());
		}
		return this.mapping.getOid() + ": (" + this.mapping.getSnmpDataType() + ") " + value;
	}

}