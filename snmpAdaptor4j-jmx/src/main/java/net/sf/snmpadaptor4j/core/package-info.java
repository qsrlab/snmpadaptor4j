/**
 * Package containing entry point classes for access to MBean attributes and for SNMP traps sending.
 */
package net.sf.snmpadaptor4j.core;