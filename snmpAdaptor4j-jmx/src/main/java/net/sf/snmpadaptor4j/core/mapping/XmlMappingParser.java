package net.sf.snmpadaptor4j.core.mapping;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.management.MBeanAttributeInfo;
import javax.management.ObjectName;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import net.sf.snmpadaptor4j.core.mapping.jaxb.GenericTrap;
import net.sf.snmpadaptor4j.core.mapping.jaxb.MBean;
import net.sf.snmpadaptor4j.core.mapping.jaxb.MBeanAttribute;
import net.sf.snmpadaptor4j.core.mapping.jaxb.MBeanNotifications;
import net.sf.snmpadaptor4j.core.mapping.jaxb.Mapping;
import net.sf.snmpadaptor4j.core.mapping.jaxb.SpecificTrap;
import net.sf.snmpadaptor4j.core.mapping.jaxb.TrapEnterprise;
import net.sf.snmpadaptor4j.core.mapping.jaxb.TrapUserData;
import net.sf.snmpadaptor4j.core.mapping.jaxb.TrapUserDataEntry;
import net.sf.snmpadaptor4j.core.mapping.jaxb.TrapUserDataMap;
import net.sf.snmpadaptor4j.core.mapping.jaxb.TrapVariableBindings;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.GenericSnmpTrapType;
import net.sf.snmpadaptor4j.object.SnmpOid;

/**
 * Parser of SNMP mapping file (XML).
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class XmlMappingParser {

	/**
	 * {@link URL} to the SNMP mapping file.
	 */
	private final URL url;

	/**
	 * JAXB representation of the SNMP mapping file.
	 */
	private final JAXBElement<Mapping> jaxbElement;

	/**
	 * Creates and returns a new instance of {@link XmlMappingParser}.
	 * @param url {@link URL} to the SNMP mapping file.
	 * @return New instance of {@link XmlMappingParser}.
	 * @throws Exception Exception if an error occurred.
	 */
	public static XmlMappingParser newInstance (final URL url) throws Exception {
		final JAXBContext jaxbContext = JAXBContext.newInstance(Mapping.class.getPackage().getName());
		final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		@SuppressWarnings("unchecked")
		final JAXBElement<Mapping> jaxbElement = (JAXBElement<Mapping>) unmarshaller.unmarshal(url);
		return new XmlMappingParser(url, jaxbElement);
	}

	/**
	 * Constructor.
	 * @param url {@link URL} to the SNMP mapping file.
	 * @param jaxbElement JAXB representation of the SNMP mapping file.
	 */
	private XmlMappingParser (final URL url, final JAXBElement<Mapping> jaxbElement) {
		super();
		this.url = url;
		this.jaxbElement = jaxbElement;
	}

	/**
	 * Finds a base OID of a MBean instance in the mapping.
	 * @param mBeanName MBean name.
	 * @param rootOidMap Map of root OIDs where the attributes of the application will stay.
	 * @param defaultRootOid Default root OID containing the attributes of the current application.
	 * @param mainDefaultRootOid Default root OID containing the attributes of the main application (must not be <code>NULL</code>).
	 * @return Base OID found (can be <code>NULL</code>).
	 */
	public String findBaseOid (final ObjectName mBeanName, final Map<String, String> rootOidMap, final String defaultRootOid, final String mainDefaultRootOid) {
		String baseOid = null;
		final Mapping mapping = this.jaxbElement.getValue();
		if (mapping.getMbeans() != null) {
			final Iterator<MBean> mBeanIterator = mapping.getMbeans().getMbean().iterator();
			final String desiredName = mBeanName.toString();
			MBean mBean;
			while (mBeanIterator.hasNext() && (baseOid == null)) {
				mBean = mBeanIterator.next();
				if (mBean.getName().equals(desiredName)) {
					if (mBean.getRoot() != null) {
						baseOid = rootOidMap.get(mBean.getRoot());
					}
					if (baseOid == null) {
						baseOid = defaultRootOid;
					}
					if (baseOid == null) {
						baseOid = mainDefaultRootOid;
					}
					baseOid = baseOid + "." + mBean.getOid();
				}
			}
		}
		return baseOid;
	}

	/**
	 * Creates and returns the list of mapping for access to each attribute of an MBean.
	 * @param mBeanAttributeInfoMap Map of informations on each MBean attribute.
	 * @param classLoader {@link ClassLoader} where the MBean has been created.
	 * @param baseOid Base OID of MBean instance.
	 * @return List of mapping for access to each attribute of an MBean (never <code>NULL</code>).
	 * @throws Exception Exception if an error occurred.
	 */
	public List<MBeanAttributeMapping> newMBeanAttributeMappingList (final Map<String, MBeanAttributeInfo> mBeanAttributeInfoMap, final ClassLoader classLoader,
			final String baseOid) throws Exception {
		final List<MBeanAttributeMapping> mappingList = new ArrayList<MBeanAttributeMapping>();
		final Iterator<MBeanAttribute> attributeIterator = this.jaxbElement.getValue().getAttributes().getAttribute().iterator();
		MBeanAttribute attribute;
		String attributeName;
		MBeanAttributeInfo mBeanAttributeInfo;
		SnmpOid oid;
		SnmpDataType snmpDataType;
		Class<?> jmxDataType;
		boolean readable;
		boolean writable;
		while (attributeIterator.hasNext()) {
			attribute = attributeIterator.next();

			// Property name & info
			attributeName = attribute.getName().substring(0, 1).toUpperCase() + attribute.getName().substring(1);
			mBeanAttributeInfo = mBeanAttributeInfoMap.get(attributeName);

			// OID
			oid = SnmpOid.newInstance(baseOid, attribute.getNode(), 0);

			// snmpDataType
			snmpDataType = SnmpDataType.valueOf(attribute.getType().value());

			// jmxDataType
			jmxDataType = toClass(classLoader, mBeanAttributeInfo.getType());

			// readable
			readable = (mBeanAttributeInfo.isReadable() && !attribute.isDisabled());

			// writable
			writable = (readable && mBeanAttributeInfo.isWritable() && attribute.isWritable());

			mappingList.add(new MBeanAttributeMapping(oid, attributeName, snmpDataType, jmxDataType, readable, writable));
		}
		return mappingList;
	}

	/**
	 * Converts a class name to its instance.
	 * @param classLoader {@link ClassLoader} where the MBean has been created.
	 * @param className Class name.
	 * @return Class instance.
	 * @throws Exception Exception if an error occurred.
	 */
	private Class<?> toClass (final ClassLoader classLoader, final String className) throws Exception {
		Class<?> result;
		if (byte.class.getName().equals(className)) {
			result = byte.class;
		}
		else if (short.class.getName().equals(className)) {
			result = short.class;
		}
		else if (int.class.getName().equals(className)) {
			result = int.class;
		}
		else if (boolean.class.getName().equals(className)) {
			result = boolean.class;
		}
		else if (long.class.getName().equals(className)) {
			result = long.class;
		}
		else if (byte[].class.getName().equals(className)) {
			result = byte[].class;
		}
		else if (short[].class.getName().equals(className)) {
			result = short[].class;
		}
		else if (int[].class.getName().equals(className)) {
			result = int[].class;
		}
		else if (long[].class.getName().equals(className)) {
			result = long[].class;
		}
		else {
			result = classLoader.loadClass(className);
		}
		return result;
	}

	/**
	 * Creates and returns the map of mapping to build SNMP traps from JMX notifications for each notification type.
	 * @param baseOid Base OID of MBean instance.
	 * @return Map of mapping to build SNMP traps from the JMX notifications for each notification type (never <code>NULL</code>).
	 */
	public Map<String, SnmpTrapMapping> newSnmpTrapMappingMap (final String baseOid) {
		final Map<String, SnmpTrapMapping> trapMapping = new HashMap<String, SnmpTrapMapping>();
		final MBeanNotifications notifications = this.jaxbElement.getValue().getNotifications();
		if (notifications != null) {
			final Map<String, DataMapTrapMapping> dataMapMappingMap = newDataMapTrapMappingMap(baseOid, notifications.getVariableBindings());
			final DataMapTrapMapping defaultDataMapMapping = newDefaultDataMapTrapMapping(baseOid, notifications.getVariableBindings());
			DataMapTrapMapping dataMapMapping;
			SnmpOid source;
			GenericSnmpTrapType genericType;
			for (final TrapEnterprise enterprise : notifications.getEnterprise()) {
				for (final GenericTrap trap : enterprise.getGenericTrap()) {
					dataMapMapping = findDataMapTrapMapping(dataMapMappingMap, defaultDataMapMapping, enterprise, trap.getUserdata());
					source = SnmpOid.newInstance(baseOid, enterprise.getNode(), 0);
					genericType = GenericSnmpTrapType.valueOf(trap.getCode().value());
					trapMapping.put(trap.getNotifType(), new GenericSnmpTrapMapping(source, dataMapMapping, genericType));
				}
				for (final SpecificTrap trap : enterprise.getSpecificTrap()) {
					dataMapMapping = findDataMapTrapMapping(dataMapMappingMap, defaultDataMapMapping, enterprise, trap.getUserdata());
					source = SnmpOid.newInstance(baseOid, enterprise.getNode(), 0);
					trapMapping.put(trap.getNotifType(), new SpecificSnmpTrapMapping(source, dataMapMapping, trap.getCode()));
				}
			}
		}
		return trapMapping;
	}

	/**
	 * Find a mapping to build the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} by its name.
	 * @param dataMapMappingMap Map of mapping to build the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} from JMX
	 *            notifications (must not be <code>NULL</code>).
	 * @param defaultDataMapMapping Default mapping to build the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} from JMX
	 *            notifications (must not be <code>NULL</code>).
	 * @param enterprise Represents the contents of <code>enterprise</code> tag of XML file.
	 * @param userDataName Name of mapping to find for build the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 * @return Mapping found to build the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 */
	private DataMapTrapMapping findDataMapTrapMapping (final Map<String, DataMapTrapMapping> dataMapMappingMap, final DataMapTrapMapping defaultDataMapMapping,
			final TrapEnterprise enterprise, final String userDataName) {
		DataMapTrapMapping dataMapMapping = null;
		String dataMapMappingName = (userDataName != null ? userDataName : enterprise.getUserdata());
		if (dataMapMappingName != null) {
			dataMapMapping = dataMapMappingMap.get(dataMapMappingName);
		}
		if (dataMapMapping == null) {
			dataMapMapping = defaultDataMapMapping;
		}
		return dataMapMapping;
	}

	/**
	 * Creates and returns the map of mapping to build the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} from JMX
	 * notifications.
	 * @param baseOid Base OID of MBean instance.
	 * @param variableBindings Represents the contents of <code>variable-bindings</code> tag of XML file.
	 * @return Map of mapping to build the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} from JMX notifications (never
	 *         <code>NULL</code>).
	 */
	private Map<String, DataMapTrapMapping> newDataMapTrapMappingMap (final String baseOid, final TrapVariableBindings variableBindings) {
		final Map<String, DataMapTrapMapping> dataMapMappingMap = new HashMap<String, DataMapTrapMapping>();
		if (variableBindings != null) {
			final SnmpOid sequenceNumberOid = (variableBindings.getSequenceNumber() != null ? SnmpOid.newInstance(baseOid, variableBindings.getSequenceNumber()
					.getNode(), 0) : null);
			final SnmpOid messageOid = (variableBindings.getMessage() != null ? SnmpOid.newInstance(baseOid, variableBindings.getMessage().getNode(), 0) : null);
			final boolean hasSystemInfo = variableBindings.isSystemInfo();
			DataMapTrapMapping dataMapMapping;
			SnmpDataType userDataType;
			SnmpOid userDataOid;
			for (final TrapUserData userData : variableBindings.getUserdata()) {
				userDataType = SnmpDataType.valueOf(userData.getType().value());
				userDataOid = SnmpOid.newInstance(baseOid, userData.getNode(), 0);
				dataMapMapping = new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid, hasSystemInfo, userDataType, userDataOid);
				dataMapMappingMap.put(userData.getName(), dataMapMapping);
			}
			MapDataMapTrapMapping mapDataMapMapping;
			for (final TrapUserDataMap userDataMap : variableBindings.getUserdataMap()) {
				mapDataMapMapping = new MapDataMapTrapMapping(sequenceNumberOid, messageOid, hasSystemInfo);
				for (final TrapUserDataEntry entry : userDataMap.getEntry()) {
					userDataType = SnmpDataType.valueOf(entry.getType().value());
					userDataOid = SnmpOid.newInstance(baseOid, entry.getNode(), 0);
					mapDataMapMapping.addUserDataEntry(entry.getKey(), userDataType, userDataOid);
				}
				dataMapMappingMap.put(userDataMap.getName(), mapDataMapMapping);
			}
		}
		return dataMapMappingMap;
	}

	/**
	 * Creates and returns the default mapping to build the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} from JMX
	 * notifications.
	 * @param baseOid Base OID of MBean instance.
	 * @param variableBindings Represents the contents of <code>variable-bindings</code> tag of XML file.
	 * @return Default mapping to build the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} from JMX notifications (never
	 *         <code>NULL</code>).
	 */
	private DefaultDataMapTrapMapping newDefaultDataMapTrapMapping (final String baseOid, final TrapVariableBindings variableBindings) {
		DefaultDataMapTrapMapping defaultDataMapMapping;
		if (variableBindings != null) {
			final SnmpOid sequenceNumberOid = (variableBindings.getSequenceNumber() != null ? SnmpOid.newInstance(baseOid, variableBindings.getSequenceNumber()
					.getNode(), 0) : null);
			final SnmpOid messageOid = (variableBindings.getMessage() != null ? SnmpOid.newInstance(baseOid, variableBindings.getMessage().getNode(), 0) : null);
			final boolean hasSystemInfo = variableBindings.isSystemInfo();
			defaultDataMapMapping = new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, hasSystemInfo);
		}
		else {
			defaultDataMapMapping = new DefaultDataMapTrapMapping(null, null, false);
		}
		return defaultDataMapMapping;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "XmlMappingParser[" + this.url + "]";
	}

}