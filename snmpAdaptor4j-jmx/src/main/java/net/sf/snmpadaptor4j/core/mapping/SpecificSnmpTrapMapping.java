package net.sf.snmpadaptor4j.core.mapping;

import net.sf.snmpadaptor4j.object.SnmpOid;

/**
 * Object containing the mapping to build <b>specific</b> SNMP traps from JMX notifications.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SpecificSnmpTrapMapping
		extends SnmpTrapMapping {

	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = -708977968210447637L;

	/**
	 * <code>type</code> field of {@link net.sf.snmpadaptor4j.object.SpecificSnmpTrap SpecificSnmpTrap}.
	 */
	private final int type;

	/**
	 * Hidden constructor.
	 * @param source <code>source</code> field of {@link net.sf.snmpadaptor4j.object.SpecificSnmpTrap SpecificSnmpTrap}.
	 * @param dataMap Mapping to build the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SpecificSnmpTrap SpecificSnmpTrap} from a JMX
	 *            notification.
	 * @param type <code>type</code> field of {@link net.sf.snmpadaptor4j.object.SpecificSnmpTrap SpecificSnmpTrap}.
	 * @see XmlMappingParser#newSnmpTrapMappingMap(String)
	 */
	SpecificSnmpTrapMapping (final SnmpOid source, final DataMapTrapMapping dataMap, final int type) {
		super(source, dataMap);
		this.type = type;
	}

	/**
	 * Returns the <code>type</code> field of {@link net.sf.snmpadaptor4j.object.SpecificSnmpTrap SpecificSnmpTrap}.
	 * @return <code>type</code> field of {@link net.sf.snmpadaptor4j.object.SpecificSnmpTrap SpecificSnmpTrap}.
	 */
	public int getType () {
		return this.type;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.core.mapping.SnmpTrapMapping#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + this.type;
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.core.mapping.SnmpTrapMapping#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean result = false;
		if (obj == this) {
			result = true;
		}
		else {
			result = super.equals(obj);
			if (result) {
				final SpecificSnmpTrapMapping other = (SpecificSnmpTrapMapping) obj;
				result = (this.type == other.type);
			}
		}
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "SNMP trap " + getSource() + " - Type enterpriseSpecific/" + this.type;
	}

}