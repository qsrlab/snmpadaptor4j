package net.sf.snmpadaptor4j.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.management.MBeanServer;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import org.apache.log4j.Logger;
import net.sf.snmpadaptor4j.core.mapping.SnmpTrapMapping;
import net.sf.snmpadaptor4j.core.trap.SnmpManagers;
import net.sf.snmpadaptor4j.core.trap.SnmpTrapBuilder;
import net.sf.snmpadaptor4j.mbean.SystemInfo;
import net.sf.snmpadaptor4j.object.SnmpTrap;

/**
 * Object responsible of:
 * <ul>
 * <li>the conversion of JMX notifications to SNMP traps,</li>
 * <li>and the sending SNMP traps to SNMP managers.</li>
 * </ul>
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public class JmxNotificationManager {

	/**
	 * JMX notification listener.
	 */
	protected final class Listener
			implements NotificationListener {

		/**
		 * Builder of SNMP traps from JMX notifications for an MBean.
		 */
		protected final SnmpTrapBuilder trapBuilder;

		/**
		 * Constructor.
		 * @param trapBuilder Builder of SNMP traps from JMX notifications for an MBean (must not be <code>NULL</code>).
		 */
		protected Listener (final SnmpTrapBuilder trapBuilder) {
			super();
			this.trapBuilder = trapBuilder;
		}

		/*
		 * {@inheritDoc}
		 * @see javax.management.NotificationListener#handleNotification(javax.management.Notification, java.lang.Object)
		 */
		public void handleNotification (final Notification notification, final Object handback) {
			JmxNotificationManager.this.handleNotification(this.trapBuilder, notification);
		}

		/*
		 * {@inheritDoc}
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString () {
			return "Listener[trapBuilder=" + this.trapBuilder + "]";
		}

	}

	/**
	 * Logger.
	 */
	protected final Logger logger = Logger.getLogger(JmxNotificationManager.class);

	/**
	 * SNMP managers on the network to which SNMP traps should be sent.
	 */
	private final SnmpManagers managers;

	/**
	 * Informations on the system.
	 */
	private final SystemInfo systemInfo;

	/**
	 * Map of JMX notification listener for each registered MBean.
	 */
	protected final Map<ObjectName, Listener> listenerMap = new HashMap<ObjectName, Listener>();

	/**
	 * <code>TRUE</code> to handle notifications, otherwise <code>FALSE</code> to ignore.
	 */
	private boolean enabled = false;

	/**
	 * Constructor.
	 * @param managers SNMP managers on the network to which SNMP traps should be sent (must not be <code>NULL</code>).
	 * @param systemInfo Informations on the system.
	 */
	public JmxNotificationManager (final SnmpManagers managers, final SystemInfo systemInfo) {
		super();
		this.managers = managers;
		this.systemInfo = systemInfo;
	}

	/**
	 * Registers a notification listener used for send SNMP traps from the JMX notifications.
	 * @param server JMX agent (must not be <code>NULL</code>).
	 * @param mBeanName MBean name (must not be <code>NULL</code>).
	 * @param trapMappingMap Map of mapping to build SNMP traps from the JMX notifications for each notification type (must not be <code>NULL</code>).
	 */
	synchronized void register (final MBeanServer server, final ObjectName mBeanName, final Map<String, SnmpTrapMapping> trapMappingMap) {
		if (trapMappingMap.isEmpty()) {
			if (this.logger.isDebugEnabled()) {
				this.logger.debug("None SNMP trap mapped for [" + mBeanName + "]");
			}
		}
		else {
			if (this.logger.isInfoEnabled()) {
				for (final Entry<String, SnmpTrapMapping> trapMappingEntry : trapMappingMap.entrySet()) {
					this.logger.info("MBean notification registered at [" + mBeanName + "].[" + trapMappingEntry.getKey() + "] = " + trapMappingEntry.getValue());
				}
			}
			final Listener listener = new Listener(new SnmpTrapBuilder(trapMappingMap, this.systemInfo));
			try {
				server.addNotificationListener(mBeanName, listener, null, null);
				this.listenerMap.put(mBeanName, listener);
			}
			catch (final Throwable e) {
				this.logger.warn("Notifications issued in on [" + mBeanName + "] will not cause any SNMP trap sending", e);
			}
		}
	}

	/**
	 * Unregisters all notification listeners used for send SNMP traps from the JMX notifications.
	 * @param server JMX agent.
	 */
	synchronized void unregisterAll (final MBeanServer server) {
		final List<ObjectName> mBeanNameList = new ArrayList<ObjectName>(this.listenerMap.keySet());
		for (final ObjectName mBeanName : mBeanNameList) {
			unregister(server, mBeanName);
		}
	}

	/**
	 * Unregisters a notification listener used for send SNMP traps from the JMX notifications.
	 * @param server JMX agent.
	 * @param mBeanName MBean name.
	 */
	synchronized void unregister (final MBeanServer server, final ObjectName mBeanName) {
		final Listener listener = this.listenerMap.get(mBeanName);
		if (listener != null) {
			if (this.logger.isInfoEnabled()) {
				for (final String notificationType : listener.trapBuilder.getMappingMap().keySet()) {
					this.logger.info("MBean notification unregistered at [" + mBeanName + "].[" + notificationType + "]");
				}
			}
			try {
				if (server.isRegistered(mBeanName)) {
					server.removeNotificationListener(mBeanName, listener);
				}
				else {
					if (this.logger.isTraceEnabled()) {
						this.logger.trace("[" + mBeanName + "] already unregistered from JMX");
					}
				}
				this.listenerMap.remove(mBeanName);
			}
			catch (final Throwable e) {
				this.logger.warn("An error occurred when unloading of [" + mBeanName + "] to support notifications", e);
			}
		}
	}

	/**
	 * Returns <code>TRUE</code> to handle notifications, or <code>FALSE</code> to ignore.
	 * @return <code>TRUE</code> to handle notifications.
	 */
	public final boolean isEnabled () {
		return this.enabled;
	}

	/**
	 * Sets <code>TRUE</code> to handle notifications, or <code>FALSE</code> to ignore.
	 * @param enabled <code>TRUE</code> to handle notifications.
	 */
	public void setEnabled (boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Handles a JMX notification from an MBean.
	 * @param trapBuilder Builder of SNMP traps from JMX notifications for an MBean.
	 * @param notification JMX notification.
	 */
	protected final void handleNotification (final SnmpTrapBuilder trapBuilder, final Notification notification) {
		if (this.enabled) {
			if (this.logger.isTraceEnabled()) {
				this.logger.trace("JMX notification [" + notification.getSource() + "].[" + notification.getType() + "] received");
				this.logger.trace("source           = " + notification.getSource());
				this.logger.trace("sequenceNumber   = " + notification.getSequenceNumber());
				this.logger.trace("timeStamp        = " + notification.getTimeStamp());
				this.logger.trace("type             = " + notification.getType());
				this.logger.trace("message          = " + notification.getMessage());
				if (notification.getUserData() instanceof Map) {
					for (final Entry<?, ?> entry : ((Map<?, ?>) (notification.getUserData())).entrySet()) {
						this.logger.trace("userData:        " + entry.getKey() + " = " + entry.getValue());
					}
				}
				else {
					this.logger.trace("userData         = "
							+ (notification.getUserData() != null ? "(" + notification.getUserData().getClass().getName() + ")" : "") + notification.getUserData());
				}
			}
			try {
				final SnmpTrap trap = trapBuilder.newTrap(notification);
				if (trap == null) {
					if (this.logger.isDebugEnabled()) {
						this.logger.debug("None SNMP trap to send for the JMX notification [" + notification.getSource() + "].[" + notification.getType() + "]");
					}
				}
				else {
					if (this.logger.isDebugEnabled()) {
						this.logger.debug("Notification [" + notification.getSource() + "].[" + notification.getType() + "] = " + trap);
					}
					this.managers.send(trap);
				}
			}
			catch (final Throwable e) {
				this.logger.error("Unable to handle the notification " + notification + " for send a SNMP trap", e);
			}
		}
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public final String toString () {
		return "JmxNotificationManager[managers=" + this.managers + "; systemInfo=" + this.systemInfo + "; listenerMap=" + this.listenerMap + "]";
	}

}