package net.sf.snmpadaptor4j.core.mapping;

import net.sf.snmpadaptor4j.object.SnmpOid;

/**
 * Object containing the <b>default mapping</b> to build the <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap} from a JMX
 * notification.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class DefaultDataMapTrapMapping
		extends DataMapTrapMapping {

	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = -5496101012219041389L;

	/**
	 * Hidden constructor.
	 * @param sequenceNumberOid OID of <code>sequenceNumber</code> field of JMX notification. Is NULL if the <code>sequenceNumber</code> should not be present in
	 *            <code>dataMap</code> field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 * @param messageOid OID of <code>message</code> field of JMX notification. Is NULL if the <code>message</code> should not be present in <code>dataMap</code>
	 *            field of {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 * @param hasSystemInfo <code>TRUE</code> for put all system information attributes in the <code>dataMap</code> field of
	 *            {@link net.sf.snmpadaptor4j.object.SnmpTrap SnmpTrap}.
	 * @see XmlMappingParser#newSnmpTrapMappingMap(String)
	 */
	DefaultDataMapTrapMapping (final SnmpOid sequenceNumberOid, final SnmpOid messageOid, final boolean hasSystemInfo) {
		super(sequenceNumberOid, messageOid, hasSystemInfo);
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.core.mapping.DataMapTrapMapping#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean result = false;
		if (obj == this) {
			result = true;
		}
		else {
			result = super.equals(obj);
		}
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "DefaultDataMapTrapMapping[sequenceNumberOid=" + getSequenceNumberOid() + "; messageOid=" + getMessageOid() + "; hasSystemInfo=" + isHasSystemInfo()
				+ "]";
	}

}