package net.sf.snmpadaptor4j.core;

import java.math.BigInteger;
import java.net.InetAddress;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Map.Entry;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import net.sf.snmpadaptor4j.api.SnmpMib;
import net.sf.snmpadaptor4j.api.AttributeAccessor;
import net.sf.snmpadaptor4j.core.accessor.MBeanAttributeAccessor;
import net.sf.snmpadaptor4j.core.mapping.MBeanAttributeMapping;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import org.apache.log4j.Logger;

/**
 * Object representing the <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB) for access to JMX attributes.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public class JmxSnmpMib
		implements SnmpMib {

	/**
	 * Logger.
	 */
	protected final Logger logger = Logger.getLogger(JmxSnmpMib.class);

	/**
	 * Other object representing the MIB.
	 */
	private final SnmpMib other;

	/**
	 * <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 */
	private final SortedMap<SnmpOid, AttributeAccessor> mib = new TreeMap<SnmpOid, AttributeAccessor>();

	/**
	 * Constructor.
	 * @param other Other object representing the MIB.
	 */
	public JmxSnmpMib (final SnmpMib other) {
		super();
		this.other = other;
	}

	/**
	 * Returns the <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 * @return <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 */
	protected final SortedMap<SnmpOid, AttributeAccessor> getMib () {
		return this.mib;
	}

	/**
	 * Unregister all attributes.
	 */
	void unregisterAllAttributes () {
		MBeanAttributeAccessor accessor;
		synchronized (this.mib) {
			while (!this.mib.isEmpty()) {
				accessor = (MBeanAttributeAccessor) this.mib.get(this.mib.firstKey());
				unregisterAttributes(accessor.getMBeanName(), accessor.getOid());
			}
		}
	}

	/**
	 * Registers a MBean to SNMP <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 * @param server JMX agent.
	 * @param mBeanName MBean name.
	 * @param mBeanAttributeMappingList List of mapping for access to each attribute of an MBean (must not be <code>NULL</code>).
	 * @throws Exception Exception if an error occurred.
	 */
	void registerAttributes (final MBeanServer server, final ObjectName mBeanName, final List<MBeanAttributeMapping> mBeanAttributeMappingList) throws Exception {
		for (final MBeanAttributeMapping mapping : mBeanAttributeMappingList) {
			if (this.logger.isInfoEnabled()) {
				this.logger.info("SNMP attribute registering at " + mapping.getOid() + " (" + mapping.getSnmpDataType() + ") = [" + mBeanName + "]."
						+ mapping.getAttributeName() + " (" + mapping.getJmxDataType().getSimpleName() + ")");
			}
			checkDataType(mapping.getSnmpDataType(), mapping.getJmxDataType());
			synchronized (this.mib) {
				this.mib.put(mapping.getOid(), new MBeanAttributeAccessor(server, mBeanName, mapping));
			}
		}
	}

	/**
	 * Unregisters a MBean of SNMP <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 * @param server JMX agent.
	 * @param mBeanName MBean name.
	 */
	void unregisterAttributes (final MBeanServer server, final ObjectName mBeanName) {
		final Set<SnmpOid> oidSet = new TreeSet<SnmpOid>();
		synchronized (this.mib) {
			for (final AttributeAccessor node : this.mib.values()) {
				if (mBeanName.equals(((MBeanAttributeAccessor) node).getMBeanName())) {
					oidSet.add(node.getOid());
				}
			}
			for (final SnmpOid oid : oidSet) {
				unregisterAttributes(mBeanName, oid);
			}
		}
	}

	/**
	 * Unregisters a MBean of SNMP <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 * @param mBeanName MBean name.
	 * @param oid OID of parameter to unregister.
	 */
	private void unregisterAttributes (final ObjectName mBeanName, final SnmpOid oid) {
		if (this.logger.isInfoEnabled()) {
			this.logger.info("SNMP attribute unregistering at " + oid + " for [" + mBeanName + "]");
		}
		this.mib.remove(oid);
	}

	/**
	 * Checks the consistency between the data types SNMP and Java.
	 * @param snmpDataType SNMP data type of MIB node.
	 * @param jmxDataType Data type of JMX attribute.
	 * @throws Exception Exception if an error occurred.
	 */
	protected static final void checkDataType (final SnmpDataType snmpDataType, final Class<?> jmxDataType) throws Exception {
		if (snmpDataType == SnmpDataType.integer32) {
			if (!int.class.equals(jmxDataType) && !Integer.class.equals(jmxDataType) && !byte.class.equals(jmxDataType) && !Byte.class.equals(jmxDataType)
					&& !short.class.equals(jmxDataType) && !Short.class.equals(jmxDataType) && !boolean.class.equals(jmxDataType)
					&& !Boolean.class.equals(jmxDataType)) {
				throw new Exception(snmpDataType + " is inconsistent with " + jmxDataType.getName());
			}
		}
		else if ((snmpDataType == SnmpDataType.unsigned32) || (snmpDataType == SnmpDataType.gauge32) || (snmpDataType == SnmpDataType.counter32)) {
			if (!long.class.equals(jmxDataType) && !Long.class.equals(jmxDataType) && !int.class.equals(jmxDataType) && !Integer.class.equals(jmxDataType)
					&& !byte.class.equals(jmxDataType) && !Byte.class.equals(jmxDataType) && !short.class.equals(jmxDataType) && !Short.class.equals(jmxDataType)) {
				throw new Exception(snmpDataType + " is inconsistent with " + jmxDataType.getName());
			}
		}
		else if (snmpDataType == SnmpDataType.counter64) {
			if (!BigInteger.class.equals(jmxDataType) && !long.class.equals(jmxDataType) && !Long.class.equals(jmxDataType)) {
				throw new Exception(snmpDataType + " is inconsistent with " + jmxDataType.getName());
			}
		}
		else if (snmpDataType == SnmpDataType.timeTicks) {
			if (!long.class.equals(jmxDataType) && !Long.class.equals(jmxDataType)) {
				throw new Exception(snmpDataType + " is inconsistent with " + jmxDataType.getName());
			}
		}
		else if (snmpDataType == SnmpDataType.ipAddress) {
			if (!String.class.equals(jmxDataType) && !InetAddress.class.equals(jmxDataType)) {
				throw new Exception(snmpDataType + " is inconsistent with " + jmxDataType.getName());
			}
		}
		else if (snmpDataType == SnmpDataType.objectIdentifier) {
			if (!String.class.equals(jmxDataType) && !SnmpOid.class.equals(jmxDataType)) {
				throw new Exception(snmpDataType + " is inconsistent with " + jmxDataType.getName());
			}
		}
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpMib#find(net.sf.snmpadaptor4j.object.SnmpOid)
	 */
	public final AttributeAccessor find (final SnmpOid oid) {
		AttributeAccessor node;
		synchronized (this.mib) {
			node = this.mib.get(oid);
		}
		if (node == null) {
			node = this.other.find(oid);
		}
		return node;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpMib#next(net.sf.snmpadaptor4j.object.SnmpOid)
	 */
	public final AttributeAccessor next (final SnmpOid oid) {
		AttributeAccessor node;
		final Iterator<Entry<SnmpOid, AttributeAccessor>> entryIterator = nextSet(oid).entrySet().iterator();
		if (entryIterator.hasNext()) {
			node = entryIterator.next().getValue();
		}
		else {
			node = null;
		}
		return node;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpMib#nextSet(net.sf.snmpadaptor4j.object.SnmpOid)
	 */
	public final SortedMap<SnmpOid, AttributeAccessor> nextSet (final SnmpOid oid) {
		final SortedMap<SnmpOid, AttributeAccessor> nodeMap = new TreeMap<SnmpOid, AttributeAccessor>();
		synchronized (this.mib) {
			nodeMap.putAll(this.mib.tailMap(SnmpOid.newInstance(oid.getOid(), 0)));
		}
		nodeMap.putAll(this.other.nextSet(oid));
		return nodeMap;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public final String toString () {
		return "JmxSnmpMib" + this.mib.values();
	}

}