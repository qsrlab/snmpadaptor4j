package net.sf.snmpadaptor4j.core.trap;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.log4j.Logger;
import net.sf.snmpadaptor4j.SnmpManagerConfiguration;
import net.sf.snmpadaptor4j.api.SnmpApiFactory;
import net.sf.snmpadaptor4j.api.SnmpTrapSender;
import net.sf.snmpadaptor4j.object.SnmpTrap;

/**
 * Object representing all SNMP managers on the network to which SNMP traps should be sent.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public class SnmpManagers {

	/**
	 * CONSTANT: Connection duration after each sending of SNMP traps (in milliseconds).
	 */
	private static final long CONNECTION_DURATION = 30000;

	/**
	 * {@link TimerTask} for close connections to all SNMP managers after a period of inactivity.
	 */
	final class ClosingTask
			extends TimerTask {

		/*
		 * {@inheritDoc}
		 * @see java.util.TimerTask#run()
		 */
		@Override
		public void run () {
			SnmpManagers.this.close();
		}

	}

	/**
	 * Logger.
	 */
	protected final Logger logger = Logger.getLogger(SnmpManagers.class);

	/**
	 * Connection duration after each sending of SNMP traps (in milliseconds).
	 */
	private final long connectionDuration;

	/**
	 * List of SNMP trap sender.
	 */
	protected final List<SnmpTrapSender> senderList = new ArrayList<SnmpTrapSender>();

	/**
	 * Factory used for instantiate senders of SNMP traps to SNMP managers.
	 */
	private final SnmpApiFactory senderFactory;

	/**
	 * Timer used to close connections to all SNMP managers.
	 */
	protected Timer timer = null;

	/**
	 * Closing time of connections to all SNMP managers (in milliseconds).
	 */
	protected long closingTime = Long.MAX_VALUE;

	/**
	 * Constructor.
	 * @param senderFactory Factory used for instantiate senders of SNMP traps to SNMP managers (must not be <code>NULL</code>).
	 */
	public SnmpManagers (final SnmpApiFactory senderFactory) {
		this(senderFactory, SnmpManagers.CONNECTION_DURATION);
	}

	/**
	 * Constructor (only for tests).
	 * @param senderFactory Factory used for instantiate senders of SNMP traps to SNMP managers (must not be <code>NULL</code>).
	 * @param connectionDuration Connection duration after each sending of SNMP traps (in milliseconds).
	 */
	protected SnmpManagers (final SnmpApiFactory senderFactory, final long connectionDuration) {
		super();
		this.senderFactory = senderFactory;
		this.connectionDuration = connectionDuration;
	}

	/**
	 * Initializes the configuration for SNMP traps sending to SNMP managers.
	 * @param managerList Connection parameters to all SNMP managers (must not be <code>NULL</code>).
	 * @throws Exception Exception if an error has occurred.
	 */
	public final void initialize (final List<SnmpManagerConfiguration> managerList) throws Exception {
		synchronized (this.senderList) {
			this.senderList.clear();
			final InetAddress agentAddress = InetAddress.getLocalHost();
			this.logger.info("Local IP address (SNMP agent) = " + agentAddress);
			for (final SnmpManagerConfiguration manager : managerList) {
				this.logger.info("SNMP manager at " + manager.getAddress() + ":" + manager.getPort() + " (v" + manager.getVersion() + " - community "
						+ manager.getCommunity() + ")");
				this.senderList.add(this.senderFactory.newSnmpTrapSender(agentAddress, manager.getAddress(), manager.getPort(), manager.getVersion(),
						manager.getCommunity()));
			}
		}
	}

	/**
	 * Sends a SNMP trap to all managers.
	 * @param trap Object representing a specific or generic SNMP trap (must not be <code>NULL</code>).
	 */
	public void send (final SnmpTrap trap) {
		synchronized (this.senderList) {
			if (this.logger.isDebugEnabled()) {
				this.logger.debug("New " + trap + " to send");
			}
			if (this.logger.isTraceEnabled()) {
				trap.traceTo(this.logger);
			}
			for (final SnmpTrapSender sender : this.senderList) {
				try {
					if (!sender.isConnected()) {
						sender.open();
					}
					sender.send(trap);
				}
				catch (final Throwable e) {
					this.logger.error("Unable to send a trap to the SNMP manager at " + sender.getName(), e);
				}
			}
			this.closingTime = System.currentTimeMillis() + this.connectionDuration;
			if (this.timer == null) {
				this.timer = new Timer();
				this.timer.schedule(new ClosingTask(), this.connectionDuration, this.connectionDuration);
			}
		}
	}

	/**
	 * Closes connections to all SNMP managers.
	 */
	final void close () {
		this.logger.trace("Checking Connections to the SNMP managers...");
		if (this.closingTime < System.currentTimeMillis()) {
			synchronized (this.senderList) {
				if (this.closingTime < System.currentTimeMillis()) {
					this.logger.trace("Closing connections to the SNMP managers...");
					this.timer.cancel();
					this.timer = null;
					this.closingTime = Long.MAX_VALUE;
					for (final SnmpTrapSender sender : this.senderList) {
						try {
							if (sender.isConnected()) {
								sender.close();
							}
						}
						catch (final Throwable e) {
							this.logger.error("Unable to close the connection to the SNMP manager at " + sender.getName(), e);
						}
					}
					this.logger.debug("Connections closed to the SNMP managers");
				}
			}
		}
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public final String toString () {
		return "SnmpManagers";
	}

}