package net.sf.snmpadaptor4j.core;

import java.util.Iterator;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;
import net.sf.snmpadaptor4j.api.SnmpMib;
import net.sf.snmpadaptor4j.api.AttributeAccessor;
import net.sf.snmpadaptor4j.core.accessor.CompositeDataAttributeAccessor;
import net.sf.snmpadaptor4j.core.accessor.JvmAttributeAccessor;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;

/**
 * Object representing the <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB) for attributes of JVM.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public class JvmSnmpMib
		implements SnmpMib {

	/**
	 * Other object representing the MIB.
	 */
	private final SnmpMib other;

	/**
	 * <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 */
	private final SortedMap<SnmpOid, AttributeAccessor> mib = new TreeMap<SnmpOid, AttributeAccessor>();

	/**
	 * Constructor.
	 * @param other Other object representing the MIB.
	 */
	public JvmSnmpMib (final SnmpMib other) {
		super();
		this.other = other;
	}

	private static final String jvmMgtMIB = "1.3.6.1.4.1.42.2.145.3.163.1";

	private static final String jvmMgtMIBObjects = jvmMgtMIB + ".1";

	private static final String jvmMgtMIBNotifications = jvmMgtMIB + ".2";

	private static final String jvmMgtMIBConformance = jvmMgtMIB + ".3";

	private static final String jvmClassLoading = jvmMgtMIBObjects + ".1";

	private static final String jvmMemory = jvmMgtMIBObjects + ".2";

	private static final String jvmThreading = jvmMgtMIBObjects + ".3";

	private static final String jvmRuntime = jvmMgtMIBObjects + ".4";

	private static final String jvmOS = jvmMgtMIBObjects + ".5";

	/**
	 * Opens the connection with the JMX agent.
	 * @param server JMX agent.
	 * @throws Exception Exception if an error occurred.
	 */
	public synchronized void open (final MBeanServer server) throws Exception {

		// jvmClassesLoadedCount
		putAttribute(server, JvmSnmpMib.jvmClassLoading + ".1.0", SnmpDataType.gauge32, "java.lang:type=ClassLoading", "LoadedClassCount", int.class, true);

		// jvmClassesTotalLoadedCount
		putAttribute(server, JvmSnmpMib.jvmClassLoading + ".2.0", SnmpDataType.counter64, "java.lang:type=ClassLoading", "TotalLoadedClassCount", long.class, true);

		// jvmClassesUnloadedCount
		putAttribute(server, JvmSnmpMib.jvmClassLoading + ".3.0", SnmpDataType.counter64, "java.lang:type=ClassLoading", "UnloadedClassCount", long.class, true);

		// jvmMemoryPendingFinalCount
		putAttribute(server, JvmSnmpMib.jvmMemory + ".1.0", SnmpDataType.gauge32, "java.lang:type=Memory", "ObjectPendingFinalizationCount", int.class, true);

		// jvmMemoryHeap
		putCompositeDataAttribute(server, JvmSnmpMib.jvmMemory + ".10.0", "java.lang:type=Memory", "HeapMemoryUsage", "init", SnmpDataType.counter64, long.class);
		putCompositeDataAttribute(server, JvmSnmpMib.jvmMemory + ".11.0", "java.lang:type=Memory", "HeapMemoryUsage", "used", SnmpDataType.counter64, long.class);
		putCompositeDataAttribute(server, JvmSnmpMib.jvmMemory + ".12.0", "java.lang:type=Memory", "HeapMemoryUsage", "committed", SnmpDataType.counter64,
				long.class);
		putCompositeDataAttribute(server, JvmSnmpMib.jvmMemory + ".13.0", "java.lang:type=Memory", "HeapMemoryUsage", "max", SnmpDataType.counter64, long.class);

		// jvmMemoryNonHeap
		putCompositeDataAttribute(server, JvmSnmpMib.jvmMemory + ".20.0", "java.lang:type=Memory", "NonHeapMemoryUsage", "init", SnmpDataType.counter64, long.class);
		putCompositeDataAttribute(server, JvmSnmpMib.jvmMemory + ".21.0", "java.lang:type=Memory", "NonHeapMemoryUsage", "used", SnmpDataType.counter64, long.class);
		putCompositeDataAttribute(server, JvmSnmpMib.jvmMemory + ".22.0", "java.lang:type=Memory", "NonHeapMemoryUsage", "committed", SnmpDataType.counter64,
				long.class);
		putCompositeDataAttribute(server, JvmSnmpMib.jvmMemory + ".23.0", "java.lang:type=Memory", "NonHeapMemoryUsage", "max", SnmpDataType.counter64, long.class);

		// jvmThreadCount
		putAttribute(server, JvmSnmpMib.jvmThreading + ".1.0", SnmpDataType.gauge32, "java.lang:type=Threading", "ThreadCount", int.class, true);

		// jvmThreadDaemonCount
		putAttribute(server, JvmSnmpMib.jvmThreading + ".2.0", SnmpDataType.gauge32, "java.lang:type=Threading", "DaemonThreadCount", int.class, true);

		// jvmThreadPeakCount
		putAttribute(server, JvmSnmpMib.jvmThreading + ".3.0", SnmpDataType.counter32, "java.lang:type=Threading", "PeakThreadCount", int.class, true);

		// jvmThreadTotalStartedCount
		putAttribute(server, JvmSnmpMib.jvmThreading + ".4.0", SnmpDataType.counter64, "java.lang:type=Threading", "TotalStartedThreadCount", long.class, true);

		// jvmThreadContentionMonitoring
		putAttribute(server, JvmSnmpMib.jvmThreading + ".5.0", SnmpDataType.counter64, "java.lang:type=Threading", "TotalStartedThreadCount", long.class, true);

		// jvmRTName
		putAttribute(server, JvmSnmpMib.jvmRuntime + ".1.0", SnmpDataType.octetString, "java.lang:type=Runtime", "Name", String.class, true);

		// jvmRTVMName
		putAttribute(server, JvmSnmpMib.jvmRuntime + ".2.0", SnmpDataType.octetString, "java.lang:type=Runtime", "VmName", String.class, true);

		// jvmRTVMVendor
		putAttribute(server, JvmSnmpMib.jvmRuntime + ".3.0", SnmpDataType.octetString, "java.lang:type=Runtime", "VmVendor", String.class, true);

		// jvmRTVMVersion
		putAttribute(server, JvmSnmpMib.jvmRuntime + ".4.0", SnmpDataType.octetString, "java.lang:type=Runtime", "VmVersion", String.class, true);

		// jvmRTSpecName
		putAttribute(server, JvmSnmpMib.jvmRuntime + ".5.0", SnmpDataType.octetString, "java.lang:type=Runtime", "SpecName", String.class, true);

		// jvmRTSpecVendor
		putAttribute(server, JvmSnmpMib.jvmRuntime + ".6.0", SnmpDataType.octetString, "java.lang:type=Runtime", "SpecVendor", String.class, true);

		// jvmRTSpecVersion
		putAttribute(server, JvmSnmpMib.jvmRuntime + ".7.0", SnmpDataType.octetString, "java.lang:type=Runtime", "SpecVersion", String.class, true);

		// jvmRTManagementSpecVersion
		putAttribute(server, JvmSnmpMib.jvmRuntime + ".8.0", SnmpDataType.octetString, "java.lang:type=Runtime", "ManagementSpecVersion", String.class, true);

		// jvmRTUptimeMs
		putAttribute(server, JvmSnmpMib.jvmRuntime + ".11.0", SnmpDataType.counter64, "java.lang:type=Runtime", "Uptime", long.class, true);

		// jvmRTStartTimeMs
		putAttribute(server, JvmSnmpMib.jvmRuntime + ".12.0", SnmpDataType.counter64, "java.lang:type=Runtime", "StartTime", long.class, true);

		// jvmOSName
		putAttribute(server, JvmSnmpMib.jvmOS + ".1.0", SnmpDataType.octetString, "java.lang:type=OperatingSystem", "Name", String.class, true);

		// jvmOSArch
		putAttribute(server, JvmSnmpMib.jvmOS + ".2.0", SnmpDataType.octetString, "java.lang:type=OperatingSystem", "Arch", String.class, true);

		// jvmOSVersion
		putAttribute(server, JvmSnmpMib.jvmOS + ".3.0", SnmpDataType.octetString, "java.lang:type=OperatingSystem", "Version", String.class, true);

		// jvmOSProcessorCount
		putAttribute(server, JvmSnmpMib.jvmOS + ".4.0", SnmpDataType.integer32, "java.lang:type=OperatingSystem", "AvailableProcessors", int.class, true);

		// TODO
		// see http://docs.oracle.com/javase/1.5.0/docs/guide/management/JVM-MANAGEMENT-MIB.mib

	}

	/**
	 * Closes the connection with the JMX agent.
	 */
	public synchronized void close () {
		this.mib.clear();
	}

	/**
	 * Adds a new JVM attribute.
	 * @param server JMX agent.
	 * @param oidAsStr Object Identifier of attribute as {@link String}.
	 * @param snmpDataType SNMP data type of MIB node.
	 * @param mBeanName MBean name.
	 * @param attributeName Attribute name.
	 * @param jmxDataType Data type of JMX attribute.
	 * @param readonly <code>TRUE</code> if the attribute is read only.
	 * @throws Exception Exception if an error occurred.
	 */
	public final void putAttribute (final MBeanServer server, final String oidAsStr, final SnmpDataType snmpDataType, final String mBeanName,
			final String attributeName, final Class<?> jmxDataType, final boolean readonly) throws Exception {
		final SnmpOid oid = SnmpOid.newInstance(oidAsStr);
		this.mib.put(oid, new JvmAttributeAccessor(server, new ObjectName(mBeanName), oid, attributeName, snmpDataType, jmxDataType, !readonly));
	}

	/**
	 * Adds a new JVM attribute to {@link CompositeData}.
	 * @param server JMX agent.
	 * @param oidAsStr Object Identifier of attribute as {@link String}.
	 * @param mBeanName MBean name.
	 * @param attributeName Attribute name of {@link CompositeData}.
	 * @param attributeKey Attribute key of value.
	 * @param snmpDataType SNMP data type of MIB node.
	 * @param jmxDataType Data type of JMX attribute.
	 * @throws Exception Exception if an error occurred.
	 */
	public final void putCompositeDataAttribute (final MBeanServer server, final String oidAsStr, final String mBeanName, final String attributeName,
			final String attributeKey, final SnmpDataType snmpDataType, final Class<?> jmxDataType) throws Exception {
		final SnmpOid oid = SnmpOid.newInstance(oidAsStr);
		this.mib.put(oid, new CompositeDataAttributeAccessor(oid, server, new ObjectName(mBeanName), attributeName, attributeKey, snmpDataType, jmxDataType));
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpMib#find(net.sf.snmpadaptor4j.object.SnmpOid)
	 */
	public final AttributeAccessor find (final SnmpOid oid) {
		AttributeAccessor node;
		synchronized (this.mib) {
			node = this.mib.get(oid);
		}
		if (node == null) {
			node = this.other.find(oid);
		}
		return node;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpMib#next(net.sf.snmpadaptor4j.object.SnmpOid)
	 */
	public final AttributeAccessor next (final SnmpOid oid) {
		AttributeAccessor node;
		final Iterator<Entry<SnmpOid, AttributeAccessor>> entryIterator = nextSet(oid).entrySet().iterator();
		if (entryIterator.hasNext()) {
			node = entryIterator.next().getValue();
		}
		else {
			node = null;
		}
		return node;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpMib#nextSet(net.sf.snmpadaptor4j.object.SnmpOid)
	 */
	public final SortedMap<SnmpOid, AttributeAccessor> nextSet (final SnmpOid oid) {
		final SortedMap<SnmpOid, AttributeAccessor> nodeMap = new TreeMap<SnmpOid, AttributeAccessor>();
		synchronized (this.mib) {
			nodeMap.putAll(this.mib.tailMap(SnmpOid.newInstance(oid.getOid(), 0)));
		}
		nodeMap.putAll(this.other.nextSet(oid));
		return nodeMap;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public final String toString () {
		return "JvmSnmpMib[" + this.mib.values() + "]";
	}

}