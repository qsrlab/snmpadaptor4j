package net.sf.snmpadaptor4j.core;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanServer;
import javax.management.MBeanServerDelegate;
import javax.management.MBeanServerNotification;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.relation.MBeanServerNotificationFilter;
import net.sf.snmpadaptor4j.SnmpAppContext;
import net.sf.snmpadaptor4j.core.mapping.MBeanAttributeMapping;
import net.sf.snmpadaptor4j.core.mapping.SnmpTrapMapping;
import net.sf.snmpadaptor4j.core.mapping.XmlMappingParser;
import org.apache.log4j.Logger;

/**
 * Object designed to respond to each registration or deregistration of MBeans.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public class JmxListener
		implements NotificationListener {

	/**
	 * Logger.
	 */
	protected final Logger logger = Logger.getLogger(JmxListener.class);

	/**
	 * URL where to read snmp.xml files from.
	 * A snmp.xml full name is computed based on this URL + / + simpleClassName + .snmp.xml
	 */
	private final String configSnmpXmlUrl;

	/**
	 * <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB) for access to JMX attributes.
	 */
	private final JmxSnmpMib jmxSnmpMib;

	/**
	 * Manager of JMX notifications.
	 */
	private final JmxNotificationManager jmxNotificationManager;

	/**
	 * Context of main application.
	 */
	private final SnmpAppContext mainAppContext;

	/**
	 * Application context map.
	 */
	private final Map<ClassLoader, SnmpAppContext> appContextMap;

	/**
	 * <code>TRUE</code> for handle only MBeans created by the same {@link ClassLoader} that the SNMP adapter. <code>FALSE</code> for handle all MBeans of the JVM.
	 */
	private final boolean classLoaderScope;

	/**
	 * JMX agent.
	 */
	private MBeanServer jmxServer;

	/**
	 * Constructor.
	 * @param jmxSnmpMib <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB) for access to JMX attributes (must not be <code>NULL</code>).
	 * @param jmxNotificationManager Manager of JMX notifications (must not be <code>NULL</code>).
	 * @param mainAppContext Context of main application (must not be <code>NULL</code>).
	 * @param appContextMap Application context map (must not be <code>NULL</code>).
	 * @param classLoaderScope <code>TRUE</code> for handle only MBeans created by the same {@link ClassLoader} that the SNMP adapter. <code>FALSE</code> for handle
	 *            all MBeans of the JVM.
	 */
	public JmxListener (final JmxSnmpMib jmxSnmpMib, final JmxNotificationManager jmxNotificationManager, final SnmpAppContext mainAppContext,
			final Map<ClassLoader, SnmpAppContext> appContextMap, final boolean classLoaderScope,String configUrl) {
		this(jmxSnmpMib, jmxNotificationManager, mainAppContext, appContextMap, classLoaderScope, null,configUrl);
	}

	/**
	 * Constructor (used for tests).
	 * @param jmxSnmpMib <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB) for access to JMX attributes (must not be <code>NULL</code>).
	 * @param jmxNotificationManager Manager of JMX notifications.
	 * @param mainAppContext Context of main application (must not be <code>NULL</code>).
	 * @param appContextMap Application context map (must not be <code>NULL</code>).
	 * @param classLoaderScope <code>TRUE</code> for handle only MBeans created by the same {@link ClassLoader} that the SNMP adapter. <code>FALSE</code> for handle
	 *            all MBeans of the JVM.
	 * @param jmxServer JMX agent.
	 */
	protected JmxListener (final JmxSnmpMib jmxSnmpMib, final JmxNotificationManager jmxNotificationManager, final SnmpAppContext mainAppContext,
			final Map<ClassLoader, SnmpAppContext> appContextMap, final boolean classLoaderScope, final MBeanServer jmxServer, final String configUrl) {
		super();
		this.mainAppContext = mainAppContext;
		this.appContextMap = appContextMap;
		this.classLoaderScope = classLoaderScope;
		this.jmxSnmpMib = jmxSnmpMib;
		this.jmxNotificationManager = jmxNotificationManager;
		this.jmxServer = jmxServer;
		this.configSnmpXmlUrl = configUrl;
	}

	/**
	 * Returns the JMX agent.
	 * @return JMX agent.
	 */
	protected final MBeanServer getJmxServer () {
		return this.jmxServer;
	}

	/**
	 * Opens the connection with the JMX agent.
	 * @param server JMX agent.
	 * @throws Exception Exception if an error occurred.
	 */
	public synchronized void open (final MBeanServer server) throws Exception {
		this.logger.trace("JMX opening...");
		if (this.jmxServer != null) {
			throw new Exception("Already connected to a JMX agent");
		}

		// JMX notification registering
		final MBeanServerNotificationFilter filter = new MBeanServerNotificationFilter();
		filter.enableAllObjectNames();
		server.addNotificationListener(MBeanServerDelegate.DELEGATE_NAME, this, filter, server);

		// Existing MBean parsing
		final Set<ObjectName> mBeanNameList = server.queryNames(null, null);
		for (final ObjectName mBeanName : mBeanNameList) {
			register(server, mBeanName);
		}

		this.jmxServer = server;
		this.logger.trace("JMX opened");
	}

	/**
	 * Closes the connection with the JMX agent.
	 * @throws Exception Exception if an error occurred.
	 */
	public synchronized void close () throws Exception {
		this.logger.trace("JMX closing...");
		if (this.jmxServer == null) {
			throw new Exception("Not connected to a JMX agent");
		}

		// JMX notification deregistering
		this.jmxServer.removeNotificationListener(MBeanServerDelegate.DELEGATE_NAME, this);

		// Cleaning
		this.jmxNotificationManager.unregisterAll(this.jmxServer);
		this.jmxSnmpMib.unregisterAllAttributes();

		this.jmxServer = null;
		this.logger.trace("JMX closed");
	}

	/*
	 * {@inheritDoc}
	 * @see javax.management.NotificationListener#handleNotification(javax.management.Notification, java.lang.Object)
	 */
	public final synchronized void handleNotification (final Notification notification, final Object handback) {
		if ((handback instanceof MBeanServer) && (this.jmxServer == (MBeanServer) handback)) {
			if (notification instanceof MBeanServerNotification) {
				final MBeanServerNotification serverNotification = (MBeanServerNotification) notification;
				if (MBeanServerNotification.REGISTRATION_NOTIFICATION.equals(serverNotification.getType())) {
					try {
						register((MBeanServer) handback, serverNotification.getMBeanName());
					}
					catch (final Throwable e) {
						this.logger.error(serverNotification.getMBeanName() + ": MBean not loaded in SNMP adapter", e);
					}
				}
				else if (MBeanServerNotification.UNREGISTRATION_NOTIFICATION.equals(serverNotification.getType())) {
					try {
						unregister((MBeanServer) handback, serverNotification.getMBeanName());
					}
					catch (final Throwable e) {
						this.logger.error(serverNotification.getMBeanName() + ": MBean not unloaded in SNMP adapter", e);
					}
				}
			}
		}
	}

	/**
	 * Registers a MBean to SNMP <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 * @param server JMX agent.
	 * @param mBeanName MBean name.
	 * @throws Exception Exception if an error occurred.
	 */
	private void register (final MBeanServer server, final ObjectName mBeanName) throws Exception {
		if (server.isRegistered(mBeanName)) {
			final ClassLoader classLoader = server.getClassLoaderFor(mBeanName);
			if (classLoader != null) {
				if (!this.classLoaderScope || this.getClass().getClassLoader().equals(classLoader)) {
					final Class<?> mBeanClass = classLoader.loadClass(server.getObjectInstance(mBeanName).getClassName());
					final URL url;
					if(configSnmpXmlUrl==null || configSnmpXmlUrl.isEmpty()){
						url = mBeanClass.getResource(mBeanClass.getSimpleName() + ".snmp.xml");
					}else {
						url = new URL(configSnmpXmlUrl+mBeanClass.getSimpleName()+".snmp.xml");
					}
					if (url != null) {
						if (this.logger.isDebugEnabled()) {
							this.logger.debug("SNMP mapping found at " + url);
						}
						final XmlMappingParser parser = XmlMappingParser.newInstance(url);

						// Base OID finding
						final SnmpAppContext ctx = findAppContext(classLoader);
						String baseOid = ctx.getMBeanOidMap().get(mBeanName);
						if (baseOid == null) {
							baseOid = parser.findBaseOid(mBeanName, ctx.getRootOidMap(), ctx.getDefaultRootOid(), this.mainAppContext.getDefaultRootOid());
						}
						if (baseOid == null) {
							if (this.logger.isDebugEnabled()) {
								this.logger.debug("None OID found for [" + mBeanName + "]");
							}
						}
						else {

							// Mapping loading
							final Map<String, MBeanAttributeInfo> mBeanAttributeInfoMap = new HashMap<String, MBeanAttributeInfo>();
							for (final MBeanAttributeInfo mBeanAttributeInfo : server.getMBeanInfo(mBeanName).getAttributes()) {
								mBeanAttributeInfoMap.put(mBeanAttributeInfo.getName(), mBeanAttributeInfo);
							}
							final List<MBeanAttributeMapping> mBeanAttributeMappingList = parser.newMBeanAttributeMappingList(mBeanAttributeInfoMap, classLoader,
									baseOid);
							this.jmxSnmpMib.registerAttributes(server, mBeanName, mBeanAttributeMappingList);
							final Map<String, SnmpTrapMapping> trapMappingMap = parser.newSnmpTrapMappingMap(baseOid);
							this.jmxNotificationManager.register(server, mBeanName, trapMappingMap);

						}
					}
					else if (this.logger.isDebugEnabled()) {
						this.logger.debug("SNMP mapping missing for [" + mBeanName + "]");
					}
				}
				else if (this.logger.isTraceEnabled()) {
					this.logger.trace("SNMP mapping ignored because classLoaderScope = TRUE for [" + mBeanName + "]");
				}
			}
			else if (this.logger.isTraceEnabled()) {
				this.logger.trace("SNMP mapping ignored because the MBean is not accessible for [" + mBeanName + "]");
			}
		}
	}

	/**
	 * Finds the application context by its class loader.
	 * @param classLoader Class loader of application.
	 * @return Application context found.
	 */
	private SnmpAppContext findAppContext (final ClassLoader classLoader) {
		SnmpAppContext ctx = null;
		ClassLoader cl = classLoader;
		while ((ctx == null) && (cl != null)) {
			ctx = this.appContextMap.get(cl);
			cl = cl.getParent();
		}
		if (ctx == null) {
			ctx = this.mainAppContext;
		}
		return ctx;
	}

	/**
	 * Unregisters a MBean of SNMP <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 * @param server JMX agent.
	 * @param mBeanName MBean name.
	 */
	private void unregister (final MBeanServer server, final ObjectName mBeanName) {
		this.jmxSnmpMib.unregisterAttributes(server, mBeanName);
		this.jmxNotificationManager.unregister(server, mBeanName);
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public final String toString () {
		return "JmxListener[jmxSnmpMib=" + this.jmxSnmpMib + "; jmxNotificationManager=" + this.jmxNotificationManager + "; mainAppContext=" + this.mainAppContext
				+ "; appContextMap=" + this.appContextMap + "; classLoaderScope=" + this.classLoaderScope + "]";
	}

}