package net.sf.snmpadaptor4j.core.mapping;

import java.io.Serializable;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;

/**
 * Object containing the mapping for access to a MBean attribute.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class MBeanAttributeMapping
		implements Serializable {

	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = 1031970528675491341L;

	/**
	 * Object Identifier of MIB node.
	 */
	private final SnmpOid oid;

	/**
	 * Attribute name.
	 */
	private final String attributeName;

	/**
	 * SNMP data type of MIB node.
	 */
	private final SnmpDataType snmpDataType;

	/**
	 * Data type of JMX attribute.
	 */
	private final Class<?> jmxDataType;

	/**
	 * <code>TRUE</code> if the attribute can be read (for SNMP write and read community), <code>FALSE</code> otherwise.
	 */
	private final boolean readable;

	/**
	 * <code>TRUE</code> if the attribute can be write (for SNMP write community), <code>FALSE</code> otherwise.
	 */
	private final boolean writable;

	/**
	 * Hidden constructor.
	 * @param oid Object Identifier of MIB node.
	 * @param attributeName Attribute name.
	 * @param snmpDataType SNMP data type of MIB node.
	 * @param jmxDataType Data type of JMX attribute.
	 * @param readable <code>TRUE</code> if the attribute can be read (for SNMP write and read community), <code>FALSE</code> otherwise.
	 * @param writable <code>TRUE</code> if the attribute can be write (for SNMP write community), <code>FALSE</code> otherwise.
	 * @see XmlMappingParser#newMBeanAttributeMappingList(java.util.Map, ClassLoader, String)
	 */
	MBeanAttributeMapping (final SnmpOid oid, final String attributeName, final SnmpDataType snmpDataType, final Class<?> jmxDataType, final boolean readable,
			final boolean writable) {
		super();
		this.oid = oid;
		this.attributeName = attributeName;
		this.snmpDataType = snmpDataType;
		this.jmxDataType = jmxDataType;
		this.readable = readable;
		this.writable = writable;
	}

	/**
	 * Returns the Object Identifier of MIB node.
	 * @return Object Identifier of MIB node.
	 */
	public SnmpOid getOid () {
		return this.oid;
	}

	/**
	 * Returns the attribute name.
	 * @return Attribute name.
	 */
	public String getAttributeName () {
		return this.attributeName;
	}

	/**
	 * Returns the SNMP data type of MIB node.
	 * @return SNMP data type of MIB node.
	 */
	public SnmpDataType getSnmpDataType () {
		return this.snmpDataType;
	}

	/**
	 * Returns the data type of JMX attribute.
	 * @return Data type of JMX attribute.
	 */
	public Class<?> getJmxDataType () {
		return this.jmxDataType;
	}

	/**
	 * Returns <code>TRUE</code> if the attribute can be read (for SNMP write and read community), <code>FALSE</code> otherwise.
	 * @return <code>TRUE</code> if the attribute can be read (for SNMP write and read community), <code>FALSE</code> otherwise.
	 */
	public boolean isReadable () {
		return this.readable;
	}

	/**
	 * Returns <code>TRUE</code> if the attribute can be write (for SNMP write community), <code>FALSE</code> otherwise.
	 * @return <code>TRUE</code> if the attribute can be write (for SNMP write community), <code>FALSE</code> otherwise.
	 */
	public boolean isWritable () {
		return this.writable;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.attributeName == null) ? 0 : this.attributeName.hashCode());
		result = prime * result + ((this.jmxDataType == null) ? 0 : this.jmxDataType.hashCode());
		result = prime * result + ((this.oid == null) ? 0 : this.oid.hashCode());
		result = prime * result + (this.readable ? 1231 : 1237);
		result = prime * result + ((this.snmpDataType == null) ? 0 : this.snmpDataType.hashCode());
		result = prime * result + (this.writable ? 1231 : 1237);
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean result = false;
		if (obj == this) {
			result = true;
		}
		else if ((obj != null) && (MBeanAttributeMapping.class.equals(obj.getClass()))) {
			final MBeanAttributeMapping other = (MBeanAttributeMapping) obj;
			result = (this.oid != null ? this.oid.equals(other.oid) : (other.oid == null));
			if (result) {
				result = (this.attributeName != null ? this.attributeName.equals(other.attributeName) : (other.attributeName == null));
			}
			if (result) {
				result = (this.snmpDataType != null ? this.snmpDataType.equals(other.snmpDataType) : (other.snmpDataType == null));
			}
			if (result) {
				result = (this.jmxDataType != null ? this.jmxDataType.equals(other.jmxDataType) : (other.jmxDataType == null));
			}
			if (result) {
				result = (this.readable == other.readable);
			}
			if (result) {
				result = (this.writable == other.writable);
			}
		}
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "MBeanAttributeMapping[oid=" + this.oid + "; attributeName=" + this.attributeName + "; snmpDataType=" + this.snmpDataType + "; jmxDataType="
				+ this.jmxDataType + "; readable=" + this.readable + "; writable=" + this.writable + "]";
	}

}