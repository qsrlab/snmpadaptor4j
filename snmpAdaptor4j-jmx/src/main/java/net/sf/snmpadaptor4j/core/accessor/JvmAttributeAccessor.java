package net.sf.snmpadaptor4j.core.accessor;

import javax.management.Attribute;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import net.sf.snmpadaptor4j.api.AttributeAccessor;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;

/**
 * Object representing an accessor to an attribute of JVM MBean.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class JvmAttributeAccessor
		implements AttributeAccessor {

	/**
	 * JMX agent where the MBean was registered.
	 */
	private final MBeanServer server;

	/**
	 * MBean name.
	 */
	private final ObjectName mBeanName;

	/**
	 * <b>O</b>bject <b>ID</b>entifier (OID) to locate the attribute in the MIB.
	 */
	private final SnmpOid oid;

	/**
	 * Attribute name.
	 */
	private final String attributeName;

	/**
	 * SNMP data type of attribute.
	 */
	private final SnmpDataType snmpDataType;

	/**
	 * Data type of JMX attribute.
	 */
	private final Class<?> jmxDataType;

	/**
	 * <code>TRUE</code> if the attribute can be write (for SNMP write community), <code>FALSE</code> otherwise.
	 */
	private final boolean writable;

	/**
	 * Constructor.
	 * @param server JMX agent where the MBean was registered.
	 * @param mBeanName MBean name.
	 * @param oid <b>O</b>bject <b>ID</b>entifier (OID) to locate the attribute in the MIB.
	 * @param attributeName Attribute name.
	 * @param snmpDataType SNMP data type of attribute.
	 * @param jmxDataType Data type of JMX attribute.
	 * @param writable <code>TRUE</code> if the attribute can be write (for SNMP write community), <code>FALSE</code> otherwise.
	 */
	public JvmAttributeAccessor (final MBeanServer server, final ObjectName mBeanName, final SnmpOid oid, final String attributeName,
			final SnmpDataType snmpDataType, final Class<?> jmxDataType, final boolean writable) {
		super();
		this.server = server;
		this.mBeanName = mBeanName;
		this.oid = oid;
		this.attributeName = attributeName;
		this.snmpDataType = snmpDataType;
		this.jmxDataType = jmxDataType;
		this.writable = writable;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getOid()
	 */
	public SnmpOid getOid () {
		return this.oid;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getSnmpDataType()
	 */
	public SnmpDataType getSnmpDataType () {
		return this.snmpDataType;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getJmxDataType()
	 */
	public Class<?> getJmxDataType () {
		return this.jmxDataType;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getValue()
	 */
	public Object getValue () throws Exception {
		return this.server.getAttribute(this.mBeanName, this.attributeName);
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#setValue(java.lang.Object)
	 */
	public void setValue (Object value) throws Exception {
		this.server.setAttribute(this.mBeanName, new Attribute(this.attributeName, value));
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#isReadable()
	 */
	public boolean isReadable () {
		return true;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#isWritable()
	 */
	public boolean isWritable () {
		return this.writable;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "JvmAttributeAccessor[oid=" + this.oid + "; attributeName=" + this.attributeName + "; snmpDataType=" + this.snmpDataType + "; jmxDataType="
				+ this.jmxDataType + "; readable=" + isReadable() + "; writable=" + this.writable + "]";
	}

}