package net.sf.snmpadaptor4j.core.accessor;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;
import net.sf.snmpadaptor4j.api.AttributeAccessor;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;

/**
 * Object representing an accessor to an attribute for {@link CompositeData}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class CompositeDataAttributeAccessor
		implements AttributeAccessor {

	/**
	 * <b>O</b>bject <b>ID</b>entifier (OID) to locate the attribute in the MIB.
	 */
	private final SnmpOid oid;

	/**
	 * JMX agent where the MBean was registered.
	 */
	private final MBeanServer server;

	/**
	 * MBean name.
	 */
	private final ObjectName mBeanName;

	/**
	 * Attribute name of {@link CompositeData}.
	 */
	private final String attributeName;

	/**
	 * Attribute key of value.
	 */
	private final String attributeKey;

	/**
	 * SNMP data type of attribute.
	 */
	private final SnmpDataType snmpDataType;

	/**
	 * Data type of JMX attribute.
	 */
	private final Class<?> jmxDataType;

	/**
	 * Constructor.
	 * @param oid <b>O</b>bject <b>ID</b>entifier (OID) to locate the attribute in the MIB.
	 * @param server JMX agent where the MBean was registered.
	 * @param mBeanName MBean name.
	 * @param attributeName Attribute name of {@link CompositeData}.
	 * @param attributeKey Attribute key of value.
	 * @param snmpDataType SNMP data type of attribute.
	 * @param jmxDataType Data type of JMX attribute.
	 */
	public CompositeDataAttributeAccessor (final SnmpOid oid, final MBeanServer server, final ObjectName mBeanName, final String attributeName,
			final String attributeKey, final SnmpDataType snmpDataType, final Class<?> jmxDataType) {
		super();
		this.oid = oid;
		this.server = server;
		this.mBeanName = mBeanName;
		this.attributeName = attributeName;
		this.attributeKey = attributeKey;
		this.snmpDataType = snmpDataType;
		this.jmxDataType = jmxDataType;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getOid()
	 */
	public SnmpOid getOid () {
		return this.oid;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getSnmpDataType()
	 */
	public SnmpDataType getSnmpDataType () {
		return this.snmpDataType;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getJmxDataType()
	 */
	public Class<?> getJmxDataType () {
		return this.jmxDataType;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#getValue()
	 */
	public Object getValue () throws Exception {
		Object value = null;
		final CompositeData compositeData = (CompositeData) this.server.getAttribute(this.mBeanName, this.attributeName);
		if (compositeData != null) {
			value = compositeData.get(this.attributeKey);
		}
		return value;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#setValue(java.lang.Object)
	 */
	public void setValue (final Object value) {
		// NOP
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#isReadable()
	 */
	public boolean isReadable () {
		return true;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.AttributeAccessor#isWritable()
	 */
	public boolean isWritable () {
		return false;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		Object value;
		try {
			value = getValue();
		}
		catch (final Throwable e) {
			value = "ERROR: " + (e.getMessage() != null ? e.getMessage() : e.getClass().getName());
		}
		return this.oid + ": (" + getSnmpDataType() + ") " + value;
	}

}