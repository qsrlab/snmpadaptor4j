package net.sf.snmpadaptor4j;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanConstructorInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanNotificationInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanParameterInfo;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.StandardMBean;
import net.sf.snmpadaptor4j.api.SnmpApiFactory;
import net.sf.snmpadaptor4j.api.SnmpDaemon;
import net.sf.snmpadaptor4j.api.opennms.OpennmsSnmpApiFactory;
import net.sf.snmpadaptor4j.config.XmlConfigParser;
import net.sf.snmpadaptor4j.core.JmxListener;
import net.sf.snmpadaptor4j.core.JmxNotificationManager;
import net.sf.snmpadaptor4j.core.JmxSnmpMib;
import net.sf.snmpadaptor4j.core.JvmSnmpMib;
import net.sf.snmpadaptor4j.core.SystemSnmpMib;
import net.sf.snmpadaptor4j.core.trap.SnmpManagers;
import net.sf.snmpadaptor4j.mbean.SystemInfo;
import org.apache.log4j.Logger;

/**
 * <p>
 * snmpAdaptor4j is an adaptor for <a href="http://en.wikipedia.org/wiki/Java_Management_Extensions" target="_blank">Java Management eXtensions (JMX)</a> providing a
 * simple access to MBeans via the <a href="http://en.wikipedia.org/wiki/Simple_Network_Management_Protocol" target="_blank">SNMP</a> protocol. Thus, this adapter
 * you allow to connect most monitoring tools (like <a href="http://www.nagios.org/" target="_blank">Nagios</a> and <a href="http://www.cacti.net"
 * target="_blank">Cacti</a>) to your Java applications.
 * </p>
 * <p>
 * For each MBean, an XML mapping file allows to establish the relationships between attributes and the MIB of the SNMP adapter. No additional code is necessary to
 * integrate the MBeans in the SNMP protocol.
 * </p>
 * <p>
 * This adapter can work in a multi-applications environment (such as application servers).
 * </p>
 * <h4>Building of the mapping files</h4>
 * <p>
 * For each MBean of your project, create the mapping file in the <u>same package as the MBean</u>. For example:
 * </p>
 * <code>
 *        &lt;?xml version="1.0" encoding="utf-8"?&gt;<br>
 *        &lt;snmpAdaptor4j-mapping<br>
 *        &nbsp;&nbsp;&nbsp;&nbsp;xmlns="http://www.sf.net/snmpAdaptor4j/mapping/1.1"<br>
 *        &nbsp;&nbsp;&nbsp;&nbsp;xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"<br>
 *        &nbsp;&nbsp;&nbsp;&nbsp;xsi:schemaLocation="http://www.sf.net/snmpAdaptor4j/mapping/1.1<br>
 *        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;http://snmpAdaptor4j.sourceforge.net/xsd/snmpAdaptor4j-mapping-1.1.xsd"&gt;<br><br>
 *        &nbsp;&nbsp;&lt;attributes&gt;<br>
 *        &nbsp;&nbsp;&nbsp;&nbsp;&lt;attribute name="attributA" type="integer32" node="1" writable="true"/&gt;<br>
 *        &nbsp;&nbsp;&nbsp;&nbsp;&lt;attribute name="attributB" type="counter64" node="2" writable="false"/&gt;<br>
 *        &nbsp;&nbsp;&nbsp;&nbsp;&lt;attribute name="attributC" type="gauge32" node="3" writable="false"/&gt;<br>
 *        &nbsp;&nbsp;&lt;/attributes&gt;<br><br>
 *        &lt;/snmpAdaptor4j-mapping&gt;
 * 	  </code>
 * <p>
 * Here, we map only MBean attributes by indicating the SNMP data type and the node constituting the OID (Object IDentifier). The mapping of MBean instances, is
 * generally put in the configuration file (see next chapter).
 * </p>
 * <h4>Building of the configuration file</h4>
 * <p>
 * Create the configuration file (one by application) like this:
 * </p>
 * <code>
 *        &lt;?xml version="1.0" encoding="utf-8"?&gt;<br>
 *        &lt;snmpAdaptor4j-config<br>
 *        &nbsp;&nbsp;&nbsp;&nbsp;xmlns="http://www.sf.net/snmpAdaptor4j/config/1.1"<br>
 *        &nbsp;&nbsp;&nbsp;&nbsp;xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"<br>
 *        &nbsp;&nbsp;&nbsp;&nbsp;xsi:schemaLocation="http://www.sf.net/snmpAdaptor4j/config/1.1<br>
 *        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;http://snmpAdaptor4j.sourceforge.net/xsd/snmpAdaptor4j-config-1.1.xsd"&gt;<br><br>
 *        &nbsp;&nbsp;&lt;daemon address="127.0.0.1" port="161" version="2"&gt;<br>
 *        &nbsp;&nbsp;&nbsp;&nbsp;&lt;readCommunity&gt;public&lt;/readCommunity&gt;<br>
 *        &nbsp;&nbsp;&nbsp;&nbsp;&lt;writeCommunity&gt;private&lt;/writeCommunity&gt;<br>
 *        &nbsp;&nbsp;&lt;/daemon&gt;<br><br>
 *        &nbsp;&nbsp;&lt;roots default="1.3.6.1.4.1.99.12.8.1"/&gt;<br><br>
 *        &nbsp;&nbsp;&lt;mbeans&gt;<br>
 *        &nbsp;&nbsp;&nbsp;&nbsp;&lt;mbean name="fr.mydomain.myAppli:type=MyClass,name=objA" oid="1.1"/&gt;<br>
 *        &nbsp;&nbsp;&nbsp;&nbsp;&lt;mbean name="fr.mydomain.myAppli:type=MyClass,name=objB" oid="1.2"/&gt;<br>
 *        &nbsp;&nbsp;&lt;/mbeans&gt;<br><br>
 *        &lt;/snmpAdaptor4j-config&gt;
 * 	  </code>
 * <p>
 * Here, we define the OID for each instance of MBeans. The OID of an attribute, is built as follows:
 * </p>
 * <code>
 * 	  ${root}.${mbean oid}.${attribute node}.0
 * 	  </code>
 * <p>
 * For example, the <i>attributB</i> OID of first MBean will be <b>1.3.6.1.4.1.99.12.8.1.1.1.2.0</b>.
 * </p>
 * <h4>Registration and start of adaptor</h4>
 * <p>
 * Insert the following code to install the adapter inside JMX:
 * </p>
 * <code>
 *      URL url = getClass().getResource("/snmp.xml");<br>
 *      SnmpAdaptor adaptor = new SnmpAdaptor(url, true);<br>
 *      ObjectName name = new ObjectName("net.sf.snmpadaptor4j:adaptor=SnmpAdaptor");<br>
 *      ManagementFactory.getPlatformMBeanServer().registerMBean(adaptor, name);
 * 	  </code>
 * <p>
 * <i>snmp.xml</i> is the configuration file previously built.
 * </p>
 * <p>
 * Update the information about your system:
 * </p>
 * <code>
 *      adaptor.getSystemInfo().setSysName("MyApp");<br>
 *      adaptor.getSystemInfo().setSysDescr("This is a java application");<br>
 *      adaptor.getSystemInfo().setSysLocation("Web server at Montpellier (France)");<br>
 *      adaptor.getSystemInfo().setSysContact("root@mydomain.fr");
 * </code>
 * <p>
 * Start the adaptor:
 * </p>
 * <code>
 *      snmpAdaptor.start();
 * </code> <h4>Test</h4>
 * <p>
 * Install the <a href="http://www.net-snmp.org">Net-SNMP</a> commands and type:
 * </p>
 * <code>
 * 	  snmpwalk -v2c -Os -c public 127.0.0.1 .1
 * 	  </code>
 * <p>
 * You should see the attribute values of your MBean.
 * </p>
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public class SnmpAdaptor
		extends StandardMBean
		implements SnmpConfiguration, SnmpAppContext, SnmpAdaptorMBean {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(SnmpAdaptor.class);

	/**
	 * SNMP daemon.
	 */
	private final SnmpDaemon daemon;

	/**
	 * Informations on the system.
	 */
	private final SystemInfo systemInfo;

	/**
	 * Listening IP address of SNMP daemon (127.0.0.1 by default).
	 */
	private String listenerAddress = null;

	/**
	 * UDP port of SNMP daemon (161 by default).
	 */
	private Integer listenerPort = null;

	/**
	 * Protocol version of SNMP daemon (SNMP v2 by default).
	 */
	private Integer listenerSnmpVersion = null;

	/**
	 * Read community of SNMP daemon ("public" by default).
	 */
	private String listenerReadCommunity = null;

	/**
	 * Write community of SNMP daemon ("private" by default).
	 */
	private String listenerWriteCommunity = null;

	/**
	 * List of managers where to send all notifications (SNMP traps).
	 */
	private final List<SnmpManagerConfiguration> managerList = new ArrayList<SnmpManagerConfiguration>();

	/**
	 * Default root OID containing the attributes of the application.
	 */
	private final String defaultRootOid;

	/**
	 * Map of root OIDs where the attributes of the application will stay.
	 */
	private final Map<String, String> rootOidMap;

	/**
	 * Map of MBean OIDs.
	 */
	private final Map<ObjectName, String> mBeanOidMap;

	/**
	 * Application context map.
	 */
	protected final Map<ClassLoader, SnmpAppContext> appContextMap = new HashMap<ClassLoader, SnmpAppContext>();

	/**
	 * SNMP managers on the network to which SNMP traps should be sent.
	 */
	private final SnmpManagers snmpManagers;

	/**
	 * <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB) for system attributes.
	 */
	private final SystemSnmpMib systemMib;

	/**
	 * <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB) for attributes of JVM.
	 */
	private final JvmSnmpMib jvmMib;

	/**
	 * Manager of JMX notifications.
	 */
	private final JmxNotificationManager jmxNotificationManager;

	/**
	 * Object designed to respond to each registration or deregistration of MBeans.
	 */
	private final JmxListener jmxListener;

	/**
	 * Constructor with a minimum of parameters.
	 * <p>
	 * Set <code>TRUE</code> to <code>classLoaderScope</code> for web applications: this avoids to publish MBeans of other applications of application server.
	 * </p>
	 * @param classLoaderScope <code>TRUE</code> for handle only MBeans created by the same {@link ClassLoader} that the SNMP adapter. <code>FALSE</code> for handle
	 *            all MBeans of the JVM.
	 * @throws Exception Exception if an error has occurred.
	 */
	public SnmpAdaptor (final boolean classLoaderScope, final String urlBeanSnmpConfig) throws Exception {
		this(null, null, null, classLoaderScope,urlBeanSnmpConfig);
	}

	/**
	 * Constructor with an application context.
	 * <p>
	 * Set <code>TRUE</code> to <code>classLoaderScope</code> for web applications: this avoids to publish MBeans of other applications of application server.
	 * </p>
	 * @param appContext Application context.
	 * @param classLoaderScope <code>TRUE</code> for handle only MBeans created by the same {@link ClassLoader} that the SNMP adapter. <code>FALSE</code> for handle
	 *            all MBeans of the JVM.
	 * @throws Exception Exception if an error has occurred.
	 */
	public SnmpAdaptor (final SnmpAppContext appContext, final boolean classLoaderScope,final String urlBeanSnmpConfig) throws Exception {
		this(null, appContext, null, classLoaderScope, urlBeanSnmpConfig);
	}

	/**
	 * Constructor with an {@link URL} to SNMP configuration file (XML).
	 * <p>
	 * Set <code>TRUE</code> to <code>classLoaderScope</code> for web applications: this avoids to publish MBeans of other applications of application server.
	 * </p>
	 * @param url {@link URL} of SNMP configuration file (XML).
	 * @param classLoaderScope <code>TRUE</code> for handle only MBeans created by the same {@link ClassLoader} that the SNMP adapter. <code>FALSE</code> for handle
	 *            all MBeans of the JVM.
	 * @throws Exception Exception if an error occurred.
	 */
	public SnmpAdaptor (final URL url, final boolean classLoaderScope, final String urlBeanSnmpConfig) throws Exception {
		this(url, null, classLoaderScope,urlBeanSnmpConfig);
	}

	/**
	 * Constructor with an {@link URL} to SNMP configuration file (XML) and a {@link SystemInfo}.
	 * <p>
	 * Set <code>TRUE</code> to <code>classLoaderScope</code> for web applications: this avoids to publish MBeans of other applications of application server.
	 * </p>
	 * @param url {@link URL} of SNMP configuration file (XML).
	 * @param systemInfo Informations on the system.
	 * @param classLoaderScope <code>TRUE</code> for handle only MBeans created by the same {@link ClassLoader} that the SNMP adapter. <code>FALSE</code> for handle
	 *            all MBeans of the JVM.
	 * @throws Exception Exception if an error occurred.
	 */
	public SnmpAdaptor (final URL url, final SystemInfo systemInfo, final boolean classLoaderScope,final String urlBeanSnmpConfig) throws Exception {
		this(XmlConfigParser.newInstance(url), systemInfo, classLoaderScope,urlBeanSnmpConfig);
	}

	/**
	 * Hidden constructor.
	 * <p>
	 * Set <code>TRUE</code> to <code>classLoaderScope</code> for web applications: this avoids to publish MBeans of other applications of application server.
	 * </p>
	 * @param xmlConfigParser Parser of SNMP configuration file (XML).
	 * @param systemInfo Informations on the system.
	 * @param classLoaderScope <code>TRUE</code> for handle only MBeans created by the same {@link ClassLoader} that the SNMP adapter. <code>FALSE</code> for handle
	 *            all MBeans of the JVM.
	 * @throws Exception Exception if an error has occurred.
	 */
	private SnmpAdaptor (final XmlConfigParser xmlConfigParser, final SystemInfo systemInfo, final boolean classLoaderScope,final String urlBeanSnmpConfig) throws Exception {
		this(xmlConfigParser, xmlConfigParser, systemInfo, classLoaderScope,urlBeanSnmpConfig);
	}

	/**
	 * Constructor with all parameters.
	 * <p>
	 * Set <code>TRUE</code> to <code>classLoaderScope</code> for web applications: this avoids to publish MBeans of other applications of application server.
	 * </p>
	 * @param configuration SNMP configuration settings.
	 * @param appContext Application context.
	 * @param systemInfo Informations on the system.
	 * @param classLoaderScope <code>TRUE</code> for handle only MBeans created by the same {@link ClassLoader} that the SNMP adapter. <code>FALSE</code> for handle
	 *            all MBeans of the JVM.
	 * @throws Exception Exception if an error has occurred.
	 */
	public SnmpAdaptor (final SnmpConfiguration configuration, final SnmpAppContext appContext, final SystemInfo systemInfo, final boolean classLoaderScope,final String urlBeanSnmpConfig)
			throws Exception {
		super(SnmpAdaptorMBean.class);
		initializeMBeanDescriptions();
		this.defaultRootOid = ((appContext != null) && (appContext.getDefaultRootOid() != null) ? appContext.getDefaultRootOid() : "1.3.6.1.4.1.99.12.8.1");
		this.rootOidMap = Collections.unmodifiableMap(appContext != null ? appContext.getRootOidMap() : new HashMap<String, String>());
		this.mBeanOidMap = Collections.unmodifiableMap(appContext != null ? appContext.getMBeanOidMap() : new HashMap<ObjectName, String>());
		this.systemInfo = (systemInfo != null ? systemInfo : new SystemInfo());
		this.systemMib = new SystemSnmpMib(this, this.appContextMap, this.systemInfo);
		this.jvmMib = new JvmSnmpMib(this.systemMib);
		final JmxSnmpMib jmxSnmpMib = new JmxSnmpMib(this.systemMib);
		final SnmpApiFactory apiFactory = new OpennmsSnmpApiFactory();
		this.snmpManagers = new SnmpManagers(apiFactory);
		this.jmxNotificationManager = new JmxNotificationManager(this.snmpManagers, this.systemInfo);
		this.jmxListener = new JmxListener(jmxSnmpMib, this.jmxNotificationManager, this, this.appContextMap, classLoaderScope,urlBeanSnmpConfig);
		this.daemon = apiFactory.newSnmpDaemon(this, jmxSnmpMib);
		setListenerAddress(configuration != null ? configuration.getListenerAddress() : null);
		setListenerPort(configuration != null ? configuration.getListenerPort() : null);
		setListenerSnmpVersion(configuration != null ? configuration.getListenerSnmpVersion() : null);
		setListenerReadCommunity(configuration != null ? configuration.getListenerReadCommunity() : null);
		setListenerWriteCommunity(configuration != null ? configuration.getListenerWriteCommunity() : null);
		if ((configuration != null) && (configuration.getManagerList() != null)) {
			this.managerList.addAll(configuration.getManagerList());
			this.snmpManagers.initialize(this.managerList);
		}
	}

	/**
	 * Hidden constructor (used for tests only).
	 * <p>
	 * Set <code>TRUE</code> to <code>classLoaderScope</code> for web applications: this avoids to publish MBeans of other applications of application server.
	 * </p>
	 * @param defaultRootOid Default root OID containing the attributes of the application.
	 * @param configuration SNMP configuration settings.
	 * @param classLoaderScope <code>TRUE</code> for handle only MBeans created by the same {@link ClassLoader} that the SNMP adapter. <code>FALSE</code> for handle
	 *            all MBeans of the JVM.
	 * @param systemMib <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB) for system attributes.
	 * @param jvmMib <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB) for attributes of JVM.
	 * @param jmxListener Object designed to respond to each registration or deregistration of MBeans.
	 * @param jmxNotificationManager Manager of JMX notifications.
	 * @param daemon SNMP daemon.
	 * @throws Exception Exception if an error has occurred.
	 */
	protected SnmpAdaptor (final String defaultRootOid, final SnmpConfiguration configuration, final boolean classLoaderScope, final SystemSnmpMib systemMib,
			final JvmSnmpMib jvmMib, final JmxListener jmxListener, final JmxNotificationManager jmxNotificationManager, final SnmpDaemon daemon) throws Exception {
		super(SnmpAdaptorMBean.class);
		initializeMBeanDescriptions();
		this.defaultRootOid = defaultRootOid;
		this.rootOidMap = Collections.unmodifiableMap(new HashMap<String, String>());
		this.mBeanOidMap = Collections.unmodifiableMap(new HashMap<ObjectName, String>());
		this.systemInfo = new SystemInfo();
		this.systemMib = systemMib;
		this.jvmMib = jvmMib;
		this.snmpManagers = null;
		this.jmxNotificationManager = jmxNotificationManager;
		this.jmxListener = jmxListener;
		this.daemon = daemon;
		setListenerAddress(configuration != null ? configuration.getListenerAddress() : null);
		setListenerPort(configuration != null ? configuration.getListenerPort() : null);
		setListenerSnmpVersion(configuration != null ? configuration.getListenerSnmpVersion() : null);
		setListenerReadCommunity(configuration != null ? configuration.getListenerReadCommunity() : null);
		setListenerWriteCommunity(configuration != null ? configuration.getListenerWriteCommunity() : null);
		if ((configuration != null) && (configuration.getManagerList() != null)) {
			this.managerList.addAll(configuration.getManagerList());
		}
	}

	/**
	 * Initializes all descriptions of MBean and its members.
	 */
	private void initializeMBeanDescriptions () {

		// Descriptions
		final String classDescription = "JMX adaptor for SNMP protocol";
		final String listenerAddressAttributeDescription = "Listening IP address of SNMP daemon";
		final String listenerPortAttributeDescription = "Listening UDP port of SNMP daemon";
		final String listenerSnmpVersionAttributeDescription = "Protocol version of SNMP daemon (1 or 2)";
		final String listenerReadCommunityAttributeDescription = "Read community of SNMP daemon";
		final String listenerWriteCommunityAttributeDescription = "Write community of SNMP daemon";
		final String managerListAttributeDescription = "List of managers where to send all notifications (SNMP traps)";
		final String startedAttributeDescription = "TRUE if the SNMP daemon is started";
		final String addAppContextDescription = "Adds a new application context";
		final String removeAppContextDescription = "Removes an application context";
		final String startDescription = "Starts the SNMP daemon";
		final String stopDescription = "Stops the SNMP daemon";
		final String classLoaderParameterDescription = "Class loader of application";
		final String appContextParameterDescription = "Application context (any class that implements the interface " + SnmpAppContext.class.getName() + ")";
		final String urlParameterDescription = "URL of SNMP configuration file (XML)";
		final String configurationParameterDescription = "SNMP configuration settings (any class that implements the interface " + SnmpConfiguration.class.getName()
				+ ")";
		final String systemInfoParameterDescription = "Informations on the Java application";
		final String classLoaderScopeParameterDescription = "TRUE for handle only MBeans created by the same ClassLoader that the SNMP adapter - FALSE for handle all MBeans of the JVM (set TRUE to classLoaderScope for web applications: this avoids to publish MBeans of other applications of application server)";

		// Parameter info
		final MBeanParameterInfo classLoaderParameter = new MBeanParameterInfo("classLoader", ClassLoader.class.getName(), classLoaderParameterDescription);

		final MBeanParameterInfo appContextParameter = new MBeanParameterInfo("appContext", SnmpAppContext.class.getName(), appContextParameterDescription);
		final MBeanParameterInfo urlParameter = new MBeanParameterInfo("url", URL.class.getName(), urlParameterDescription);
		final MBeanParameterInfo configurationParameter = new MBeanParameterInfo("configuration", SnmpConfiguration.class.getName(),
				configurationParameterDescription);
		final MBeanParameterInfo systemInfoParameter = new MBeanParameterInfo("systemInfo", SystemInfo.class.getName(), systemInfoParameterDescription);
		final MBeanParameterInfo classLoaderScopeParameter = new MBeanParameterInfo("classLoaderScope", boolean.class.getName(),
				classLoaderScopeParameterDescription);

		// Operation info
		final MBeanOperationInfo addAppContextOperation = new MBeanOperationInfo("addAppContext", addAppContextDescription, new MBeanParameterInfo[] {
				classLoaderParameter, appContextParameter }, void.class.getName(), MBeanOperationInfo.ACTION);
		final MBeanOperationInfo removeAppContextOperation = new MBeanOperationInfo("removeAppContext", removeAppContextDescription,
				new MBeanParameterInfo[] { classLoaderParameter }, void.class.getName(), MBeanOperationInfo.ACTION);
		final MBeanOperationInfo startOperation = new MBeanOperationInfo("start", startDescription, new MBeanParameterInfo[] {}, void.class.getName(),
				MBeanOperationInfo.ACTION);
		final MBeanOperationInfo stopOperation = new MBeanOperationInfo("stop", stopDescription, new MBeanParameterInfo[] {}, void.class.getName(),
				MBeanOperationInfo.ACTION);
		final MBeanOperationInfo[] operations = new MBeanOperationInfo[] { addAppContextOperation, removeAppContextOperation, startOperation, stopOperation };

		// Constructor info
		final MBeanConstructorInfo constructor1 = new MBeanConstructorInfo(getClass().getName(), "Constructor with only classLoaderScope parameter",
				new MBeanParameterInfo[] { classLoaderScopeParameter });
		final MBeanConstructorInfo constructor2 = new MBeanConstructorInfo(getClass().getName(), "Constructor with only appContext and classLoaderScope parameters",
				new MBeanParameterInfo[] { appContextParameter, classLoaderScopeParameter });
		final MBeanConstructorInfo constructor3 = new MBeanConstructorInfo(getClass().getName(), "Constructor with only url and classLoaderScope parameters",
				new MBeanParameterInfo[] { urlParameter, classLoaderScopeParameter });
		final MBeanConstructorInfo constructor4 = new MBeanConstructorInfo(getClass().getName(),
				"Constructor with only url, systemInfo and classLoaderScope parameters", new MBeanParameterInfo[] { urlParameter, systemInfoParameter,
						classLoaderScopeParameter });
		final MBeanConstructorInfo constructor5 = new MBeanConstructorInfo(getClass().getName(),
				"Constructor with configuration, appContext, systemInfo and classLoaderScope parameters", new MBeanParameterInfo[] { configurationParameter,
						appContextParameter, systemInfoParameter, classLoaderScopeParameter });
		final MBeanConstructorInfo[] constructors = new MBeanConstructorInfo[] { constructor1, constructor2, constructor3, constructor4, constructor5 };

		// Attribute info
		final MBeanAttributeInfo listenerAddressAttribute = new MBeanAttributeInfo("ListenerAddress", String.class.getName(), listenerAddressAttributeDescription,
				true, true, false);
		final MBeanAttributeInfo listenerPortAttribute = new MBeanAttributeInfo("ListenerPort", Integer.class.getName(), listenerPortAttributeDescription, true,
				true, false);
		final MBeanAttributeInfo listenerSnmpVersionAttribute = new MBeanAttributeInfo("ListenerSnmpVersion", Integer.class.getName(),
				listenerSnmpVersionAttributeDescription, true, true, false);
		final MBeanAttributeInfo listenerReadCommunityAttribute = new MBeanAttributeInfo("ListenerReadCommunity", String.class.getName(),
				listenerReadCommunityAttributeDescription, true, true, false);
		final MBeanAttributeInfo listenerWriteCommunityAttribute = new MBeanAttributeInfo("ListenerWriteCommunity", String.class.getName(),
				listenerWriteCommunityAttributeDescription, true, true, false);
		final MBeanAttributeInfo managerListAttribute = new MBeanAttributeInfo("ManagerList", List.class.getName(), managerListAttributeDescription, true, true,
				false);
		final MBeanAttributeInfo startedAttribute = new MBeanAttributeInfo("Started", boolean.class.getName(), startedAttributeDescription, true, false, true);
		final MBeanAttributeInfo[] attributes = new MBeanAttributeInfo[] { listenerAddressAttribute, listenerPortAttribute, listenerSnmpVersionAttribute,
				listenerReadCommunityAttribute, listenerWriteCommunityAttribute, managerListAttribute, startedAttribute };

		// MBeanInfo
		final MBeanInfo mBeanInfo = new MBeanInfo(getClass().getName(), classDescription, attributes, constructors, operations, new MBeanNotificationInfo[] {},
				getMBeanInfo().getDescriptor());
		cacheMBeanInfo(mBeanInfo);

	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpAdaptorMBean#addAppContext(java.lang.ClassLoader, net.sf.snmpadaptor4j.SnmpAppContext)
	 */
	public void addAppContext (final ClassLoader classLoader, final SnmpAppContext appContext) {
		synchronized (this.appContextMap) {
			this.appContextMap.put(classLoader, appContext);
			this.systemMib.initSysObjectIDSet();
		}
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpAdaptorMBean#removeAppContext(java.lang.ClassLoader)
	 */
	public void removeAppContext (final ClassLoader classLoader) {
		synchronized (this.appContextMap) {
			this.appContextMap.remove(classLoader);
			this.systemMib.initSysObjectIDSet();
		}
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerAddress()
	 */
	public final String getListenerAddress () {
		return this.listenerAddress;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpAdaptorMBean#setListenerAddress(java.lang.String)
	 */
	public final void setListenerAddress (final String listenerAddress) {
		String newListenerAddress = formatAttribute(listenerAddress);
		if (newListenerAddress == null) {
			newListenerAddress = "localhost";
		}
		if (this.listenerAddress != null ? !this.listenerAddress.equals(newListenerAddress) : true) {
			try {
				this.daemon.stop();
				this.jmxNotificationManager.setEnabled(false);
				this.listenerAddress = newListenerAddress;
			}
			catch (final Throwable e) {
				this.logger.error(e);
			}
		}
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerPort()
	 */
	public final Integer getListenerPort () {
		return this.listenerPort;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpAdaptorMBean#setListenerPort(java.lang.Integer)
	 */
	public final void setListenerPort (final Integer listenerPort) {
		final Integer newListenerPort = (listenerPort != null ? listenerPort : new Integer(161));
		if (this.listenerPort != null ? !this.listenerPort.equals(newListenerPort) : true) {
			try {
				this.daemon.stop();
				this.jmxNotificationManager.setEnabled(false);
				this.listenerPort = newListenerPort;
			}
			catch (final Throwable e) {
				this.logger.error(e);
			}
		}
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerSnmpVersion()
	 */
	public final Integer getListenerSnmpVersion () {
		return this.listenerSnmpVersion;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpAdaptorMBean#setListenerSnmpVersion(java.lang.Integer)
	 */
	public final void setListenerSnmpVersion (final Integer listenerSnmpVersion) {
		Integer newListenerSnmpVersion;
		if ((listenerSnmpVersion == null) || (listenerSnmpVersion.intValue() < 1) || (listenerSnmpVersion.intValue() > 2)) {
			newListenerSnmpVersion = new Integer(2);
		}
		else {
			newListenerSnmpVersion = listenerSnmpVersion;
		}
		if (this.listenerSnmpVersion != null ? !this.listenerSnmpVersion.equals(newListenerSnmpVersion) : true) {
			try {
				this.daemon.stop();
				this.jmxNotificationManager.setEnabled(false);
				this.listenerSnmpVersion = newListenerSnmpVersion;
			}
			catch (final Throwable e) {
				this.logger.error(e);
			}
		}
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerReadCommunity()
	 */
	public final String getListenerReadCommunity () {
		return this.listenerReadCommunity;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpAdaptorMBean#setListenerReadCommunity(java.lang.String)
	 */
	public final void setListenerReadCommunity (final String listenerReadCommunity) {
		String newListenerReadCommunity = formatAttribute(listenerReadCommunity);
		if (newListenerReadCommunity == null) {
			newListenerReadCommunity = "public";
		}
		if (this.listenerReadCommunity != null ? !this.listenerReadCommunity.equals(newListenerReadCommunity) : true) {
			try {
				this.daemon.stop();
				this.jmxNotificationManager.setEnabled(false);
				this.listenerReadCommunity = newListenerReadCommunity;
			}
			catch (final Throwable e) {
				this.logger.error(e);
			}
		}
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerWriteCommunity()
	 */
	public final String getListenerWriteCommunity () {
		return this.listenerWriteCommunity;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpAdaptorMBean#setListenerWriteCommunity(java.lang.String)
	 */
	public final void setListenerWriteCommunity (final String listenerWriteCommunity) {
		String newListenerWriteCommunity = formatAttribute(listenerWriteCommunity);
		if (newListenerWriteCommunity == null) {
			newListenerWriteCommunity = "private";
		}
		if (this.listenerWriteCommunity != null ? !this.listenerWriteCommunity.equals(newListenerWriteCommunity) : true) {
			try {
				this.daemon.stop();
				this.jmxNotificationManager.setEnabled(false);
				this.listenerWriteCommunity = newListenerWriteCommunity;
			}
			catch (final Throwable e) {
				this.logger.error(e);
			}
		}
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpConfiguration#getManagerList()
	 */
	public final List<SnmpManagerConfiguration> getManagerList () {
		return Collections.unmodifiableList(this.managerList);
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpAppContext#getDefaultRootOid()
	 */
	public final String getDefaultRootOid () {
		return this.defaultRootOid;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpAppContext#getRootOidMap()
	 */
	public final Map<String, String> getRootOidMap () {
		return this.rootOidMap;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpAppContext#getMBeanOidMap()
	 */
	public final Map<ObjectName, String> getMBeanOidMap () {
		return this.mBeanOidMap;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpAdaptorMBean#start()
	 */
	public void start () throws Exception {
		this.daemon.start();
		if (!this.daemon.isStarted()) {
			throw new Exception("The SNMP daemon did not start");
		}
		this.jmxNotificationManager.setEnabled(true);
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpAdaptorMBean#stop()
	 */
	public void stop () throws Exception {
		this.daemon.stop();
		if (this.daemon.isStarted()) {
			throw new Exception("The SNMP daemon has not been stopped");
		}
		this.jmxNotificationManager.setEnabled(false);
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpAdaptorMBean#isStarted()
	 */
	public final boolean isStarted () {
		return this.daemon.isStarted();
	}

	/**
	 * Returns the informations on the system.
	 * <p>
	 * The returned object is a MBean. It is possible to register it in JMX.
	 * </p>
	 * @return Informations on the system.
	 */
	public final SystemInfo getSystemInfo () {
		return this.systemInfo;
	}

	/**
	 * Formats an attribute value.
	 * @param value Value to format.
	 * @return Value formatted.
	 */
	protected static final String formatAttribute (final String value) {
		String result = value;
		if (result != null) {
			result = result.trim();
			if (result.length() == 0) {
				result = null;
			}
		}
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see javax.management.StandardMBean#preRegister(javax.management.MBeanServer, javax.management.ObjectName)
	 */
	@Override
	public ObjectName preRegister (final MBeanServer server, final ObjectName name) throws Exception {
		final ObjectName result = super.preRegister(server, name);
		this.jmxListener.open(server);
		this.jvmMib.open(server);
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see javax.management.StandardMBean#preDeregister()
	 */
	@Override
	public void preDeregister () throws Exception {
		super.preDeregister();
		this.jmxListener.close();
		this.jvmMib.close();
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "SnmpAdaptor[" + this.listenerAddress + ":" + this.listenerPort + "]";
	}

}