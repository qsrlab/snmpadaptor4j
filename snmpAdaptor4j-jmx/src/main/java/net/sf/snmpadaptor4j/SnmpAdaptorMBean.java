package net.sf.snmpadaptor4j;

import java.util.List;

/**
 * JMX adaptor for SNMP protocol.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public interface SnmpAdaptorMBean {

	/**
	 * Adds a new application context.
	 * <p>
	 * SnmpAdaptor4j may be used by an application server. In this case you will inform the SNMP adapter to each deployment of a new application.
	 * </p>
	 * @param classLoader Class loader of application.
	 * @param appContext Application context.
	 */
	void addAppContext (ClassLoader classLoader, SnmpAppContext appContext);

	/**
	 * Removes an application context.
	 * @param classLoader Class loader of application.
	 */
	void removeAppContext (final ClassLoader classLoader);

	/**
	 * Returns the listening IP address of SNMP daemon (127.0.0.1 by default).
	 * @return Listening IP address.
	 */
	String getListenerAddress ();

	/**
	 * Sets the listening IP address of SNMP daemon.
	 * @param listenerAddress Listening IP address.
	 */
	void setListenerAddress (String listenerAddress);

	/**
	 * Returns the UDP port of SNMP daemon (161 by default).
	 * @return UDP port.
	 */
	Integer getListenerPort ();

	/**
	 * Sets the UDP port of SNMP daemon.
	 * @param listenerPort UDP port.
	 */
	void setListenerPort (Integer listenerPort);

	/**
	 * Returns the protocol version of SNMP daemon (SNMP v2 by default).
	 * @return SNMP protocol version.
	 */
	Integer getListenerSnmpVersion ();

	/**
	 * Sets the protocol version of SNMP daemon.
	 * @param listenerSnmpVersion SNMP protocol version.
	 */
	void setListenerSnmpVersion (Integer listenerSnmpVersion);

	/**
	 * Returns the read community of SNMP daemon ("public" by default).
	 * @return Read community of SNMP daemon.
	 */
	String getListenerReadCommunity ();

	/**
	 * Sets the read community of SNMP daemon.
	 * @param listenerReadCommunity Read community of SNMP daemon.
	 */
	void setListenerReadCommunity (String listenerReadCommunity);

	/**
	 * Returns the write community of SNMP daemon ("private" by default).
	 * @return Write community of SNMP daemon.
	 */
	String getListenerWriteCommunity ();

	/**
	 * Sets the write community of SNMP daemon.
	 * @param listenerWriteCommunity Write community of SNMP daemon.
	 */
	void setListenerWriteCommunity (String listenerWriteCommunity);

	/**
	 * Returns the list of managers where to send all notifications (SNMP traps).
	 * @return List of managers.
	 */
	List<SnmpManagerConfiguration> getManagerList ();

	/**
	 * Starts the SNMP daemon.
	 * @throws Exception Exception if an error has occurred.
	 */
	void start () throws Exception;

	/**
	 * Stops the SNMP daemon.
	 * @throws Exception Exception if an error has occurred.
	 */
	void stop () throws Exception;

	/**
	 * Returns <code>TRUE</code> if the SNMP daemon is started.
	 * @return <code>TRUE</code> if the SNMP daemon is started.
	 */
	boolean isStarted ();

}