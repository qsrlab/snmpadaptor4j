package net.sf.snmpadaptor4j;

import java.io.Serializable;

/**
 * Object containing all parameters to connect to a SNMP manager for trap sending.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SnmpManagerConfiguration
		implements Serializable {

	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = 923122651757199428L;

	/**
	 * IP address of SNMP manager responsible of traps handling.
	 */
	private final String address;

	/**
	 * UDP port of SNMP manager responsible of traps handling.
	 */
	private final int port;

	/**
	 * Protocol version of SNMP manager.
	 */
	private final int version;

	/**
	 * Community of SNMP manager.
	 */
	private final String community;

	/**
	 * Constructor.
	 * @param address IP address of SNMP manager responsible of traps handling.
	 * @param port UDP port of SNMP manager responsible of traps handling.
	 * @param version Protocol version of SNMP manager.
	 * @param community Community of SNMP manager.
	 */
	public SnmpManagerConfiguration (final String address, final int port, final int version, final String community) {
		super();
		this.address = address;
		this.port = port;
		this.version = version;
		this.community = community;
	}

	/**
	 * Returns the IP address of SNMP manager responsible of traps handling.
	 * @return IP address of SNMP manager.
	 */
	public String getAddress () {
		return this.address;
	}

	/**
	 * Returns the UDP port of SNMP manager responsible of traps handling.
	 * @return UDP port of SNMP manager.
	 */
	public int getPort () {
		return this.port;
	}

	/**
	 * Returns the protocol version of SNMP manager.
	 * @return Protocol version of SNMP manager.
	 */
	public int getVersion () {
		return this.version;
	}

	/**
	 * Returns the community of SNMP manager.
	 * @return Community of SNMP manager.
	 */
	public String getCommunity () {
		return this.community;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.address == null) ? 0 : this.address.hashCode());
		result = prime * result + ((this.community == null) ? 0 : this.community.hashCode());
		result = prime * result + this.port;
		result = prime * result + this.version;
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean result = false;
		if (obj == this) {
			result = true;
		}
		else if ((obj != null) && (getClass().equals(obj.getClass()))) {
			final SnmpManagerConfiguration other = (SnmpManagerConfiguration) obj;
			result = (this.address != null ? this.address.equals(other.address) : (other.address == null));
			if (result) {
				result = (this.port == other.port);
			}
			if (result) {
				result = (this.version == other.version);
			}
			if (result) {
				result = (this.community != null ? this.community.equals(other.community) : (other.community == null));
			}
		}
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return this.address + ":" + this.port + "/" + this.community;
	}

}