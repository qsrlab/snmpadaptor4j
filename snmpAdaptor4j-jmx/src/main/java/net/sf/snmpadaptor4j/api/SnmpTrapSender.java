package net.sf.snmpadaptor4j.api;

import net.sf.snmpadaptor4j.object.SnmpTrap;

/**
 * Object responsible for sending of SNMP traps to one manager.
 * <p>
 * Classes implementing this interface encapsulate the SNMP API. To change the API, just create a new implementation of {@link SnmpTrapSender}.
 * </p>
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public interface SnmpTrapSender {

	/**
	 * Returns the name given to SNMP manager.
	 * @return Name given to SNMP manager.
	 */
	String getName ();

	/**
	 * Opens the connection to the SNMP manager.
	 * @throws Exception Exception if an error has occurred.
	 */
	void open () throws Exception;

	/**
	 * Closes the connection with the SNMP manager.
	 */
	void close ();

	/**
	 * Returns <code>TRUE</code> if the connection is established to the SNMP manager.
	 * @return <code>TRUE</code> if the connection is established.
	 */
	boolean isConnected ();

	/**
	 * Sends a SNMP trap to the manager.
	 * @param trap Object representing a specific or generic SNMP trap.
	 * @throws Exception Exception if an error has occurred.
	 */
	void send (SnmpTrap trap) throws Exception;

}