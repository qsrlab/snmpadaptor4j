package net.sf.snmpadaptor4j.api;

import org.apache.log4j.Level;

/**
 * SNMP exception.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SnmpException
		extends RuntimeException {

	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = -5077386573357989009L;

	/**
	 * Error status.
	 */
	private final int errorStatus;

	/**
	 * Logger level.
	 */
	private final Level loggerLevel;

	/**
	 * Constructor.
	 * @param errorStatus Error status.
	 */
	public SnmpException (final int errorStatus) {
		this(null, errorStatus, (Level) null);
	}

	/**
	 * Constructor.
	 * @param message Detailed message on error.
	 * @param errorStatus Error status.
	 * @param cause Error cause.
	 */
	public SnmpException (final String message, final int errorStatus, final Throwable cause) {
		super("error " + errorStatus + " (" + message + ")", cause);
		this.errorStatus = errorStatus;
		this.loggerLevel = Level.ERROR;
	}

	/**
	 * Constructor.
	 * @param message Detailed message on error.
	 * @param errorStatus Error status.
	 */
	public SnmpException (final String message, final int errorStatus) {
		this(message, errorStatus, (Level) null);
	}

	/**
	 * Constructor.
	 * @param message Detailed message on error.
	 * @param errorStatus Error status.
	 * @param loggerLevel Logger level.
	 */
	public SnmpException (final String message, final int errorStatus, final Level loggerLevel) {
		super("error " + errorStatus + (message != null ? " (" + message + ")" : ""));
		this.errorStatus = errorStatus;
		this.loggerLevel = (loggerLevel != null ? loggerLevel : Level.ERROR);
	}

	/**
	 * Returns the error status.
	 * @return Error status.
	 */
	public int getErrorStatus () {
		return this.errorStatus;
	}

	/**
	 * Returns the logger level.
	 * @return Logger level.
	 */
	public Level getLoggerLevel () {
		return this.loggerLevel;
	}

}