package net.sf.snmpadaptor4j.api.opennms;

import java.net.InetAddress;
import net.sf.snmpadaptor4j.api.SnmpApiFactory;
import net.sf.snmpadaptor4j.api.SnmpDaemon;
import net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration;
import net.sf.snmpadaptor4j.api.SnmpMib;
import net.sf.snmpadaptor4j.api.SnmpTrapSender;

/**
 * Factory of facades to <b>joesnmp</b> API.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class OpennmsSnmpApiFactory
		implements SnmpApiFactory {

	/**
	 * Constructor.
	 */
	public OpennmsSnmpApiFactory () {
		super();
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpApiFactory#newSnmpDaemon(net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration, net.sf.snmpadaptor4j.api.SnmpMib)
	 */
	public SnmpDaemon newSnmpDaemon (final SnmpDaemonConfiguration configuration, final SnmpMib snmpMib) {
		return new OpennmsSnmpDaemon(configuration, snmpMib);
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpApiFactory#newSnmpTrapSender(java.net.InetAddress, java.lang.String, int, int, java.lang.String)
	 */
	public SnmpTrapSender newSnmpTrapSender (final InetAddress agentAddress, final String managerAddress, final int managerPort, final int managerVersion,
			final String managerCommunity) {
		return new OpennmsSnmpTrapSender(agentAddress, managerAddress, managerPort, managerVersion, managerCommunity);
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "SnmpApiFactory";
	}

}