package net.sf.snmpadaptor4j.api;

import java.net.InetAddress;

/**
 * Factory of facades to SNMP API.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public interface SnmpApiFactory {

	/**
	 * Creates and returns a new instance of SNMP daemon.
	 * @param configuration Configuration settings of SNMP daemon.
	 * @param snmpMib SNMP <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 * @return New instance of SNMP daemon.
	 */
	SnmpDaemon newSnmpDaemon (SnmpDaemonConfiguration configuration, SnmpMib snmpMib);

	/**
	 * Creates and returns a new instance of SNMP trap sender.
	 * @param agentAddress IP address of SNMP agent (must be a local address of the host).
	 * @param managerAddress IP address of SNMP manager responsible of traps handling.
	 * @param managerPort UDP port of SNMP manager responsible of traps handling.
	 * @param managerVersion Protocol version of SNMP manager.
	 * @param managerCommunity Community of SNMP manager.
	 * @return New instance of SNMP trap sender.
	 */
	SnmpTrapSender newSnmpTrapSender (InetAddress agentAddress, String managerAddress, int managerPort, int managerVersion, String managerCommunity);

}