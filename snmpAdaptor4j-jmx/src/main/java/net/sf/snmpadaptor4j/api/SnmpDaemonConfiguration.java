package net.sf.snmpadaptor4j.api;

/**
 * Objects containing all configuration settings of SNMP daemon.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public interface SnmpDaemonConfiguration {

	/**
	 * Returns the listening IP address of SNMP daemon (127.0.0.1 by default).
	 * @return Listening IP address.
	 */
	String getListenerAddress ();

	/**
	 * Returns the UDP port of SNMP daemon (161 by default).
	 * @return UDP port.
	 */
	Integer getListenerPort ();

	/**
	 * Returns the protocol version of SNMP daemon (SNMP v2 by default).
	 * @return SNMP protocol version.
	 */
	Integer getListenerSnmpVersion ();

	/**
	 * Returns the read community of SNMP daemon ("public" by default).
	 * @return Read community of SNMP daemon.
	 */
	String getListenerReadCommunity ();

	/**
	 * Returns the write community of SNMP daemon ("private" by default).
	 * @return Write community of SNMP daemon.
	 */
	String getListenerWriteCommunity ();

}