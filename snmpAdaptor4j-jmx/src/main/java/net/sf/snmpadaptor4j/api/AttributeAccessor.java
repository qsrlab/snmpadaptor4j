package net.sf.snmpadaptor4j.api;

import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;

/**
 * Object representing an accessor to an attribute.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public interface AttributeAccessor {

	/**
	 * Returns the <b>O</b>bject <b>ID</b>entifier (OID) to locate the attribute in the MIB.
	 * @return Object Identifier to locate the attribute in the MIB.
	 */
	SnmpOid getOid ();

	/**
	 * Returns the SNMP data type of attribute.
	 * @return SNMP data type of attribute.
	 */
	SnmpDataType getSnmpDataType ();

	/**
	 * Returns the data type of JMX attribute.
	 * @return Data type of JMX attribute.
	 */
	Class<?> getJmxDataType ();

	/**
	 * Returns the value of attribute.
	 * @return Value of attribute.
	 * @throws Exception Exception if an error occurred.
	 */
	Object getValue () throws Exception;

	/**
	 * Sets the value of attribute.
	 * @param value New value of attribute.
	 * @throws Exception Exception if an error occurred.
	 */
	void setValue (Object value) throws Exception;

	/**
	 * Returns <code>TRUE</code> if the attribute can be read (for SNMP write and read community), <code>FALSE</code> otherwise.
	 * @return <code>TRUE</code> if the attribute can be read.
	 */
	boolean isReadable ();

	/**
	 * Returns <code>TRUE</code> if the attribute can be write (for SNMP write community), <code>FALSE</code> otherwise.
	 * @return <code>TRUE</code> if the attribute can be write.
	 */
	boolean isWritable ();

}