package net.sf.snmpadaptor4j.api.opennms;

import java.net.InetAddress;
import java.util.Iterator;
import java.util.Map.Entry;
import net.sf.snmpadaptor4j.api.SnmpDaemon;
import net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration;
import net.sf.snmpadaptor4j.api.SnmpException;
import net.sf.snmpadaptor4j.api.SnmpMib;
import net.sf.snmpadaptor4j.api.AttributeAccessor;
import net.sf.snmpadaptor4j.object.SnmpOid;
import org.apache.log4j.Level;
import org.opennms.protocols.snmp.SnmpAgentHandler;
import org.opennms.protocols.snmp.SnmpAgentSession;
import org.opennms.protocols.snmp.SnmpEndOfMibView;
import org.opennms.protocols.snmp.SnmpObjectId;
import org.opennms.protocols.snmp.SnmpOctetString;
import org.opennms.protocols.snmp.SnmpParameters;
import org.opennms.protocols.snmp.SnmpPduBulk;
import org.opennms.protocols.snmp.SnmpPduPacket;
import org.opennms.protocols.snmp.SnmpPduRequest;
import org.opennms.protocols.snmp.SnmpPeer;
import org.opennms.protocols.snmp.SnmpSMI;
import org.opennms.protocols.snmp.SnmpSyntax;
import org.opennms.protocols.snmp.SnmpVarBind;
import org.opennms.protocols.snmp.asn1.AsnEncodingException;

/**
 * SNMP daemon implementation for <b>joesnmp</b> API.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public class OpennmsSnmpDaemon
		extends OpennmsSupport
		implements SnmpDaemon, SnmpAgentHandler {

	/**
	 * Configuration settings of SNMP daemon.
	 */
	private final SnmpDaemonConfiguration configuration;

	/**
	 * SNMP <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 */
	protected final SnmpMib snmpMib;

	/**
	 * SNMP session.
	 */
	private SnmpAgentSession snmpSession = null;

	/**
	 * Hidden constructor.
	 * @param configuration Configuration settings of SNMP daemon.
	 * @param snmpMib SNMP <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 * @see OpennmsSnmpApiFactory#newSnmpDaemon(SnmpDaemonConfiguration, SnmpMib)
	 */
	OpennmsSnmpDaemon (final SnmpDaemonConfiguration configuration, final SnmpMib snmpMib) {
		super();
		this.configuration = configuration;
		this.snmpMib = snmpMib;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpDaemon#start()
	 */
	public final synchronized void start () throws Exception {
		if (this.snmpSession == null) {
			this.logger.trace("SNMP daemon starting...");
			this.logger.debug("SNMP daemon will listen on " + this.configuration.getListenerAddress() + ":" + this.configuration.getListenerPort().intValue());
			final SnmpPeer peer = new SnmpPeer(InetAddress.getByName(this.configuration.getListenerAddress()), this.configuration.getListenerPort().intValue());
			final SnmpParameters params = peer.getParameters();
			switch (this.configuration.getListenerSnmpVersion().intValue()) {
				case 1:
					params.setVersion(SnmpSMI.SNMPV1);
					break;
				default:
					params.setVersion(SnmpSMI.SNMPV2);
					break;
			}
			params.setReadCommunity(this.configuration.getListenerReadCommunity());
			params.setWriteCommunity(this.configuration.getListenerWriteCommunity());
			this.snmpSession = new SnmpAgentSession(this, peer);
			this.logger.info("SNMP daemon listen on " + this.configuration.getListenerAddress() + ":" + this.configuration.getListenerPort().intValue());
			this.logger.trace("SNMP daemon started");
		}
		else {
			this.logger.trace("SNMP daemon already started");
		}
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpDaemon#stop()
	 */
	public final synchronized void stop () {
		if (this.snmpSession != null) {
			this.logger.trace("SNMP daemon stopping...");
			this.snmpSession.close();
			this.snmpSession = null;
			this.logger.trace("SNMP daemon stopped");
		}
		else {
			this.logger.trace("SNMP daemon already stopped");
		}
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpDaemon#isStarted()
	 */
	public final boolean isStarted () {
		return (this.snmpSession != null);
	}

	/*
	 * {@inheritDoc}
	 * @see org.opennms.protocols.snmp.SnmpAgentHandler#SnmpAgentSessionError(org.opennms.protocols.snmp.SnmpAgentSession, int, java.lang.Object)
	 */
	public final void SnmpAgentSessionError (final SnmpAgentSession session, final int error, final Object ref) {
		throw new SnmpException(error);
	}

	/*
	 * {@inheritDoc}
	 * @see org.opennms.protocols.snmp.SnmpAgentHandler#snmpReceivedPdu(org.opennms.protocols.snmp.SnmpAgentSession, java.net.InetAddress, int,
	 * org.opennms.protocols.snmp.SnmpOctetString, org.opennms.protocols.snmp.SnmpPduPacket)
	 */
	public final void snmpReceivedPdu (final SnmpAgentSession session, final InetAddress manager, final int port, final SnmpOctetString community,
			final SnmpPduPacket pdu) {
		if (this.logger.isTraceEnabled()) {
			this.logger.trace(pdu.getRequestId() + ": in treating by snmpReceivedPdu...");
		}
		if (pdu instanceof SnmpPduBulk) {
			final SnmpPduRequest response = new SnmpPduRequest(SnmpPduPacket.RESPONSE);
			response.setRequestId(pdu.getRequestId());
			doBulk(pdu.toVarBindArray(), (SnmpPduBulk) pdu, response);
			try {
				sendPduPacket(session, new SnmpPeer(manager, port), response);
			}
			catch (final AsnEncodingException e) {
				this.logger.error(pdu.getRequestId() + ": error " + SnmpPduPacket.ErrTooBig + " (response too big)");
				final SnmpPduRequest errorResponse = new SnmpPduRequest(SnmpPduPacket.RESPONSE);
				errorResponse.setRequestId(pdu.getRequestId());
				errorResponse.setErrorStatus(SnmpPduPacket.ErrTooBig);
				errorResponse.setErrorIndex(0);
				try {
					sendPduPacket(session, new SnmpPeer(manager, port), errorResponse);
				}
				catch (final Throwable e1) {
					this.logger.error(pdu.getRequestId() + ": an error has occurred when the error response sending", e1);
				}
			}
			catch (final Throwable e) {
				this.logger.error(pdu.getRequestId() + ": error " + SnmpPduPacket.ErrGenError + " (general error)", e);
				final SnmpPduRequest errorResponse = new SnmpPduRequest(SnmpPduPacket.RESPONSE);
				errorResponse.setRequestId(pdu.getRequestId());
				errorResponse.setErrorStatus(SnmpPduPacket.ErrGenError);
				errorResponse.setErrorIndex(0);
				try {
					sendPduPacket(session, new SnmpPeer(manager, port), errorResponse);
				}
				catch (final Throwable e1) {
					this.logger.error(pdu.getRequestId() + ": an error has occurred when the error response sending", e1);
				}
			}
		}
		else {
			this.logger.error(pdu.getRequestId() + ": request ignored (PDU " + pdu.getClass().getSimpleName() + " unhandled)");
		}
		if (this.logger.isTraceEnabled()) {
			this.logger.trace(pdu.getRequestId() + ": treated by snmpReceivedPdu");
		}
	}

	/**
	 * Sends a {@link SnmpPduPacket}.
	 * @param session SNMP session.
	 * @param peer Request destination.
	 * @param pdu Request.
	 * @throws Exception Exception if an error has occurred.
	 */
	@SuppressWarnings("static-method")
	protected void sendPduPacket (final SnmpAgentSession session, final SnmpPeer peer, final SnmpPduPacket pdu) throws Exception {
		session.send(peer, pdu);
	}

	/*
	 * {@inheritDoc}
	 * @see org.opennms.protocols.snmp.SnmpAgentHandler#snmpReceivedGet(org.opennms.protocols.snmp.SnmpPduPacket, boolean)
	 */
	public final SnmpPduRequest snmpReceivedGet (final SnmpPduPacket pdu, final boolean getNext) {
		if (this.logger.isTraceEnabled()) {
			this.logger.trace(pdu.getRequestId() + ": in treating by snmpReceivedGet...");
		}
		final SnmpPduRequest response = new SnmpPduRequest(SnmpPduPacket.RESPONSE);
		response.setRequestId(pdu.getRequestId());
		final SnmpVarBind[] binds = pdu.toVarBindArray();
		if (binds.length > 20) {
			this.logger.error(pdu.getRequestId() + ": error " + SnmpPduPacket.ErrTooBig + " (too OIDs - limited to 20)");
			response.setErrorStatus(SnmpPduPacket.ErrTooBig);
			response.setErrorIndex(0);
		}

		// SNMP v1
		else if (pdu instanceof SnmpPduRequest) {
			int resultIndex = 1;
			try {
				SnmpOid oid;
				AttributeAccessor attributeAccessor;
				for (final SnmpVarBind varBind : binds) {
					oid = SnmpOid.newInstance(varBind.getName().getIdentifiers());
					if (getNext) {
						attributeAccessor = this.snmpMib.next(oid);
						if (attributeAccessor == null) {
							response.addVarBind(new SnmpVarBind(varBind.getName()));
							throw new SnmpException("none OID found after " + oid, SnmpPduPacket.ErrNoSuchName, Level.DEBUG);
						}
						if (this.logger.isDebugEnabled()) {
							this.logger.debug(pdu.getRequestId() + ": " + oid + " --next--> " + attributeAccessor);
						}
						response.addVarBind(newSnmpVarBind(attributeAccessor));
					}
					else {
						attributeAccessor = this.snmpMib.find(oid);
						if (attributeAccessor == null) {
							response.addVarBind(new SnmpVarBind(varBind.getName()));
							throw new SnmpException("OID " + oid + " not found", SnmpPduPacket.ErrNoSuchName, Level.DEBUG);
						}
						if (this.logger.isDebugEnabled()) {
							this.logger.debug(pdu.getRequestId() + ": " + attributeAccessor);
						}
						varBind.setValue(newSnmpValue(attributeAccessor));
						response.addVarBind(varBind);
					}
					resultIndex++;
				}
			}
			catch (final SnmpException e) {
				this.logger.log(e.getLoggerLevel(), pdu.getRequestId() + ": " + e.getMessage(), e.getCause());
				response.setErrorStatus(e.getErrorStatus());
				response.setErrorIndex(resultIndex);
			}
			catch (final Throwable e) {
				this.logger.error(pdu.getRequestId() + ": error " + SnmpPduPacket.ErrGenError + " (general error)", e);
				response.setErrorStatus(SnmpPduPacket.ErrGenError);
				response.setErrorIndex(resultIndex);
			}
		}

		// SNMP v2
		else if (pdu instanceof SnmpPduBulk) {
			doBulk(binds, (SnmpPduBulk) pdu, response);
		}

		else {
			this.logger.error(pdu.getRequestId() + ": error " + SnmpPduPacket.ErrGenError + " (PDU " + pdu.getClass().getSimpleName() + " unhandled)");
			response.setErrorStatus(SnmpPduPacket.ErrGenError);
			response.setErrorIndex(0);
		}
		if (this.logger.isTraceEnabled()) {
			this.logger.trace(pdu.getRequestId() + ": treated by snmpReceivedGet");
		}
		return response;
	}

	/**
	 * Treats a <code>get-bulk</code> operation (SNMP v2).
	 * @param binds OID list to apply a <code>get-bulk</code>.
	 * @param bulk <code>get-bulk</code> operation.
	 * @param response Response to the request.
	 */
	private void doBulk (final SnmpVarBind binds[], final SnmpPduBulk bulk, final SnmpPduRequest response) {
		int resultIndex = 1;
		try {
			SnmpOid oid;
			AttributeAccessor attributeAccessor;
			Iterator<Entry<SnmpOid, AttributeAccessor>> attributeAccessorEntryIterator;
			int i;
			for (final SnmpVarBind varBind : binds) {
				oid = SnmpOid.newInstance(varBind.getName().getIdentifiers());
				if (resultIndex <= bulk.getNonRepeaters()) {
					attributeAccessor = this.snmpMib.next(oid);
					if (attributeAccessor != null) {
						if (this.logger.isDebugEnabled()) {
							this.logger.debug(bulk.getRequestId() + ": " + oid + " --next--> " + attributeAccessor);
						}
						response.addVarBind(newSnmpVarBind(attributeAccessor));
					}
					else {
						if (this.logger.isDebugEnabled()) {
							this.logger.debug(bulk.getRequestId() + ": " + oid + " --next--> END OF MIB");
						}
						response.addVarBind(new SnmpVarBind(varBind.getName(), new SnmpEndOfMibView()));
					}
				}
				else {
					attributeAccessorEntryIterator = this.snmpMib.nextSet(oid).entrySet().iterator();
					if (attributeAccessorEntryIterator.hasNext()) {
						i = 0;
						while (attributeAccessorEntryIterator.hasNext() && (i < bulk.getMaxRepititions())) {
							attributeAccessor = attributeAccessorEntryIterator.next().getValue();
							if (this.logger.isDebugEnabled()) {
								if (i == 0) {
									this.logger.debug(bulk.getRequestId() + ": " + oid + " --next--> " + attributeAccessor);
								}
								else {
									this.logger.debug(bulk.getRequestId() + ": " + String.format("%" + oid.toString().length() + "s", "") + " --next--> "
											+ attributeAccessor);
								}
							}
							response.addVarBind(newSnmpVarBind(attributeAccessor));
							i++;
						}
						if (i < bulk.getMaxRepititions()) {
							if (this.logger.isDebugEnabled()) {
								this.logger.debug(bulk.getRequestId() + ": " + oid + " --next--> .1.9: END OF MIB");
							}
							response.addVarBind(new SnmpVarBind(".1.9", new SnmpEndOfMibView()));
						}
					}
					else {
						if (this.logger.isDebugEnabled()) {
							this.logger.debug(bulk.getRequestId() + ": " + oid + " --next--> END OF MIB");
						}
						response.addVarBind(new SnmpVarBind(varBind.getName(), new SnmpEndOfMibView()));
					}
				}
				resultIndex++;
			}
		}
		catch (final SnmpException e) {
			this.logger.log(e.getLoggerLevel(), bulk.getRequestId() + ": " + e.getMessage(), e.getCause());
			response.setErrorStatus(e.getErrorStatus());
			response.setErrorIndex(resultIndex);
		}
		catch (final Throwable e) {
			this.logger.error(bulk.getRequestId() + ": error " + SnmpPduPacket.ErrGenError + " (general error)", e);
			response.setErrorStatus(SnmpPduPacket.ErrGenError);
			response.setErrorIndex(resultIndex);
		}
	}

	/*
	 * {@inheritDoc}
	 * @see org.opennms.protocols.snmp.SnmpAgentHandler#snmpReceivedSet(org.opennms.protocols.snmp.SnmpPduPacket)
	 */
	public SnmpPduRequest snmpReceivedSet (final SnmpPduPacket pdu) {
		if (this.logger.isTraceEnabled()) {
			this.logger.trace(pdu.getRequestId() + ": in treating by snmpReceivedSet...");
		}
		final SnmpPduRequest response = new SnmpPduRequest(SnmpPduPacket.RESPONSE);
		response.setRequestId(pdu.getRequestId());
		final SnmpVarBind[] binds = pdu.toVarBindArray();
		if (binds.length > 20) {
			this.logger.error(pdu.getRequestId() + ": error " + SnmpPduPacket.ErrTooBig + " (too OIDs - limited to 20)");
			response.setErrorStatus(SnmpPduPacket.ErrTooBig);
			response.setErrorIndex(0);
		}
		else {
			int resultIndex = 1;
			try {
				SnmpOid oid;
				SnmpSyntax newValue;
				AttributeAccessor attributeAccessor;
				for (final SnmpVarBind varBind : binds) {
					oid = SnmpOid.newInstance(varBind.getName().getIdentifiers());
					newValue = varBind.getValue();
					attributeAccessor = this.snmpMib.find(oid);
					if (attributeAccessor == null) {
						response.addVarBind(new SnmpVarBind(varBind.getName()));
						throw new SnmpException("OID " + oid + " not found", SnmpPduPacket.ErrNoSuchName, Level.DEBUG);
					}
					if (this.logger.isDebugEnabled()) {
						this.logger.debug(pdu.getRequestId() + ": old " + attributeAccessor);
					}
					setValue(attributeAccessor, newValue);
					response.addVarBind(newSnmpVarBind(attributeAccessor));
					if (this.logger.isDebugEnabled()) {
						this.logger.debug(pdu.getRequestId() + ": new " + attributeAccessor);
					}
					resultIndex++;
				}
			}
			catch (final SnmpException e) {
				this.logger.log(e.getLoggerLevel(), pdu.getRequestId() + ": " + e.getMessage(), e.getCause());
				response.setErrorStatus(e.getErrorStatus());
				response.setErrorIndex(resultIndex);
			}
			catch (final Throwable e) {
				this.logger.error(pdu.getRequestId() + ": error " + SnmpPduPacket.ErrGenError + " (general error)", e);
				response.setErrorStatus(SnmpPduPacket.ErrGenError);
				response.setErrorIndex(resultIndex);
			}
		}
		if (this.logger.isTraceEnabled()) {
			this.logger.trace(pdu.getRequestId() + ": treated by snmpReceivedSet");
		}
		return response;
	}

	/**
	 * Creates and returns a new {@link SnmpVarBind} by a {@link AttributeAccessor}.
	 * @param attributeAccessor {@link AttributeAccessor} containing all the necessary data for create the {@link SnmpVarBind}.
	 * @return New {@link SnmpVarBind}.
	 * @throws Exception Exception if an error has occurred.
	 */
	private SnmpVarBind newSnmpVarBind (final AttributeAccessor attributeAccessor) throws Exception {
		return new SnmpVarBind(new SnmpObjectId(attributeAccessor.getOid().getOid()), newSnmpValue(attributeAccessor));
	}

	/**
	 * Creates and returns a new {@link SnmpSyntax} by a {@link AttributeAccessor}.
	 * @param attributeAccessor {@link AttributeAccessor} containing all the necessary data for create the {@link SnmpSyntax}.
	 * @return New {@link SnmpSyntax}.
	 * @throws Exception Exception if an error has occurred.
	 */
	protected static SnmpSyntax newSnmpValue (final AttributeAccessor attributeAccessor) throws Exception {
		if (!attributeAccessor.isReadable()) {
			throw new SnmpException("MBean attribute is not readable", SnmpPduPacket.ErrNoAccess);
		}
		Object value;
		try {
			value = attributeAccessor.getValue();
		}
		catch (final Throwable e) {
			throw new SnmpException("Impossible to read the attribute due to error", SnmpPduPacket.ErrNoAccess, e);
		}
		return newSnmpValue(attributeAccessor.getSnmpDataType(), value);
	}

	/**
	 * Sets a new value to {@link AttributeAccessor}.
	 * @param attributeAccessor {@link AttributeAccessor} to update.
	 * @param newValue New value encapsulated to an object {@link SnmpSyntax}.
	 * @throws Exception Exception if an error has occurred.
	 */
	protected static void setValue (final AttributeAccessor attributeAccessor, final SnmpSyntax newValue) throws Exception {
		if (!attributeAccessor.isReadable()) {
			throw new SnmpException("MBean attribute is not readable", SnmpPduPacket.ErrNoAccess);
		}
		if (!attributeAccessor.isWritable()) {
			throw new SnmpException("MBean attribute is not writable", SnmpPduPacket.ErrNotWritable);
		}
		final Object value = newJmxValue(attributeAccessor.getJmxDataType(), attributeAccessor.getSnmpDataType(), newValue);
		try {
			attributeAccessor.setValue(value);
		}
		catch (final Throwable e) {
			throw new SnmpException("Impossible to write in the attribute due to error", SnmpPduPacket.ErrNotWritable, e);
		}
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "SnmpDaemon:opennms[" + (this.configuration != null ? this.configuration.getListenerAddress() + ":" + this.configuration.getListenerPort() : "null")
				+ "]";
	}

}