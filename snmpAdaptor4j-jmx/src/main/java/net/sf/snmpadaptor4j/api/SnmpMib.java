package net.sf.snmpadaptor4j.api;

import java.util.SortedMap;
import net.sf.snmpadaptor4j.object.SnmpOid;

/**
 * Object representing the <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB) of SNMP daemon.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public interface SnmpMib {

	/**
	 * Finds a {@link AttributeAccessor} by its OID.
	 * @param oid OID of {@link AttributeAccessor} to find.
	 * @return {@link AttributeAccessor} found or <code>NULL</code> if not found.
	 */
	AttributeAccessor find (SnmpOid oid);

	/**
	 * Finds the next {@link AttributeAccessor} from an OID
	 * @param oid OID not included in the research but from which it begins.
	 * @return {@link AttributeAccessor} found or <code>NULL</code> if not found.
	 */
	AttributeAccessor next (SnmpOid oid);

	/**
	 * Finds the list of next {@link AttributeAccessor} from an OID
	 * @param oid OID not included in the research but from which it begins.
	 * @return List of {@link AttributeAccessor} found (never <code>NULL</code>).
	 */
	SortedMap<SnmpOid, AttributeAccessor> nextSet (SnmpOid oid);

}