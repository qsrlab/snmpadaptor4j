package net.sf.snmpadaptor4j.api.opennms;

import java.net.InetAddress;
import java.util.Map.Entry;
import org.opennms.protocols.snmp.SnmpHandler;
import org.opennms.protocols.snmp.SnmpIPAddress;
import org.opennms.protocols.snmp.SnmpObjectId;
import org.opennms.protocols.snmp.SnmpParameters;
import org.opennms.protocols.snmp.SnmpPduPacket;
import org.opennms.protocols.snmp.SnmpPduRequest;
import org.opennms.protocols.snmp.SnmpPduTrap;
import org.opennms.protocols.snmp.SnmpPeer;
import org.opennms.protocols.snmp.SnmpSMI;
import org.opennms.protocols.snmp.SnmpSession;
import org.opennms.protocols.snmp.SnmpSyntax;
import org.opennms.protocols.snmp.SnmpVarBind;
import net.sf.snmpadaptor4j.api.SnmpTrapSender;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import net.sf.snmpadaptor4j.object.SnmpTrap;
import net.sf.snmpadaptor4j.object.SnmpTrapData;
import net.sf.snmpadaptor4j.object.GenericSnmpTrap;
import net.sf.snmpadaptor4j.object.SpecificSnmpTrap;
import net.sf.snmpadaptor4j.object.GenericSnmpTrapType;

/**
 * SNMP trap sender implementation for <b>joesnmp</b> API.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class OpennmsSnmpTrapSender
		extends OpennmsSupport
		implements SnmpTrapSender {

	/**
	 * OID CONSTANT: Time stamp.
	 */
	private static final SnmpObjectId TIME_STAMP_OID = new SnmpObjectId("1.3.6.1.2.1.1.3.0");

	/**
	 * OID CONSTANT: Type.
	 */
	private static final SnmpObjectId TYPE_OID = new SnmpObjectId("1.3.6.1.6.3.1.1.4.1.0");

	/**
	 * OID CONSTANT: Agent address.
	 */
	private static final SnmpObjectId AGENT_ADDRESS_OID = new SnmpObjectId("1.3.6.1.6.3.18.1.3.0");

	/**
	 * OID CONSTANT: Community name.
	 */
	private static final SnmpObjectId COMMUNITY_OID = new SnmpObjectId("1.3.6.1.6.3.18.1.4.0");

	/**
	 * OID CONSTANT: Enterprise.
	 */
	private static final SnmpObjectId ENTERPRISE_OID = new SnmpObjectId("1.3.6.1.6.3.1.1.4.3.0");

	/**
	 * TYPE VALUE CONSTANT: coldStart.
	 */
	private static final SnmpOid GENERIC_COLD_START_TYPE_VALUE = SnmpOid.newInstance("1.3.6.1.6.3.1.1.5.1");

	/**
	 * TYPE VALUE CONSTANT: warmStart.
	 */
	private static final SnmpOid GENERIC_WARM_START_TYPE_VALUE = SnmpOid.newInstance("1.3.6.1.6.3.1.1.5.2");

	/**
	 * TYPE VALUE CONSTANT: linkDown.
	 */
	private static final SnmpOid GENERIC_LINK_DOWN_TYPE_VALUE = SnmpOid.newInstance("1.3.6.1.6.3.1.1.5.3");

	/**
	 * TYPE VALUE CONSTANT: linkUp.
	 */
	private static final SnmpOid GENERIC_LINK_UP_TYPE_VALUE = SnmpOid.newInstance("1.3.6.1.6.3.1.1.5.4");

	/**
	 * TYPE VALUE CONSTANT: authenticationFailure.
	 */
	private static final SnmpOid GENERIC_AUTHENTICATION_FAILURE_TYPE_VALUE = SnmpOid.newInstance("1.3.6.1.6.3.1.1.5.5");

	/**
	 * TYPE VALUE CONSTANT: egpNeighborLoss.
	 */
	private static final SnmpOid GENERIC_EGP_NEIGHBOR_LOSS_TYPE_VALUE = SnmpOid.newInstance("1.3.6.1.6.3.1.1.5.6");

	/**
	 * Null SNMP handler.
	 */
	protected final class NullHandler
			implements SnmpHandler {

		/*
		 * {@inheritDoc}
		 * @see org.opennms.protocols.snmp.SnmpHandler#snmpReceivedPdu(org.opennms.protocols.snmp.SnmpSession, int, org.opennms.protocols.snmp.SnmpPduPacket)
		 */
		public void snmpReceivedPdu (final SnmpSession session, final int command, final SnmpPduPacket pdu) {
			// NOP
		}

		/*
		 * {@inheritDoc}
		 * @see org.opennms.protocols.snmp.SnmpHandler#snmpInternalError(org.opennms.protocols.snmp.SnmpSession, int, org.opennms.protocols.snmp.SnmpSyntax)
		 */
		public void snmpInternalError (final SnmpSession session, final int err, final SnmpSyntax pdu) {
			// NOP
		}

		/*
		 * {@inheritDoc}
		 * @see org.opennms.protocols.snmp.SnmpHandler#snmpTimeoutError(org.opennms.protocols.snmp.SnmpSession, org.opennms.protocols.snmp.SnmpSyntax)
		 */
		public void snmpTimeoutError (final SnmpSession session, final SnmpSyntax pdu) {
			// NOP
		}

	}

	/**
	 * IP address of SNMP agent.
	 */
	private final SnmpIPAddress agentAddress;

	/**
	 * IP address of SNMP manager responsible of traps handling.
	 */
	private final String managerAddress;

	/**
	 * UDP port of SNMP manager responsible of traps handling.
	 */
	private final int managerPort;

	/**
	 * Protocol version of SNMP manager.
	 */
	private final int managerVersion;

	/**
	 * Community of SNMP manager.
	 */
	private final String managerCommunity;

	/**
	 * SNMP session.
	 */
	private SnmpSession snmpSession;

	/**
	 * Hidden constructor.
	 * @param agentAddress IP address of SNMP agent (must be a local address of the host).
	 * @param managerAddress IP address of SNMP manager responsible of traps handling.
	 * @param managerPort UDP port of SNMP manager responsible of traps handling.
	 * @param managerVersion Protocol version of SNMP manager.
	 * @param managerCommunity Community of SNMP manager.
	 * @see OpennmsSnmpApiFactory#newSnmpTrapSender(InetAddress, String, int, int, String)
	 */
	OpennmsSnmpTrapSender (final InetAddress agentAddress, final String managerAddress, final int managerPort, final int managerVersion,
			final String managerCommunity) {
		super();
		this.agentAddress = new SnmpIPAddress(agentAddress);
		this.managerAddress = managerAddress;
		this.managerPort = managerPort;
		this.managerVersion = managerVersion;
		this.managerCommunity = managerCommunity;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpTrapSender#getName()
	 */
	public String getName () {
		return this.managerAddress + ":" + this.managerPort + "/" + this.managerCommunity;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpTrapSender#open()
	 */
	public void open () throws Exception {
		synchronized (this) {
			if (this.snmpSession == null) {
				if (this.logger.isTraceEnabled()) {
					this.logger.trace("Connecting to " + this.managerAddress + ":" + this.managerPort + "...");
				}
				final SnmpPeer peer = new SnmpPeer(InetAddress.getByName(this.managerAddress));
				peer.setPort(this.managerPort);
				final SnmpParameters params = peer.getParameters();
				params.setReadCommunity(this.managerCommunity);
				switch (this.managerVersion) {
					case 1:
						params.setVersion(SnmpSMI.SNMPV1);
						break;
					default:
						params.setVersion(SnmpSMI.SNMPV2);
						break;
				}
				this.snmpSession = new SnmpSession(peer);
				this.snmpSession.setDefaultHandler(new NullHandler());
				Thread.sleep(100);
				this.logger.trace("Connected");
			}
			else if (this.logger.isTraceEnabled()) {
				this.logger.trace(this.managerAddress + ":" + this.managerPort + " already connected");
			}
		}
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpTrapSender#close()
	 */
	public void close () {
		synchronized (this) {
			if (this.snmpSession != null) {
				if (this.logger.isTraceEnabled()) {
					this.logger.trace("Disconnecting from " + this.managerAddress + ":" + this.managerPort + "...");
				}
				this.snmpSession.close();
				this.snmpSession = null;
				this.logger.trace("Disconnected");
			}
			else if (this.logger.isTraceEnabled()) {
				this.logger.trace(this.managerAddress + ":" + this.managerPort + " already disconnected");
			}
		}
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpTrapSender#isConnected()
	 */
	public boolean isConnected () {
		return (this.snmpSession != null);
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpTrapSender#send(net.sf.snmpadaptor4j.object.SnmpTrap)
	 */
	public void send (final SnmpTrap trap) throws Exception {
		switch (this.managerVersion) {
			case 1:
				sendV1(trap);
				break;
			default:
				sendV2(trap);
				break;
		}
	}

	/**
	 * Sends a SNMP trap V1 to the manager.
	 * @param trap Object representing a specific or generic SNMP trap.
	 * @throws Exception Exception if an error has occurred.
	 */
	private void sendV1 (final SnmpTrap trap) throws Exception {

		// Packet building
		final SnmpPduTrap trapPdu = new SnmpPduTrap();
		trapPdu.setTimeStamp(trap.getTimeStamp());
		if (trap.getSource() == null) {
			throw new Exception("The source of trap is missing");
		}
		trapPdu.setEnterprise(new SnmpObjectId(trap.getSource().getOid()));
		trapPdu.setAgentAddress(this.agentAddress);
		if (trap instanceof SpecificSnmpTrap) {
			trapPdu.setGeneric(SnmpPduTrap.GenericEnterpriseSpecific);
			trapPdu.setSpecific(((SpecificSnmpTrap) trap).getType());
		}
		else if (trap instanceof GenericSnmpTrap) {
			final GenericSnmpTrap genericTrap = (GenericSnmpTrap) trap;
			if (genericTrap.getType() == GenericSnmpTrapType.coldStart) {
				trapPdu.setGeneric(SnmpPduTrap.GenericColdStart);
			}
			else if (genericTrap.getType() == GenericSnmpTrapType.warmStart) {
				trapPdu.setGeneric(SnmpPduTrap.GenericWarmStart);
			}
			else if (genericTrap.getType() == GenericSnmpTrapType.linkDown) {
				trapPdu.setGeneric(SnmpPduTrap.GenericLinkDown);
			}
			else if (genericTrap.getType() == GenericSnmpTrapType.linkUp) {
				trapPdu.setGeneric(SnmpPduTrap.GenericLinkUp);
			}
			else if (genericTrap.getType() == GenericSnmpTrapType.authenticationFailure) {
				trapPdu.setGeneric(SnmpPduTrap.GenericAuthenticationFailure);
			}
			else if (genericTrap.getType() == GenericSnmpTrapType.egpNeighborLoss) {
				trapPdu.setGeneric(SnmpPduTrap.GenericEgpNeighborLoss);
			}
			else {
				throw new Exception("Generic trap type unknown");
			}
			trapPdu.setSpecific(0);
		}
		else {
			throw new Exception("Trap type unknown");
		}
		for (Entry<SnmpOid, SnmpTrapData> entry : trap.getDataMap().entrySet()) {
			trapPdu.addVarBind(new SnmpVarBind(new SnmpObjectId(entry.getKey().getOid()), newSnmpValue(entry.getValue().getType(), entry.getValue().getValue())));
		}

		// Sending
		synchronized (this) {
			if (this.logger.isTraceEnabled()) {
				this.logger.trace(trap + " sending to " + this.managerAddress + ":" + this.managerPort + "...");
			}
			if (this.snmpSession == null) {
				throw new Exception("The connection to the SNMP manager is closed");
			}
			this.snmpSession.send(trapPdu);
			if (this.logger.isTraceEnabled()) {
				this.logger.trace(trap + " sent");
			}
		}

	}

	/**
	 * Sends a SNMP trap V2 to the manager.
	 * @param trap Object representing a SNMP trap V2.
	 * @throws Exception Exception if an error has occurred.
	 */
	private void sendV2 (final SnmpTrap trap) throws Exception {

		// Packet building
		final SnmpPduRequest trapPdu = new SnmpPduRequest(SnmpPduPacket.V2TRAP);
		trapPdu.addVarBind(new SnmpVarBind(OpennmsSnmpTrapSender.TIME_STAMP_OID, newSnmpValue(SnmpDataType.timeTicks, new Long(trap.getTimeStamp()))));
		if (trap.getSource() == null) {
			throw new Exception("The source of trap is missing");
		}
		SnmpOid typeValue;
		if (trap instanceof SpecificSnmpTrap) {
			typeValue = SnmpOid.newInstance(trap.getSource(), 0, ((SpecificSnmpTrap) trap).getType() + 1);
		}
		else if (trap instanceof GenericSnmpTrap) {
			final GenericSnmpTrap genericTrap = (GenericSnmpTrap) trap;
			if (genericTrap.getType() == GenericSnmpTrapType.coldStart) {
				typeValue = OpennmsSnmpTrapSender.GENERIC_COLD_START_TYPE_VALUE;
			}
			else if (genericTrap.getType() == GenericSnmpTrapType.warmStart) {
				typeValue = OpennmsSnmpTrapSender.GENERIC_WARM_START_TYPE_VALUE;
			}
			else if (genericTrap.getType() == GenericSnmpTrapType.linkDown) {
				typeValue = OpennmsSnmpTrapSender.GENERIC_LINK_DOWN_TYPE_VALUE;
			}
			else if (genericTrap.getType() == GenericSnmpTrapType.linkUp) {
				typeValue = OpennmsSnmpTrapSender.GENERIC_LINK_UP_TYPE_VALUE;
			}
			else if (genericTrap.getType() == GenericSnmpTrapType.authenticationFailure) {
				typeValue = OpennmsSnmpTrapSender.GENERIC_AUTHENTICATION_FAILURE_TYPE_VALUE;
			}
			else if (genericTrap.getType() == GenericSnmpTrapType.egpNeighborLoss) {
				typeValue = OpennmsSnmpTrapSender.GENERIC_EGP_NEIGHBOR_LOSS_TYPE_VALUE;
			}
			else {
				throw new Exception("Generic trap type unknown");
			}
		}
		else {
			throw new Exception("Trap type unknown");
		}
		trapPdu.addVarBind(new SnmpVarBind(OpennmsSnmpTrapSender.TYPE_OID, newSnmpValue(SnmpDataType.objectIdentifier, typeValue)));
		trapPdu.addVarBind(new SnmpVarBind(OpennmsSnmpTrapSender.AGENT_ADDRESS_OID, newSnmpValue(SnmpDataType.ipAddress, this.agentAddress.convertToIpAddress())));
		trapPdu.addVarBind(new SnmpVarBind(OpennmsSnmpTrapSender.COMMUNITY_OID, newSnmpValue(SnmpDataType.octetString, this.managerCommunity)));
		trapPdu.addVarBind(new SnmpVarBind(OpennmsSnmpTrapSender.ENTERPRISE_OID, newSnmpValue(SnmpDataType.objectIdentifier, trap.getSource())));
		for (Entry<SnmpOid, SnmpTrapData> entry : trap.getDataMap().entrySet()) {
			trapPdu.addVarBind(new SnmpVarBind(new SnmpObjectId(entry.getKey().getOid()), newSnmpValue(entry.getValue().getType(), entry.getValue().getValue())));
		}

		// Sending
		synchronized (this) {
			if (this.logger.isTraceEnabled()) {
				this.logger.trace(trap + " sending to " + this.managerAddress + ":" + this.managerPort + "...");
			}
			if (this.snmpSession == null) {
				throw new Exception("The connection to the SNMP manager is closed");
			}
			this.snmpSession.send(trapPdu);
			if (this.logger.isTraceEnabled()) {
				this.logger.trace(trap + " sent");
			}
		}

	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "SnmpTrapSender:opennms[" + getName() + "]";
	}

}