package net.sf.snmpadaptor4j.api;

/**
 * SNMP daemon.
 * <p>
 * Classes implementing this interface encapsulate the SNMP API. To change the API, just create a new implementation of {@link SnmpDaemon}.
 * </p>
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public interface SnmpDaemon {

	/**
	 * Starts the SNMP daemon.
	 * @throws Exception Exception if an error has occurred.
	 */
	void start () throws Exception;

	/**
	 * Stops the SNMP daemon.
	 * @throws Exception Exception if an error has occurred.
	 */
	void stop () throws Exception;

	/**
	 * Returns <code>TRUE</code> if the SNMP daemon is started.
	 * @return <code>TRUE</code> if the SNMP daemon is started.
	 */
	boolean isStarted ();

}