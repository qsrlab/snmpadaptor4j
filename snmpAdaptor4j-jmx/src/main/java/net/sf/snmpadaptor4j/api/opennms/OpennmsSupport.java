package net.sf.snmpadaptor4j.api.opennms;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import net.sf.snmpadaptor4j.api.SnmpException;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import org.apache.log4j.Logger;
import org.opennms.protocols.snmp.SnmpCounter32;
import org.opennms.protocols.snmp.SnmpCounter64;
import org.opennms.protocols.snmp.SnmpGauge32;
import org.opennms.protocols.snmp.SnmpIPAddress;
import org.opennms.protocols.snmp.SnmpInt32;
import org.opennms.protocols.snmp.SnmpNull;
import org.opennms.protocols.snmp.SnmpObjectId;
import org.opennms.protocols.snmp.SnmpOctetString;
import org.opennms.protocols.snmp.SnmpOpaque;
import org.opennms.protocols.snmp.SnmpPduPacket;
import org.opennms.protocols.snmp.SnmpSyntax;
import org.opennms.protocols.snmp.SnmpTimeTicks;
import org.opennms.protocols.snmp.SnmpUInt32;

/**
 * Support class for <b>joesnmp</b> API.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public abstract class OpennmsSupport {

	/**
	 * Logger.
	 */
	protected final Logger logger = Logger.getLogger(getClass());

	/**
	 * Hidden constructor.
	 */
	protected OpennmsSupport () {
		super();
	}

	/**
	 * Creates and returns a new {@link SnmpSyntax}.
	 * @param snmpDataType SNMP data type of value.
	 * @param value Value.
	 * @return New {@link SnmpSyntax}.
	 * @throws Exception Exception if an error has occurred.
	 */
	protected static final SnmpSyntax newSnmpValue (final SnmpDataType snmpDataType, final Object value) throws Exception {
		SnmpSyntax snmpValue;
		if (snmpDataType == SnmpDataType.integer32) {
			snmpValue = new SnmpInt32(toInteger32(value));
		}
		else if (snmpDataType == SnmpDataType.unsigned32) {
			snmpValue = new SnmpUInt32(toUnsigned32(value));
		}
		else if (snmpDataType == SnmpDataType.gauge32) {
			snmpValue = new SnmpGauge32(toUnsigned32(value));
		}
		else if (snmpDataType == SnmpDataType.counter32) {
			snmpValue = new SnmpCounter32(toUnsigned32(value));
		}
		else if (snmpDataType == SnmpDataType.counter64) {
			snmpValue = new SnmpCounter64(toUnsigned64(value));
		}
		else if (snmpDataType == SnmpDataType.timeTicks) {
			snmpValue = new SnmpTimeTicks(toTimeTicks(value));
		}
		else if (snmpDataType == SnmpDataType.octetString) {
			snmpValue = new SnmpOctetString(toOctetString(value));
		}
		else if (snmpDataType == SnmpDataType.ipAddress) {
			snmpValue = new SnmpIPAddress(toIpAddress(value));
		}
		else if (snmpDataType == SnmpDataType.objectIdentifier) {
			snmpValue = new SnmpObjectId(toObjectIdentifier(value));
		}
		else if (snmpDataType == SnmpDataType.opaque) {
			snmpValue = new SnmpOpaque(toOctetString(value));
		}
		else {
			throw new SnmpException(snmpDataType + " unhandled", SnmpPduPacket.ErrWrongType);
		}
		return snmpValue;
	}

	/**
	 * Creates and returns a new JMX object from a {@link SnmpSyntax}.
	 * @param jmxDataType JMX data type of value.
	 * @param snmpDataType SNMP data type of value.
	 * @param newValue New value encapsulated to an object {@link SnmpSyntax}.
	 * @throws Exception Exception if an error has occurred.
	 */
	protected static Object newJmxValue (final Class<?> jmxDataType, final SnmpDataType snmpDataType, final SnmpSyntax newValue) throws Exception {
		Object value;
		if (newValue instanceof SnmpNull) {
			value = null;
		}
		else if (snmpDataType == SnmpDataType.integer32) {
			if (!(newValue instanceof SnmpInt32)) {
				throw new SnmpException("New value is not an integer32", SnmpPduPacket.ErrBadValue);
			}
			value = fromInteger32(jmxDataType, ((SnmpInt32) newValue).getValue());
		}
		else if (snmpDataType == SnmpDataType.unsigned32) {
			if (!(newValue instanceof SnmpUInt32) || (newValue instanceof SnmpCounter32) || (newValue instanceof SnmpGauge32) || (newValue instanceof SnmpTimeTicks)) {
				throw new SnmpException("New value is not an unsigned32", SnmpPduPacket.ErrBadValue);
			}
			value = fromUnsigned32("unsigned32", jmxDataType, ((SnmpUInt32) newValue).getValue());
		}
		else if (snmpDataType == SnmpDataType.gauge32) {
			if (!(newValue instanceof SnmpGauge32)) {
				throw new SnmpException("New value is not a gauge32", SnmpPduPacket.ErrBadValue);
			}
			value = fromUnsigned32("gauge32", jmxDataType, ((SnmpGauge32) newValue).getValue());
		}
		else if (snmpDataType == SnmpDataType.counter32) {
			throw new SnmpException("counter32 are not writable", SnmpPduPacket.ErrNotWritable);
		}
		else if (snmpDataType == SnmpDataType.counter64) {
			throw new SnmpException("counter64 are not writable", SnmpPduPacket.ErrNotWritable);
		}
		else if (snmpDataType == SnmpDataType.timeTicks) {
			if (!(newValue instanceof SnmpTimeTicks)) {
				throw new SnmpException("New value is not a timeTicks", SnmpPduPacket.ErrBadValue);
			}
			value = fromTimeTicks(jmxDataType, ((SnmpTimeTicks) newValue).getValue());
		}
		else if (snmpDataType == SnmpDataType.octetString) {
			if (!(newValue instanceof SnmpOctetString) || (newValue instanceof SnmpIPAddress) || (newValue instanceof SnmpOpaque)) {
				throw new SnmpException("New value is not an octetString", SnmpPduPacket.ErrBadValue);
			}
			value = fromOctetString("octetString", jmxDataType, ((SnmpOctetString) newValue).getString());
		}
		else if (snmpDataType == SnmpDataType.ipAddress) {
			if (!(newValue instanceof SnmpIPAddress)) {
				throw new SnmpException("New value is not an ipAddress", SnmpPduPacket.ErrBadValue);
			}
			value = fromIpAddress(jmxDataType, ((SnmpIPAddress) newValue).convertToIpAddress());
		}
		else if (snmpDataType == SnmpDataType.objectIdentifier) {
			if (!(newValue instanceof SnmpObjectId)) {
				throw new SnmpException("New value is not an objectIdentifier", SnmpPduPacket.ErrBadValue);
			}
			value = fromObjectIdentifier(jmxDataType, ((SnmpObjectId) newValue).getIdentifiers());
		}
		else if (snmpDataType == SnmpDataType.opaque) {
			if (!(newValue instanceof SnmpOpaque)) {
				throw new SnmpException("New value is not an opaque", SnmpPduPacket.ErrBadValue);
			}
			value = fromOctetString("opaque", jmxDataType, ((SnmpOpaque) newValue).getString());
		}
		else {
			throw new SnmpException(snmpDataType + " unhandled", SnmpPduPacket.ErrWrongType);
		}
		return value;
	}

	/**
	 * Converts a value to signed 32-bit integer.
	 * @param value Value to convert.
	 * @return Value as signed 32-bit integer.
	 */
	private static Integer toInteger32 (final Object value) {
		Integer result;
		if (value instanceof Integer) {
			result = (Integer) value;
		}
		else if (value instanceof Byte) {
			result = new Integer(((Byte) value).intValue());
		}
		else if (value instanceof Short) {
			result = new Integer(((Short) value).intValue());
		}
		else if (value instanceof Boolean) {
			result = new Integer(((Boolean) value).booleanValue() ? 1 : 2);
		}
		else if (value == null) {
			result = new Integer(0);
		}
		else {
			throw new SnmpException(value.getClass() + " is inconsistent with integer32", SnmpPduPacket.ErrWrongType);
		}
		return result;
	}

	/**
	 * Converts a SNMP signed 32-bit integer to an object.
	 * @param type Object type to put in JMX attribute.
	 * @param value Value as signed 32-bit integer.
	 * @return Object to put in JMX attribute.
	 */
	private static Object fromInteger32 (final Class<?> type, final int value) {
		Object result;
		if (Integer.class.equals(type) || int.class.equals(type)) {
			result = new Integer(value);
		}
		else if (Byte.class.equals(type) || byte.class.equals(type)) {
			if ((value < Byte.MIN_VALUE) || (value > Byte.MAX_VALUE)) {
				throw new SnmpException("the value must be between " + Byte.MIN_VALUE + " and " + Byte.MAX_VALUE, SnmpPduPacket.ErrBadValue);
			}
			result = new Byte((byte) value);
		}
		else if (Short.class.equals(type) || short.class.equals(type)) {
			if ((value < Short.MIN_VALUE) || (value > Short.MAX_VALUE)) {
				throw new SnmpException("the value must be between " + Short.MIN_VALUE + " and " + Short.MAX_VALUE, SnmpPduPacket.ErrBadValue);
			}
			result = new Short((short) value);
		}
		else if (Boolean.class.equals(type) || boolean.class.equals(type)) {
			if (value == 1) {
				result = Boolean.TRUE;
			}
			else if (value == 2) {
				result = Boolean.FALSE;
			}
			else {
				throw new SnmpException("the value must be between true(1) and false(2)", SnmpPduPacket.ErrBadValue);
			}
		}
		else {
			throw new SnmpException(type + " is inconsistent with integer32", SnmpPduPacket.ErrWrongType);
		}
		return result;
	}

	/**
	 * Converts a value to non-negative 32-bit integer.
	 * @param value Value to convert.
	 * @return Value as non-negative 32-bit integer.
	 */
	private static Long toUnsigned32 (final Object value) {
		Long result;
		if (value instanceof Long) {
			result = (Long) value;
		}
		else if (value instanceof Integer) {
			result = new Long(((Integer) value).longValue());
		}
		else if (value instanceof Byte) {
			result = new Long(((Byte) value).longValue());
		}
		else if (value instanceof Short) {
			result = new Long(((Short) value).longValue());
		}
		else if (value == null) {
			result = new Long(0L);
		}
		else {
			throw new SnmpException(value.getClass() + " is inconsistent with unsigned32", SnmpPduPacket.ErrWrongType);
		}
		final long maxValue = 2 * (long) Integer.MAX_VALUE + 1;
		if ((result.longValue() < 0) || (result.longValue() > maxValue)) {
			throw new SnmpException("the value must be between 0 and " + maxValue, SnmpPduPacket.ErrWrongType);
		}
		return result;
	}

	/**
	 * Converts a SNMP non-negative 32-bit integer to an object.
	 * @param snmpDataTypeName Name of SNMP data type used in the error message.
	 * @param type Object type to put in JMX attribute.
	 * @param value Value as non-negative 32-bit integer.
	 * @return Object to put in JMX attribute.
	 */
	private static Object fromUnsigned32 (final String snmpDataTypeName, final Class<?> type, final long value) {
		Object result;
		if (Long.class.equals(type) || long.class.equals(type)) {
			result = new Long(value);
		}
		else if (Integer.class.equals(type) || int.class.equals(type)) {
			if (value > Integer.MAX_VALUE) {
				throw new SnmpException("the value must be between 0 and " + Integer.MAX_VALUE, SnmpPduPacket.ErrBadValue);
			}
			result = new Integer((int) value);
		}
		else if (Byte.class.equals(type) || byte.class.equals(type)) {
			if (value > Byte.MAX_VALUE) {
				throw new SnmpException("the value must be between 0 and " + Byte.MAX_VALUE, SnmpPduPacket.ErrBadValue);
			}
			result = new Byte((byte) value);
		}
		else if (Short.class.equals(type) || short.class.equals(type)) {
			if (value > Short.MAX_VALUE) {
				throw new SnmpException("the value must be between 0 and " + Short.MAX_VALUE, SnmpPduPacket.ErrBadValue);
			}
			result = new Short((short) value);
		}
		else {
			throw new SnmpException(type + " is inconsistent with " + snmpDataTypeName, SnmpPduPacket.ErrWrongType);
		}
		return result;
	}

	/**
	 * Converts a value to non-negative 64-bit integer.
	 * @param value Value to convert.
	 * @return Value as non-negative 64-bit integer.
	 */
	private static BigInteger toUnsigned64 (final Object value) {
		BigInteger result;
		if (value instanceof BigInteger) {
			result = (BigInteger) value;
		}
		else if (value instanceof Long) {
			result = BigInteger.valueOf(((Long) value).longValue());
		}
		else if (value == null) {
			result = BigInteger.valueOf(0L);
		}
		else {
			throw new SnmpException(value.getClass() + " is inconsistent with unsigned64", SnmpPduPacket.ErrWrongType);
		}
		final BigInteger minValue = BigInteger.valueOf(0);
		final BigInteger maxValue = BigInteger.valueOf(Long.MAX_VALUE).multiply(BigInteger.valueOf(2)).add(BigInteger.valueOf(1));
		if ((result.compareTo(minValue) < 0) || (result.compareTo(maxValue) > 0)) {
			throw new SnmpException("the value must be between " + minValue + " and " + maxValue, SnmpPduPacket.ErrWrongType);
		}
		return result;
	}

	/**
	 * Converts a value to non-negative 32-bit integer in hundredths of a second.
	 * @param value Value to convert in milliseconds.
	 * @return Value as non-negative 32-bit integer in hundredths of a second.
	 */
	private static Long toTimeTicks (final Object value) {
		Long result;
		if (value instanceof Long) {
			result = new Long(((Long) value).longValue() / 10);
		}
		else if (value == null) {
			result = new Long(0L);
		}
		else {
			throw new SnmpException(value.getClass() + " is inconsistent with timeTicks", SnmpPduPacket.ErrWrongType);
		}
		final long maxValue = 2 * (long) Integer.MAX_VALUE + 1;
		if ((result.longValue() < 0) || (result.longValue() > maxValue)) {
			throw new SnmpException("the value must be between 0 and " + maxValue, SnmpPduPacket.ErrWrongType);
		}
		return result;
	}

	/**
	 * Converts a SNMP non-negative 32-bit integer in hundredths of a second to an object.
	 * @param type Object type to put in JMX attribute.
	 * @param value Value as non-negative 32-bit integer in hundredths of a second.
	 * @return Object to put in JMX attribute in milliseconds.
	 */
	private static Object fromTimeTicks (final Class<?> type, final long value) {
		Object result;
		if (Long.class.equals(type) || long.class.equals(type)) {
			result = new Long(10 * value);
		}
		else {
			throw new SnmpException(type + " is inconsistent with timeTicks", SnmpPduPacket.ErrWrongType);
		}
		return result;
	}

	/**
	 * Converts a value to array of ASCII code.
	 * @param value Value to convert.
	 * @return Value as array of ASCII code.
	 */
	private static byte[] toOctetString (final Object value) {
		byte[] result;
		if (value instanceof String) {
			result = ((String) value).getBytes();
		}
		else if (value instanceof byte[]) {
			result = (byte[]) value;
		}
		else if (value == null) {
			result = "".getBytes();
		}
		else {
			result = value.toString().getBytes();
		}
		return result;
	}

	/**
	 * Converts a SNMP array of ASCII code to an object.
	 * @param snmpDataTypeName Name of SNMP data type used in the error message.
	 * @param type Object type to put in JMX attribute.
	 * @param value Value as ASCII code.
	 * @return Object to put in JMX attribute.
	 */
	private static Object fromOctetString (final String snmpDataTypeName, final Class<?> type, final byte[] value) {
		Object result;
		if (String.class.equals(type)) {
			result = new String(value);
		}
		else if (byte[].class.equals(type)) {
			result = value;
		}
		else {
			throw new SnmpException(type + " is inconsistent with " + snmpDataTypeName, SnmpPduPacket.ErrWrongType);
		}
		return result;
	}

	/**
	 * Converts a value to internet address.
	 * @param value Value to convert.
	 * @return Value as internet address.
	 * @throws Exception Exception if an error has occurred.
	 */
	private static InetAddress toIpAddress (final Object value) throws Exception {
		InetAddress result;
		if (value instanceof InetAddress) {
			result = (InetAddress) value;
		}
		else if (value instanceof String) {
			try {
				result = InetAddress.getByName((String) value);
			}
			catch (final UnknownHostException e) {
				throw new SnmpException("IP address is malformed", SnmpPduPacket.ErrWrongValue);
			}
		}
		else if (value == null) {
			result = InetAddress.getByAddress(new byte[] { 0, 0, 0, 0 });
		}
		else {
			throw new SnmpException(value.getClass() + " is inconsistent with ipAddress", SnmpPduPacket.ErrWrongType);
		}
		return result;
	}

	/**
	 * Converts a SNMP internet address to an object.
	 * @param type Object type to put in JMX attribute.
	 * @param value Value as internet address.
	 * @return Object to put in JMX attribute.
	 */
	private static Object fromIpAddress (final Class<?> type, final InetAddress value) {
		Object result;
		if (InetAddress.class.equals(type)) {
			result = value;
		}
		else if (String.class.equals(type)) {
			result = value.getHostAddress();
		}
		else {
			throw new SnmpException(type + " is inconsistent with ipAddress", SnmpPduPacket.ErrWrongType);
		}
		return result;
	}

	/**
	 * Converts a value to OID of SNMP <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 * @param value Value to convert.
	 * @return Value as OID of SNMP <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 */
	private static int[] toObjectIdentifier (final Object value) {
		int[] result;
		if (value instanceof SnmpOid) {
			result = ((SnmpOid) value).getOid();
		}
		else if (value instanceof String) {
			result = SnmpOid.newInstance((String) value).getOid();
		}
		else if (value == null) {
			result = new int[] { 1 };
		}
		else {
			throw new SnmpException(value.getClass() + " is inconsistent with objectIdentifier", SnmpPduPacket.ErrWrongType);
		}
		return result;
	}

	/**
	 * Converts an OID of SNMP <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB) to an object.
	 * @param type Object type to put in JMX attribute.
	 * @param value Value as OID of SNMP <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 * @return Object to put in JMX attribute.
	 */
	private static Object fromObjectIdentifier (final Class<?> type, final int[] value) {
		Object result;
		if (SnmpOid.class.equals(type)) {
			result = SnmpOid.newInstance(value);
		}
		else if (String.class.equals(type)) {
			result = SnmpOid.newInstance(value).toString();
		}
		else {
			throw new SnmpException(type + " is inconsistent with objectIdentifier", SnmpPduPacket.ErrWrongType);
		}
		return result;
	}

}