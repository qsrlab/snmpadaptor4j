/**
 * Package contains all classes and interfaces manipulated by classes as facade with SNMP APIs.
 */
package net.sf.snmpadaptor4j.api;