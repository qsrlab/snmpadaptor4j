package net.sf.snmpadaptor4j;

import java.util.List;
import net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration;

/**
 * Objects containing all configuration settings of snmpAdaptor4j.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public interface SnmpConfiguration
		extends SnmpDaemonConfiguration {

	/**
	 * Returns the parameter list to connect to each manager where to send all notifications (SNMP traps).
	 * @return Parameter list to connect to each manager.
	 */
	List<SnmpManagerConfiguration> getManagerList ();

}