/**
 * Package containing all simple objects having no treatment.
 */
package net.sf.snmpadaptor4j.object;