package net.sf.snmpadaptor4j.object;

/**
 * Enumeration of SNMP data type.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public enum SnmpDataType {

	/**
	 * Signed 32-bit integer.
	 */
	integer32,

	/**
	 * Non-negative 32-bit integer.
	 */
	unsigned32,

	/**
	 * Non-negative 32-bit gauge.
	 */
	gauge32,

	/**
	 * Non-negative, non-decreasing and not-writable 32-bit integer for a counter.
	 */
	counter32,

	/**
	 * Non-negative, non-decreasing and not-writable 64-bit integer for a counter.
	 */
	counter64,

	/**
	 * Non-negative and non-decreasing 32-bit integer for an elapsed time in hundredths of a second between two epochs.
	 */
	timeTicks,

	/**
	 * Array of ASCII code.
	 */
	octetString,

	/**
	 * Internet address.
	 */
	ipAddress,

	/**
	 * OID of SNMP <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
	 */
	objectIdentifier,

	/**
	 * Any data type.
	 */
	opaque;

}