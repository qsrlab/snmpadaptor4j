package net.sf.snmpadaptor4j.object;

import java.io.Serializable;

/**
 * Object representing a data of a SNMP trap.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SnmpTrapData
		implements Serializable {

	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = -8267283230287605599L;

	/**
	 * SNMP data type.
	 */
	private final SnmpDataType type;

	/**
	 * Value of data.
	 */
	private final Object value;

	/**
	 * Constructor.
	 * @param type SNMP data type.
	 * @param value Value of data.
	 */
	public SnmpTrapData (final SnmpDataType type, final Object value) {
		super();
		this.type = type;
		this.value = value;
	}

	/**
	 * Returns the SNMP data type.
	 * @return SNMP data type.
	 */
	public SnmpDataType getType () {
		return this.type;
	}

	/**
	 * Returns the value of data.
	 * @return Value of data.
	 */
	public Object getValue () {
		return this.value;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.type == null) ? 0 : this.type.hashCode());
		result = prime * result + ((this.value == null) ? 0 : this.value.hashCode());
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean result = false;
		if (obj == this) {
			result = true;
		}
		else if ((obj != null) && (obj.getClass() == getClass())) {
			final SnmpTrapData other = (SnmpTrapData) obj;
			if (other.type == this.type) {
				result = (this.value != null ? this.value.equals(other.value) : (other.value == null));
			}
		}
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "(" + this.type + ") " + this.value;
	}

}