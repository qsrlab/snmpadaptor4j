package net.sf.snmpadaptor4j.object;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * Object representing a specific or generic SNMP trap.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public abstract class SnmpTrap
		implements Serializable {

	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = -2950288105650647983L;

	/**
	 * Elapsed time in <b>100th of second</b> since the application launch.
	 */
	private final long timeStamp;

	/**
	 * OID indexing the source of the trap.
	 */
	private final SnmpOid source;

	/**
	 * Map of "interesting" information.
	 */
	private final Map<SnmpOid, SnmpTrapData> dataMap;

	/**
	 * Creates and returns an object representing a specific SNMP trap.
	 * @param timeStamp Elapsed time in <b>100th of second</b> since the application launch.
	 * @param source OID indexing the source of the trap (must not be <code>NULL</code>).
	 * @param type Trap type.
	 * @param dataMap Map of "interesting" information.
	 * @return Object representing a specific SNMP trap.
	 */
	public static SpecificSnmpTrap newInstance (final long timeStamp, final SnmpOid source, final int type, final Map<SnmpOid, SnmpTrapData> dataMap) {
		return new SpecificSnmpTrap(timeStamp, source, type, dataMap);
	}

	/**
	 * Creates and returns an object representing a generic SNMP trap.
	 * @param timeStamp Elapsed time in <b>100th of second</b> since the application launch.
	 * @param source OID indexing the source of the trap (must not be <code>NULL</code>).
	 * @param type Trap type (must not be <code>NULL</code>).
	 * @param dataMap Map of "interesting" information.
	 * @return Object representing a generic SNMP trap.
	 */
	public static GenericSnmpTrap newInstance (final long timeStamp, final SnmpOid source, final GenericSnmpTrapType type, final Map<SnmpOid, SnmpTrapData> dataMap) {
		return new GenericSnmpTrap(timeStamp, source, type, dataMap);
	}

	/**
	 * Hidden constructor (abstract class).
	 * @param timeStamp Elapsed time in <b>100th of second</b> since the application launch.
	 * @param source OID indexing the source of the trap.
	 * @param dataMap Map of "interesting" information.
	 */
	protected SnmpTrap (final long timeStamp, final SnmpOid source, final Map<SnmpOid, SnmpTrapData> dataMap) {
		super();
		this.timeStamp = timeStamp;
		this.source = source;
		this.dataMap = Collections.unmodifiableMap(dataMap != null ? dataMap : new HashMap<SnmpOid, SnmpTrapData>());
	}

	/**
	 * Returns the elapsed time in <b>100th of second</b> since the application launch.
	 * @return Elapsed time in <b>100th of second</b> since the application launch.
	 */
	public final long getTimeStamp () {
		return this.timeStamp;
	}

	/**
	 * Returns the OID indexing the source of the trap.
	 * @return OID indexing the source of the trap.
	 */
	public final SnmpOid getSource () {
		return this.source;
	}

	/**
	 * Returns the map of "interesting" information.
	 * @return Map of "interesting" information (never <code>NULL</code>).
	 */
	public final Map<SnmpOid, SnmpTrapData> getDataMap () {
		return this.dataMap;
	}

	/**
	 * Traces the content of SNMP trap to logs.
	 * @param logger Logger (must not be <code>NULL</code>).
	 */
	public abstract void traceTo (Logger logger);

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.dataMap.hashCode();
		result = prime * result + ((this.source == null) ? 0 : this.source.hashCode());
		result = prime * result + (int) (this.timeStamp ^ (this.timeStamp >>> 32));
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean result = false;
		if ((obj != null) && getClass().equals(obj.getClass())) {
			final SnmpTrap other = (SnmpTrap) obj;
			result = (this.timeStamp == other.timeStamp);
			if (result) {
				result = (this.source != null ? this.source.equals(other.source) : (other.source == null));
			}
			if (result) {
				result = this.dataMap.equals(other.dataMap);
			}
		}
		return result;
	}

}