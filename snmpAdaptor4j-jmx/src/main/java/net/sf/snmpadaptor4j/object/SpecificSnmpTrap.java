package net.sf.snmpadaptor4j.object;

import java.util.Map;
import java.util.Map.Entry;
import org.apache.log4j.Logger;

/**
 * Object representing a specific SNMP trap.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SpecificSnmpTrap
		extends SnmpTrap {

	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = 2590666174837567870L;

	/**
	 * Trap type.
	 */
	private final int type;

	/**
	 * Hidden constructor.
	 * @param timeStamp Elapsed time in <b>100th of second</b> since the application launch.
	 * @param source OID indexing the source of the trap.
	 * @param type Trap type.
	 * @param dataMap Map of "interesting" information.
	 * @see SnmpTrap#newInstance(long, SnmpOid, int, Map)
	 */
	protected SpecificSnmpTrap (final long timeStamp, final SnmpOid source, final int type, final Map<SnmpOid, SnmpTrapData> dataMap) {
		super(timeStamp, source, dataMap);
		this.type = type;
	}

	/**
	 * Returns the trap type.
	 * @return Trap type.
	 */
	public int getType () {
		return this.type;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.object.SnmpTrap#traceTo(org.apache.log4j.Logger)
	 */
	@Override
	public void traceTo (final Logger logger) {
		logger.trace("enterprise         = " + getSource());
		logger.trace("generic-trap       = enterpriseSpecific");
		logger.trace("specific-trap      = " + this.type);
		logger.trace("time-stamp         = " + getTimeStamp());
		for (final Entry<SnmpOid, SnmpTrapData> data : getDataMap().entrySet()) {
			logger.trace("variable-bindings: " + data.getKey() + " = " + data.getValue());
		}
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + this.type;
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean result = false;
		if (obj == this) {
			result = true;
		}
		else {
			result = super.equals(obj);
			if (result) {
				final SpecificSnmpTrap other = (SpecificSnmpTrap) obj;
				result = (this.type == other.type);
			}
		}
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "SNMP trap " + getSource() + " - Type enterpriseSpecific/" + this.type;
	}

}