package net.sf.snmpadaptor4j.object;

import java.io.Serializable;
import java.util.Arrays;
import java.util.StringTokenizer;

/**
 * Object representing an object identifier in a SNMP <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB).
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SnmpOid
		implements Comparable<SnmpOid>, Serializable {

	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = -1099844003876586949L;

	/**
	 * OID: system.sysName.0
	 */
	public static SnmpOid SYSNAME_OID = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 5, 0 });

	/**
	 * OID: system.sysDescr.0
	 */
	public static SnmpOid SYSDESCR_OID = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 1, 0 });

	/**
	 * OID: system.sysLocation.0
	 */
	public static SnmpOid SYSLOCATION_OID = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 6, 0 });

	/**
	 * OID: system.sysContact.0
	 */
	public static SnmpOid SYSCONTACT_OID = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 4, 0 });

	/**
	 * OID: system.sysUpTime.0
	 */
	public static SnmpOid SYSUPTIME_OID = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 3, 0 });

	/**
	 * Object identifier as numeric sequence.
	 */
	private final int[] oid;

	/**
	 * Creates and returns a new instance of {@link SnmpOid} by its string value.
	 * @param oid Object identifier as string.
	 * @param nodeNum Node number to insert at the last position of object identifier.
	 * @param index Index to insert at the last position of object identifier.
	 * @return New instance of {@link SnmpOid}.
	 */
	public static SnmpOid newInstance (final String oid, final int nodeNum, final int index) {
		final StringTokenizer tokens = new StringTokenizer(oid.trim(), ".");
		int[] array = new int[tokens.countTokens() + 2];
		int i = 0;
		while (tokens.hasMoreTokens()) {
			array[i] = Integer.parseInt(tokens.nextToken());
			i++;
		}
		array[i] = nodeNum;
		array[i + 1] = index;
		return new SnmpOid(array);
	}

	/**
	 * Creates and returns a new instance of {@link SnmpOid} by an another {@link SnmpOid}.
	 * @param oid Object identifier as {@link SnmpOid}.
	 * @param nodeNum Node number to insert at the last position of object identifier.
	 * @param index Index to insert at the last position of object identifier.
	 * @return New instance of {@link SnmpOid}.
	 */
	public static SnmpOid newInstance (final SnmpOid oid, final int nodeNum, final int index) {
		final int[] array = new int[oid.oid.length + 2];
		for (int i = 0; i < oid.oid.length; i++) {
			array[i] = oid.oid[i];
		}
		array[oid.oid.length] = nodeNum;
		array[oid.oid.length + 1] = index;
		return new SnmpOid(array);
	}

	/**
	 * Creates and returns a new instance of {@link SnmpOid} by its string value.
	 * @param oid Object identifier as string.
	 * @return New instance of {@link SnmpOid}.
	 */
	public static SnmpOid newInstance (final String oid) {
		final StringTokenizer tokens = new StringTokenizer(oid.trim(), ".");
		int[] array = new int[tokens.countTokens()];
		int i = 0;
		while (tokens.hasMoreTokens()) {
			array[i] = Integer.parseInt(tokens.nextToken());
			i++;
		}
		return new SnmpOid(array);
	}

	/**
	 * Creates and returns a new instance of {@link SnmpOid} by its numeric sequence.
	 * @param oid Object identifier as numeric sequence.
	 * @param index Index to insert at the last position of object identifier.
	 * @return New instance of {@link SnmpOid}.
	 */
	public static SnmpOid newInstance (final int[] oid, final int index) {
		int[] newOid = new int[oid.length + 1];
		for (int i = 0; i < oid.length; i++) {
			newOid[i] = oid[i];
		}
		newOid[oid.length] = index;
		return new SnmpOid(newOid);
	}

	/**
	 * Creates and returns a new instance of {@link SnmpOid} by its numeric sequence.
	 * @param oid Object identifier as numeric sequence.
	 * @return New instance of {@link SnmpOid}.
	 */
	public static SnmpOid newInstance (final int[] oid) {
		return new SnmpOid(oid);
	}

	/**
	 * Hidden constructor.
	 * @param oid Object identifier as numeric sequence.
	 */
	private SnmpOid (final int[] oid) {
		super();
		this.oid = oid;
	}

	/**
	 * Returns the object identifier as numeric sequence.
	 * @return Object identifier.
	 */
	public int[] getOid () {
		return this.oid;
	}

	/**
	 * Returns the index part of the object identifier.
	 * @return Index part of the object identifier.
	 */
	public int getIndex () {
		return this.oid[this.oid.length - 1];
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Arrays.hashCode(this.oid);
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean result = false;
		if (obj == this) {
			result = true;
		}
		else if ((obj != null) && (getClass().equals(obj.getClass()))) {
			result = Arrays.equals(this.oid, ((SnmpOid) obj).oid);
		}
		return result;
	}

	/**
	 * Returns <code>TRUE</code> if the OID without its index is equals to another OID.
	 * @param other Another OID.
	 * @return <code>TRUE</code> if the OID without its index is equals to another OID.
	 */
	public boolean equalsWithoutIndex (final SnmpOid other) {
		boolean result = false;
		if ((other != null) && (other.oid.length == (this.oid.length - 1))) {
			result = true;
			int i = 0;
			while ((i < other.oid.length) && result) {
				result = (other.oid[i] == this.oid[i]);
				i++;
			}
		}
		return result;
	}

	/**
	 * Returns <code>TRUE</code> if the OID is the root of another OID passed as parameter.
	 * @param other Another OID.
	 * @return <code>TRUE</code> if the OID is the root of another OID passed as parameter.
	 */
	public boolean isRootOf (final SnmpOid other) {
		boolean result = false;
		if ((other != null) && (other.oid.length >= this.oid.length)) {
			result = true;
			int i = 0;
			while ((i < this.oid.length) && result) {
				result = (other.oid[i] == this.oid[i]);
				i++;
			}
		}
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo (final SnmpOid obj) {
		int result = 0;
		int i = 0;
		boolean compare = true;
		while ((i < this.oid.length) && compare) {
			if (i < obj.oid.length) {
				if (this.oid[i] != obj.oid[i]) {
					result = this.oid[i] - obj.oid[i];
					compare = false;
				}
			}
			else {
				result = 1;
				compare = false;
			}
			i++;
		}
		if (compare && (i < obj.oid.length)) {
			result = -1;
		}
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		String result = "";
		for (final int num : this.oid) {
			result = result + "." + num;
		}
		return result;
	}

}