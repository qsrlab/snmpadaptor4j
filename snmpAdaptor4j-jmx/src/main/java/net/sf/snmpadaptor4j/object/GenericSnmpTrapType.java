package net.sf.snmpadaptor4j.object;

/**
 * Enumeration of generic type of SNMP traps.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public enum GenericSnmpTrapType {

	/**
	 * RFC1157 says:
	 * <p>
	 * A coldStart(0) trap signifies that the sending protocol entity is reinitializing itself such that the agent's configuration or the protocol entity
	 * implementation may be altered.
	 * </p>
	 */
	coldStart,

	/**
	 * RFC1157 says:
	 * <p>
	 * A warmStart(1) trap signifies that the sending protocol entity is reinitializing itself such that neither the agent configuration nor the protocol entity
	 * implementation is altered.
	 * </p>
	 */
	warmStart,

	/**
	 * RFC1157 says:
	 * <p>
	 * A linkDown(2) trap signifies that the sending protocol entity recognizes a failure in one of the communication links represented in the agent's configuration.
	 * </p>
	 * <p>
	 * The Trap-PDU of type linkDown contains as the first element of its variable-bindings, the name and value of the ifIndex instance for the affected interface.
	 * </p>
	 */
	linkDown,

	/**
	 * RFC1157 says:
	 * <p>
	 * A linkUp(3) trap signifies that the sending protocol entity recognizes that one of the communication links represented in the agent's configuration has come
	 * up.
	 * </p>
	 * <p>
	 * The Trap-PDU of type linkUp contains as the first element of its variable-bindings, the name and value of the ifIndex instance for the affected interface.
	 * </p>
	 */
	linkUp,

	/**
	 * RFC1157 says:
	 * <p>
	 * An authenticationFailure(4) trap signifies that the sending protocol entity is the addressee of a protocol message that is not properly authenticated. While
	 * implementations of the SNMP must be capable of generating this trap, they must also be capable of suppressing the emission of such traps via an
	 * implementation-specific mechanism.
	 * </p>
	 */
	authenticationFailure,

	/**
	 * RFC1157 says:
	 * <p>
	 * An egpNeighborLoss(5) trap signifies that an EGP neighbor for whom the sending protocol entity was an EGP peer has been marked down and the peer relationship
	 * no longer obtains.
	 * </p>
	 * <p>
	 * The Trap-PDU of type egpNeighborLoss contains as the first element of its variable-bindings, the name and value of the egpNeighAddr instance for the affected
	 * neighbor.
	 * </p>
	 */
	egpNeighborLoss;

}