package net.sf.snmpadaptor4j.object;

import java.util.Map;
import java.util.Map.Entry;
import org.apache.log4j.Logger;

/**
 * Object representing a generic SNMP trap.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class GenericSnmpTrap
		extends SnmpTrap {

	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = -6485304599608042063L;

	/**
	 * Trap type.
	 */
	private final GenericSnmpTrapType type;

	/**
	 * Hidden constructor.
	 * @param timeStamp Elapsed time in <b>100th of second</b> since the application launch.
	 * @param source OID indexing the source of the trap.
	 * @param type Trap type.
	 * @param dataMap Map of "interesting" information.
	 * @see SnmpTrap#newInstance(long, SnmpOid, GenericSnmpTrapType, Map)
	 */
	protected GenericSnmpTrap (final long timeStamp, final SnmpOid source, final GenericSnmpTrapType type, final Map<SnmpOid, SnmpTrapData> dataMap) {
		super(timeStamp, source, dataMap);
		this.type = type;
	}

	/**
	 * Returns the trap type.
	 * @return Trap type.
	 */
	public GenericSnmpTrapType getType () {
		return this.type;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.object.SnmpTrap#traceTo(org.apache.log4j.Logger)
	 */
	@Override
	public void traceTo (final Logger logger) {
		logger.trace("enterprise         = " + getSource());
		logger.trace("generic-trap       = " + this.type);
		logger.trace("specific-trap      = 0");
		logger.trace("time-stamp         = " + getTimeStamp());
		for (final Entry<SnmpOid, SnmpTrapData> data : getDataMap().entrySet()) {
			logger.trace("variable-bindings: " + data.getKey() + " = " + data.getValue());
		}
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.type == null) ? 0 : this.type.hashCode());
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean result = false;
		if (obj == this) {
			result = true;
		}
		else {
			result = super.equals(obj);
			if (result) {
				final GenericSnmpTrap other = (GenericSnmpTrap) obj;
				result = (this.type == other.type);
			}
		}
		return result;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "SNMP trap " + getSource() + " - Type " + this.type + "/0";
	}

}