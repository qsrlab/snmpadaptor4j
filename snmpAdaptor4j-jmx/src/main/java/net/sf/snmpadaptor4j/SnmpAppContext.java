package net.sf.snmpadaptor4j;

import java.util.Map;
import javax.management.ObjectName;

/**
 * Objects containing the context of an application.
 * <p>
 * SnmpAdaptor4j may be used by an application server. In this case you will inform the SNMP adapter to each deployment of a new application.
 * </p>
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public interface SnmpAppContext {

	/**
	 * Returns the default root OID containing the attributes of the application.
	 * @return Default root OID.
	 */
	String getDefaultRootOid ();

	/**
	 * Returns the map of root OIDs where the attributes of the application will stay.
	 * @return Map of root OIDs (never <code>NULL</code> but can be empty).
	 */
	Map<String, String> getRootOidMap ();

	/**
	 * Returns the map of MBean OIDs.
	 * @return Map of MBean OIDs (never <code>NULL</code> but can be empty).
	 */
	Map<ObjectName, String> getMBeanOidMap ();

}