/**
 * Package containing all classes providing the configuration loading of SNMP adaptor from an XML file.
 */
package net.sf.snmpadaptor4j.config;