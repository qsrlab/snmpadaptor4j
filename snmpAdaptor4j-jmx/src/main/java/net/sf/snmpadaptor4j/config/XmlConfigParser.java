package net.sf.snmpadaptor4j.config;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.management.ObjectName;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import net.sf.snmpadaptor4j.SnmpAppContext;
import net.sf.snmpadaptor4j.SnmpConfiguration;
import net.sf.snmpadaptor4j.SnmpManagerConfiguration;
import net.sf.snmpadaptor4j.config.jaxb.Config;
import net.sf.snmpadaptor4j.config.jaxb.Daemon;
import net.sf.snmpadaptor4j.config.jaxb.MBean;
import net.sf.snmpadaptor4j.config.jaxb.Manager;
import net.sf.snmpadaptor4j.config.jaxb.Root;
import net.sf.snmpadaptor4j.config.jaxb.Roots;

/**
 * XML parser of SNMP configuration.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class XmlConfigParser
		implements SnmpConfiguration, SnmpAppContext {

	/**
	 * {@link URL} to the SNMP configuration file.
	 */
	private final URL url;

	/**
	 * XSD object representing the daemon configuration.
	 */
	private final Daemon daemon;

	/**
	 * List of managers where to send all notifications (SNMP traps).
	 */
	private final List<SnmpManagerConfiguration> managerList;

	/**
	 * XSD object containing all root OIDs.
	 */
	private final Roots roots;

	/**
	 * Map of root OIDs where the attributes of the application will stay.
	 */
	private final Map<String, String> rootOidMap;

	/**
	 * Map of MBean OIDs.
	 */
	private final Map<ObjectName, String> mBeanOidMap;

	/**
	 * Creates and returns a new instance of {@link XmlConfigParser}.
	 * @param url {@link URL} to the SNMP configuration file.
	 * @return New instance of {@link XmlConfigParser}.
	 * @throws Exception Exception if an error occurred.
	 */
	public static XmlConfigParser newInstance (final URL url) throws Exception {
		final JAXBContext jaxbContext = JAXBContext.newInstance(Config.class.getPackage().getName());
		final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		@SuppressWarnings("unchecked")
		final JAXBElement<Config> jaxbElement = (JAXBElement<Config>) unmarshaller.unmarshal(url);
		final Config config = jaxbElement.getValue();

		// managerList
		final List<SnmpManagerConfiguration> managerList = new ArrayList<SnmpManagerConfiguration>();
		if (config.getManagers() != null) {
			for (final Manager manager : config.getManagers().getManager()) {
				managerList.add(new SnmpManagerConfiguration(manager.getAddress(), manager.getPort(), manager.getVersion(), manager.getCommunity()));
			}
		}

		// rootOidMap
		final Map<String, String> rootOidMap = new HashMap<String, String>();
		for (final Root root : config.getRoots().getRoot()) {
			if (root.getOid().trim().length() > 0) {
				rootOidMap.put(root.getId(), root.getOid());
			}
		}

		// mBeanOidMap
		final Map<ObjectName, String> mBeanOidMap = new HashMap<ObjectName, String>();
		if (config.getMbeans() != null) {
			String oid;
			for (final MBean mBean : config.getMbeans().getMbean()) {
				oid = null;
				if (mBean.getRoot() != null) {
					oid = rootOidMap.get(mBean.getRoot());
				}
				if (oid == null) {
					oid = config.getRoots().getDefault();
				}
				oid = oid + "." + mBean.getOid();
				mBeanOidMap.put(new ObjectName(mBean.getName()), oid);
			}
		}

		return new XmlConfigParser(url, config.getDaemon(), managerList, config.getRoots(), rootOidMap, mBeanOidMap);
	}

	/**
	 * Constructor.
	 * @param url {@link URL} to the SNMP configuration file.
	 * @param daemon XSD object representing the daemon configuration.
	 * @param managerList List of managers where to send all notifications (SNMP traps).
	 * @param roots XSD object containing all root OIDs.
	 * @param rootOidMap Map of root OIDs where the attributes of the application will stay.
	 * @param mBeanOidMap Map of MBean OIDs.
	 */
	private XmlConfigParser (final URL url, final Daemon daemon, final List<SnmpManagerConfiguration> managerList, final Roots roots,
			final Map<String, String> rootOidMap, final Map<ObjectName, String> mBeanOidMap) {
		super();
		this.url = url;
		this.daemon = daemon;
		this.managerList = Collections.unmodifiableList(managerList);
		this.roots = roots;
		this.rootOidMap = Collections.unmodifiableMap(rootOidMap);
		this.mBeanOidMap = Collections.unmodifiableMap(mBeanOidMap);
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerAddress()
	 */
	public String getListenerAddress () {
		return this.daemon.getAddress();
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerPort()
	 */
	public Integer getListenerPort () {
		return new Integer(this.daemon.getPort());
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerSnmpVersion()
	 */
	public Integer getListenerSnmpVersion () {
		return new Integer(this.daemon.getVersion());
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerReadCommunity()
	 */
	public String getListenerReadCommunity () {
		return this.daemon.getReadCommunity();
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerWriteCommunity()
	 */
	public String getListenerWriteCommunity () {
		return this.daemon.getWriteCommunity();
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpConfiguration#getManagerList()
	 */
	public List<SnmpManagerConfiguration> getManagerList () {
		return this.managerList;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpAppContext#getDefaultRootOid()
	 */
	public String getDefaultRootOid () {
		return this.roots.getDefault();
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpAppContext#getRootOidMap()
	 */
	public Map<String, String> getRootOidMap () {
		return this.rootOidMap;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpAppContext#getMBeanOidMap()
	 */
	public Map<ObjectName, String> getMBeanOidMap () {
		return this.mBeanOidMap;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "XmlConfigParser[" + this.url + "]";
	}

}