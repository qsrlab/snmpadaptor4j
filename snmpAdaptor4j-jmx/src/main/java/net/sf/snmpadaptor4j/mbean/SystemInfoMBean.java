package net.sf.snmpadaptor4j.mbean;

/**
 * MBean containing all informations on the system.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public interface SystemInfoMBean {

	/**
	 * Returns the name given to the system (<code>system.sysName.0</code>).
	 * @return Name given to the system.
	 */
	String getSysName ();

	/**
	 * Sets the name given to the system (<code>system.sysName.0</code>).
	 * @param sysName Name given to the system.
	 */
	void setSysName (String sysName);

	/**
	 * Returns the description on the system (<code>system.sysDescr.0</code>).
	 * @return Description on the system.
	 */
	String getSysDescr ();

	/**
	 * Sets the description on the system (<code>system.sysDescr.0</code>).
	 * @param sysDescr Description on the system.
	 */
	void setSysDescr (String sysDescr);

	/**
	 * Returns the location of the system (<code>system.sysLocation.0</code>).
	 * @return Location of the system.
	 */
	String getSysLocation ();

	/**
	 * Sets the location of the system (<code>system.sysLocation.0</code>).
	 * @param sysLocation Location of the system.
	 */
	void setSysLocation (String sysLocation);

	/**
	 * Returns the administrator contact of system (<code>system.sysContact.0</code>).
	 * @return Administrator contact of system.
	 */
	String getSysContact ();

	/**
	 * Sets the administrator contact of system (<code>system.sysContact.0</code>).
	 * @param sysContact Administrator contact of system.
	 */
	void setSysContact (String sysContact);

	/**
	 * Returns the elapsed time in milliseconds since the last boot (<code>system.sysUpTime.0</code>).
	 * @return Elapsed time in milliseconds since the last boot.
	 */
	long getSysUpTime ();

}