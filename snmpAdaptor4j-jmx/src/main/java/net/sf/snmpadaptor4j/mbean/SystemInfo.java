package net.sf.snmpadaptor4j.mbean;

import java.io.Serializable;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanConstructorInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanNotificationInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanParameterInfo;
import javax.management.NotCompliantMBeanException;
import javax.management.StandardMBean;

/**
 * MBean containing all informations on the system.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SystemInfo
		extends StandardMBean
		implements SystemInfoMBean, Serializable {

	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = -6965962577731511748L;

	/**
	 * Name given to the system (<code>system.sysName.0</code>).
	 */
	private String sysName;

	/**
	 * Description on the system (<code>system.sysDescr.0</code>).
	 */
	private String sysDescr;

	/**
	 * Location of the system (<code>system.sysLocation.0</code>).
	 */
	private String sysLocation;

	/**
	 * Administrator contact of system (<code>system.sysContact.0</code>).
	 */
	private String sysContact;

	/**
	 * Time in milliseconds at the last boot.
	 */
	private final long lastBootTime;

	/**
	 * Constructor.
	 * @throws NotCompliantMBeanException if the <var>mbeanInterface</var> does not follow JMX design patterns for Management Interfaces, or if <var>this</var> does
	 *             not implement the specified interface.
	 */
	public SystemInfo () throws NotCompliantMBeanException {
		this(null, null, null, null);
	}

	/**
	 * Constructor.
	 * @param sysName Name given to the system (<code>system.sysName.0</code>).
	 * @param sysDescr Description on the system (<code>system.sysDescr.0</code>).
	 * @throws NotCompliantMBeanException if the <var>mbeanInterface</var> does not follow JMX design patterns for Management Interfaces, or if <var>this</var> does
	 *             not implement the specified interface.
	 */
	public SystemInfo (final String sysName, final String sysDescr) throws NotCompliantMBeanException {
		this(sysName, sysDescr, null, null);
	}

	/**
	 * Constructor.
	 * @param sysName Name given to the system (<code>system.sysName.0</code>).
	 * @param sysDescr Description on the system (<code>system.sysDescr.0</code>).
	 * @param sysLocation Location of the system (<code>system.sysLocation.0</code>).
	 * @param sysContact Administrator contact of system (<code>system.sysContact.0</code>).
	 * @throws NotCompliantMBeanException if the <var>mbeanInterface</var> does not follow JMX design patterns for Management Interfaces, or if <var>this</var> does
	 *             not implement the specified interface.
	 */
	public SystemInfo (final String sysName, final String sysDescr, final String sysLocation, final String sysContact) throws NotCompliantMBeanException {
		super(SystemInfoMBean.class);
		final String sysNameDescription = "Name given to the Java application (corresponds to SNMP attribute system.sysName.0)";
		final String sysDescrDescription = "Description on the Java application (corresponds to SNMP attribute system.sysDescr.0)";
		final String sysLocationDescription = "Location of the computer hosting the Java application (corresponds to SNMP attribute system.sysLocation.0)";
		final String sysContactDescription = "Administrator contact of the Java application (corresponds to SNMP attribute system.sysContact.0)";
		final String sysUpTimeDescription = "Elapsed time in milliseconds since the last boot of the Java application (corresponds to SNMP attribute system.sysUpTime.0)";
		final MBeanAttributeInfo[] attributes = new MBeanAttributeInfo[] {
				new MBeanAttributeInfo("SysName", String.class.getName(), sysNameDescription, true, true, false),
				new MBeanAttributeInfo("SysDescr", String.class.getName(), sysDescrDescription, true, true, false),
				new MBeanAttributeInfo("SysLocation", String.class.getName(), sysLocationDescription, true, true, false),
				new MBeanAttributeInfo("SysContact", String.class.getName(), sysContactDescription, true, true, false),
				new MBeanAttributeInfo("SysUpTime", long.class.getName(), sysUpTimeDescription, true, false, false) };
		final MBeanConstructorInfo constructor1 = new MBeanConstructorInfo(getClass().getName(), "Constructor without parameter", new MBeanParameterInfo[] {});
		final MBeanConstructorInfo constructor2 = new MBeanConstructorInfo(getClass().getName(), "Constructor with only sysName and sysDescr parameters",
				new MBeanParameterInfo[] { new MBeanParameterInfo("sysName", String.class.getName(), sysNameDescription),
						new MBeanParameterInfo("sysDescr", String.class.getName(), sysDescrDescription) });
		final MBeanConstructorInfo constructor3 = new MBeanConstructorInfo(getClass().getName(), "Constructor with all parameters", new MBeanParameterInfo[] {
				new MBeanParameterInfo("sysName", String.class.getName(), sysNameDescription),
				new MBeanParameterInfo("sysDescr", String.class.getName(), sysDescrDescription),
				new MBeanParameterInfo("sysLocation", String.class.getName(), sysLocationDescription),
				new MBeanParameterInfo("sysContact", String.class.getName(), sysContactDescription) });
		final MBeanConstructorInfo[] constructors = new MBeanConstructorInfo[] { constructor1, constructor2, constructor3 };
		cacheMBeanInfo(new MBeanInfo(getClass().getName(), "Informations on the Java application (used by SNMP)", attributes, constructors,
				new MBeanOperationInfo[] {}, new MBeanNotificationInfo[] {}));
		this.sysName = (sysName != null ? (sysName.trim().length() > 0 ? sysName : "javaApp") : "javaApp");
		this.sysDescr = (sysDescr != null ? (sysDescr.trim().length() > 0 ? sysDescr : "Java application") : "Java application");
		this.sysLocation = (sysLocation != null ? (sysLocation.trim().length() > 0 ? sysLocation : null) : null);
		this.sysContact = (sysContact != null ? (sysContact.trim().length() > 0 ? sysContact : null) : null);
		this.lastBootTime = System.currentTimeMillis();
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.mbean.SystemInfoMBean#getSysName()
	 */
	public String getSysName () {
		return this.sysName;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.mbean.SystemInfoMBean#setSysName(java.lang.String)
	 */
	public void setSysName (final String sysName) {
		this.sysName = (sysName != null ? (sysName.trim().length() > 0 ? sysName : null) : null);
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.mbean.SystemInfoMBean#getSysDescr()
	 */
	public String getSysDescr () {
		return this.sysDescr;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.mbean.SystemInfoMBean#setSysDescr(java.lang.String)
	 */
	public void setSysDescr (final String sysDescr) {
		this.sysDescr = (sysDescr != null ? (sysDescr.trim().length() > 0 ? sysDescr : null) : null);
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.mbean.SystemInfoMBean#getSysLocation()
	 */
	public String getSysLocation () {
		return this.sysLocation;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.mbean.SystemInfoMBean#setSysLocation(java.lang.String)
	 */
	public void setSysLocation (final String sysLocation) {
		this.sysLocation = (sysLocation != null ? (sysLocation.trim().length() > 0 ? sysLocation : null) : null);
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.mbean.SystemInfoMBean#getSysContact()
	 */
	public String getSysContact () {
		return this.sysContact;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.mbean.SystemInfoMBean#setSysContact(java.lang.String)
	 */
	public void setSysContact (final String sysContact) {
		this.sysContact = (sysContact != null ? (sysContact.trim().length() > 0 ? sysContact : null) : null);
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.mbean.SystemInfoMBean#getSysUpTime()
	 */
	public long getSysUpTime () {
		return System.currentTimeMillis() - this.lastBootTime;
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return this.sysName + " (" + this.sysDescr + ") - location: " + this.sysLocation + " - contact: " + this.sysContact;
	}

}