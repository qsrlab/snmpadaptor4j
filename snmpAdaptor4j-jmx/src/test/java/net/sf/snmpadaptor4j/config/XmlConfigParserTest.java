package net.sf.snmpadaptor4j.config;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.management.ObjectName;
import net.sf.snmpadaptor4j.SnmpManagerConfiguration;
import net.sf.snmpadaptor4j.config.XmlConfigParser;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.config.XmlConfigParser}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class XmlConfigParserTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(XmlConfigParserTest.class);

	/**
	 * Tests the <b>snmp1.xml</b> loading.
	 * <p>
	 * Test method for {@link XmlConfigParser#newInstance(java.net.URL)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewInstance1 () throws Exception {
		this.logger.info("Tests the configuration loading with snmp1.xml...");
		final XmlConfigParser parser = XmlConfigParser.newInstance(getClass().getResource("/snmp1.xml"));
		assertEquals("Bad listening IP address of SNMP daemon", "192.168.1.1", parser.getListenerAddress());
		assertEquals("Bad UDP port number of SNMP daemon", new Integer(1161), parser.getListenerPort());
		assertEquals("Bad protocol version of SNMP daemon", new Integer(1), parser.getListenerSnmpVersion());
		assertEquals("Bad read community of SNMP daemon", "test.public", parser.getListenerReadCommunity());
		assertEquals("Bad write community of SNMP daemon", "test.private", parser.getListenerWriteCommunity());
		final List<SnmpManagerConfiguration> expectedManagerList = new ArrayList<SnmpManagerConfiguration>();
		expectedManagerList.add(new SnmpManagerConfiguration("192.168.1.2", 162, 2, "public"));
		expectedManagerList.add(new SnmpManagerConfiguration("192.168.1.3", 1162, 1, "trap.public"));
		assertEquals("Bad SNMP manager list", expectedManagerList, parser.getManagerList());
		assertEquals("Bad default root OID containing the attributes of application", "1.3.6.1.4.1.99.12.8.1", parser.getDefaultRootOid());
		final Map<String, String> rootOidMap = new HashMap<String, String>(parser.getRootOidMap());
		assertEquals("Bad root OID named framework", "1.3.6.1.4.1.9999.1", rootOidMap.remove("framework"));
		assertEquals("Bad root OID named example", "1.3.6.1.4.1.9999.50", rootOidMap.remove("example"));
		assertTrue("There is an unknown root OID", rootOidMap.isEmpty());
		final Map<ObjectName, String> mBeanOidMap = new HashMap<ObjectName, String>(parser.getMBeanOidMap());
		assertEquals("Bad MBean instance named net.sf.snmpadaptor4j.example:type=Example,id=1", "1.3.6.1.4.1.9999.50.1.1",
				mBeanOidMap.remove(new ObjectName("net.sf.snmpadaptor4j.example:type=Example,id=1")));
		assertEquals("Bad MBean instance named net.sf.snmpadaptor4j.example:type=Example,id=2", "1.3.6.1.4.1.9999.50.1.2",
				mBeanOidMap.remove(new ObjectName("net.sf.snmpadaptor4j.example:type=Example,id=2")));
		assertEquals("Bad MBean instance named net.sf.snmpadaptor4j.example:type=Example,id=3", "1.3.6.1.4.1.99.12.8.1.1.3",
				mBeanOidMap.remove(new ObjectName("net.sf.snmpadaptor4j.example:type=Example,id=3")));
		assertEquals("Bad MBean instance named net.sf.snmpadaptor4j.example:type=Example,id=4", "1.3.6.1.4.1.99.12.8.1.1.4",
				mBeanOidMap.remove(new ObjectName("net.sf.snmpadaptor4j.example:type=Example,id=4")));
		assertTrue("There is an unknown MBean instance", mBeanOidMap.isEmpty());
	}

	/**
	 * Tests the <b>snmp2.xml</b> loading.
	 * <p>
	 * Test method for {@link XmlConfigParser#newInstance(java.net.URL)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewInstance2 () throws Exception {
		this.logger.info("Tests the configuration loading with snmp2.xml...");
		final XmlConfigParser parser = XmlConfigParser.newInstance(getClass().getResource("/snmp2.xml"));
		assertEquals("Bad listening IP address of SNMP daemon", "127.0.0.1", parser.getListenerAddress());
		assertEquals("Bad UDP port number of SNMP daemon", new Integer(161), parser.getListenerPort());
		assertEquals("Bad protocol version of SNMP daemon", new Integer(2), parser.getListenerSnmpVersion());
		assertNull("Bad read community of SNMP daemon", parser.getListenerReadCommunity());
		assertNull("Bad write community of SNMP daemon", parser.getListenerWriteCommunity());
		assertTrue("There is an unknown SNMP manager", parser.getManagerList().isEmpty());
		assertEquals("Bad default root OID containing the attributes of application", "1.3.6.1.4.1.99.12.8.1", parser.getDefaultRootOid());
		assertTrue("There is an unknown root OID", parser.getRootOidMap().isEmpty());
		assertTrue("There is an unknown MBean instance", parser.getMBeanOidMap().isEmpty());
	}

	/**
	 * Test method for {@link XmlConfigParser#toString()}.
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testToString () throws Exception {
		this.logger.info("Tests of toString...");
		final XmlConfigParser parser = XmlConfigParser.newInstance(getClass().getResource("/snmp1.xml"));
		assertEquals("The value returned by toString is bad", "XmlConfigParser[" + getClass().getResource("/snmp1.xml") + "]", parser.toString());
	}

}