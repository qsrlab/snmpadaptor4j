package net.sf.snmpadaptor4j.mbean;

import static org.junit.Assert.*;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.mbean.SystemInfo}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SystemInfoTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(SystemInfoTest.class);

	/**
	 * Tests the constructor with none parameter.
	 * <p>
	 * Test method for {@link SystemInfo#SystemInfo()}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSystemInfoWithNoneParameter () throws Exception {
		this.logger.info("Tests the constructor with none parameter...");
		final SystemInfo systemInfo = new SystemInfo();
		assertEquals("sysName is bad", "javaApp", systemInfo.getSysName());
		assertEquals("sysDescr is bad", "Java application", systemInfo.getSysDescr());
		assertNull("sysLocation is not null", systemInfo.getSysLocation());
		assertNull("sysContact is not null", systemInfo.getSysContact());
	}

	/**
	 * Tests the constructor with a minimum of parameters.
	 * <p>
	 * Test method for {@link SystemInfo#SystemInfo(String, String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSystemInfoWithMinimumParameters () throws Exception {
		this.logger.info("Tests the constructor with a minimum of parameters...");
		final SystemInfo systemInfo = new SystemInfo("This is the name", "This is the description");
		assertEquals("sysName is bad", "This is the name", systemInfo.getSysName());
		assertEquals("sysDescr is bad", "This is the description", systemInfo.getSysDescr());
		assertNull("sysLocation is not null", systemInfo.getSysLocation());
		assertNull("sysContact is not null", systemInfo.getSysContact());
	}

	/**
	 * Tests the constructor with all parameters.
	 * <p>
	 * Test method for {@link SystemInfo#SystemInfo(String, String, String, String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSystemInfoWithAllParameters () throws Exception {
		this.logger.info("Tests the constructor with all parameters...");
		final SystemInfo systemInfo = new SystemInfo("This is the name", "This is the description", "This is the location", "This is the contact");
		assertEquals("sysName is bad", "This is the name", systemInfo.getSysName());
		assertEquals("sysDescr is bad", "This is the description", systemInfo.getSysDescr());
		assertEquals("sysLocation is bad", "This is the location", systemInfo.getSysLocation());
		assertEquals("sysContact is bad", "This is the contact", systemInfo.getSysContact());
	}

	/**
	 * Tests the constructor with all parameters of empty strings.
	 * <p>
	 * Test method for {@link SystemInfo#SystemInfo(String, String, String, String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSystemInfoWithAllParametersOfEmptyStrings () throws Exception {
		this.logger.info("Tests the constructor with all parameters of empty strings...");
		final SystemInfo systemInfo = new SystemInfo("  ", "  ", "  ", "  ");
		assertEquals("sysName is bad", "javaApp", systemInfo.getSysName());
		assertEquals("sysDescr is bad", "Java application", systemInfo.getSysDescr());
		assertNull("sysLocation is not null", systemInfo.getSysLocation());
		assertNull("sysContact is not null", systemInfo.getSysContact());
	}

	/**
	 * Tests the <b>sysName</b> setting.
	 * <p>
	 * Test method for {@link SystemInfo#setSysName(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetSysName () throws Exception {
		this.logger.info("Tests the sysName setting...");
		final SystemInfo systemInfo = new SystemInfo();
		systemInfo.setSysName("This is the name");
		assertEquals("sysName is bad", "This is the name", systemInfo.getSysName());
		assertEquals("sysDescr is bad", "Java application", systemInfo.getSysDescr());
		assertNull("sysLocation is not null", systemInfo.getSysLocation());
		assertNull("sysContact is not null", systemInfo.getSysContact());
	}

	/**
	 * Tests the <b>sysName</b> setting with a <code>NULL</code> value.
	 * <p>
	 * Test method for {@link SystemInfo#setSysName(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetSysNameWithNull () throws Exception {
		this.logger.info("Tests the sysName setting with a NULL value...");
		final SystemInfo systemInfo = new SystemInfo();
		systemInfo.setSysName(null);
		assertNull("sysName is not null", systemInfo.getSysName());
		assertEquals("sysDescr is bad", "Java application", systemInfo.getSysDescr());
		assertNull("sysLocation is not null", systemInfo.getSysLocation());
		assertNull("sysContact is not null", systemInfo.getSysContact());
	}

	/**
	 * Tests the <b>sysName</b> setting with an empty String.
	 * <p>
	 * Test method for {@link SystemInfo#setSysName(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetSysNameWithEmptyString () throws Exception {
		this.logger.info("Tests the sysName setting with an empty String...");
		final SystemInfo systemInfo = new SystemInfo();
		systemInfo.setSysName("  ");
		assertNull("sysName is not null", systemInfo.getSysName());
		assertEquals("sysDescr is bad", "Java application", systemInfo.getSysDescr());
		assertNull("sysLocation is not null", systemInfo.getSysLocation());
		assertNull("sysContact is not null", systemInfo.getSysContact());
	}

	/**
	 * Tests the <b>sysDescr</b> setting.
	 * <p>
	 * Test method for {@link SystemInfo#setSysDescr(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetSysDescr () throws Exception {
		this.logger.info("Tests the sysDescr setting...");
		final SystemInfo systemInfo = new SystemInfo();
		systemInfo.setSysDescr("This is the description");
		assertEquals("sysName is bad", "javaApp", systemInfo.getSysName());
		assertEquals("sysDescr is bad", "This is the description", systemInfo.getSysDescr());
		assertNull("sysLocation is not null", systemInfo.getSysLocation());
		assertNull("sysContact is not null", systemInfo.getSysContact());
	}

	/**
	 * Tests the <b>sysDescr</b> setting with a <code>NULL</code> value.
	 * <p>
	 * Test method for {@link SystemInfo#setSysDescr(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetSysDescrWithNull () throws Exception {
		this.logger.info("Tests the sysDescr setting with a NULL value...");
		final SystemInfo systemInfo = new SystemInfo();
		systemInfo.setSysDescr(null);
		assertEquals("sysName is bad", "javaApp", systemInfo.getSysName());
		assertNull("sysDescr is not null", systemInfo.getSysDescr());
		assertNull("sysLocation is not null", systemInfo.getSysLocation());
		assertNull("sysContact is not null", systemInfo.getSysContact());
	}

	/**
	 * Tests the <b>sysDescr</b> setting with an empty String.
	 * <p>
	 * Test method for {@link SystemInfo#setSysDescr(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetSysDescrWithEmptyString () throws Exception {
		this.logger.info("Tests the sysDescr setting with an empty String...");
		final SystemInfo systemInfo = new SystemInfo();
		systemInfo.setSysDescr("  ");
		assertEquals("sysName is bad", "javaApp", systemInfo.getSysName());
		assertNull("sysDescr is not null", systemInfo.getSysDescr());
		assertNull("sysLocation is not null", systemInfo.getSysLocation());
		assertNull("sysContact is not null", systemInfo.getSysContact());
	}

	/**
	 * Tests the <b>sysLocation</b> setting.
	 * <p>
	 * Test method for {@link SystemInfo#setSysLocation(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetSysLocation () throws Exception {
		this.logger.info("Tests the sysLocation setting...");
		final SystemInfo systemInfo = new SystemInfo();
		systemInfo.setSysLocation("This is the location");
		assertEquals("sysName is bad", "javaApp", systemInfo.getSysName());
		assertEquals("sysDescr is bad", "Java application", systemInfo.getSysDescr());
		assertEquals("sysLocation is bad", "This is the location", systemInfo.getSysLocation());
		assertNull("sysContact is not null", systemInfo.getSysContact());
	}

	/**
	 * Tests the <b>sysLocation</b> setting with a <code>NULL</code> value.
	 * <p>
	 * Test method for {@link SystemInfo#setSysLocation(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetSysLocationWithNull () throws Exception {
		this.logger.info("Tests the sysLocation setting with a NULL value...");
		final SystemInfo systemInfo = new SystemInfo(null, null, "My location", "My contact");
		systemInfo.setSysLocation(null);
		assertEquals("sysName is bad", "javaApp", systemInfo.getSysName());
		assertEquals("sysDescr is bad", "Java application", systemInfo.getSysDescr());
		assertNull("sysLocation is not null", systemInfo.getSysLocation());
		assertEquals("sysContact is bad", "My contact", systemInfo.getSysContact());
	}

	/**
	 * Tests the <b>sysLocation</b> setting with an empty String.
	 * <p>
	 * Test method for {@link SystemInfo#setSysLocation(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetSysLocationWithEmptyString () throws Exception {
		this.logger.info("Tests the sysLocation setting with an empty String...");
		final SystemInfo systemInfo = new SystemInfo(null, null, "My location", "My contact");
		systemInfo.setSysLocation("  ");
		assertEquals("sysName is bad", "javaApp", systemInfo.getSysName());
		assertEquals("sysDescr is bad", "Java application", systemInfo.getSysDescr());
		assertNull("sysLocation is not null", systemInfo.getSysLocation());
		assertEquals("sysContact is bad", "My contact", systemInfo.getSysContact());
	}

	/**
	 * Tests the <b>sysContact</b> setting.
	 * <p>
	 * Test method for {@link SystemInfo#setSysContact(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetSysContact () throws Exception {
		this.logger.info("Tests the sysLocation setting...");
		final SystemInfo systemInfo = new SystemInfo();
		systemInfo.setSysContact("This is the contact");
		assertEquals("sysName is bad", "javaApp", systemInfo.getSysName());
		assertEquals("sysDescr is bad", "Java application", systemInfo.getSysDescr());
		assertNull("sysLocation is not null", systemInfo.getSysLocation());
		assertEquals("sysContact is bad", "This is the contact", systemInfo.getSysContact());
	}

	/**
	 * Tests the <b>sysContact</b> setting with a <code>NULL</code> value.
	 * <p>
	 * Test method for {@link SystemInfo#setSysContact(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetSysContactWithNull () throws Exception {
		this.logger.info("Tests the sysLocation setting with a NULL value...");
		final SystemInfo systemInfo = new SystemInfo(null, null, "My location", "My contact");
		systemInfo.setSysContact(null);
		assertEquals("sysName is bad", "javaApp", systemInfo.getSysName());
		assertEquals("sysDescr is bad", "Java application", systemInfo.getSysDescr());
		assertEquals("sysLocation is bad", "My location", systemInfo.getSysLocation());
		assertNull("sysContact is not null", systemInfo.getSysContact());
	}

	/**
	 * Tests the <b>sysContact</b> setting with an empty String.
	 * <p>
	 * Test method for {@link SystemInfo#setSysContact(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetSysContactWithEmptyString () throws Exception {
		this.logger.info("Tests the sysLocation setting with an empty String...");
		final SystemInfo systemInfo = new SystemInfo(null, null, "My location", "My contact");
		systemInfo.setSysContact("  ");
		assertEquals("sysName is bad", "javaApp", systemInfo.getSysName());
		assertEquals("sysDescr is bad", "Java application", systemInfo.getSysDescr());
		assertEquals("sysLocation is bad", "My location", systemInfo.getSysLocation());
		assertNull("sysContact is not null", systemInfo.getSysContact());
	}

	/**
	 * Tests the <b>sysUpTime</b> getting.
	 * <p>
	 * Test method for {@link SystemInfo#getSysUpTime()}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testGetSysUpTime () throws Exception {
		this.logger.info("Tests the sysUpTime getting...");
		final SystemInfo systemInfo = new SystemInfo();
		long sysUpTime = systemInfo.getSysUpTime();
		assertTrue("step 1: sysUpTime is bad", sysUpTime < 500);
		Thread.sleep(500);
		sysUpTime = systemInfo.getSysUpTime() - sysUpTime;
		assertTrue("step 2: sysUpTime is bad", (sysUpTime >= 500) && (sysUpTime <= 600));
	}

	/**
	 * Test method for {@link SystemInfo#toString()}.
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testToString () throws Exception {
		this.logger.info("Tests the toString...");
		final SystemInfo systemInfo = new SystemInfo("MySystem", "My description", "My location", "My contact");
		assertEquals("The value returned by toString is bad", "MySystem (My description) - location: My location - contact: My contact", systemInfo.toString());
	}

}