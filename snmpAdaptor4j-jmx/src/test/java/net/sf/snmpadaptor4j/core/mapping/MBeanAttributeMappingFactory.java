package net.sf.snmpadaptor4j.core.mapping;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;

/**
 * Factory of mapping for access to a MBean attribute.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class MBeanAttributeMappingFactory {

	/**
	 * Singleton instance.
	 */
	private static final MBeanAttributeMappingFactory INSTANCE = new MBeanAttributeMappingFactory();

	/**
	 * Returns the singleton instance.
	 * @return Singleton instance.
	 */
	public static MBeanAttributeMappingFactory getInstance () {
		return MBeanAttributeMappingFactory.INSTANCE;
	}

	/**
	 * Hidden constructor (singleton).
	 * @see MBeanAttributeMappingFactory#getInstance()
	 */
	private MBeanAttributeMappingFactory () {
		super();
	}

	/**
	 * Creates and returns the expected list of mappings for access to a MBean attribute from <b>Example.snmp.xml</b>.
	 * @param mBeanRootOid Root OID of MBean.
	 * @return Expected list of mappings for access to a MBean attribute.
	 */
	public List<MBeanAttributeMapping> newExpectedAttributeMappingsFromExample (final String mBeanRootOid) {
		final List<MBeanAttributeMapping> mappingList = new ArrayList<MBeanAttributeMapping>();
		mappingList.add(MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(SnmpOid.newInstance(mBeanRootOid + ".1.0"), "LongAttribute",
				SnmpDataType.counter64, long.class, true, true));
		mappingList.add(MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(SnmpOid.newInstance(mBeanRootOid + ".2.0"), "StringAttribute",
				SnmpDataType.octetString, String.class, false, false));
		mappingList.add(MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(SnmpOid.newInstance(mBeanRootOid + ".3.0"), "HiddenAttribute",
				SnmpDataType.opaque, Object.class, false, false));
		mappingList.add(MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(SnmpOid.newInstance(mBeanRootOid + ".4.0"), "InetAddressAttribute",
				SnmpDataType.ipAddress, InetAddress.class, true, false));
		mappingList.add(MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(SnmpOid.newInstance(mBeanRootOid + ".5.0"), "TimesticksAttribute",
				SnmpDataType.timeTicks, long.class, true, false));
		mappingList.add(MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(SnmpOid.newInstance(mBeanRootOid + ".6.0"), "ByteAttribute",
				SnmpDataType.integer32, byte.class, true, false));
		mappingList.add(MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(SnmpOid.newInstance(mBeanRootOid + ".7.0"), "ShortAttribute",
				SnmpDataType.gauge32, short.class, true, false));
		mappingList.add(MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(SnmpOid.newInstance(mBeanRootOid + ".8.0"), "IntAttribute",
				SnmpDataType.unsigned32, int.class, true, false));
		mappingList.add(MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(SnmpOid.newInstance(mBeanRootOid + ".9.0"), "BooleanAttribute",
				SnmpDataType.integer32, boolean.class, true, true));
		mappingList.add(MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(SnmpOid.newInstance(mBeanRootOid + ".10.0"), "ByteArrayAttribute",
				SnmpDataType.opaque, byte[].class, true, false));
		mappingList.add(MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(SnmpOid.newInstance(mBeanRootOid + ".11.0"), "ShortArrayAttribute",
				SnmpDataType.opaque, short[].class, true, false));
		mappingList.add(MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(SnmpOid.newInstance(mBeanRootOid + ".12.0"), "IntArrayAttribute",
				SnmpDataType.opaque, int[].class, true, false));
		mappingList.add(MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(SnmpOid.newInstance(mBeanRootOid + ".13.0"), "LongArrayAttribute",
				SnmpDataType.opaque, long[].class, true, false));
		return mappingList;
	}

	/**
	 * Creates and returns the expected list of mappings for access to a MBean attribute from <b>ExampleB.snmp.xml</b>.
	 * @param mBeanRootOid Root OID of MBean.
	 * @return Expected list of mappings for access to a MBean attribute.
	 */
	public List<MBeanAttributeMapping> newExpectedAttributeMappingsFromExampleB (final String mBeanRootOid) {
		final List<MBeanAttributeMapping> mappingList = new ArrayList<MBeanAttributeMapping>();
		mappingList.add(MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(SnmpOid.newInstance(mBeanRootOid + ".1.0"), "LongAttribute",
				SnmpDataType.counter64, long.class, true, true));
		return mappingList;
	}

	/**
	 * Creates and returns a new mapping for access to a MBean attribute.
	 * @param oid Object Identifier of MIB node.
	 * @param attributeName Attribute name.
	 * @param snmpDataType SNMP data type of MIB node.
	 * @param jmxDataType Data type of JMX attribute.
	 * @param readable <code>TRUE</code> if the attribute can be read (for SNMP write and read community), <code>FALSE</code> otherwise.
	 * @param writable <code>TRUE</code> if the attribute can be write (for SNMP write community), <code>FALSE</code> otherwise.
	 * @return New mapping for access to a MBean attribute.
	 */
	public MBeanAttributeMapping newMBeanAttributeMapping (final SnmpOid oid, final String attributeName, final SnmpDataType snmpDataType,
			final Class<?> jmxDataType, final boolean readable, final boolean writable) {
		return new MBeanAttributeMapping(oid, attributeName, snmpDataType, jmxDataType, readable, writable);
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "MBeanAttributeMappingFactory";
	}

}