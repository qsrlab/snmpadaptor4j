package net.sf.snmpadaptor4j.core;

import static org.junit.Assert.*;
import java.lang.management.ManagementFactory;
import java.math.BigInteger;
import java.net.InetAddress;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import net.sf.snmpadaptor4j.api.SnmpMib;
import net.sf.snmpadaptor4j.api.AttributeAccessor;
import net.sf.snmpadaptor4j.core.accessor.MBeanAttributeAccessor;
import net.sf.snmpadaptor4j.core.mapping.MBeanAttributeMapping;
import net.sf.snmpadaptor4j.core.mapping.MBeanAttributeMappingFactory;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import net.sf.snmpadaptor4j.test.mbean.Example;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.core.JmxSnmpMib}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class JmxSnmpMibTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(JmxSnmpMibTest.class);

	/**
	 * MBean name.
	 */
	private final ObjectName mBeanName1 = new ObjectName("net.sf.snmpadaptor4j.test:type=Example,id=1");

	/**
	 * MBean name.
	 */
	private final ObjectName mBeanName2 = new ObjectName("net.sf.snmpadaptor4j.test:type=Example,id=2");

	/**
	 * MBean name for a test with a MBean a MBean not registered.
	 */
	private final ObjectName mBeanNameNotRegistered = new ObjectName("net.sf.snmpadaptor4j.test:type=Example,id=notRegistered");

	/**
	 * Constructor.
	 * @throws Exception Exception if an error has occurred.
	 */
	public JmxSnmpMibTest () throws Exception {
		super();
	}

	/**
	 * Test initialization.
	 * @throws Exception Exception if an error has occurred.
	 */
	@Before
	public void setUp () throws Exception {
		ManagementFactory.getPlatformMBeanServer().registerMBean(new Example(), this.mBeanName1);
	}

	/**
	 * Test finalization.
	 * @throws Exception Exception if an error has occurred.
	 */
	@After
	public void tearDown () throws Exception {
		ManagementFactory.getPlatformMBeanServer().unregisterMBean(this.mBeanName1);
	}

	/**
	 * Tests the unregistration of all MBean attributes.
	 * <p>
	 * Test method for {@link JmxSnmpMib#unregisterAllAttributes()}.
	 * </p>
	 */
	@Test
	public void testUnregisterAllAttributes () {
		testUnregisterAllAttributes(false);
		testUnregisterAllAttributes(true);
	}

	/**
	 * Tests the unregistration of all MBean attributes.
	 * <p>
	 * Test method for {@link JmxSnmpMib#unregisterAllAttributes()}.
	 * </p>
	 * @param allLevel <code>TRUE</code> for trace all logs.
	 */
	public void testUnregisterAllAttributes (final boolean allLevel) {
		this.logger.info("Tests the unregistration of all MBean attributes (log level = " + (allLevel ? "ALL" : "OFF") + ")...");
		final JmxSnmpMib jmxMib = new JmxSnmpMib(null);
		jmxMib.logger.setLevel(allLevel ? Level.ALL : Level.OFF);
		final String rootOid = "1.3.6.1.4.1.9999.1";
		assertTrue("The MIB is not empty", jmxMib.getMib().isEmpty());
		loadMib(jmxMib, this.mBeanName2, rootOid + "1.1");
		assertEquals("The loading of MIB has failed", 5, jmxMib.getMib().size());
		loadMib(jmxMib, this.mBeanName1, rootOid + "1.2");
		assertEquals("The loading of MIB has failed", 10, jmxMib.getMib().size());

		// Test
		jmxMib.unregisterAllAttributes();

		// Checking
		assertTrue("The MIB is bad", jmxMib.getMib().isEmpty());

	}

	/**
	 * Tests the registration of a MBean attributes.
	 * <p>
	 * Test method for {@link JmxSnmpMib#registerAttributes(MBeanServer, ObjectName, List)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testRegisterAttributes () throws Exception {
		testRegisterAttributes(false);
		testRegisterAttributes(true);
	}

	/**
	 * Tests the registration of a MBean attributes.
	 * <p>
	 * Test method for {@link JmxSnmpMib#registerAttributes(MBeanServer, ObjectName, List)}.
	 * </p>
	 * @param allLevel <code>TRUE</code> for trace all logs.
	 * @throws Exception Exception if an error has occurred.
	 */
	public void testRegisterAttributes (final boolean allLevel) throws Exception {
		this.logger.info("Tests the registration of a MBean attributes (log level = " + (allLevel ? "ALL" : "OFF") + ")...");
		final JmxSnmpMib jmxMib = new JmxSnmpMib(null);
		jmxMib.logger.setLevel(allLevel ? Level.ALL : Level.OFF);
		final String rootOid = "1.3.6.1.4.1.9999.1";
		final List<MBeanAttributeMapping> mBeanAttributeMappingList = MBeanAttributeMappingFactory.getInstance().newExpectedAttributeMappingsFromExample(
				rootOid + ".1.1");
		assertTrue("The MIB is not empty", jmxMib.getMib().isEmpty());

		// Test
		jmxMib.registerAttributes(ManagementFactory.getPlatformMBeanServer(), this.mBeanName1, mBeanAttributeMappingList);

		// Checking
		assertEquals("The MIB is bad", mBeanAttributeMappingList.size(), jmxMib.getMib().size());

	}

	/**
	 * Tests the unregistration of a MBean attributes not registered.
	 * <p>
	 * Test method for {@link JmxSnmpMib#unregisterAttributes(MBeanServer, ObjectName)}.
	 * </p>
	 */
	@Test
	public void testUnregisterAttributesNotRegistered () {
		testUnregisterAttributesNotRegistered(false);
		testUnregisterAttributesNotRegistered(true);
	}

	/**
	 * Tests the unregistration of a MBean attributes not registered.
	 * <p>
	 * Test method for {@link JmxSnmpMib#unregisterAttributes(MBeanServer, ObjectName)}.
	 * </p>
	 * @param allLevel <code>TRUE</code> for trace all logs.
	 */
	public void testUnregisterAttributesNotRegistered (final boolean allLevel) {
		this.logger.info("Tests the unregistration of a MBean attributes not registered (log level = " + (allLevel ? "ALL" : "OFF") + ")...");
		final JmxSnmpMib jmxMib = new JmxSnmpMib(null);
		jmxMib.logger.setLevel(allLevel ? Level.ALL : Level.OFF);
		final String rootOid = "1.3.6.1.4.1.9999.1";
		assertTrue("The MIB is not empty", jmxMib.getMib().isEmpty());
		loadMib(jmxMib, this.mBeanName2, rootOid + "1.1");
		assertEquals("The loading of MIB has failed", 5, jmxMib.getMib().size());
		loadMib(jmxMib, this.mBeanName1, rootOid + "1.2");
		assertEquals("The loading of MIB has failed", 10, jmxMib.getMib().size());

		// Test
		jmxMib.unregisterAttributes(ManagementFactory.getPlatformMBeanServer(), this.mBeanNameNotRegistered);

		// Checking
		assertEquals("The MIB is bad", 10, jmxMib.getMib().size());

	}

	/**
	 * Tests the unregistration of a MBean attributes.
	 * <p>
	 * Test method for {@link JmxSnmpMib#unregisterAttributes(MBeanServer, ObjectName)}.
	 * </p>
	 */
	@Test
	public void testUnregisterAttributes () {
		testUnregisterAttributes(false);
		testUnregisterAttributes(true);
	}

	/**
	 * Tests the unregistration of a MBean attributes.
	 * <p>
	 * Test method for {@link JmxSnmpMib#unregisterAttributes(MBeanServer, ObjectName)}.
	 * </p>
	 * @param allLevel <code>TRUE</code> for trace all logs.
	 */
	public void testUnregisterAttributes (final boolean allLevel) {
		this.logger.info("Tests the unregistration of a MBean attributes (log level = " + (allLevel ? "ALL" : "OFF") + ")...");
		final JmxSnmpMib jmxMib = new JmxSnmpMib(null);
		jmxMib.logger.setLevel(allLevel ? Level.ALL : Level.OFF);
		final String rootOid = "1.3.6.1.4.1.9999.1";
		assertTrue("The MIB is not empty", jmxMib.getMib().isEmpty());
		loadMib(jmxMib, this.mBeanName2, rootOid + "1.1");
		assertEquals("The loading of MIB has failed", 5, jmxMib.getMib().size());
		loadMib(jmxMib, this.mBeanName1, rootOid + "1.2");
		assertEquals("The loading of MIB has failed", 10, jmxMib.getMib().size());

		// Test
		jmxMib.unregisterAttributes(ManagementFactory.getPlatformMBeanServer(), this.mBeanName2);

		// Checking
		assertEquals("The MIB is bad", 5, jmxMib.getMib().size());

	}

	/**
	 * Tests the consistency checking between the integer32 and Java data types.
	 * <p>
	 * Test method for {@link JmxSnmpMib#checkDataType(SnmpDataType, Class)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testCheckDataTypeForInteger32 () throws Exception {
		this.logger.info("Tests the consistency checking between the integer32 and Java data types...");
		JmxSnmpMib.checkDataType(SnmpDataType.integer32, byte.class);
		JmxSnmpMib.checkDataType(SnmpDataType.integer32, Byte.class);
		JmxSnmpMib.checkDataType(SnmpDataType.integer32, short.class);
		JmxSnmpMib.checkDataType(SnmpDataType.integer32, Short.class);
		JmxSnmpMib.checkDataType(SnmpDataType.integer32, int.class);
		JmxSnmpMib.checkDataType(SnmpDataType.integer32, Integer.class);
		JmxSnmpMib.checkDataType(SnmpDataType.integer32, boolean.class);
		JmxSnmpMib.checkDataType(SnmpDataType.integer32, Boolean.class);
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.integer32, long.class);
			fail("None exception for long type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for long type", "integer32 is inconsistent with long", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.integer32, Long.class);
			fail("None exception for Long type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for Long type", "integer32 is inconsistent with java.lang.Long", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.integer32, BigInteger.class);
			fail("None exception for BigInteger type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for BigInteger type", "integer32 is inconsistent with java.math.BigInteger", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.integer32, InetAddress.class);
			fail("None exception for InetAddress type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for InetAddress type", "integer32 is inconsistent with java.net.InetAddress", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.integer32, SnmpOid.class);
			fail("None exception for SnmpOid type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for SnmpOid type", "integer32 is inconsistent with net.sf.snmpadaptor4j.object.SnmpOid", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.integer32, String.class);
			fail("None exception for String type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for String type", "integer32 is inconsistent with java.lang.String", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.integer32, byte[].class);
			fail("None exception for byte[] type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for byte[] type", "integer32 is inconsistent with [B", e.getMessage());
		}
	}

	/**
	 * Tests the consistency checking between the unsigned32 and Java data types.
	 * <p>
	 * Test method for {@link JmxSnmpMib#checkDataType(SnmpDataType, Class)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testCheckDataTypeForUnsigned32 () throws Exception {
		this.logger.info("Tests the consistency checking between the unsigned32 and Java data types...");
		JmxSnmpMib.checkDataType(SnmpDataType.unsigned32, byte.class);
		JmxSnmpMib.checkDataType(SnmpDataType.unsigned32, Byte.class);
		JmxSnmpMib.checkDataType(SnmpDataType.unsigned32, short.class);
		JmxSnmpMib.checkDataType(SnmpDataType.unsigned32, Short.class);
		JmxSnmpMib.checkDataType(SnmpDataType.unsigned32, int.class);
		JmxSnmpMib.checkDataType(SnmpDataType.unsigned32, Integer.class);
		JmxSnmpMib.checkDataType(SnmpDataType.unsigned32, long.class);
		JmxSnmpMib.checkDataType(SnmpDataType.unsigned32, Long.class);
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.unsigned32, BigInteger.class);
			fail("None exception for BigInteger type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for BigInteger type", "unsigned32 is inconsistent with java.math.BigInteger", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.unsigned32, InetAddress.class);
			fail("None exception for InetAddress type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for InetAddress type", "unsigned32 is inconsistent with java.net.InetAddress", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.unsigned32, SnmpOid.class);
			fail("None exception for SnmpOid type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for SnmpOid type", "unsigned32 is inconsistent with net.sf.snmpadaptor4j.object.SnmpOid", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.unsigned32, String.class);
			fail("None exception for String type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for String type", "unsigned32 is inconsistent with java.lang.String", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.unsigned32, byte[].class);
			fail("None exception for byte[] type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for byte[] type", "unsigned32 is inconsistent with [B", e.getMessage());
		}
	}

	/**
	 * Tests the consistency checking between the gauge32 and Java data types.
	 * <p>
	 * Test method for {@link JmxSnmpMib#checkDataType(SnmpDataType, Class)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testCheckDataTypeForGauge32 () throws Exception {
		this.logger.info("Tests the consistency checking between the gauge32 and Java data types...");
		JmxSnmpMib.checkDataType(SnmpDataType.gauge32, byte.class);
		JmxSnmpMib.checkDataType(SnmpDataType.gauge32, Byte.class);
		JmxSnmpMib.checkDataType(SnmpDataType.gauge32, short.class);
		JmxSnmpMib.checkDataType(SnmpDataType.gauge32, Short.class);
		JmxSnmpMib.checkDataType(SnmpDataType.gauge32, int.class);
		JmxSnmpMib.checkDataType(SnmpDataType.gauge32, Integer.class);
		JmxSnmpMib.checkDataType(SnmpDataType.gauge32, long.class);
		JmxSnmpMib.checkDataType(SnmpDataType.gauge32, Long.class);
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.gauge32, BigInteger.class);
			fail("None exception for BigInteger type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for BigInteger type", "gauge32 is inconsistent with java.math.BigInteger", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.gauge32, InetAddress.class);
			fail("None exception for InetAddress type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for InetAddress type", "gauge32 is inconsistent with java.net.InetAddress", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.gauge32, SnmpOid.class);
			fail("None exception for SnmpOid type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for SnmpOid type", "gauge32 is inconsistent with net.sf.snmpadaptor4j.object.SnmpOid", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.gauge32, String.class);
			fail("None exception for String type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for String type", "gauge32 is inconsistent with java.lang.String", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.gauge32, byte[].class);
			fail("None exception for byte[] type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for byte[] type", "gauge32 is inconsistent with [B", e.getMessage());
		}
	}

	/**
	 * Tests the consistency checking between the counter32 and Java data types.
	 * <p>
	 * Test method for {@link JmxSnmpMib#checkDataType(SnmpDataType, Class)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testCheckDataTypeForCounter32 () throws Exception {
		this.logger.info("Tests the consistency checking between the counter32 and Java data types...");
		JmxSnmpMib.checkDataType(SnmpDataType.counter32, byte.class);
		JmxSnmpMib.checkDataType(SnmpDataType.counter32, Byte.class);
		JmxSnmpMib.checkDataType(SnmpDataType.counter32, short.class);
		JmxSnmpMib.checkDataType(SnmpDataType.counter32, Short.class);
		JmxSnmpMib.checkDataType(SnmpDataType.counter32, int.class);
		JmxSnmpMib.checkDataType(SnmpDataType.counter32, Integer.class);
		JmxSnmpMib.checkDataType(SnmpDataType.counter32, long.class);
		JmxSnmpMib.checkDataType(SnmpDataType.counter32, Long.class);
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.counter32, BigInteger.class);
			fail("None exception for BigInteger type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for BigInteger type", "counter32 is inconsistent with java.math.BigInteger", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.counter32, InetAddress.class);
			fail("None exception for InetAddress type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for InetAddress type", "counter32 is inconsistent with java.net.InetAddress", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.counter32, SnmpOid.class);
			fail("None exception for SnmpOid type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for SnmpOid type", "counter32 is inconsistent with net.sf.snmpadaptor4j.object.SnmpOid", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.counter32, String.class);
			fail("None exception for String type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for String type", "counter32 is inconsistent with java.lang.String", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.counter32, byte[].class);
			fail("None exception for byte[] type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for byte[] type", "counter32 is inconsistent with [B", e.getMessage());
		}
	}

	/**
	 * Tests the consistency checking between the counter64 and Java data types.
	 * <p>
	 * Test method for {@link JmxSnmpMib#checkDataType(SnmpDataType, Class)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testCheckDataTypeForCounter64 () throws Exception {
		this.logger.info("Tests the consistency checking between the counter64 and Java data types...");
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.counter64, byte.class);
			fail("None exception for byte type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for byte type", "counter64 is inconsistent with byte", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.counter64, Byte.class);
			fail("None exception for Byte type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for Byte type", "counter64 is inconsistent with java.lang.Byte", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.counter64, short.class);
			fail("None exception for short type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for short type", "counter64 is inconsistent with short", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.counter64, Short.class);
			fail("None exception for Short type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for Short type", "counter64 is inconsistent with java.lang.Short", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.counter64, int.class);
			fail("None exception for int type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for int type", "counter64 is inconsistent with int", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.counter64, Integer.class);
			fail("None exception for Integer type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for Integer type", "counter64 is inconsistent with java.lang.Integer", e.getMessage());
		}
		JmxSnmpMib.checkDataType(SnmpDataType.counter64, long.class);
		JmxSnmpMib.checkDataType(SnmpDataType.counter64, Long.class);
		JmxSnmpMib.checkDataType(SnmpDataType.counter64, BigInteger.class);
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.counter64, InetAddress.class);
			fail("None exception for InetAddress type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for InetAddress type", "counter64 is inconsistent with java.net.InetAddress", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.counter64, SnmpOid.class);
			fail("None exception for SnmpOid type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for SnmpOid type", "counter64 is inconsistent with net.sf.snmpadaptor4j.object.SnmpOid", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.counter64, String.class);
			fail("None exception for String type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for String type", "counter64 is inconsistent with java.lang.String", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.counter64, byte[].class);
			fail("None exception for byte[] type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for byte[] type", "counter64 is inconsistent with [B", e.getMessage());
		}
	}

	/**
	 * Tests the consistency checking between the timeTicks and Java data types.
	 * <p>
	 * Test method for {@link JmxSnmpMib#checkDataType(SnmpDataType, Class)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testCheckDataTypeForTimeTicks () throws Exception {
		this.logger.info("Tests the consistency checking between the timeTicks and Java data types...");
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.timeTicks, byte.class);
			fail("None exception for byte type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for byte type", "timeTicks is inconsistent with byte", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.timeTicks, Byte.class);
			fail("None exception for Byte type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for Byte type", "timeTicks is inconsistent with java.lang.Byte", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.timeTicks, short.class);
			fail("None exception for short type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for short type", "timeTicks is inconsistent with short", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.timeTicks, Short.class);
			fail("None exception for Short type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for Short type", "timeTicks is inconsistent with java.lang.Short", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.timeTicks, int.class);
			fail("None exception for int type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for int type", "timeTicks is inconsistent with int", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.timeTicks, Integer.class);
			fail("None exception for Integer type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for Integer type", "timeTicks is inconsistent with java.lang.Integer", e.getMessage());
		}
		JmxSnmpMib.checkDataType(SnmpDataType.timeTicks, long.class);
		JmxSnmpMib.checkDataType(SnmpDataType.timeTicks, Long.class);
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.timeTicks, BigInteger.class);
			fail("None exception for BigInteger type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for BigInteger type", "timeTicks is inconsistent with java.math.BigInteger", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.timeTicks, InetAddress.class);
			fail("None exception for InetAddress type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for InetAddress type", "timeTicks is inconsistent with java.net.InetAddress", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.timeTicks, SnmpOid.class);
			fail("None exception for SnmpOid type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for SnmpOid type", "timeTicks is inconsistent with net.sf.snmpadaptor4j.object.SnmpOid", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.timeTicks, String.class);
			fail("None exception for String type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for String type", "timeTicks is inconsistent with java.lang.String", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.timeTicks, byte[].class);
			fail("None exception for byte[] type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for byte[] type", "timeTicks is inconsistent with [B", e.getMessage());
		}
	}

	/**
	 * Tests the consistency checking between the octetString and Java data types.
	 * <p>
	 * Test method for {@link JmxSnmpMib#checkDataType(SnmpDataType, Class)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testCheckDataTypeForOctetString () throws Exception {
		this.logger.info("Tests the consistency checking between the octetString and Java data types...");
		JmxSnmpMib.checkDataType(SnmpDataType.octetString, byte.class);
		JmxSnmpMib.checkDataType(SnmpDataType.octetString, Byte.class);
		JmxSnmpMib.checkDataType(SnmpDataType.octetString, short.class);
		JmxSnmpMib.checkDataType(SnmpDataType.octetString, Short.class);
		JmxSnmpMib.checkDataType(SnmpDataType.octetString, int.class);
		JmxSnmpMib.checkDataType(SnmpDataType.octetString, Integer.class);
		JmxSnmpMib.checkDataType(SnmpDataType.octetString, long.class);
		JmxSnmpMib.checkDataType(SnmpDataType.octetString, Long.class);
		JmxSnmpMib.checkDataType(SnmpDataType.octetString, BigInteger.class);
		JmxSnmpMib.checkDataType(SnmpDataType.octetString, InetAddress.class);
		JmxSnmpMib.checkDataType(SnmpDataType.octetString, SnmpOid.class);
		JmxSnmpMib.checkDataType(SnmpDataType.octetString, String.class);
		JmxSnmpMib.checkDataType(SnmpDataType.octetString, byte[].class);
	}

	/**
	 * Tests the consistency checking between the ipAddress and Java data types.
	 * <p>
	 * Test method for {@link JmxSnmpMib#checkDataType(SnmpDataType, Class)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testCheckDataTypeForIpAddress () throws Exception {
		this.logger.info("Tests the consistency checking between the ipAddress and Java data types...");
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.ipAddress, byte.class);
			fail("None exception for byte type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for byte type", "ipAddress is inconsistent with byte", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.ipAddress, Byte.class);
			fail("None exception for Byte type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for Byte type", "ipAddress is inconsistent with java.lang.Byte", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.ipAddress, short.class);
			fail("None exception for short type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for short type", "ipAddress is inconsistent with short", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.ipAddress, Short.class);
			fail("None exception for Short type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for Short type", "ipAddress is inconsistent with java.lang.Short", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.ipAddress, int.class);
			fail("None exception for int type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for int type", "ipAddress is inconsistent with int", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.ipAddress, Integer.class);
			fail("None exception for Integer type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for Integer type", "ipAddress is inconsistent with java.lang.Integer", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.ipAddress, long.class);
			fail("None exception for long type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for long type", "ipAddress is inconsistent with long", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.ipAddress, Long.class);
			fail("None exception for Long type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for Long type", "ipAddress is inconsistent with java.lang.Long", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.ipAddress, BigInteger.class);
			fail("None exception for BigInteger type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for BigInteger type", "ipAddress is inconsistent with java.math.BigInteger", e.getMessage());
		}
		JmxSnmpMib.checkDataType(SnmpDataType.ipAddress, InetAddress.class);
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.ipAddress, SnmpOid.class);
			fail("None exception for SnmpOid type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for SnmpOid type", "ipAddress is inconsistent with net.sf.snmpadaptor4j.object.SnmpOid", e.getMessage());
		}
		JmxSnmpMib.checkDataType(SnmpDataType.ipAddress, String.class);
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.ipAddress, byte[].class);
			fail("None exception for byte[] type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for byte[] type", "ipAddress is inconsistent with [B", e.getMessage());
		}
	}

	/**
	 * Tests the consistency checking between the objectIdentifier and Java data types.
	 * <p>
	 * Test method for {@link JmxSnmpMib#checkDataType(SnmpDataType, Class)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testCheckDataTypeForObjectIdentifier () throws Exception {
		this.logger.info("Tests the consistency checking between the objectIdentifier and Java data types...");
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.objectIdentifier, byte.class);
			fail("None exception for byte type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for byte type", "objectIdentifier is inconsistent with byte", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.objectIdentifier, Byte.class);
			fail("None exception for Byte type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for Byte type", "objectIdentifier is inconsistent with java.lang.Byte", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.objectIdentifier, short.class);
			fail("None exception for short type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for short type", "objectIdentifier is inconsistent with short", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.objectIdentifier, Short.class);
			fail("None exception for Short type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for Short type", "objectIdentifier is inconsistent with java.lang.Short", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.objectIdentifier, int.class);
			fail("None exception for int type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for int type", "objectIdentifier is inconsistent with int", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.objectIdentifier, Integer.class);
			fail("None exception for Integer type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for Integer type", "objectIdentifier is inconsistent with java.lang.Integer", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.objectIdentifier, long.class);
			fail("None exception for long type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for long type", "objectIdentifier is inconsistent with long", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.objectIdentifier, Long.class);
			fail("None exception for Long type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for Long type", "objectIdentifier is inconsistent with java.lang.Long", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.objectIdentifier, BigInteger.class);
			fail("None exception for BigInteger type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for BigInteger type", "objectIdentifier is inconsistent with java.math.BigInteger", e.getMessage());
		}
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.objectIdentifier, InetAddress.class);
			fail("None exception for InetAddress type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for InetAddress type", "objectIdentifier is inconsistent with java.net.InetAddress", e.getMessage());
		}
		JmxSnmpMib.checkDataType(SnmpDataType.objectIdentifier, SnmpOid.class);
		JmxSnmpMib.checkDataType(SnmpDataType.objectIdentifier, String.class);
		try {
			JmxSnmpMib.checkDataType(SnmpDataType.objectIdentifier, byte[].class);
			fail("None exception for byte[] type");
		}
		catch (final Exception e) {
			assertEquals("Bad error message for byte[] type", "objectIdentifier is inconsistent with [B", e.getMessage());
		}
	}

	/**
	 * Tests the consistency checking between the opaque and Java data types.
	 * <p>
	 * Test method for {@link JmxSnmpMib#checkDataType(SnmpDataType, Class)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testCheckDataTypeForOpaque () throws Exception {
		this.logger.info("Tests the consistency checking between the opaque and Java data types...");
		JmxSnmpMib.checkDataType(SnmpDataType.opaque, byte.class);
		JmxSnmpMib.checkDataType(SnmpDataType.opaque, Byte.class);
		JmxSnmpMib.checkDataType(SnmpDataType.opaque, short.class);
		JmxSnmpMib.checkDataType(SnmpDataType.opaque, Short.class);
		JmxSnmpMib.checkDataType(SnmpDataType.opaque, int.class);
		JmxSnmpMib.checkDataType(SnmpDataType.opaque, Integer.class);
		JmxSnmpMib.checkDataType(SnmpDataType.opaque, long.class);
		JmxSnmpMib.checkDataType(SnmpDataType.opaque, Long.class);
		JmxSnmpMib.checkDataType(SnmpDataType.opaque, BigInteger.class);
		JmxSnmpMib.checkDataType(SnmpDataType.opaque, InetAddress.class);
		JmxSnmpMib.checkDataType(SnmpDataType.opaque, SnmpOid.class);
		JmxSnmpMib.checkDataType(SnmpDataType.opaque, String.class);
		JmxSnmpMib.checkDataType(SnmpDataType.opaque, byte[].class);
	}

	/**
	 * Tests a research of SnmpMibExternalNode not found.
	 * <p>
	 * Test method for {@link JmxSnmpMib#find(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testFindNotFound () {
		this.logger.info("Tests a research of SnmpMibExternalNode not found...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpMib otherSnmpMib = mocksControl.createMock(SnmpMib.class);
		final JmxSnmpMib jmxSnmpMib = new JmxSnmpMib(otherSnmpMib);
		assertTrue("MBeans were loaded", jmxSnmpMib.getMib().isEmpty());
		loadMib(jmxSnmpMib, this.mBeanName1, ".1.3.6.1.4.1.9999.1.1.1");
		loadMib(jmxSnmpMib, this.mBeanName2, ".1.3.6.1.4.1.9999.1.1.2");
		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.4.1.9999.99");

		// Scenario
		otherSnmpMib.find(EasyMock.eq(oid));
		EasyMock.expectLastCall().andReturn(null);
		mocksControl.replay();

		// Test
		final AttributeAccessor node = jmxSnmpMib.find(oid);

		// Checking
		mocksControl.verify();
		assertNull("A SnmpMibExternalNode was found", node);

	}

	/**
	 * Tests a research of SnmpMibExternalNode found by another MIB.
	 * <p>
	 * Test method for {@link JmxSnmpMib#find(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testFindFoundByOtherMib () {
		this.logger.info("Tests a research of SnmpMibExternalNode found by another MIB...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpMib otherSnmpMib = mocksControl.createMock(SnmpMib.class);
		final JmxSnmpMib jmxSnmpMib = new JmxSnmpMib(otherSnmpMib);
		assertTrue("MBeans were loaded", jmxSnmpMib.getMib().isEmpty());
		loadMib(jmxSnmpMib, this.mBeanName1, ".1.3.6.1.4.1.9999.1.1.1");
		loadMib(jmxSnmpMib, this.mBeanName2, ".1.3.6.1.4.1.9999.1.1.2");
		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.4.1.9999.99");
		final MBeanAttributeMapping mapping = MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(oid, "longAttribute", SnmpDataType.counter64,
				long.class, true, true);
		final MBeanAttributeAccessor expectedNode = new MBeanAttributeAccessor(null, null, mapping);

		// Scenario
		otherSnmpMib.find(EasyMock.eq(oid));
		EasyMock.expectLastCall().andReturn(expectedNode);
		mocksControl.replay();

		// Test
		final AttributeAccessor node = jmxSnmpMib.find(oid);

		// Checking
		mocksControl.verify();
		assertNotNull("The SnmpMibExternalNode was not found", node);
		assertEquals("The SnmpMibExternalNode is bad", expectedNode, node);

	}

	/**
	 * Tests a research of SnmpMibExternalNode found.
	 * <p>
	 * Test method for {@link JmxSnmpMib#find(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testFindFound () {
		this.logger.info("Tests a research of SnmpMibExternalNode found...");
		final JmxSnmpMib jmxSnmpMib = new JmxSnmpMib(null);
		assertTrue("MBeans were loaded", jmxSnmpMib.getMib().isEmpty());
		loadMib(jmxSnmpMib, this.mBeanName1, ".1.3.6.1.4.1.9999.1.1.1");
		loadMib(jmxSnmpMib, this.mBeanName2, ".1.3.6.1.4.1.9999.1.1.2");
		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.1.5.0");

		// Test
		final AttributeAccessor node = jmxSnmpMib.find(oid);

		// Checking
		assertNotNull("The SnmpMibExternalNode was not found", node);
		assertEquals("The SnmpMibExternalNode is bad", oid, node.getOid());

	}

	/**
	 * Tests a research of following SnmpMibExternalNode.
	 * <p>
	 * Test method for {@link JmxSnmpMib#next(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testNext () {
		this.logger.info("Tests a research of following SnmpMibExternalNode...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpMib otherSnmpMib = mocksControl.createMock(SnmpMib.class);
		final JmxSnmpMib jmxSnmpMib = new JmxSnmpMib(otherSnmpMib);
		assertTrue("MBeans were loaded", jmxSnmpMib.getMib().isEmpty());
		loadMib(jmxSnmpMib, this.mBeanName1, ".1.3.6.1.4.1.9999.1.1.1");
		loadMib(jmxSnmpMib, this.mBeanName2, ".1.3.6.1.4.1.9999.1.1.2");
		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.1.6");
		final SnmpOid expectedOid = SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.1.7.42.1.0");
		final MBeanAttributeMapping mapping = MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(expectedOid, "longAttribute",
				SnmpDataType.counter64, long.class, true, true);
		final MBeanAttributeAccessor expectedNode = new MBeanAttributeAccessor(null, null, mapping);
		final SortedMap<SnmpOid, AttributeAccessor> expectedNodeMap = new TreeMap<SnmpOid, AttributeAccessor>();
		expectedNodeMap.put(expectedOid, expectedNode);

		// Scenario
		otherSnmpMib.nextSet(EasyMock.eq(oid));
		EasyMock.expectLastCall().andReturn(expectedNodeMap);
		mocksControl.replay();

		// Test
		final AttributeAccessor node = jmxSnmpMib.next(oid);

		// Checking
		mocksControl.verify();
		assertNotNull("The SnmpMibExternalNode was not found", node);
		assertEquals("The SnmpMibExternalNode is bad", expectedNode, node);

	}

	/**
	 * Tests a research of following SnmpMibExternalNode with end reached.
	 * <p>
	 * Test method for {@link JmxSnmpMib#next(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testNextWithEndReached () {
		this.logger.info("Tests a research of following SnmpMibExternalNode with end reached...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpMib otherSnmpMib = mocksControl.createMock(SnmpMib.class);
		final JmxSnmpMib jmxSnmpMib = new JmxSnmpMib(otherSnmpMib);
		assertTrue("MBeans were loaded", jmxSnmpMib.getMib().isEmpty());
		loadMib(jmxSnmpMib, this.mBeanName1, ".1.3.6.1.4.1.9999.1.1.1");
		loadMib(jmxSnmpMib, this.mBeanName2, ".1.3.6.1.4.1.9999.1.1.2");
		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.2.6");
		final SortedMap<SnmpOid, AttributeAccessor> expectedNodeMap = new TreeMap<SnmpOid, AttributeAccessor>();

		// Scenario
		otherSnmpMib.nextSet(EasyMock.eq(oid));
		EasyMock.expectLastCall().andReturn(expectedNodeMap);
		mocksControl.replay();

		// Test
		final AttributeAccessor node = jmxSnmpMib.next(oid);

		// Checking
		mocksControl.verify();
		assertNull("A SnmpMibExternalNode was found", node);

	}

	/**
	 * Tests a research of following SnmpMibExternalNode list.
	 * <p>
	 * Test method for {@link JmxSnmpMib#nextSet(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testNextSet () {
		this.logger.info("Tests a research of following SnmpMibExternalNode list...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpMib otherSnmpMib = mocksControl.createMock(SnmpMib.class);
		final JmxSnmpMib jmxSnmpMib = new JmxSnmpMib(otherSnmpMib);
		assertTrue("MBeans were loaded", jmxSnmpMib.getMib().isEmpty());
		loadMib(jmxSnmpMib, this.mBeanName1, ".1.3.6.1.4.1.9999.1.1.1");
		loadMib(jmxSnmpMib, this.mBeanName2, ".1.3.6.1.4.1.9999.1.1.2");
		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.1.5");
		final SnmpOid expectedOid = SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.1.7.42.1.0");
		final MBeanAttributeMapping mapping = MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(expectedOid, "longAttribute",
				SnmpDataType.counter64, long.class, true, true);
		final MBeanAttributeAccessor expectedNode = new MBeanAttributeAccessor(null, null, mapping);
		final SortedMap<SnmpOid, AttributeAccessor> expectedNodeMap = new TreeMap<SnmpOid, AttributeAccessor>();
		expectedNodeMap.put(expectedOid, expectedNode);

		// Scenario
		otherSnmpMib.nextSet(EasyMock.eq(oid));
		EasyMock.expectLastCall().andReturn(expectedNodeMap);
		mocksControl.replay();

		// Test
		final SortedMap<SnmpOid, AttributeAccessor> nodeMap = jmxSnmpMib.nextSet(oid);

		// Checking
		mocksControl.verify();
		assertNotNull("The SnmpMibExternalNode was not found", nodeMap);
		final Iterator<Entry<SnmpOid, AttributeAccessor>> entryIterator = nodeMap.entrySet().iterator();
		assertTrue("The SnmpMibExternalNode list is bad (1)", entryIterator.hasNext());
		assertEquals("The SnmpMibExternalNode is bad (1)", SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.1.5.0"), entryIterator.next().getKey());
		assertTrue("The SnmpMibExternalNode list is bad (2)", entryIterator.hasNext());
		assertEquals("The SnmpMibExternalNode is bad (2)", SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.1.7.42.1.0"), entryIterator.next().getKey());
		assertTrue("The SnmpMibExternalNode list is bad (3)", entryIterator.hasNext());
		assertEquals("The SnmpMibExternalNode is bad (3)", SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.2.1.0"), entryIterator.next().getKey());
		assertTrue("The SnmpMibExternalNode list is bad (4)", entryIterator.hasNext());
		assertEquals("The SnmpMibExternalNode is bad (4)", SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.2.2.0"), entryIterator.next().getKey());
		assertTrue("The SnmpMibExternalNode list is bad (5)", entryIterator.hasNext());
		assertEquals("The SnmpMibExternalNode is bad (5)", SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.2.3.0"), entryIterator.next().getKey());
		assertTrue("The SnmpMibExternalNode list is bad (6)", entryIterator.hasNext());
		assertEquals("The SnmpMibExternalNode is bad (6)", SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.2.4.0"), entryIterator.next().getKey());
		assertTrue("The SnmpMibExternalNode list is bad (7)", entryIterator.hasNext());
		assertEquals("The SnmpMibExternalNode is bad (7)", SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.2.5.0"), entryIterator.next().getKey());
		assertFalse("The SnmpMibExternalNode list is bad (8)", entryIterator.hasNext());
	}

	/**
	 * Test method for {@link JmxSnmpMib#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests of toString...");
		final JmxSnmpMib jmxSnmpMib = new JmxSnmpMib(null);
		assertEquals("toString is bad", "JmxSnmpMib[]", jmxSnmpMib.toString());
	}

	/**
	 * Loads the MIB with attributes.
	 * @param jmxSnmpMib Object representing the <b>M</b>anagement <b>I</b>nformation <b>B</b>ase (MIB) for access to JMX.
	 * @param mBeanName MBean name.
	 * @param rootOid Root OID of MBean.
	 */
	private void loadMib (final JmxSnmpMib jmxSnmpMib, final ObjectName mBeanName, final String rootOid) {
		SnmpOid oid = SnmpOid.newInstance(rootOid + ".1.0");
		MBeanAttributeMapping mapping = MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(oid, "longAttribute", SnmpDataType.counter64,
				long.class, true, true);
		jmxSnmpMib.getMib().put(oid, new MBeanAttributeAccessor(null, mBeanName, mapping));
		oid = SnmpOid.newInstance(rootOid + ".2.0");
		mapping = MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(oid, "stringAttribute", SnmpDataType.octetString, String.class, false, false);
		jmxSnmpMib.getMib().put(oid, new MBeanAttributeAccessor(null, mBeanName, mapping));
		oid = SnmpOid.newInstance(rootOid + ".3.0");
		mapping = MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(oid, "hiddenAttribute", SnmpDataType.opaque, Object.class, false, false);
		jmxSnmpMib.getMib().put(oid, new MBeanAttributeAccessor(null, mBeanName, mapping));
		oid = SnmpOid.newInstance(rootOid + ".4.0");
		mapping = MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(oid, "inetAddressAttribute", SnmpDataType.ipAddress, InetAddress.class, true,
				false);
		jmxSnmpMib.getMib().put(oid, new MBeanAttributeAccessor(null, mBeanName, mapping));
		oid = SnmpOid.newInstance(rootOid + ".5.0");
		mapping = MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(oid, "timesticksAttribute", SnmpDataType.timeTicks, long.class, true, false);
		jmxSnmpMib.getMib().put(oid, new MBeanAttributeAccessor(null, mBeanName, mapping));
	}

}