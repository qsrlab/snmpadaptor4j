package net.sf.snmpadaptor4j.core.mapping;

import static org.junit.Assert.*;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Object containing the mapping for access to a MBean attribute.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class MBeanAttributeMappingTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(MapDataMapTrapMappingTest.class);

	/**
	 * Tests the constructor.
	 * <p>
	 * Test method for {@link MBeanAttributeMapping#MBeanAttributeMapping(SnmpOid, String, SnmpDataType, Class, boolean, boolean)}.
	 * </p>
	 */
	@Test
	public void testMBeanAttributeMapping () {
		this.logger.info("Tests the constructor...");
		final SnmpOid oid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.2.0");
		final MBeanAttributeMapping mapping = new MBeanAttributeMapping(oid, "attributeName", SnmpDataType.counter64, long.class, true, false);
		assertEquals("oid is bad", oid, mapping.getOid());
		assertEquals("attributeName is bad", "attributeName", mapping.getAttributeName());
		assertEquals("snmpDataType is bad", SnmpDataType.counter64, mapping.getSnmpDataType());
		assertEquals("jmxDataType is bad", long.class, mapping.getJmxDataType());
		assertTrue("readable is bad", mapping.isReadable());
		assertFalse("writable is bad", mapping.isWritable());
	}

	/**
	 * Test method for {@link MBeanAttributeMapping#hashCode()}.
	 */
	@Test
	public void testHashCode () {
		this.logger.info("Tests the hashCode...");
		final MBeanAttributeMapping mapping1 = new MBeanAttributeMapping(null, null, null, null, false, false);
		final MBeanAttributeMapping mapping2 = new MBeanAttributeMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.2.0"), "attributeName1",
				SnmpDataType.counter64, long.class, true, false);
		final MBeanAttributeMapping mapping3 = new MBeanAttributeMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.3.0"), "attributeName2",
				SnmpDataType.counter64, long.class, true, false);
		final MBeanAttributeMapping mapping4 = new MBeanAttributeMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.3.0"), "attributeName3",
				SnmpDataType.counter64, long.class, true, false);
		final MBeanAttributeMapping mapping5 = new MBeanAttributeMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.3.0"), "attributeName3",
				SnmpDataType.counter32, long.class, true, false);
		final MBeanAttributeMapping mapping6 = new MBeanAttributeMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.3.0"), "attributeName3",
				SnmpDataType.counter32, int.class, true, false);
		final MBeanAttributeMapping mapping7 = new MBeanAttributeMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.3.0"), "attributeName3",
				SnmpDataType.counter32, int.class, false, false);
		final MBeanAttributeMapping mapping8 = new MBeanAttributeMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.3.0"), "attributeName3",
				SnmpDataType.counter32, int.class, false, true);
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping2", mapping1.hashCode() != mapping2.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping3", mapping1.hashCode() != mapping3.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping4", mapping1.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping5", mapping1.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping6", mapping1.hashCode() != mapping6.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping7", mapping1.hashCode() != mapping7.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping8", mapping1.hashCode() != mapping8.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping3", mapping2.hashCode() != mapping3.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping4", mapping2.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping5", mapping2.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping6", mapping2.hashCode() != mapping6.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping7", mapping2.hashCode() != mapping7.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping8", mapping2.hashCode() != mapping8.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping4", mapping3.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping5", mapping3.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping6", mapping3.hashCode() != mapping6.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping7", mapping3.hashCode() != mapping7.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping8", mapping3.hashCode() != mapping8.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping4 and mapping5", mapping4.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping4 and mapping6", mapping4.hashCode() != mapping6.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping4 and mapping7", mapping4.hashCode() != mapping7.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping4 and mapping8", mapping4.hashCode() != mapping8.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping5 and mapping6", mapping5.hashCode() != mapping6.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping5 and mapping7", mapping5.hashCode() != mapping7.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping5 and mapping8", mapping5.hashCode() != mapping8.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping6 and mapping7", mapping6.hashCode() != mapping7.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping6 and mapping8", mapping6.hashCode() != mapping8.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping7 and mapping8", mapping7.hashCode() != mapping8.hashCode());
	}

	/**
	 * Tests the equality with <code>NULL</code>.
	 * <p>
	 * Test method for {@link MBeanAttributeMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithNull () {
		this.logger.info("Tests the equality with NULL...");
		final SnmpOid oid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.2.0");
		final MBeanAttributeMapping mapping = new MBeanAttributeMapping(oid, "attributeName", SnmpDataType.counter64, long.class, true, false);
		assertFalse("The equality is bad", mapping.equals(null));
	}

	/**
	 * Tests the equality with another object.
	 * <p>
	 * Test method for {@link MBeanAttributeMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithAnotherObject () {
		this.logger.info("Tests the equality with another object...");
		final SnmpOid oid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.2.0");
		final MBeanAttributeMapping mapping = new MBeanAttributeMapping(oid, "attributeName", SnmpDataType.counter64, long.class, true, false);
		assertFalse("The equality is bad", mapping.equals(new Object()));
	}

	/**
	 * Tests the equality with itself.
	 * <p>
	 * Test method for {@link MBeanAttributeMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithItself () {
		this.logger.info("Tests the equality with itself...");
		final SnmpOid oid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.2.0");
		final MBeanAttributeMapping mapping = new MBeanAttributeMapping(oid, "attributeName", SnmpDataType.counter64, long.class, true, false);
		assertTrue("The equality is bad", mapping.equals(mapping));
	}

	/**
	 * Tests the equality with the same {@link MBeanAttributeMapping}.
	 * <p>
	 * Test method for {@link MBeanAttributeMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithSameMBeanAttributeMapping () {
		this.logger.info("Tests the equality with the same MBeanAttributeMapping...");
		final SnmpOid oid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.2.0");
		final MBeanAttributeMapping mapping1 = new MBeanAttributeMapping(oid, "attributeName", SnmpDataType.counter64, long.class, true, false);
		final MBeanAttributeMapping mapping2 = new MBeanAttributeMapping(oid, "attributeName", SnmpDataType.counter64, long.class, true, false);
		assertTrue("The equality is bad", mapping1.equals(mapping2));
		assertTrue("The equality with NULL values is bad",
				new MBeanAttributeMapping(null, null, null, null, false, false).equals(new MBeanAttributeMapping(null, null, null, null, false, false)));
	}

	/**
	 * Tests the equality with a different oid.
	 * <p>
	 * Test method for {@link MBeanAttributeMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentOid () {
		this.logger.info("Tests the equality with a different oid...");
		final SnmpOid oid1 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.2.0");
		final SnmpOid oid2 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.3.0");
		final MBeanAttributeMapping mapping1 = new MBeanAttributeMapping(oid1, "attributeName", SnmpDataType.counter64, long.class, true, false);
		final MBeanAttributeMapping mapping2 = new MBeanAttributeMapping(oid2, "attributeName", SnmpDataType.counter64, long.class, true, false);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)",
				mapping1.equals(new MBeanAttributeMapping(null, "attributeName", SnmpDataType.counter64, long.class, true, false)));
		assertFalse("The equality is bad (NULL value)",
				new MBeanAttributeMapping(null, "attributeName", SnmpDataType.counter64, long.class, true, false).equals(mapping2));
	}

	/**
	 * Tests the equality with a different attributeName.
	 * <p>
	 * Test method for {@link MBeanAttributeMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentAttributeName () {
		this.logger.info("Tests the equality with a different attributeName...");
		final SnmpOid oid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.2.0");
		final MBeanAttributeMapping mapping1 = new MBeanAttributeMapping(oid, "attributeName1", SnmpDataType.counter64, long.class, true, false);
		final MBeanAttributeMapping mapping2 = new MBeanAttributeMapping(oid, "attributeName2", SnmpDataType.counter64, long.class, true, false);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)", mapping1.equals(new MBeanAttributeMapping(oid, null, SnmpDataType.counter64, long.class, true, false)));
		assertFalse("The equality is bad (NULL value)", new MBeanAttributeMapping(oid, null, SnmpDataType.counter64, long.class, true, false).equals(mapping2));
	}

	/**
	 * Tests the equality with a different snmpDataType.
	 * <p>
	 * Test method for {@link MBeanAttributeMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentSnmpDataType () {
		this.logger.info("Tests the equality with a different snmpDataType...");
		final SnmpOid oid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.2.0");
		final MBeanAttributeMapping mapping1 = new MBeanAttributeMapping(oid, "attributeName", SnmpDataType.counter32, long.class, true, false);
		final MBeanAttributeMapping mapping2 = new MBeanAttributeMapping(oid, "attributeName", SnmpDataType.counter64, long.class, true, false);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)", mapping1.equals(new MBeanAttributeMapping(oid, "attributeName", null, long.class, true, false)));
		assertFalse("The equality is bad (NULL value)", new MBeanAttributeMapping(oid, "attributeName", null, long.class, true, false).equals(mapping2));
	}

	/**
	 * Tests the equality with a different jmxDataType.
	 * <p>
	 * Test method for {@link MBeanAttributeMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentJmxDataType () {
		this.logger.info("Tests the equality with a different jmxDataType...");
		final SnmpOid oid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.2.0");
		final MBeanAttributeMapping mapping1 = new MBeanAttributeMapping(oid, "attributeName", SnmpDataType.counter64, long.class, true, false);
		final MBeanAttributeMapping mapping2 = new MBeanAttributeMapping(oid, "attributeName", SnmpDataType.counter64, int.class, true, false);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)", mapping1.equals(new MBeanAttributeMapping(oid, "attributeName", SnmpDataType.counter64, null, true, false)));
		assertFalse("The equality is bad (NULL value)", new MBeanAttributeMapping(oid, "attributeName", SnmpDataType.counter64, null, true, false).equals(mapping2));
	}

	/**
	 * Tests the equality with a different readable.
	 * <p>
	 * Test method for {@link MBeanAttributeMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentReadable () {
		this.logger.info("Tests the equality with a different readable...");
		final SnmpOid oid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.2.0");
		final MBeanAttributeMapping mapping1 = new MBeanAttributeMapping(oid, "attributeName", SnmpDataType.counter64, long.class, true, false);
		final MBeanAttributeMapping mapping2 = new MBeanAttributeMapping(oid, "attributeName", SnmpDataType.counter64, long.class, false, false);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
	}

	/**
	 * Tests the equality with a different writable.
	 * <p>
	 * Test method for {@link MBeanAttributeMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentWritable () {
		this.logger.info("Tests the equality with a different writable...");
		final SnmpOid oid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.2.0");
		final MBeanAttributeMapping mapping1 = new MBeanAttributeMapping(oid, "attributeName", SnmpDataType.counter64, long.class, true, false);
		final MBeanAttributeMapping mapping2 = new MBeanAttributeMapping(oid, "attributeName", SnmpDataType.counter64, long.class, true, true);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
	}

	/**
	 * Test method for {@link MBeanAttributeMapping#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests the toString...");
		final MBeanAttributeMapping mapping = new MBeanAttributeMapping(null, null, null, null, false, false);
		assertEquals("The value returned by toString is bad",
				"MBeanAttributeMapping[oid=null; attributeName=null; snmpDataType=null; jmxDataType=null; readable=false; writable=false]", mapping.toString());
	}

}