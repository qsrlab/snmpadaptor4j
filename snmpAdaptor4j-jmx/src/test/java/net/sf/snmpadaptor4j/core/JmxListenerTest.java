package net.sf.snmpadaptor4j.core;

import static org.junit.Assert.*;
import java.lang.management.ManagementFactory;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.MBeanServerNotification;
import javax.management.Notification;
import javax.management.ObjectName;
import net.sf.snmpadaptor4j.SnmpAppContext;
import net.sf.snmpadaptor4j.core.mapping.MBeanAttributeMapping;
import net.sf.snmpadaptor4j.core.mapping.MBeanAttributeMappingFactory;
import net.sf.snmpadaptor4j.core.mapping.SnmpTrapMapping;
import net.sf.snmpadaptor4j.core.mapping.SnmpTrapMappingFactory;
import net.sf.snmpadaptor4j.core.mapping.XmlMappingParser;
import net.sf.snmpadaptor4j.test.mbean.Example;
import net.sf.snmpadaptor4j.test.mbean.ExampleA;
import net.sf.snmpadaptor4j.test.mbean.ExampleB;
import net.sf.snmpadaptor4j.test.mock.SnmpAppContextMock;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.sun.org.apache.bcel.internal.util.ClassPath;

/**
 * Test class for {@link net.sf.snmpadaptor4j.core.JmxListener}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class JmxListenerTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(JmxListenerTest.class);

	/**
	 * MBean name.
	 */
	private final ObjectName mBeanName1 = new ObjectName("net.sf.snmpadaptor4j.test:type=Example,id=1");

	/**
	 * MBean name.
	 */
	private final ObjectName mBeanName2 = new ObjectName("net.sf.snmpadaptor4j.test:type=Example,id=2");

	/**
	 * MBean name.
	 */
	private final ObjectName mBeanNameB = new ObjectName("net.sf.snmpadaptor4j.test:type=ExampleB");

	/**
	 * MBean name for a test with another {@link ClassLoader}.
	 */
	private final ObjectName mBeanNameForAnotherClassLoader = new ObjectName("net.sf.snmpadaptor4j.test:type=Example,id=anotherClassLoader");

	/**
	 * MBean name for a test with a MBean without mapping file.
	 */
	private final ObjectName mBeanNameWithoutMappingFile = new ObjectName("net.sf.snmpadaptor4j.test:type=ExampleA");

	/**
	 * MBean name for a test with a MBean missing in the mapping file.
	 */
	private final ObjectName mBeanNameWithoutMap = new ObjectName("net.sf.snmpadaptor4j.test:type=Example,id=withoutMap");

	/**
	 * Other {@link ClassLoader}.
	 */
	private final ClassLoader otherClassLoader = URLClassLoader.newInstance(new URL[] { getClass().getResource("/") }, Class.class.getClassLoader());

	/**
	 * Application context map.
	 */
	private final Map<ClassLoader, SnmpAppContext> appContextMap = new HashMap<ClassLoader, SnmpAppContext>();

	/**
	 * Constructor.
	 * @throws Exception Exception if an error has occurred.
	 */
	public JmxListenerTest () throws Exception {
		super();
	}

	/**
	 * Test initialization.
	 * @throws Exception Exception if an error has occurred.
	 */
	@Before
	public void setUp () throws Exception {
		ManagementFactory.getPlatformMBeanServer().registerMBean(new Example(), this.mBeanName1);
	}

	/**
	 * Test finalization.
	 * @throws Exception Exception if an error has occurred.
	 */
	@After
	public void tearDown () throws Exception {
		ManagementFactory.getPlatformMBeanServer().unregisterMBean(this.mBeanName1);
	}

	/**
	 * Tests the opening and closing of the connection with the JMX agent.
	 * <p>
	 * Test methods for:
	 * </p>
	 * <ul>
	 * <li>{@link JmxListener#open(MBeanServer)}</li>,
	 * <li>{@link JmxListener#close()}</li>.
	 * </ul>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testOpenClose () throws Exception {
		this.logger.info("Tests the opening and closing of the connection with the JMX agent...");
		final MBeanServer server = ManagementFactory.getPlatformMBeanServer();
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final JmxSnmpMib jmxMib = mocksControl.createMock(JmxSnmpMib.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);
		final String baseOid = "1.3.6.1.4.1.9999.1.1.1";

		// Scenario
		final Map<String, MBeanAttributeInfo> mBeanAttributeInfoMap = new HashMap<String, MBeanAttributeInfo>();
		for (final MBeanAttributeInfo mBeanAttributeInfo : server.getMBeanInfo(this.mBeanName1).getAttributes()) {
			mBeanAttributeInfoMap.put(mBeanAttributeInfo.getName(), mBeanAttributeInfo);
		}
		final XmlMappingParser parser = XmlMappingParser.newInstance(Example.class.getResource(Example.class.getSimpleName() + ".snmp.xml"));
		final List<MBeanAttributeMapping> expectedAttributeMappingList = parser.newMBeanAttributeMappingList(mBeanAttributeInfoMap, getClass().getClassLoader(),
				baseOid);
		jmxMib.registerAttributes(EasyMock.eq(server), EasyMock.eq(this.mBeanName1), EasyMock.eq(expectedAttributeMappingList));
		final Map<String, SnmpTrapMapping> expectedTrapMappingMap = parser.newSnmpTrapMappingMap(baseOid);
		jmxNotificationManager.register(EasyMock.eq(server), EasyMock.eq(this.mBeanName1), EasyMock.eq(expectedTrapMappingMap));
		jmxNotificationManager.unregisterAll(EasyMock.eq(server));
		jmxMib.unregisterAllAttributes();
		mocksControl.replay();

		// Test and checks
		final JmxListener jmxListener = new JmxListener(jmxMib, jmxNotificationManager, new SnmpAppContextMock("1.3.6.1.4.1.9999.1"), this.appContextMap, false,
				null);
		jmxListener.logger.setLevel(Level.ALL);
		assertNull("JMX agent already opened", jmxListener.getJmxServer());
		jmxListener.open(server);
		try {
			assertNotNull("JMX agent not opened", jmxListener.getJmxServer());
		}
		finally {
			jmxListener.close();
		}
		mocksControl.verify();
		assertNull("JMX agent not closed", jmxListener.getJmxServer());

	}

	/**
	 * Tests the opening of connection with the JMX agent already opened.
	 * <p>
	 * Test methods for:
	 * </p>
	 * <ul>
	 * <li>{@link JmxListener#open(MBeanServer)}</li>,
	 * <li>{@link JmxListener#close()}</li>.
	 * </ul>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testReOpen () throws Exception {
		this.logger.info("Tests the opening of connection with the JMX agent already opened...");
		final MBeanServer server = ManagementFactory.getPlatformMBeanServer();
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final JmxSnmpMib jmxMib = mocksControl.createMock(JmxSnmpMib.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);
		final String baseOid = "1.3.6.1.4.1.9999.1.1.1";

		// Scenario
		final Map<String, MBeanAttributeInfo> mBeanAttributeInfoMap = new HashMap<String, MBeanAttributeInfo>();
		for (final MBeanAttributeInfo mBeanAttributeInfo : server.getMBeanInfo(this.mBeanName1).getAttributes()) {
			mBeanAttributeInfoMap.put(mBeanAttributeInfo.getName(), mBeanAttributeInfo);
		}
		final XmlMappingParser parser = XmlMappingParser.newInstance(Example.class.getResource(Example.class.getSimpleName() + ".snmp.xml"));
		final List<MBeanAttributeMapping> expectedAttributeMappingList = parser.newMBeanAttributeMappingList(mBeanAttributeInfoMap, getClass().getClassLoader(),
				baseOid);
		jmxMib.registerAttributes(EasyMock.eq(server), EasyMock.eq(this.mBeanName1), EasyMock.eq(expectedAttributeMappingList));
		final Map<String, SnmpTrapMapping> expectedTrapMappingMap = parser.newSnmpTrapMappingMap(baseOid);
		jmxNotificationManager.register(EasyMock.eq(server), EasyMock.eq(this.mBeanName1), EasyMock.eq(expectedTrapMappingMap));
		jmxNotificationManager.unregisterAll(EasyMock.eq(server));
		jmxMib.unregisterAllAttributes();
		mocksControl.replay();

		// Test and checks
		final JmxListener jmxListener = new JmxListener(jmxMib, jmxNotificationManager, new SnmpAppContextMock("1.3.6.1.4.1.9999.1"), this.appContextMap, true, null);
		jmxListener.logger.setLevel(Level.OFF);
		assertNull("JMX agent already opened", jmxListener.getJmxServer());
		jmxListener.open(server);
		try {
			assertNotNull("JMX agent not opened", jmxListener.getJmxServer());
			try {
				jmxListener.open(server);
			}
			catch (final Exception e) {
				this.logger.info(e);
				assertEquals("Error message is bad", Exception.class, e.getClass());
				assertEquals("Error message is bad", "Already connected to a JMX agent", e.getMessage());
			}
		}
		finally {
			jmxListener.close();
		}
		mocksControl.verify();
		assertNull("JMX agent not closed", jmxListener.getJmxServer());

	}

	/**
	 * Tests the closing of connection with the JMX agent already closed.
	 * <p>
	 * Test methods for {@link JmxListener#close()}.
	 * </p>
	 */
	@Test
	public void testReClose () {
		this.logger.info("Tests the closing of connection with the JMX agent already opened...");
		final JmxListener jmxListener = new JmxListener(null, null, new SnmpAppContextMock("1.3.6.1.4.1.9999.1"), this.appContextMap, true, null);
		assertNull("JMX agent already opened", jmxListener.getJmxServer());
		try {
			jmxListener.close();
		}
		catch (final Exception e) {
			this.logger.info(e);
			assertEquals("Error message is bad", Exception.class, e.getClass());
			assertEquals("Error message is bad", "Not connected to a JMX agent", e.getMessage());
		}
	}

	/**
	 * Tests the notification handling with none <i>handback</i>.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 */
	@Test
	public void testHandleNotificationWithNoneHandback () {
		this.logger.info("Tests the notification handling with none handback...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final JmxSnmpMib jmxMib = mocksControl.createMock(JmxSnmpMib.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		mocksControl.replay();

		// Test and checks
		final JmxListener jmxListener = new JmxListener(jmxMib, jmxNotificationManager, new SnmpAppContextMock("1.3.6.1.4.1.9999.1"), this.appContextMap, false,
				ManagementFactory.getPlatformMBeanServer(),null);
		jmxListener.handleNotification(new Notification("OTHER", this, 1L, "Other notification"), null);
		mocksControl.verify();

	}

	/**
	 * Tests the notification handling from another MBean server.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 */
	@Test
	public void testHandleNotificationFromAnotherMBeanServer () {
		this.logger.info("Tests the notification handling from another MBean server...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final JmxSnmpMib jmxMib = mocksControl.createMock(JmxSnmpMib.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		mocksControl.replay();

		// Test and checks
		final JmxListener jmxListener = new JmxListener(jmxMib, jmxNotificationManager, new SnmpAppContextMock("1.3.6.1.4.1.9999.1"), this.appContextMap, false,
				ManagementFactory.getPlatformMBeanServer(),null);
		assertNotNull("JMX agent not setted", jmxListener.getJmxServer());
		jmxListener.handleNotification(new Notification("OTHER", this, 1L, "Other notification"), MBeanServerFactory.createMBeanServer());
		mocksControl.verify();

	}

	/**
	 * Tests the notification handling with an other notification.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 */
	@Test
	public void testHandleNotificationWithOtherNotification () {
		this.logger.info("Tests the notification handling with an other notification...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final JmxSnmpMib jmxMib = mocksControl.createMock(JmxSnmpMib.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		mocksControl.replay();

		// Test and checks
		final JmxListener jmxListener = new JmxListener(jmxMib, jmxNotificationManager, new SnmpAppContextMock("1.3.6.1.4.1.9999.1"), this.appContextMap, false,
				ManagementFactory.getPlatformMBeanServer(),null);
		assertNotNull("JMX agent not setted", jmxListener.getJmxServer());
		jmxListener.handleNotification(new Notification("OTHER", this, 1L, "Other notification"), ManagementFactory.getPlatformMBeanServer());
		mocksControl.verify();

	}

	/**
	 * Tests the notification handling with a registration error.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 */
	@Test
	public void testHandleNotificationWithRegistrationError () {
		this.logger.info("Tests the notification handling with a registration error...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final JmxSnmpMib jmxMib = mocksControl.createMock(JmxSnmpMib.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);
		final MBeanServerNotification notification = mocksControl.createMock(MBeanServerNotification.class);
		final JmxListener jmxListener = new JmxListener(jmxMib, jmxNotificationManager, new SnmpAppContextMock("1.3.6.1.4.1.9999.1"), this.appContextMap, false,
				ManagementFactory.getPlatformMBeanServer(),null);
		assertNotNull("JMX agent not setted", jmxListener.getJmxServer());

		// Scenario
		notification.getType();
		EasyMock.expectLastCall().andReturn(MBeanServerNotification.REGISTRATION_NOTIFICATION);
		notification.getMBeanName();
		EasyMock.expectLastCall().andThrow(new RuntimeException("MOCK EXCEPTION"));
		notification.getMBeanName();
		EasyMock.expectLastCall().andReturn(this.mBeanName1);
		mocksControl.replay();

		// Test
		jmxListener.handleNotification(notification, ManagementFactory.getPlatformMBeanServer());

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the notification handling with an unregistration error.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 */
	@Test
	public void testHandleNotificationWithUnregistrationError () {
		this.logger.info("Tests the notification handling with an unregistration error...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final JmxSnmpMib jmxMib = mocksControl.createMock(JmxSnmpMib.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);
		final MBeanServerNotification notification = mocksControl.createMock(MBeanServerNotification.class);
		final JmxListener jmxListener = new JmxListener(jmxMib, jmxNotificationManager, new SnmpAppContextMock("1.3.6.1.4.1.9999.1"), this.appContextMap, false,
				ManagementFactory.getPlatformMBeanServer(),null);
		assertNotNull("JMX agent not setted", jmxListener.getJmxServer());

		// Scenario
		notification.getType();
		EasyMock.expectLastCall().andReturn(MBeanServerNotification.UNREGISTRATION_NOTIFICATION);
		notification.getType();
		EasyMock.expectLastCall().andReturn(MBeanServerNotification.UNREGISTRATION_NOTIFICATION);
		notification.getMBeanName();
		EasyMock.expectLastCall().andThrow(new RuntimeException("MOCK EXCEPTION"));
		notification.getMBeanName();
		EasyMock.expectLastCall().andReturn(this.mBeanName1);
		mocksControl.replay();

		// Test
		jmxListener.handleNotification(notification, ManagementFactory.getPlatformMBeanServer());

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the notification handling with an unknown type.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testHandleNotificationWithUnknownType () throws Exception {
		this.logger.info("Tests the notification handling with an unknown type...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final JmxSnmpMib jmxMib = mocksControl.createMock(JmxSnmpMib.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);
		final JmxListener jmxListener = new JmxListener(jmxMib, jmxNotificationManager, new SnmpAppContextMock("1.3.6.1.4.1.9999.1"), this.appContextMap, false,
				ManagementFactory.getPlatformMBeanServer(),null);
		assertNotNull("JMX agent not setted", jmxListener.getJmxServer());

		// Scenario
		mocksControl.replay();

		// Test
		final MBeanServerNotification notif = new MBeanServerNotification("UNKNOWN", this, 2L, new ObjectName("net.sf.snmpadaptor4j.test:type=Unknown"));
		jmxListener.handleNotification(notif, ManagementFactory.getPlatformMBeanServer());

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the registration notification handling for an unknown MBean.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testHandleRegistrationNotificationForUnknownMBean () throws Exception {
		this.logger.info("Tests the registration notification handling for an unknown MBean...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final JmxSnmpMib jmxMib = mocksControl.createMock(JmxSnmpMib.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);
		final JmxListener jmxListener = new JmxListener(jmxMib, jmxNotificationManager, new SnmpAppContextMock("1.3.6.1.4.1.9999.1"), this.appContextMap, false,
				ManagementFactory.getPlatformMBeanServer(),null);
		assertNotNull("JMX agent not setted", jmxListener.getJmxServer());

		// Scenario
		mocksControl.replay();

		// Test
		final MBeanServerNotification notif = new MBeanServerNotification(MBeanServerNotification.REGISTRATION_NOTIFICATION, this, 3L, new ObjectName(
				"net.sf.snmpadaptor4j.test:type=Unknown"));
		jmxListener.handleNotification(notif, ManagementFactory.getPlatformMBeanServer());

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the registration notification handling with a MBean from another {@link ClassPath}.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testHandleRegistrationNotificationWithAnotherClassLoader () throws Exception {
		testHandleRegistrationNotificationWithAnotherClassLoader(true);
		testHandleRegistrationNotificationWithAnotherClassLoader(false);
	}

	/**
	 * Tests the registration notification handling with a MBean from another {@link ClassPath}.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 * @param allLevel <code>TRUE</code> for trace all logs.
	 * @throws Exception Exception if an error has occurred.
	 */
	private void testHandleRegistrationNotificationWithAnotherClassLoader (final boolean allLevel) throws Exception {
		this.logger.info("Tests the registration notification handling with a MBean from another ClassLoader (log level = " + (allLevel ? "ALL" : "OFF") + ")...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final JmxSnmpMib jmxMib = mocksControl.createMock(JmxSnmpMib.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);
		final JmxListener jmxListener = new JmxListener(jmxMib, jmxNotificationManager, new SnmpAppContextMock("1.3.6.1.4.1.9999.1"), this.appContextMap, true,
				ManagementFactory.getPlatformMBeanServer(),null);
		jmxListener.logger.setLevel(allLevel ? Level.ALL : Level.OFF);
		assertNotNull("JMX agent not setted", jmxListener.getJmxServer());
		final Class<?> clz = this.otherClassLoader.loadClass("net.sf.snmpadaptor4j.test.mbean.Example");
		final Object mBean = clz.newInstance();
		ManagementFactory.getPlatformMBeanServer().registerMBean(mBean, this.mBeanNameForAnotherClassLoader);

		// Scenario
		mocksControl.replay();

		// Test
		try {
			final MBeanServerNotification notif = new MBeanServerNotification(MBeanServerNotification.REGISTRATION_NOTIFICATION, this, 0L,
					this.mBeanNameForAnotherClassLoader);
			jmxListener.handleNotification(notif, ManagementFactory.getPlatformMBeanServer());
		}
		finally {
			ManagementFactory.getPlatformMBeanServer().unregisterMBean(this.mBeanNameForAnotherClassLoader);
		}

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the registration notification handling with a MBean without mapping file.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testHandleRegistrationNotificationWithoutMappingFile () throws Exception {
		testHandleRegistrationNotificationWithoutMappingFile(true);
		testHandleRegistrationNotificationWithoutMappingFile(false);
	}

	/**
	 * Tests the registration notification handling with a MBean without mapping file.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 * @param allLevel <code>TRUE</code> for trace all logs.
	 * @throws Exception Exception if an error has occurred.
	 */
	private void testHandleRegistrationNotificationWithoutMappingFile (final boolean allLevel) throws Exception {
		this.logger.info("Tests the registration notification handling with a MBean without mapping file (log level = " + (allLevel ? "ALL" : "OFF") + ")...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final JmxSnmpMib jmxMib = mocksControl.createMock(JmxSnmpMib.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);
		final JmxListener jmxListener = new JmxListener(jmxMib, jmxNotificationManager, new SnmpAppContextMock("1.3.6.1.4.1.9999.1"), this.appContextMap, false,
				ManagementFactory.getPlatformMBeanServer(),null);
		jmxListener.logger.setLevel(allLevel ? Level.ALL : Level.OFF);
		assertNotNull("JMX agent not setted", jmxListener.getJmxServer());
		final Object mBean = new ExampleA();
		ManagementFactory.getPlatformMBeanServer().registerMBean(mBean, this.mBeanNameWithoutMappingFile);

		// Scenario
		mocksControl.replay();

		// Test
		try {
			final MBeanServerNotification notif = new MBeanServerNotification(MBeanServerNotification.REGISTRATION_NOTIFICATION, this, 0L,
					this.mBeanNameWithoutMappingFile);
			jmxListener.handleNotification(notif, ManagementFactory.getPlatformMBeanServer());
		}
		finally {
			ManagementFactory.getPlatformMBeanServer().unregisterMBean(this.mBeanNameWithoutMappingFile);
		}

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the registration notification handling with a MBean missing in the mapping file.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testHandleRegistrationNotificationWithoutMap () throws Exception {
		testHandleRegistrationNotificationWithoutMap(true);
		testHandleRegistrationNotificationWithoutMap(false);
	}

	/**
	 * Tests the registration notification handling with a MBean missing in the mapping file.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 * @param allLevel <code>TRUE</code> for trace all logs.
	 * @throws Exception Exception if an error has occurred.
	 */
	private void testHandleRegistrationNotificationWithoutMap (final boolean allLevel) throws Exception {
		this.logger
				.info("Tests the registration notification handling with a MBean missing in the mapping file (log level = " + (allLevel ? "ALL" : "OFF") + ")...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final JmxSnmpMib jmxMib = mocksControl.createMock(JmxSnmpMib.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);
		final JmxListener jmxListener = new JmxListener(jmxMib, jmxNotificationManager, new SnmpAppContextMock("1.3.6.1.4.1.9999.1"), this.appContextMap, false,
				ManagementFactory.getPlatformMBeanServer(),null);
		jmxListener.logger.setLevel(allLevel ? Level.ALL : Level.OFF);
		assertNotNull("JMX agent not setted", jmxListener.getJmxServer());

		// Scenario
		mocksControl.replay();

		// Test
		final Object mBean = new Example();
		ManagementFactory.getPlatformMBeanServer().registerMBean(mBean, this.mBeanNameWithoutMap);
		try {
			final MBeanServerNotification notif = new MBeanServerNotification(MBeanServerNotification.REGISTRATION_NOTIFICATION, this, 0L, this.mBeanNameWithoutMap);
			jmxListener.handleNotification(notif, ManagementFactory.getPlatformMBeanServer());
		}
		finally {
			ManagementFactory.getPlatformMBeanServer().unregisterMBean(this.mBeanNameWithoutMap);
		}

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the registration notification handling with base OID from mapping file.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testHandleRegistrationNotificationWithBaseOidFromMappingFile () throws Exception {
		testHandleRegistrationNotificationWithBaseOidFromMappingFile(true);
		testHandleRegistrationNotificationWithBaseOidFromMappingFile(false);
	}

	/**
	 * Tests the registration notification handling with base OID from mapping file.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 * @param allLevel <code>TRUE</code> for trace all logs.
	 * @throws Exception Exception if an error has occurred.
	 */
	private void testHandleRegistrationNotificationWithBaseOidFromMappingFile (final boolean allLevel) throws Exception {
		this.logger.info("Tests the registration notification handling with base OID from mapping file (log level = " + (allLevel ? "ALL" : "OFF") + ")...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final JmxSnmpMib jmxMib = mocksControl.createMock(JmxSnmpMib.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);
		final MBeanServer server = ManagementFactory.getPlatformMBeanServer();
		final JmxListener jmxListener = new JmxListener(jmxMib, jmxNotificationManager, new SnmpAppContextMock("1.3.6.1.4.1.9999.1"), this.appContextMap, false,
				server,null);
		jmxListener.logger.setLevel(allLevel ? Level.ALL : Level.OFF);
		assertNotNull("JMX agent not setted", jmxListener.getJmxServer());
		final String expectedRootOid = "1.3.6.1.4.1.9999.1";

		// Scenario
		final List<MBeanAttributeMapping> expectedAttributeMappingList = MBeanAttributeMappingFactory.getInstance().newExpectedAttributeMappingsFromExample(
				expectedRootOid + ".1.1");
		jmxMib.registerAttributes(EasyMock.eq(server), EasyMock.eq(this.mBeanName1), EasyMock.eq(expectedAttributeMappingList));
		final Map<String, SnmpTrapMapping> expectedTrapMappingMap = SnmpTrapMappingFactory.getInstance().newExpectedSnmpTrapMappingFromExample(
				expectedRootOid + ".1.1");
		jmxNotificationManager.register(EasyMock.eq(server), EasyMock.eq(this.mBeanName1), EasyMock.eq(expectedTrapMappingMap));
		mocksControl.replay();

		// Test
		final MBeanServerNotification notif = new MBeanServerNotification(MBeanServerNotification.REGISTRATION_NOTIFICATION, this, 0L, this.mBeanName1);
		jmxListener.handleNotification(notif, ManagementFactory.getPlatformMBeanServer());

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the registration notification handling with base OID from application context.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testHandleRegistrationNotificationWithBaseOidFromAppContext () throws Exception {
		testHandleRegistrationNotificationWithBaseOidFromAppContext(true);
		testHandleRegistrationNotificationWithBaseOidFromAppContext(false);
	}

	/**
	 * Tests the registration notification handling with base OID from application context.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 * @param allLevel <code>TRUE</code> for trace all logs.
	 * @throws Exception Exception if an error has occurred.
	 */
	private void testHandleRegistrationNotificationWithBaseOidFromAppContext (final boolean allLevel) throws Exception {
		this.logger.info("Tests the registration notification handling with base OID from application context (log level = " + (allLevel ? "ALL" : "OFF") + ")...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final JmxSnmpMib jmxMib = mocksControl.createMock(JmxSnmpMib.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);
		final Map<ClassLoader, SnmpAppContext> map = new HashMap<ClassLoader, SnmpAppContext>();
		map.put(this.getClass().getClassLoader(), new SnmpAppContextMock(this.mBeanNameB, ".1.3.6.1.4.1.9999.1.2"));
		final MBeanServer server = ManagementFactory.getPlatformMBeanServer();
		final JmxListener jmxListener = new JmxListener(jmxMib, jmxNotificationManager, new SnmpAppContextMock("1.3.6.1.4.1.9999.1"), map, false, server,null);
		jmxListener.logger.setLevel(allLevel ? Level.ALL : Level.OFF);
		assertNotNull("JMX agent not setted", jmxListener.getJmxServer());
		final String expectedRootOid = "1.3.6.1.4.1.9999.1.2";

		// Scenario
		final List<MBeanAttributeMapping> expectedAttributeMappingList = MBeanAttributeMappingFactory.getInstance().newExpectedAttributeMappingsFromExampleB(
				expectedRootOid);
		jmxMib.registerAttributes(EasyMock.eq(server), EasyMock.eq(this.mBeanNameB), EasyMock.eq(expectedAttributeMappingList));
		final Map<String, SnmpTrapMapping> expectedTrapMappingMap = SnmpTrapMappingFactory.getInstance().newExpectedSnmpTrapMappingFromExampleB(expectedRootOid);
		jmxNotificationManager.register(EasyMock.eq(server), EasyMock.eq(this.mBeanNameB), EasyMock.eq(expectedTrapMappingMap));
		mocksControl.replay();

		// Test
		final Object mBean = new ExampleB();
		server.registerMBean(mBean, this.mBeanNameB);
		try {
			final MBeanServerNotification notif = new MBeanServerNotification(MBeanServerNotification.REGISTRATION_NOTIFICATION, this, 0L, this.mBeanNameB);
			jmxListener.handleNotification(notif, server);
		}
		finally {
			server.unregisterMBean(this.mBeanNameB);
		}

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the registration notification handling with a missing base OID.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testHandleRegistrationNotificationWithMissingBaseOid () throws Exception {
		testHandleRegistrationNotificationWithMissingBaseOid(true);
		testHandleRegistrationNotificationWithMissingBaseOid(false);
	}

	/**
	 * Tests the registration notification handling with a missing base OID.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 * @param allLevel <code>TRUE</code> for trace all logs.
	 * @throws Exception Exception if an error has occurred.
	 */
	private void testHandleRegistrationNotificationWithMissingBaseOid (final boolean allLevel) throws Exception {
		this.logger.info("Tests the registration notification handling with a missing base OID (log level = " + (allLevel ? "ALL" : "OFF") + ")...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final JmxSnmpMib jmxMib = mocksControl.createMock(JmxSnmpMib.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);
		final JmxListener jmxListener = new JmxListener(jmxMib, jmxNotificationManager, new SnmpAppContextMock("1.3.6.1.4.1.9999.1"), this.appContextMap, false,
				ManagementFactory.getPlatformMBeanServer(),null);
		jmxListener.logger.setLevel(allLevel ? Level.ALL : Level.OFF);
		assertNotNull("JMX agent not setted", jmxListener.getJmxServer());

		// Scenario
		mocksControl.replay();

		// Test
		final Object mBean = new ExampleB();
		ManagementFactory.getPlatformMBeanServer().registerMBean(mBean, this.mBeanNameB);
		try {
			final MBeanServerNotification notif = new MBeanServerNotification(MBeanServerNotification.REGISTRATION_NOTIFICATION, this, 0L, this.mBeanNameB);
			jmxListener.handleNotification(notif, ManagementFactory.getPlatformMBeanServer());
		}
		finally {
			ManagementFactory.getPlatformMBeanServer().unregisterMBean(this.mBeanNameB);
		}

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the unregistration notification handling.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 */
	@Test
	public void testHandleUnregistrationNotification () {
		testHandleUnregistrationNotification(true);
		testHandleUnregistrationNotification(false);
	}

	/**
	 * Tests the unregistration notification handling.
	 * <p>
	 * Test method for {@link JmxListener#handleNotification(Notification, Object)}.
	 * </p>
	 * @param allLevel <code>TRUE</code> for trace all logs.
	 */
	private void testHandleUnregistrationNotification (final boolean allLevel) {
		this.logger.info("Tests the unregistration notification handling (log level = " + (allLevel ? "ALL" : "OFF") + ")...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final JmxSnmpMib jmxMib = mocksControl.createMock(JmxSnmpMib.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);
		final JmxListener jmxListener = new JmxListener(jmxMib, jmxNotificationManager, new SnmpAppContextMock("1.3.6.1.4.1.9999.1"), this.appContextMap, false,
				ManagementFactory.getPlatformMBeanServer(),null);
		jmxListener.logger.setLevel(allLevel ? Level.ALL : Level.OFF);
		assertNotNull("JMX agent not setted", jmxListener.getJmxServer());

		// Scenario
		jmxMib.unregisterAttributes(EasyMock.eq(ManagementFactory.getPlatformMBeanServer()), EasyMock.eq(this.mBeanName2));
		jmxNotificationManager.unregister(EasyMock.eq(ManagementFactory.getPlatformMBeanServer()), EasyMock.eq(this.mBeanName2));
		mocksControl.replay();

		// Test
		final MBeanServerNotification notif = new MBeanServerNotification(MBeanServerNotification.UNREGISTRATION_NOTIFICATION, this, 0L, this.mBeanName2);
		jmxListener.handleNotification(notif, ManagementFactory.getPlatformMBeanServer());

		// Checking
		mocksControl.verify();

	}

	/**
	 * Test method for {@link JmxListener#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests of toString...");
		final JmxListener jmxListener = new JmxListener(null, null, null, null, true, null);
		assertEquals("toString is bad", "JmxListener[jmxSnmpMib=null; jmxNotificationManager=null; mainAppContext=null; appContextMap=null; classLoaderScope=true]",
				jmxListener.toString());
	}

}