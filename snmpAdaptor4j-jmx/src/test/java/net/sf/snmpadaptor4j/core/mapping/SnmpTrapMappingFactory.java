package net.sf.snmpadaptor4j.core.mapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.GenericSnmpTrapType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import net.sf.snmpadaptor4j.object.SnmpTrap;
import net.sf.snmpadaptor4j.object.GenericSnmpTrap;
import net.sf.snmpadaptor4j.object.SpecificSnmpTrap;

/**
 * Factory of mappings to build SNMP traps from JMX notifications (singleton).
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SnmpTrapMappingFactory {

	/**
	 * Singleton instance.
	 */
	private static final SnmpTrapMappingFactory INSTANCE = new SnmpTrapMappingFactory();

	/**
	 * Returns the singleton instance.
	 * @return Singleton instance.
	 */
	public static SnmpTrapMappingFactory getInstance () {
		return SnmpTrapMappingFactory.INSTANCE;
	}

	/**
	 * Hidden constructor (singleton).
	 * @see SnmpTrapMappingFactory#getInstance()
	 */
	private SnmpTrapMappingFactory () {
		super();
	}

	/**
	 * Creates and returns the expected list of mappings to build SNMP traps from JMX notifications from <b>Example.snmp.xml</b>.
	 * @param mBeanRootOid Root OID of MBean.
	 * @return Expected list of mappings to build SNMP traps from JMX notifications.
	 */
	public Map<String, SnmpTrapMapping> newExpectedSnmpTrapMappingFromExample (final String mBeanRootOid) {
		final DataMapTrapMapping simpleDataMap = SnmpTrapMappingFactory.getInstance().newDataMapTrapMapping(SnmpOid.newInstance(mBeanRootOid + ".20.0"),
				SnmpOid.newInstance(mBeanRootOid + ".21.0"), true, SnmpDataType.octetString, SnmpOid.newInstance(mBeanRootOid + ".22.0"));
		List<UserDataEntryDataMapTrapMapping> entryList = new ArrayList<UserDataEntryDataMapTrapMapping>();
		entryList.add(SnmpTrapMappingFactory.getInstance().newUserDataEntry("vvvvv", SnmpDataType.octetString, SnmpOid.newInstance(mBeanRootOid + ".23.0")));
		entryList.add(SnmpTrapMappingFactory.getInstance().newUserDataEntry("wwwww", SnmpDataType.octetString, SnmpOid.newInstance(mBeanRootOid + ".24.0")));
		final DataMapTrapMapping mapDataMap = SnmpTrapMappingFactory.getInstance().newDataMapTrapMapping(SnmpOid.newInstance(mBeanRootOid + ".20.0"),
				SnmpOid.newInstance(mBeanRootOid + ".21.0"), true, entryList);
		entryList.clear();
		entryList.add(SnmpTrapMappingFactory.getInstance().newUserDataEntry("vvvvv", SnmpDataType.octetString, SnmpOid.newInstance(mBeanRootOid + ".25.0")));
		entryList.add(SnmpTrapMappingFactory.getInstance().newUserDataEntry("wwwww", SnmpDataType.octetString, SnmpOid.newInstance(mBeanRootOid + ".26.0")));
		final DataMapTrapMapping map1DataMap = SnmpTrapMappingFactory.getInstance().newDataMapTrapMapping(SnmpOid.newInstance(mBeanRootOid + ".20.0"),
				SnmpOid.newInstance(mBeanRootOid + ".21.0"), true, entryList);
		final DataMapTrapMapping defaultDataMap = SnmpTrapMappingFactory.getInstance().newDataMapTrapMapping(SnmpOid.newInstance(mBeanRootOid + ".20.0"),
				SnmpOid.newInstance(mBeanRootOid + ".21.0"), true);
		final Map<String, SnmpTrapMapping> mappingMap = new HashMap<String, SnmpTrapMapping>();
		mappingMap.put("shortAttribute.level.up",
				SnmpTrapMappingFactory.getInstance().newSnmpTrapMapping(SnmpOid.newInstance(mBeanRootOid + ".7.0"), simpleDataMap, GenericSnmpTrapType.linkUp));
		mappingMap.put("shortAttribute.level.down",
				SnmpTrapMappingFactory.getInstance().newSnmpTrapMapping(SnmpOid.newInstance(mBeanRootOid + ".7.0"), mapDataMap, GenericSnmpTrapType.linkDown));
		mappingMap.put("shortAttribute.level.init",
				SnmpTrapMappingFactory.getInstance().newSnmpTrapMapping(SnmpOid.newInstance(mBeanRootOid + ".7.0"), simpleDataMap, GenericSnmpTrapType.coldStart));
		mappingMap.put("intAttribute.level.critical",
				SnmpTrapMappingFactory.getInstance().newSnmpTrapMapping(SnmpOid.newInstance(mBeanRootOid + ".8.0"), mapDataMap, 0));
		mappingMap.put("intAttribute.level.error", SnmpTrapMappingFactory.getInstance()
				.newSnmpTrapMapping(SnmpOid.newInstance(mBeanRootOid + ".8.0"), mapDataMap, 1));
		mappingMap.put("byteArrayAttribute.level.critical",
				SnmpTrapMappingFactory.getInstance().newSnmpTrapMapping(SnmpOid.newInstance(mBeanRootOid + ".10.0"), defaultDataMap, GenericSnmpTrapType.warmStart));
		mappingMap.put("byteArrayAttribute.level.error",
				SnmpTrapMappingFactory.getInstance().newSnmpTrapMapping(SnmpOid.newInstance(mBeanRootOid + ".10.0"), map1DataMap, 1));
		return mappingMap;
	}

	/**
	 * Creates and returns the expected list of mappings to build SNMP traps from JMX notifications from <b>Test1.snmp.xml</b>.
	 * @param mBeanRootOid Root OID of MBean.
	 * @return Expected list of mappings to build SNMP traps from JMX notifications.
	 */
	public Map<String, SnmpTrapMapping> newExpectedSnmpTrapMappingFromTest1 (final String mBeanRootOid) {
		final DataMapTrapMapping defaultDataMap = SnmpTrapMappingFactory.getInstance().newDataMapTrapMapping(null, null, false);
		final Map<String, SnmpTrapMapping> mappingMap = new HashMap<String, SnmpTrapMapping>();
		mappingMap.put("shortAttribute.level.up",
				SnmpTrapMappingFactory.getInstance().newSnmpTrapMapping(SnmpOid.newInstance(mBeanRootOid + ".7.0"), defaultDataMap, GenericSnmpTrapType.linkUp));
		mappingMap.put("shortAttribute.level.down",
				SnmpTrapMappingFactory.getInstance().newSnmpTrapMapping(SnmpOid.newInstance(mBeanRootOid + ".7.0"), defaultDataMap, GenericSnmpTrapType.linkDown));
		mappingMap.put("shortAttribute.level.init",
				SnmpTrapMappingFactory.getInstance().newSnmpTrapMapping(SnmpOid.newInstance(mBeanRootOid + ".7.0"), defaultDataMap, GenericSnmpTrapType.coldStart));
		mappingMap.put("intAttribute.level.critical",
				SnmpTrapMappingFactory.getInstance().newSnmpTrapMapping(SnmpOid.newInstance(mBeanRootOid + ".8.0"), defaultDataMap, 0));
		mappingMap.put("intAttribute.level.error",
				SnmpTrapMappingFactory.getInstance().newSnmpTrapMapping(SnmpOid.newInstance(mBeanRootOid + ".8.0"), defaultDataMap, 1));
		mappingMap.put("byteArrayAttribute.level.critical",
				SnmpTrapMappingFactory.getInstance().newSnmpTrapMapping(SnmpOid.newInstance(mBeanRootOid + ".9.0"), defaultDataMap, GenericSnmpTrapType.warmStart));
		mappingMap.put("byteArrayAttribute.level.error",
				SnmpTrapMappingFactory.getInstance().newSnmpTrapMapping(SnmpOid.newInstance(mBeanRootOid + ".9.0"), defaultDataMap, 1));
		return mappingMap;
	}

	/**
	 * Creates and returns the expected list of mappings to build SNMP traps from JMX notifications from <b>ExampleB.snmp.xml</b>.
	 * @param mBeanRootOid Root OID of MBean.
	 * @return Expected list of mappings to build SNMP traps from JMX notifications.
	 */
	public Map<String, SnmpTrapMapping> newExpectedSnmpTrapMappingFromExampleB (final String mBeanRootOid) {
		return new HashMap<String, SnmpTrapMapping>();
	}

	/**
	 * Creates and returns a new mappings to build SNMP traps from JMX notifications.
	 * @param source <code>source</code> field of {@link GenericSnmpTrap}.
	 * @param dataMap Mapping to build the <code>dataMap</code> field of {@link GenericSnmpTrap} from a JMX notification.
	 * @param type <code>type</code> field of {@link GenericSnmpTrap}.
	 * @return New mappings to build SNMP traps from JMX notifications.
	 */
	public SnmpTrapMapping newSnmpTrapMapping (final SnmpOid source, final DataMapTrapMapping dataMap, final GenericSnmpTrapType type) {
		return new GenericSnmpTrapMapping(source, dataMap, type);
	}

	/**
	 * Creates and returns a new mapping to build SNMP traps from JMX notifications.
	 * @param source <code>source</code> field of {@link SpecificSnmpTrap}.
	 * @param dataMap Mapping to build the <code>dataMap</code> field of {@link SpecificSnmpTrap} from a JMX notification.
	 * @param type <code>type</code> field of {@link SpecificSnmpTrap}.
	 * @return New mappings to build SNMP traps from JMX notifications.
	 */
	public SnmpTrapMapping newSnmpTrapMapping (final SnmpOid source, final DataMapTrapMapping dataMap, final int type) {
		return new SpecificSnmpTrapMapping(source, dataMap, type);
	}

	/**
	 * Creates and returns a new mapping to build the <code>dataMap</code> field of {@link SnmpTrap} from a JMX notification.
	 * @param sequenceNumberOid OID of <code>sequenceNumber</code> field of JMX notification. Is NULL if the <code>sequenceNumber</code> should not be present in
	 *            <code>dataMap</code> field of {@link SnmpTrap}.
	 * @param messageOid OID of <code>message</code> field of JMX notification. Is NULL if the <code>message</code> should not be present in <code>dataMap</code>
	 *            field of {@link SnmpTrap}.
	 * @param hasSystemInfo <code>TRUE</code> for put all system information attributes in the <code>dataMap</code> field of {@link SnmpTrap}.
	 * @return New mapping to build the <code>dataMap</code> field of {@link SnmpTrap} from a JMX notification.
	 */
	public DataMapTrapMapping newDataMapTrapMapping (final SnmpOid sequenceNumberOid, final SnmpOid messageOid, final boolean hasSystemInfo) {
		return new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, hasSystemInfo);
	}

	/**
	 * Creates and returns a new mapping to build the <code>dataMap</code> field of {@link SnmpTrap} from a JMX notification.
	 * @param sequenceNumberOid OID of <code>sequenceNumber</code> field of JMX notification. Is NULL if the <code>sequenceNumber</code> should not be present in
	 *            <code>dataMap</code> field of {@link SnmpTrap}.
	 * @param messageOid OID of <code>message</code> field of JMX notification. Is NULL if the <code>message</code> should not be present in <code>dataMap</code>
	 *            field of {@link SnmpTrap}.
	 * @param hasSystemInfo <code>TRUE</code> for put all system information attributes in the <code>dataMap</code> field of {@link SnmpTrap}.
	 * @param userDataType SNMP data type of <code>userData</code> field of JMX notification (must not be <code>NULL</code>).
	 * @param userDataOid OID of <code>userData</code> field of JMX notification (must not be <code>NULL</code>).
	 * @return New mapping to build the <code>dataMap</code> field of {@link SnmpTrap} from a JMX notification.
	 */
	public DataMapTrapMapping newDataMapTrapMapping (final SnmpOid sequenceNumberOid, final SnmpOid messageOid, final boolean hasSystemInfo,
			final SnmpDataType userDataType, final SnmpOid userDataOid) {
		return new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid, hasSystemInfo, userDataType, userDataOid);
	}

	/**
	 * Creates and returns a new mapping to build the <code>dataMap</code> field of {@link SnmpTrap} from a JMX notification.
	 * @param sequenceNumberOid OID of <code>sequenceNumber</code> field of JMX notification. Is NULL if the <code>sequenceNumber</code> should not be present in
	 *            <code>dataMap</code> field of {@link SnmpTrap}.
	 * @param messageOid OID of <code>message</code> field of JMX notification. Is NULL if the <code>message</code> should not be present in <code>dataMap</code>
	 *            field of {@link SnmpTrap}.
	 * @param hasSystemInfo <code>TRUE</code> for put all system information attributes in the <code>dataMap</code> field of {@link SnmpTrap}.
	 * @param userDataEntryList List of mappings to build entries of the map in <code>dataMap</code> field of {@link SnmpTrap} from all entries of map in
	 *            <code>userData</code> field of JMX notification.
	 * @return New mapping to build the <code>dataMap</code> field of {@link SnmpTrap} from a JMX notification.
	 */
	public DataMapTrapMapping newDataMapTrapMapping (final SnmpOid sequenceNumberOid, final SnmpOid messageOid, final boolean hasSystemInfo,
			final List<UserDataEntryDataMapTrapMapping> userDataEntryList) {
		final MapDataMapTrapMapping mapping = new MapDataMapTrapMapping(sequenceNumberOid, messageOid, hasSystemInfo);
		for (final UserDataEntryDataMapTrapMapping userDataEntry : userDataEntryList) {
			mapping.addUserDataEntry(userDataEntry.getKey(), userDataEntry.getType(), userDataEntry.getOid());
		}
		return mapping;
	}

	/**
	 * Creates and returns a new mapping to build an entry of the map in <code>dataMap</code> field of {@link SnmpTrap} from an entry of map in <code>userData</code>
	 * field of JMX notification.
	 * @param userDataKey Key for find the entry in the map of <code>userData</code> field of JMX notification (must not be <code>NULL</code>).
	 * @param userDataType SNMP data type of entry in the map of <code>userData</code> field of JMX notification (must not be <code>NULL</code>).
	 * @param userDataOid OID of entry in the map of <code>userData</code> field of JMX notification (must not be <code>NULL</code>).
	 * @return New mapping to build an entry of the map in <code>dataMap</code> field of {@link SnmpTrap} from an entry of map in <code>userData</code> field of JMX
	 *         notification.
	 */
	public UserDataEntryDataMapTrapMapping newUserDataEntry (final String userDataKey, final SnmpDataType userDataType, final SnmpOid userDataOid) {
		return new UserDataEntryDataMapTrapMapping(userDataKey, userDataType, userDataOid);
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "SnmpTrapMappingFactory";
	}

}