package net.sf.snmpadaptor4j.core.mapping;

import static org.junit.Assert.*;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.core.mapping.SimpleDataMapTrapMapping}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SimpleDataMapTrapMappingTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(SimpleDataMapTrapMappingTest.class);

	/**
	 * Tests the constructor.
	 * <p>
	 * Test method for {@link SimpleDataMapTrapMapping#SimpleDataMapTrapMapping(SnmpOid, SnmpOid, boolean, SnmpDataType, SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testSimpleDataMapTrapMapping () {
		this.logger.info("Tests the constructor...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final SnmpOid userDataOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final SimpleDataMapTrapMapping mapping = new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid, true, SnmpDataType.octetString, userDataOid);
		assertEquals("sequenceNumberOid is bad", sequenceNumberOid, mapping.getSequenceNumberOid());
		assertEquals("messageOid is bad", messageOid, mapping.getMessageOid());
		assertTrue("hasSystemInfo is bad", mapping.isHasSystemInfo());
		assertEquals("userDataType is bad", SnmpDataType.octetString, mapping.getUserDataType());
		assertEquals("userDataOid is bad", userDataOid, mapping.getUserDataOid());
	}

	/**
	 * Test method for {@link SimpleDataMapTrapMapping#hashCode()}.
	 */
	@Test
	public void testHashCode () {
		this.logger.info("Tests the hashCode...");
		final SimpleDataMapTrapMapping mapping1 = new SimpleDataMapTrapMapping(null, null, false, null, null);
		final SimpleDataMapTrapMapping mapping2 = new SimpleDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"), true, SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"));
		final SimpleDataMapTrapMapping mapping3 = new SimpleDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"), true, SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"));
		final SimpleDataMapTrapMapping mapping4 = new SimpleDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.23.0"), true, SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"));
		final SimpleDataMapTrapMapping mapping5 = new SimpleDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.23.0"), false, SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"));
		final SimpleDataMapTrapMapping mapping6 = new SimpleDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.23.0"), false, SnmpDataType.opaque, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"));
		final SimpleDataMapTrapMapping mapping7 = new SimpleDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.23.0"), false, SnmpDataType.opaque, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.24.0"));
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping2", mapping1.hashCode() != mapping2.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping3", mapping1.hashCode() != mapping3.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping4", mapping1.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping5", mapping1.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping6", mapping1.hashCode() != mapping6.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping7", mapping1.hashCode() != mapping7.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping3", mapping2.hashCode() != mapping3.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping4", mapping2.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping5", mapping2.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping6", mapping2.hashCode() != mapping6.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping7", mapping2.hashCode() != mapping7.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping4", mapping3.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping5", mapping3.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping6", mapping3.hashCode() != mapping6.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping7", mapping3.hashCode() != mapping7.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping4 and mapping5", mapping4.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping4 and mapping6", mapping4.hashCode() != mapping6.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping4 and mapping7", mapping4.hashCode() != mapping7.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping5 and mapping6", mapping5.hashCode() != mapping6.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping5 and mapping7", mapping5.hashCode() != mapping7.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping6 and mapping7", mapping6.hashCode() != mapping7.hashCode());
	}

	/**
	 * Tests the equality with <code>NULL</code>.
	 * <p>
	 * Test method for {@link SimpleDataMapTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithNull () {
		this.logger.info("Tests the equality with NULL...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final SnmpOid userDataOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final SimpleDataMapTrapMapping mapping = new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid, true, SnmpDataType.octetString, userDataOid);
		assertFalse("The equality is bad", mapping.equals(null));
	}

	/**
	 * Tests the equality with another object.
	 * <p>
	 * Test method for {@link SimpleDataMapTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithAnotherObject () {
		this.logger.info("Tests the equality with another object...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final SnmpOid userDataOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final SimpleDataMapTrapMapping mapping = new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid, true, SnmpDataType.octetString, userDataOid);
		assertFalse("The equality is bad", mapping.equals(new Object()));
	}

	/**
	 * Tests the equality with itself.
	 * <p>
	 * Test method for {@link SimpleDataMapTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithItself () {
		this.logger.info("Tests the equality with itself...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final SnmpOid userDataOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final SimpleDataMapTrapMapping mapping = new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid, true, SnmpDataType.octetString, userDataOid);
		assertTrue("The equality is bad", mapping.equals(mapping));
	}

	/**
	 * Tests the equality with the same {@link SimpleDataMapTrapMapping}.
	 * <p>
	 * Test method for {@link SimpleDataMapTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithSameSimpleDataMapTrapMapping () {
		this.logger.info("Tests the equality with the same SimpleDataMapTrapMapping...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final SnmpOid userDataOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final SimpleDataMapTrapMapping mapping1 = new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid, true, SnmpDataType.octetString, userDataOid);
		final SimpleDataMapTrapMapping mapping2 = new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid, true, SnmpDataType.octetString, userDataOid);
		assertTrue("The equality is bad", mapping1.equals(mapping2));
		assertTrue("The equality with NULL values is bad",
				new SimpleDataMapTrapMapping(null, null, true, null, null).equals(new SimpleDataMapTrapMapping(null, null, true, null, null)));
	}

	/**
	 * Tests the equality with a different sequenceNumberOid.
	 * <p>
	 * Test method for {@link SimpleDataMapTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentSequenceNumberOid () {
		this.logger.info("Tests the equality with a different sequenceNumberOid...");
		final SnmpOid sequenceNumberOid1 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid sequenceNumberOid2 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final SnmpOid userDataOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final SimpleDataMapTrapMapping mapping1 = new SimpleDataMapTrapMapping(sequenceNumberOid1, messageOid, true, SnmpDataType.octetString, userDataOid);
		final SimpleDataMapTrapMapping mapping2 = new SimpleDataMapTrapMapping(sequenceNumberOid2, messageOid, true, SnmpDataType.octetString, userDataOid);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)", mapping1.equals(new SimpleDataMapTrapMapping(null, messageOid, true, SnmpDataType.octetString, userDataOid)));
		assertFalse("The equality is bad (NULL value)", new SimpleDataMapTrapMapping(null, messageOid, true, SnmpDataType.octetString, userDataOid).equals(mapping2));
	}

	/**
	 * Tests the equality with a different messageOid.
	 * <p>
	 * Test method for {@link SimpleDataMapTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentMessageOid () {
		this.logger.info("Tests the equality with a different messageOid...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid1 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final SnmpOid messageOid2 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.23.0");
		final SnmpOid userDataOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final SimpleDataMapTrapMapping mapping1 = new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid1, true, SnmpDataType.octetString, userDataOid);
		final SimpleDataMapTrapMapping mapping2 = new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid2, true, SnmpDataType.octetString, userDataOid);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)",
				mapping1.equals(new SimpleDataMapTrapMapping(sequenceNumberOid, null, true, SnmpDataType.octetString, userDataOid)));
		assertFalse("The equality is bad (NULL value)",
				new SimpleDataMapTrapMapping(sequenceNumberOid, null, true, SnmpDataType.octetString, userDataOid).equals(mapping2));
	}

	/**
	 * Tests the equality with a different hasSystemInfo.
	 * <p>
	 * Test method for {@link SimpleDataMapTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentHasSystemInfo () {
		this.logger.info("Tests the equality with a different hasSystemInfo...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final SnmpOid userDataOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final SimpleDataMapTrapMapping mapping1 = new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid, true, SnmpDataType.octetString, userDataOid);
		final SimpleDataMapTrapMapping mapping2 = new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid, false, SnmpDataType.octetString, userDataOid);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
	}

	/**
	 * Tests the equality with a different userDataType.
	 * <p>
	 * Test method for {@link SimpleDataMapTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentUserDataType () {
		this.logger.info("Tests the equality with a different userDataType...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final SnmpOid userDataOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final SimpleDataMapTrapMapping mapping1 = new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid, true, SnmpDataType.octetString, userDataOid);
		final SimpleDataMapTrapMapping mapping2 = new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid, true, SnmpDataType.opaque, userDataOid);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)", mapping1.equals(new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid, true, null, userDataOid)));
		assertFalse("The equality is bad (NULL value)", new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid, true, null, userDataOid).equals(mapping2));
	}

	/**
	 * Tests the equality with a different userDataOid.
	 * <p>
	 * Test method for {@link SimpleDataMapTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentUserDataOid () {
		this.logger.info("Tests the equality with a different userDataOid...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final SnmpOid userDataOid1 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final SnmpOid userDataOid2 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.23.0");
		final SimpleDataMapTrapMapping mapping1 = new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid, true, SnmpDataType.octetString, userDataOid1);
		final SimpleDataMapTrapMapping mapping2 = new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid, true, SnmpDataType.octetString, userDataOid2);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)",
				mapping1.equals(new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid, true, SnmpDataType.octetString, null)));
		assertFalse("The equality is bad (NULL value)",
				new SimpleDataMapTrapMapping(sequenceNumberOid, messageOid, true, SnmpDataType.octetString, null).equals(mapping2));
	}

	/**
	 * Test method for {@link SimpleDataMapTrapMapping#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests the toString...");
		final SimpleDataMapTrapMapping mapping = new SimpleDataMapTrapMapping(null, null, false, null, null);
		assertEquals("The value returned by toString is bad",
				"SimpleDataMapTrapMapping[sequenceNumberOid=null; messageOid=null; hasSystemInfo=false; userDataType=null; userDataOid=null]", mapping.toString());
	}

}