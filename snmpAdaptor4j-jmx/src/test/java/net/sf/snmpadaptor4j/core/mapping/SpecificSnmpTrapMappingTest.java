package net.sf.snmpadaptor4j.core.mapping;

import static org.junit.Assert.*;
import net.sf.snmpadaptor4j.object.SnmpOid;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.core.mapping.SpecificSnmpTrapMapping}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SpecificSnmpTrapMappingTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(SpecificSnmpTrapMappingTest.class);

	/**
	 * Tests the constructor.
	 * <p>
	 * Test method for {@link SpecificSnmpTrapMapping#SpecificSnmpTrapMapping(SnmpOid, DataMapTrapMapping, int)}.
	 * </p>
	 */
	@Test
	public void testSpecificSnmpTrapMapping () {
		this.logger.info("Tests the constructor...");
		final SnmpOid source = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0");
		final DataMapTrapMapping dataMap = new DefaultDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"), true);
		final SpecificSnmpTrapMapping mapping = new SpecificSnmpTrapMapping(source, dataMap, 1);
		assertEquals("source is bad", source, mapping.getSource());
		assertEquals("dataMap is bad", dataMap, mapping.getDataMap());
		assertEquals("type is bad", 1, mapping.getType());
	}

	/**
	 * Test method for {@link SpecificSnmpTrapMapping#hashCode()}.
	 */
	@Test
	public void testHashCode () {
		this.logger.info("Tests the hashCode...");
		final DataMapTrapMapping dataMapA = new DefaultDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"), false);
		final DataMapTrapMapping dataMapB = new DefaultDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.23.0"), true);
		final SpecificSnmpTrapMapping mapping1 = new SpecificSnmpTrapMapping(null, null, 1);
		final SpecificSnmpTrapMapping mapping2 = new SpecificSnmpTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0"), dataMapA, 1);
		final SpecificSnmpTrapMapping mapping3 = new SpecificSnmpTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.8.0"), dataMapA, 1);
		final SpecificSnmpTrapMapping mapping4 = new SpecificSnmpTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.8.0"), dataMapB, 1);
		final SpecificSnmpTrapMapping mapping5 = new SpecificSnmpTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.8.0"), dataMapB, 2);
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping2", mapping1.hashCode() != mapping2.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping3", mapping1.hashCode() != mapping3.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping4", mapping1.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping5", mapping1.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping3", mapping2.hashCode() != mapping3.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping4", mapping2.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping5", mapping2.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping4", mapping3.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping5", mapping3.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping4 and mapping5", mapping4.hashCode() != mapping5.hashCode());
	}

	/**
	 * Tests the equality with <code>NULL</code>.
	 * <p>
	 * Test method for {@link SpecificSnmpTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithNull () {
		this.logger.info("Tests the equality with NULL...");
		final SnmpOid source = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final SpecificSnmpTrapMapping mapping = new SpecificSnmpTrapMapping(source, new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, true), 1);
		assertFalse("The equality is bad", mapping.equals(null));
	}

	/**
	 * Tests the equality with another object.
	 * <p>
	 * Test method for {@link SpecificSnmpTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithAnotherObject () {
		this.logger.info("Tests the equality with another object...");
		final SnmpOid source = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final SpecificSnmpTrapMapping mapping = new SpecificSnmpTrapMapping(source, new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, true), 1);
		assertFalse("The equality is bad", mapping.equals(new Object()));
	}

	/**
	 * Tests the equality with itself.
	 * <p>
	 * Test method for {@link SpecificSnmpTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithItself () {
		this.logger.info("Tests the equality with itself...");
		final SnmpOid source = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final SpecificSnmpTrapMapping mapping = new SpecificSnmpTrapMapping(source, new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, true), 1);
		assertTrue("The equality is bad", mapping.equals(mapping));
	}

	/**
	 * Tests the equality with the same {@link SpecificSnmpTrapMapping}.
	 * <p>
	 * Test method for {@link SpecificSnmpTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithSameSpecificSnmpTrapMapping () {
		this.logger.info("Tests the equality with the same SpecificSnmpTrapMapping...");
		final SnmpOid source = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final SpecificSnmpTrapMapping mapping1 = new SpecificSnmpTrapMapping(source, new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, true), 1);
		final SpecificSnmpTrapMapping mapping2 = new SpecificSnmpTrapMapping(source, new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, true), 1);
		assertTrue("The equality is bad", mapping1.equals(mapping2));
		assertTrue("The equality with NULL values is bad", new SpecificSnmpTrapMapping(null, null, 1).equals(new SpecificSnmpTrapMapping(null, null, 1)));
	}

	/**
	 * Tests the equality with a different source.
	 * <p>
	 * Test method for {@link SpecificSnmpTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentSource () {
		this.logger.info("Tests the equality with a different source...");
		final SnmpOid source1 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0");
		final SnmpOid source2 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.8.0");
		final DataMapTrapMapping dataMap = new DefaultDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"), true);
		final SpecificSnmpTrapMapping mapping1 = new SpecificSnmpTrapMapping(source1, dataMap, 1);
		final SpecificSnmpTrapMapping mapping2 = new SpecificSnmpTrapMapping(source2, dataMap, 1);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)", mapping1.equals(new SpecificSnmpTrapMapping(null, dataMap, 1)));
		assertFalse("The equality is bad (NULL value)", new SpecificSnmpTrapMapping(null, dataMap, 1).equals(mapping2));
	}

	/**
	 * Tests the equality with a different dataMap.
	 * <p>
	 * Test method for {@link SpecificSnmpTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentDataMap () {
		this.logger.info("Tests the equality with a different dataMap...");
		final SnmpOid source = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0");
		final DataMapTrapMapping dataMap1 = new DefaultDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"), true);
		final DataMapTrapMapping dataMap2 = new DefaultDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.23.0"), false);
		final SpecificSnmpTrapMapping mapping1 = new SpecificSnmpTrapMapping(source, dataMap1, 1);
		final SpecificSnmpTrapMapping mapping2 = new SpecificSnmpTrapMapping(source, dataMap2, 1);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)", mapping1.equals(new SpecificSnmpTrapMapping(source, null, 1)));
		assertFalse("The equality is bad (NULL value)", new SpecificSnmpTrapMapping(source, null, 1).equals(mapping2));
	}

	/**
	 * Tests the equality with a different type.
	 * <p>
	 * Test method for {@link SpecificSnmpTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentType () {
		this.logger.info("Tests the equality with a different type...");
		final SnmpOid source = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0");
		final DataMapTrapMapping dataMap = new DefaultDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"), true);
		final SpecificSnmpTrapMapping mapping1 = new SpecificSnmpTrapMapping(source, dataMap, 1);
		final SpecificSnmpTrapMapping mapping2 = new SpecificSnmpTrapMapping(source, dataMap, 2);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
	}

	/**
	 * Test method for {@link SpecificSnmpTrapMapping#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests the toString...");
		final SpecificSnmpTrapMapping mapping = new SpecificSnmpTrapMapping(null, null, 1);
		assertEquals("The value returned by toString is bad", "SNMP trap null - Type enterpriseSpecific/1", mapping.toString());
	}

}