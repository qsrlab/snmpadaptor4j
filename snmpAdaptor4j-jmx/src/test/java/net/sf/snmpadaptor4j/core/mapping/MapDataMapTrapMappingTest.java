package net.sf.snmpadaptor4j.core.mapping;

import static org.junit.Assert.*;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.core.mapping.MapDataMapTrapMapping}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class MapDataMapTrapMappingTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(MapDataMapTrapMappingTest.class);

	/**
	 * Tests the constructor.
	 * <p>
	 * Test method for {@link MapDataMapTrapMapping#MapDataMapTrapMapping(SnmpOid, SnmpOid, boolean)}.
	 * </p>
	 */
	@Test
	public void testMapDataMapTrapMapping () {
		this.logger.info("Tests the constructor...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final MapDataMapTrapMapping mapping = new MapDataMapTrapMapping(sequenceNumberOid, messageOid, true);
		assertEquals("sequenceNumberOid is bad", sequenceNumberOid, mapping.getSequenceNumberOid());
		assertEquals("messageOid is bad", messageOid, mapping.getMessageOid());
		assertTrue("hasSystemInfo is bad", mapping.isHasSystemInfo());
		assertTrue("userDataEntryList is bad", mapping.getUserDataEntryList().isEmpty());
	}

	/**
	 * Tests the adding of user data entries.
	 * <p>
	 * Test method for {@link MapDataMapTrapMapping#addUserDataEntry(String, SnmpDataType, SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testAddUserDataEntry () {
		this.logger.info("Tests the adding of user data entries...");
		final MapDataMapTrapMapping mapping = new MapDataMapTrapMapping(null, null, true);
		assertTrue("userDataEntryList is bad", mapping.getUserDataEntryList().isEmpty());
		final SnmpOid userDataOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0");
		mapping.addUserDataEntry("key1", SnmpDataType.octetString, userDataOid);
		assertEquals("userDataEntryList is bad", 1, mapping.getUserDataEntryList().size());
		final UserDataEntryDataMapTrapMapping entry = mapping.getUserDataEntryList().get(0);
		assertEquals("key of userDataEntry is bad", "key1", entry.getKey());
		assertEquals("type of userDataEntry is bad", SnmpDataType.octetString, entry.getType());
		assertEquals("oid of userDataEntry is bad", userDataOid, entry.getOid());
	}

	/**
	 * Test method for {@link MapDataMapTrapMapping#hashCode()}.
	 */
	@Test
	public void testHashCode () {
		this.logger.info("Tests the hashCode...");
		final MapDataMapTrapMapping mapping1 = new MapDataMapTrapMapping(null, null, false);
		final MapDataMapTrapMapping mapping2 = new MapDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"), true);
		final MapDataMapTrapMapping mapping3 = new MapDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"), true);
		final MapDataMapTrapMapping mapping4 = new MapDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.23.0"), true);
		final MapDataMapTrapMapping mapping5 = new MapDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.23.0"), false);
		final MapDataMapTrapMapping mapping6 = new MapDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.23.0"), false);
		final MapDataMapTrapMapping mapping7 = new MapDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.23.0"), false);
		final MapDataMapTrapMapping mapping8 = new MapDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.23.0"), false);
		mapping1.addUserDataEntry("key1", SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0"));
		mapping2.addUserDataEntry("key1", SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0"));
		mapping3.addUserDataEntry("key1", SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0"));
		mapping4.addUserDataEntry("key1", SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0"));
		mapping5.addUserDataEntry("key1", SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0"));
		mapping6.addUserDataEntry("key2", SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0"));
		mapping7.addUserDataEntry("key2", SnmpDataType.opaque, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0"));
		mapping8.addUserDataEntry("key2", SnmpDataType.opaque, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.70.0"));
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping2", mapping1.hashCode() != mapping2.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping3", mapping1.hashCode() != mapping3.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping4", mapping1.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping5", mapping1.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping6", mapping1.hashCode() != mapping6.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping7", mapping1.hashCode() != mapping7.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping8", mapping1.hashCode() != mapping8.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping3", mapping2.hashCode() != mapping3.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping4", mapping2.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping5", mapping2.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping6", mapping2.hashCode() != mapping6.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping7", mapping2.hashCode() != mapping7.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping8", mapping2.hashCode() != mapping8.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping4", mapping3.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping5", mapping3.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping6", mapping3.hashCode() != mapping6.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping7", mapping3.hashCode() != mapping7.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping8", mapping3.hashCode() != mapping8.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping4 and mapping5", mapping4.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping4 and mapping6", mapping4.hashCode() != mapping6.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping4 and mapping7", mapping4.hashCode() != mapping7.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping4 and mapping8", mapping4.hashCode() != mapping8.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping5 and mapping6", mapping5.hashCode() != mapping6.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping5 and mapping7", mapping5.hashCode() != mapping7.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping5 and mapping8", mapping5.hashCode() != mapping8.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping6 and mapping7", mapping6.hashCode() != mapping7.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping6 and mapping8", mapping6.hashCode() != mapping8.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping7 and mapping8", mapping7.hashCode() != mapping8.hashCode());
	}

	/**
	 * Tests the equality with <code>NULL</code>.
	 * <p>
	 * Test method for {@link MapDataMapTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithNull () {
		this.logger.info("Tests the equality with NULL...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final MapDataMapTrapMapping mapping = new MapDataMapTrapMapping(sequenceNumberOid, messageOid, true);
		mapping.addUserDataEntry("key1", SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0"));
		assertFalse("The equality is bad", mapping.equals(null));
	}

	/**
	 * Tests the equality with another object.
	 * <p>
	 * Test method for {@link MapDataMapTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithAnotherObject () {
		this.logger.info("Tests the equality with another object...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final MapDataMapTrapMapping mapping = new MapDataMapTrapMapping(sequenceNumberOid, messageOid, true);
		mapping.addUserDataEntry("key1", SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0"));
		assertFalse("The equality is bad", mapping.equals(new Object()));
	}

	/**
	 * Tests the equality with itself.
	 * <p>
	 * Test method for {@link MapDataMapTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithItself () {
		this.logger.info("Tests the equality with itself...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final MapDataMapTrapMapping mapping = new MapDataMapTrapMapping(sequenceNumberOid, messageOid, true);
		mapping.addUserDataEntry("key1", SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0"));
		assertTrue("The equality is bad", mapping.equals(mapping));
	}

	/**
	 * Tests the equality with the same {@link MapDataMapTrapMapping}.
	 * <p>
	 * Test method for {@link MapDataMapTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithSameMapDataMapTrapMapping () {
		this.logger.info("Tests the equality with the same MapDataMapTrapMapping...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final MapDataMapTrapMapping mapping1 = new MapDataMapTrapMapping(sequenceNumberOid, messageOid, true);
		mapping1.addUserDataEntry("key1", SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0"));
		final MapDataMapTrapMapping mapping2 = new MapDataMapTrapMapping(sequenceNumberOid, messageOid, true);
		mapping2.addUserDataEntry("key1", SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0"));
		assertTrue("The equality is bad", mapping1.equals(mapping2));
		assertTrue("The equality with NULL values is bad", new MapDataMapTrapMapping(null, null, true).equals(new MapDataMapTrapMapping(null, null, true)));
	}

	/**
	 * Tests the equality with a different sequenceNumberOid.
	 * <p>
	 * Test method for {@link MapDataMapTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentSequenceNumberOid () {
		this.logger.info("Tests the equality with a different sequenceNumberOid...");
		final SnmpOid sequenceNumberOid1 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid sequenceNumberOid2 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final MapDataMapTrapMapping mapping1 = new MapDataMapTrapMapping(sequenceNumberOid1, messageOid, true);
		mapping1.addUserDataEntry("key1", SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0"));
		final MapDataMapTrapMapping mapping2 = new MapDataMapTrapMapping(sequenceNumberOid2, messageOid, true);
		mapping2.addUserDataEntry("key1", SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0"));
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)", mapping1.equals(new MapDataMapTrapMapping(null, messageOid, true)));
		assertFalse("The equality is bad (NULL value)", new MapDataMapTrapMapping(null, messageOid, true).equals(mapping2));
	}

	/**
	 * Tests the equality with a different messageOid.
	 * <p>
	 * Test method for {@link MapDataMapTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentMessageOid () {
		this.logger.info("Tests the equality with a different messageOid...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid1 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final SnmpOid messageOid2 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final MapDataMapTrapMapping mapping1 = new MapDataMapTrapMapping(sequenceNumberOid, messageOid1, true);
		mapping1.addUserDataEntry("key1", SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0"));
		final MapDataMapTrapMapping mapping2 = new MapDataMapTrapMapping(sequenceNumberOid, messageOid2, true);
		mapping2.addUserDataEntry("key1", SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0"));
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)", mapping1.equals(new MapDataMapTrapMapping(sequenceNumberOid, null, true)));
		assertFalse("The equality is bad (NULL value)", new MapDataMapTrapMapping(sequenceNumberOid, null, true).equals(mapping2));
	}

	/**
	 * Tests the equality with a different hasSystemInfo.
	 * <p>
	 * Test method for {@link MapDataMapTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentHasSystemInfo () {
		this.logger.info("Tests the equality with a different hasSystemInfo...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final MapDataMapTrapMapping mapping1 = new MapDataMapTrapMapping(sequenceNumberOid, messageOid, true);
		mapping1.addUserDataEntry("key1", SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0"));
		final MapDataMapTrapMapping mapping2 = new MapDataMapTrapMapping(sequenceNumberOid, messageOid, false);
		mapping2.addUserDataEntry("key1", SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0"));
		assertFalse("The equality is bad", mapping1.equals(mapping2));
	}

	/**
	 * Tests the equality with a different userDataEntry.
	 * <p>
	 * Test method for {@link MapDataMapTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentUserDataEntry () {
		this.logger.info("Tests the equality with a different userDataEntry...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final MapDataMapTrapMapping mapping1 = new MapDataMapTrapMapping(sequenceNumberOid, messageOid, true);
		mapping1.addUserDataEntry("key1", SnmpDataType.octetString, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.80.0"));
		final MapDataMapTrapMapping mapping2 = new MapDataMapTrapMapping(sequenceNumberOid, messageOid, true);
		mapping2.addUserDataEntry("key2", SnmpDataType.opaque, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.70.0"));
		assertFalse("The equality is bad", mapping1.equals(mapping2));
	}

	/**
	 * Test method for {@link MapDataMapTrapMapping#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests the toString...");
		final MapDataMapTrapMapping mapping = new MapDataMapTrapMapping(null, null, false);
		assertEquals("The value returned by toString is bad",
				"MapDataMapTrapMapping[sequenceNumberOid=null; messageOid=null; hasSystemInfo=false; userDataEntryList=[]]", mapping.toString());
	}

}