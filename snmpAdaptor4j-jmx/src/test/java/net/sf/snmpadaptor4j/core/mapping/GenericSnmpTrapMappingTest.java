package net.sf.snmpadaptor4j.core.mapping;

import static org.junit.Assert.*;
import net.sf.snmpadaptor4j.object.GenericSnmpTrapType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.core.mapping.GenericSnmpTrapMapping}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class GenericSnmpTrapMappingTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(GenericSnmpTrapMappingTest.class);

	/**
	 * Tests the constructor.
	 * <p>
	 * Test method for {@link GenericSnmpTrapMapping#GenericSnmpTrapMapping(SnmpOid, DataMapTrapMapping, GenericSnmpTrapType)}.
	 * </p>
	 */
	@Test
	public void testGenericSnmpTrapMapping () {
		this.logger.info("Tests the constructor...");
		final SnmpOid source = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0");
		final DataMapTrapMapping dataMap = new DefaultDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"), true);
		final GenericSnmpTrapMapping mapping = new GenericSnmpTrapMapping(source, dataMap, GenericSnmpTrapType.linkUp);
		assertEquals("source is bad", source, mapping.getSource());
		assertEquals("dataMap is bad", dataMap, mapping.getDataMap());
		assertEquals("type is bad", GenericSnmpTrapType.linkUp, mapping.getType());
	}

	/**
	 * Test method for {@link GenericSnmpTrapMapping#hashCode()}.
	 */
	@Test
	public void testHashCode () {
		this.logger.info("Tests the hashCode...");
		final DataMapTrapMapping dataMapA = new DefaultDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"), false);
		final DataMapTrapMapping dataMapB = new DefaultDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.23.0"), true);
		final GenericSnmpTrapMapping mapping1 = new GenericSnmpTrapMapping(null, null, null);
		final GenericSnmpTrapMapping mapping2 = new GenericSnmpTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0"), dataMapA, GenericSnmpTrapType.linkUp);
		final GenericSnmpTrapMapping mapping3 = new GenericSnmpTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.8.0"), dataMapA, GenericSnmpTrapType.linkUp);
		final GenericSnmpTrapMapping mapping4 = new GenericSnmpTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.8.0"), dataMapB, GenericSnmpTrapType.linkUp);
		final GenericSnmpTrapMapping mapping5 = new GenericSnmpTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.8.0"), dataMapB, GenericSnmpTrapType.linkDown);
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping2", mapping1.hashCode() != mapping2.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping3", mapping1.hashCode() != mapping3.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping4", mapping1.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping5", mapping1.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping3", mapping2.hashCode() != mapping3.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping4", mapping2.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping5", mapping2.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping4", mapping3.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping5", mapping3.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping4 and mapping5", mapping4.hashCode() != mapping5.hashCode());
	}

	/**
	 * Tests the equality with <code>NULL</code>.
	 * <p>
	 * Test method for {@link GenericSnmpTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithNull () {
		this.logger.info("Tests the equality with NULL...");
		final SnmpOid source = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final GenericSnmpTrapMapping mapping = new GenericSnmpTrapMapping(source, new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, true),
				GenericSnmpTrapType.linkUp);
		assertFalse("The equality is bad", mapping.equals(null));
	}

	/**
	 * Tests the equality with another object.
	 * <p>
	 * Test method for {@link GenericSnmpTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithAnotherObject () {
		this.logger.info("Tests the equality with another object...");
		final SnmpOid source = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final GenericSnmpTrapMapping mapping = new GenericSnmpTrapMapping(source, new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, true),
				GenericSnmpTrapType.linkUp);
		assertFalse("The equality is bad", mapping.equals(new Object()));
	}

	/**
	 * Tests the equality with itself.
	 * <p>
	 * Test method for {@link GenericSnmpTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithItself () {
		this.logger.info("Tests the equality with itself...");
		final SnmpOid source = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final GenericSnmpTrapMapping mapping = new GenericSnmpTrapMapping(source, new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, true),
				GenericSnmpTrapType.linkUp);
		assertTrue("The equality is bad", mapping.equals(mapping));
	}

	/**
	 * Tests the equality with the same {@link GenericSnmpTrapMapping}.
	 * <p>
	 * Test method for {@link GenericSnmpTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithSameGenericSnmpTrapMapping () {
		this.logger.info("Tests the equality with the same GenericSnmpTrapMapping...");
		final SnmpOid source = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final GenericSnmpTrapMapping mapping1 = new GenericSnmpTrapMapping(source, new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, true),
				GenericSnmpTrapType.linkUp);
		final GenericSnmpTrapMapping mapping2 = new GenericSnmpTrapMapping(source, new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, true),
				GenericSnmpTrapType.linkUp);
		assertTrue("The equality is bad", mapping1.equals(mapping2));
		assertTrue("The equality with NULL values is bad", new GenericSnmpTrapMapping(null, null, null).equals(new GenericSnmpTrapMapping(null, null, null)));
	}

	/**
	 * Tests the equality with a different source.
	 * <p>
	 * Test method for {@link GenericSnmpTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentSource () {
		this.logger.info("Tests the equality with a different source...");
		final SnmpOid source1 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0");
		final SnmpOid source2 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.8.0");
		final DataMapTrapMapping dataMap = new DefaultDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"), true);
		final GenericSnmpTrapMapping mapping1 = new GenericSnmpTrapMapping(source1, dataMap, GenericSnmpTrapType.linkUp);
		final GenericSnmpTrapMapping mapping2 = new GenericSnmpTrapMapping(source2, dataMap, GenericSnmpTrapType.linkUp);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)", mapping1.equals(new GenericSnmpTrapMapping(null, dataMap, GenericSnmpTrapType.linkUp)));
		assertFalse("The equality is bad (NULL value)", new GenericSnmpTrapMapping(null, dataMap, GenericSnmpTrapType.linkUp).equals(mapping2));
	}

	/**
	 * Tests the equality with a different dataMap.
	 * <p>
	 * Test method for {@link GenericSnmpTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentDataMap () {
		this.logger.info("Tests the equality with a different dataMap...");
		final SnmpOid source = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0");
		final DataMapTrapMapping dataMap1 = new DefaultDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"), true);
		final DataMapTrapMapping dataMap2 = new DefaultDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.23.0"), false);
		final GenericSnmpTrapMapping mapping1 = new GenericSnmpTrapMapping(source, dataMap1, GenericSnmpTrapType.linkUp);
		final GenericSnmpTrapMapping mapping2 = new GenericSnmpTrapMapping(source, dataMap2, GenericSnmpTrapType.linkUp);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)", mapping1.equals(new GenericSnmpTrapMapping(source, null, GenericSnmpTrapType.linkUp)));
		assertFalse("The equality is bad (NULL value)", new GenericSnmpTrapMapping(source, null, GenericSnmpTrapType.linkUp).equals(mapping2));
	}

	/**
	 * Tests the equality with a different type.
	 * <p>
	 * Test method for {@link GenericSnmpTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentType () {
		this.logger.info("Tests the equality with a different type...");
		final SnmpOid source = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0");
		final DataMapTrapMapping dataMap = new DefaultDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"), true);
		final GenericSnmpTrapMapping mapping1 = new GenericSnmpTrapMapping(source, dataMap, GenericSnmpTrapType.linkUp);
		final GenericSnmpTrapMapping mapping2 = new GenericSnmpTrapMapping(source, dataMap, GenericSnmpTrapType.linkDown);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)", mapping1.equals(new GenericSnmpTrapMapping(source, dataMap, null)));
		assertFalse("The equality is bad (NULL value)", new GenericSnmpTrapMapping(source, dataMap, null).equals(mapping2));
	}

	/**
	 * Test method for {@link GenericSnmpTrapMapping#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests the toString...");
		final GenericSnmpTrapMapping mapping = new GenericSnmpTrapMapping(null, null, null);
		assertEquals("The value returned by toString is bad", "SNMP trap null - Type null/0", mapping.toString());
	}

}