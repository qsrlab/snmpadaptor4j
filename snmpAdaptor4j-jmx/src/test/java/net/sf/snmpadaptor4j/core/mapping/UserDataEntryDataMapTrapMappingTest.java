package net.sf.snmpadaptor4j.core.mapping;

import static org.junit.Assert.*;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.core.mapping.UserDataEntryDataMapTrapMapping}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class UserDataEntryDataMapTrapMappingTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(UserDataEntryDataMapTrapMappingTest.class);

	/**
	 * Tests the constructor.
	 * <p>
	 * Test method for {@link UserDataEntryDataMapTrapMapping#UserDataEntryDataMapTrapMapping(String, SnmpDataType, SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testUserDataEntryDataMapTrapMapping () {
		this.logger.info("Tests the constructor...");
		final SnmpOid oid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final UserDataEntryDataMapTrapMapping mapping = new UserDataEntryDataMapTrapMapping("key", SnmpDataType.octetString, oid);
		assertEquals("key is bad", "key", mapping.getKey());
		assertEquals("type is bad", SnmpDataType.octetString, mapping.getType());
		assertEquals("oid is bad", oid, mapping.getOid());
	}

	/**
	 * Test method for {@link UserDataEntryDataMapTrapMapping#hashCode()}.
	 */
	@Test
	public void testHashCode () {
		this.logger.info("Tests the hashCode...");
		final UserDataEntryDataMapTrapMapping mapping1 = new UserDataEntryDataMapTrapMapping(null, null, null);
		final UserDataEntryDataMapTrapMapping mapping2 = new UserDataEntryDataMapTrapMapping("key1", SnmpDataType.octetString,
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"));
		final UserDataEntryDataMapTrapMapping mapping3 = new UserDataEntryDataMapTrapMapping("key2", SnmpDataType.octetString,
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"));
		final UserDataEntryDataMapTrapMapping mapping4 = new UserDataEntryDataMapTrapMapping("key2", SnmpDataType.opaque,
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"));
		final UserDataEntryDataMapTrapMapping mapping5 = new UserDataEntryDataMapTrapMapping("key2", SnmpDataType.opaque,
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.23.0"));
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping2", mapping1.hashCode() != mapping2.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping3", mapping1.hashCode() != mapping3.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping4", mapping1.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping5", mapping1.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping3", mapping2.hashCode() != mapping3.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping4", mapping2.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping5", mapping2.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping4", mapping3.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping5", mapping3.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping4 and mapping5", mapping4.hashCode() != mapping5.hashCode());
	}

	/**
	 * Tests the equality with <code>NULL</code>.
	 * <p>
	 * Test method for {@link UserDataEntryDataMapTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithNull () {
		this.logger.info("Tests the equality with NULL...");
		final SnmpOid oid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final UserDataEntryDataMapTrapMapping mapping = new UserDataEntryDataMapTrapMapping("key1", SnmpDataType.octetString, oid);
		assertFalse("The equality is bad", mapping.equals(null));
	}

	/**
	 * Tests the equality with another object.
	 * <p>
	 * Test method for {@link UserDataEntryDataMapTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithAnotherObject () {
		this.logger.info("Tests the equality with another object...");
		final SnmpOid oid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final UserDataEntryDataMapTrapMapping mapping = new UserDataEntryDataMapTrapMapping("key1", SnmpDataType.octetString, oid);
		assertFalse("The equality is bad", mapping.equals(new Object()));
	}

	/**
	 * Tests the equality with itself.
	 * <p>
	 * Test method for {@link UserDataEntryDataMapTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithItself () {
		this.logger.info("Tests the equality with itself...");
		final SnmpOid oid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final UserDataEntryDataMapTrapMapping mapping = new UserDataEntryDataMapTrapMapping("key1", SnmpDataType.octetString, oid);
		assertTrue("The equality is bad", mapping.equals(mapping));
	}

	/**
	 * Tests the equality with the same {@link UserDataEntryDataMapTrapMapping}.
	 * <p>
	 * Test method for {@link UserDataEntryDataMapTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithSameUserDataEntryDataMapTrapMapping () {
		this.logger.info("Tests the equality with the same UserDataEntryDataMapTrapMapping...");
		final SnmpOid oid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final UserDataEntryDataMapTrapMapping mapping1 = new UserDataEntryDataMapTrapMapping("key1", SnmpDataType.octetString, oid);
		final UserDataEntryDataMapTrapMapping mapping2 = new UserDataEntryDataMapTrapMapping("key1", SnmpDataType.octetString, oid);
		assertTrue("The equality is bad", mapping1.equals(mapping2));
		assertTrue("The equality with NULL values is bad",
				new UserDataEntryDataMapTrapMapping(null, null, null).equals(new UserDataEntryDataMapTrapMapping(null, null, null)));
	}

	/**
	 * Tests the equality with a different key.
	 * <p>
	 * Test method for {@link UserDataEntryDataMapTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentKey () {
		this.logger.info("Tests the equality with a different key...");
		final SnmpOid oid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final UserDataEntryDataMapTrapMapping mapping1 = new UserDataEntryDataMapTrapMapping("key1", SnmpDataType.octetString, oid);
		final UserDataEntryDataMapTrapMapping mapping2 = new UserDataEntryDataMapTrapMapping("key2", SnmpDataType.octetString, oid);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)", mapping1.equals(new UserDataEntryDataMapTrapMapping(null, SnmpDataType.octetString, oid)));
		assertFalse("The equality is bad (NULL value)", new UserDataEntryDataMapTrapMapping(null, SnmpDataType.octetString, oid).equals(mapping2));
	}

	/**
	 * Tests the equality with a different type.
	 * <p>
	 * Test method for {@link UserDataEntryDataMapTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentType () {
		this.logger.info("Tests the equality with a different type...");
		final SnmpOid oid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final UserDataEntryDataMapTrapMapping mapping1 = new UserDataEntryDataMapTrapMapping("key1", SnmpDataType.octetString, oid);
		final UserDataEntryDataMapTrapMapping mapping2 = new UserDataEntryDataMapTrapMapping("key1", SnmpDataType.opaque, oid);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)", mapping1.equals(new UserDataEntryDataMapTrapMapping("key1", null, oid)));
		assertFalse("The equality is bad (NULL value)", new UserDataEntryDataMapTrapMapping("key1", null, oid).equals(mapping2));
	}

	/**
	 * Tests the equality with a different oid.
	 * <p>
	 * Test method for {@link UserDataEntryDataMapTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentUserDataOid () {
		this.logger.info("Tests the equality with a different oid...");
		final SnmpOid oid1 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final SnmpOid oid2 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.23.0");
		final UserDataEntryDataMapTrapMapping mapping1 = new UserDataEntryDataMapTrapMapping("key1", SnmpDataType.octetString, oid1);
		final UserDataEntryDataMapTrapMapping mapping2 = new UserDataEntryDataMapTrapMapping("key1", SnmpDataType.octetString, oid2);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)", mapping1.equals(new UserDataEntryDataMapTrapMapping("key1", SnmpDataType.octetString, null)));
		assertFalse("The equality is bad (NULL value)", new UserDataEntryDataMapTrapMapping("key1", SnmpDataType.octetString, null).equals(mapping2));
	}

	/**
	 * Test method for {@link UserDataEntryDataMapTrapMapping#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests the toString...");
		final UserDataEntryDataMapTrapMapping mapping = new UserDataEntryDataMapTrapMapping(null, null, null);
		assertEquals("The value returned by toString is bad", "UserDataEntryDataMapTrapMapping[key=null; type=null; oid=null]", mapping.toString());
	}

}