package net.sf.snmpadaptor4j.core.mapping;

import static org.junit.Assert.*;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import net.sf.snmpadaptor4j.core.mapping.XmlMappingParser;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.GenericSnmpTrapType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import net.sf.snmpadaptor4j.test.mbean.Example;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.core.mapping.XmlMappingParser}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class XmlMappingParserTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(XmlMappingParserTest.class);

	/**
	 * Tests the search for base OID in empty file <b>ExampleB.snmp.xml</b>.
	 * <p>
	 * Test method for {@link XmlMappingParser#findBaseOid(javax.management.ObjectName, java.util.Map, String, String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testFindBaseOidInEmptyFile () throws Exception {
		this.logger.info("Tests the search for base OID in empty file ExampleB.snmp.xml...");
		final XmlMappingParser parser = XmlMappingParser.newInstance(getClass().getResource("/net/sf/snmpadaptor4j/test/mbean/ExampleB.snmp.xml"));
		final ObjectName mBeanName = new ObjectName("net.sf.snmpadaptor4j.test:type=Example,id=anotherClasspath");
		final Map<String, String> rootOidMap = new HashMap<String, String>();
		rootOidMap.put("framework", "1.3.6.1.4.1.9999.1");
		rootOidMap.put("example", "1.3.6.1.4.1.9999.50");
		final String defaultRootOid = "1.3.6.1.4.1.99.12.8.1";
		final String mainDefaultRootOid = "1.3.6.1.4.1.88.12.8.1";
		final String baseOid = parser.findBaseOid(mBeanName, rootOidMap, defaultRootOid, mainDefaultRootOid);
		assertNull("A base OID was found", baseOid);
	}

	/**
	 * Tests the unsuccessful search for base OID in file <b>Example.snmp.xml</b>.
	 * <p>
	 * Test method for {@link XmlMappingParser#findBaseOid(javax.management.ObjectName, java.util.Map, String, String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testFindBaseOidForUnsuccessful () throws Exception {
		this.logger.info("Tests the unsuccessful search for base OID in file Example.snmp.xml...");
		final XmlMappingParser parser = XmlMappingParser.newInstance(getClass().getResource("/net/sf/snmpadaptor4j/test/mbean/Example.snmp.xml"));
		final ObjectName mBeanName = new ObjectName("net.sf.snmpadaptor4j.test:type=Missing");
		final Map<String, String> rootOidMap = new HashMap<String, String>();
		rootOidMap.put("framework", "1.3.6.1.4.1.9999.1");
		rootOidMap.put("example", "1.3.6.1.4.1.9999.50");
		final String defaultRootOid = "1.3.6.1.4.1.99.12.8.1";
		final String mainDefaultRootOid = "1.3.6.1.4.1.88.12.8.1";
		final String baseOid = parser.findBaseOid(mBeanName, rootOidMap, defaultRootOid, mainDefaultRootOid);
		assertNull("A base OID was found", baseOid);
	}

	/**
	 * Tests the successful search for base OID in file <b>Example.snmp.xml</b> with a specific root.
	 * <p>
	 * Test method for {@link XmlMappingParser#findBaseOid(javax.management.ObjectName, java.util.Map, String, String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testFindBaseOidWithSpecificRoot () throws Exception {
		this.logger.info("Tests the successful search for base OID in file Example.snmp.xml with a specific root...");
		final XmlMappingParser parser = XmlMappingParser.newInstance(getClass().getResource("/net/sf/snmpadaptor4j/test/mbean/Example.snmp.xml"));
		final ObjectName mBeanName = new ObjectName("net.sf.snmpadaptor4j.test:type=Example,id=specificRootOid");
		final Map<String, String> rootOidMap = new HashMap<String, String>();
		rootOidMap.put("framework", "1.3.6.1.4.1.9999.1");
		rootOidMap.put("example", "1.3.6.1.4.1.9999.50");
		rootOidMap.put("specificRootOid", "1.3.6.1.4.1.9999.77");
		final String defaultRootOid = "1.3.6.1.4.1.99.12.8.1";
		final String mainDefaultRootOid = "1.3.6.1.4.1.88.12.8.1";
		final String baseOid = parser.findBaseOid(mBeanName, rootOidMap, defaultRootOid, mainDefaultRootOid);
		assertEquals("The base OID found is bad", "1.3.6.1.4.1.9999.77.1.3", baseOid);
	}

	/**
	 * Tests the successful search for base OID in file <b>Example.snmp.xml</b> with an unknown specific root (case using the default root OID of the current
	 * application).
	 * <p>
	 * Test method for {@link XmlMappingParser#findBaseOid(javax.management.ObjectName, java.util.Map, String, String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testFindBaseOidWithUnknownSpecificRoot1 () throws Exception {
		this.logger
				.info("Tests the successful search for base OID in file Example.snmp.xml with an unknown specific root (case using the default root OID of the current application)...");
		final XmlMappingParser parser = XmlMappingParser.newInstance(getClass().getResource("/net/sf/snmpadaptor4j/test/mbean/Example.snmp.xml"));
		final ObjectName mBeanName = new ObjectName("net.sf.snmpadaptor4j.test:type=Example,id=specificRootOid");
		final Map<String, String> rootOidMap = new HashMap<String, String>();
		rootOidMap.put("framework", "1.3.6.1.4.1.9999.1");
		rootOidMap.put("example", "1.3.6.1.4.1.9999.50");
		final String defaultRootOid = "1.3.6.1.4.1.99.12.8.1";
		final String mainDefaultRootOid = "1.3.6.1.4.1.88.12.8.1";
		final String baseOid = parser.findBaseOid(mBeanName, rootOidMap, defaultRootOid, mainDefaultRootOid);
		assertEquals("The base OID found is bad", "1.3.6.1.4.1.99.12.8.1.1.3", baseOid);
	}

	/**
	 * Tests the successful search for base OID in file <b>Example.snmp.xml</b> with an unknown specific root (case using the default root OID of the main
	 * application).
	 * <p>
	 * Test method for {@link XmlMappingParser#findBaseOid(javax.management.ObjectName, java.util.Map, String, String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testFindBaseOidWithUnknownSpecificRoot2 () throws Exception {
		this.logger
				.info("Tests the successful search for base OID in file Example.snmp.xml with an unknown specific root (case using the default root OID of the main application)...");
		final XmlMappingParser parser = XmlMappingParser.newInstance(getClass().getResource("/net/sf/snmpadaptor4j/test/mbean/Example.snmp.xml"));
		final ObjectName mBeanName = new ObjectName("net.sf.snmpadaptor4j.test:type=Example,id=specificRootOid");
		final Map<String, String> rootOidMap = new HashMap<String, String>();
		rootOidMap.put("framework", "1.3.6.1.4.1.9999.1");
		rootOidMap.put("example", "1.3.6.1.4.1.9999.50");
		final String defaultRootOid = null;
		final String mainDefaultRootOid = "1.3.6.1.4.1.88.12.8.1";
		final String baseOid = parser.findBaseOid(mBeanName, rootOidMap, defaultRootOid, mainDefaultRootOid);
		assertEquals("The base OID found is bad", "1.3.6.1.4.1.88.12.8.1.1.3", baseOid);
	}

	/**
	 * Tests the successful search for base OID in file <b>Example.snmp.xml</b> with the default root of current application.
	 * <p>
	 * Test method for {@link XmlMappingParser#findBaseOid(javax.management.ObjectName, java.util.Map, String, String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testFindBaseOidWithDefaultRoot () throws Exception {
		this.logger.info("Tests the successful search for base OID in file Example.snmp.xml with the default root of current application...");
		final XmlMappingParser parser = XmlMappingParser.newInstance(getClass().getResource("/net/sf/snmpadaptor4j/test/mbean/Example.snmp.xml"));
		final ObjectName mBeanName = new ObjectName("net.sf.snmpadaptor4j.test:type=Example,id=anotherClasspath");
		final Map<String, String> rootOidMap = new HashMap<String, String>();
		rootOidMap.put("framework", "1.3.6.1.4.1.9999.1");
		rootOidMap.put("example", "1.3.6.1.4.1.9999.50");
		final String defaultRootOid = "1.3.6.1.4.1.99.12.8.1";
		final String mainDefaultRootOid = "1.3.6.1.4.1.88.12.8.1";
		final String baseOid = parser.findBaseOid(mBeanName, rootOidMap, defaultRootOid, mainDefaultRootOid);
		assertEquals("The base OID found is bad", "1.3.6.1.4.1.99.12.8.1.1.2", baseOid);
	}

	/**
	 * Tests the successful search for base OID in file <b>Example.snmp.xml</b> with the default root of main application.
	 * <p>
	 * Test method for {@link XmlMappingParser#findBaseOid(javax.management.ObjectName, java.util.Map, String, String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testFindBaseOidWithMainDefaultRoot () throws Exception {
		this.logger.info("Tests the successful search for base OID in file Example.snmp.xml with the default root of main application...");
		final XmlMappingParser parser = XmlMappingParser.newInstance(getClass().getResource("/net/sf/snmpadaptor4j/test/mbean/Example.snmp.xml"));
		final ObjectName mBeanName = new ObjectName("net.sf.snmpadaptor4j.test:type=Example,id=anotherClasspath");
		final Map<String, String> rootOidMap = new HashMap<String, String>();
		rootOidMap.put("framework", "1.3.6.1.4.1.9999.1");
		rootOidMap.put("example", "1.3.6.1.4.1.9999.50");
		final String defaultRootOid = null;
		final String mainDefaultRootOid = "1.3.6.1.4.1.88.12.8.1";
		final String baseOid = parser.findBaseOid(mBeanName, rootOidMap, defaultRootOid, mainDefaultRootOid);
		assertEquals("The base OID found is bad", "1.3.6.1.4.1.88.12.8.1.1.2", baseOid);
	}

	/**
	 * Tests the mapping creation of MBean attributes from file <b>Example.snmp.xml</b>.
	 * <p>
	 * Test method for {@link XmlMappingParser#newMBeanAttributeMappingList(Map, ClassLoader, String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewMBeanAttributeMappingList () throws Exception {
		this.logger.info("Tests the mapping creation of MBean attributes from file Example.snmp.xml...");
		final XmlMappingParser parser = XmlMappingParser.newInstance(getClass().getResource("/net/sf/snmpadaptor4j/test/mbean/Example.snmp.xml"));
		final MBeanServer server = ManagementFactory.getPlatformMBeanServer();
		final ObjectName mBeanName = new ObjectName("net.sf.snmpadaptor4j.test:type=Example");
		server.registerMBean(new Example(), mBeanName);
		try {
			final Map<String, MBeanAttributeInfo> mBeanAttributeInfoMap = new HashMap<String, MBeanAttributeInfo>();
			for (final MBeanAttributeInfo mBeanAttributeInfo : server.getMBeanInfo(mBeanName).getAttributes()) {
				mBeanAttributeInfoMap.put(mBeanAttributeInfo.getName(), mBeanAttributeInfo);
			}
			final String baseOid = "1.3.6.1.4.1.99.12.8.1";

			// Expected result
			final MBeanAttributeMappingFactory factory = MBeanAttributeMappingFactory.getInstance();
			final List<MBeanAttributeMapping> expectedMappingList = new ArrayList<MBeanAttributeMapping>();
			SnmpOid oid = SnmpOid.newInstance(baseOid, 1, 0);
			expectedMappingList.add(factory.newMBeanAttributeMapping(oid, "LongAttribute", SnmpDataType.counter64, long.class, true, true));
			oid = SnmpOid.newInstance(baseOid, 2, 0);
			expectedMappingList.add(factory.newMBeanAttributeMapping(oid, "StringAttribute", SnmpDataType.octetString, String.class, false, false));
			oid = SnmpOid.newInstance(baseOid, 3, 0);
			expectedMappingList.add(factory.newMBeanAttributeMapping(oid, "HiddenAttribute", SnmpDataType.opaque, Object.class, false, false));
			oid = SnmpOid.newInstance(baseOid, 4, 0);
			expectedMappingList.add(factory.newMBeanAttributeMapping(oid, "InetAddressAttribute", SnmpDataType.ipAddress, InetAddress.class, true, false));
			oid = SnmpOid.newInstance(baseOid, 5, 0);
			expectedMappingList.add(factory.newMBeanAttributeMapping(oid, "TimesticksAttribute", SnmpDataType.timeTicks, long.class, true, false));
			oid = SnmpOid.newInstance(baseOid, 6, 0);
			expectedMappingList.add(factory.newMBeanAttributeMapping(oid, "ByteAttribute", SnmpDataType.integer32, byte.class, true, false));
			oid = SnmpOid.newInstance(baseOid, 7, 0);
			expectedMappingList.add(factory.newMBeanAttributeMapping(oid, "ShortAttribute", SnmpDataType.gauge32, short.class, true, false));
			oid = SnmpOid.newInstance(baseOid, 8, 0);
			expectedMappingList.add(factory.newMBeanAttributeMapping(oid, "IntAttribute", SnmpDataType.unsigned32, int.class, true, false));
			

			oid = SnmpOid.newInstance(baseOid, 9, 0);
			expectedMappingList.add(factory.newMBeanAttributeMapping(oid, "BooleanAttribute", SnmpDataType.integer32, boolean.class, true, true));
			
			
			oid = SnmpOid.newInstance(baseOid, 10, 0);
			expectedMappingList.add(factory.newMBeanAttributeMapping(oid, "ByteArrayAttribute", SnmpDataType.opaque, byte[].class, true, false));
			oid = SnmpOid.newInstance(baseOid, 11, 0);
			expectedMappingList.add(factory.newMBeanAttributeMapping(oid, "ShortArrayAttribute", SnmpDataType.opaque, short[].class, true, false));
			oid = SnmpOid.newInstance(baseOid, 12, 0);
			expectedMappingList.add(factory.newMBeanAttributeMapping(oid, "IntArrayAttribute", SnmpDataType.opaque, int[].class, true, false));
			oid = SnmpOid.newInstance(baseOid, 13, 0);
			expectedMappingList.add(factory.newMBeanAttributeMapping(oid, "LongArrayAttribute", SnmpDataType.opaque, long[].class, true, false));

			// Test
			final List<MBeanAttributeMapping> mappingList = parser.newMBeanAttributeMappingList(mBeanAttributeInfoMap, getClass().getClassLoader(), baseOid);

			// Checks
			assertEquals("The mapping list is bad", expectedMappingList, mappingList);

		}
		finally {
			server.unregisterMBean(mBeanName);
		}
	}

	/**
	 * Tests the creation of mappings to build SNMP traps from empty file <b>ExampleB.snmp.xml</b>.
	 * <p>
	 * Test method for {@link XmlMappingParser#newSnmpTrapMappingMap(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSnmpTrapMappingMapInEmptyFile () throws Exception {
		this.logger.info("Tests the creation of mappings to build SNMP traps from empty file ExampleB.snmp.xml...");
		final XmlMappingParser parser = XmlMappingParser.newInstance(getClass().getResource("/net/sf/snmpadaptor4j/test/mbean/ExampleB.snmp.xml"));
		final String baseOid = "1.3.6.1.4.1.99.12.8.1";
		final Map<String, SnmpTrapMapping> resultMap = parser.newSnmpTrapMappingMap(baseOid);
		assertNotNull("The map of mappings is NULL", resultMap);
		assertTrue("The map of mappings is not empty", resultMap.isEmpty());
	}

	/**
	 * Tests the creation of mappings to build SNMP traps from file <b>Test1.snmp.xml</b> without <code>variable-bindings</code>.
	 * <p>
	 * Test method for {@link XmlMappingParser#newSnmpTrapMappingMap(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSnmpTrapMappingMapWithoutVariableBindings () throws Exception {
		this.logger.info("Tests the creation of mappings to build SNMP traps from file Test1.snmp.xml without variable-bindings...");
		final XmlMappingParser parser = XmlMappingParser.newInstance(getClass().getResource("/net/sf/snmpadaptor4j/test/mbean/Test1.snmp.xml"));
		final String baseOid = "1.3.6.1.4.1.99.12.8.1";
		final Map<String, SnmpTrapMapping> resultMap = parser.newSnmpTrapMappingMap(baseOid);
		assertNotNull("The map of mappings is NULL", resultMap);
		assertEquals("The map of mappings is bad", newExpectedSnmpTrapMappingMap(baseOid, false, false, false, false), resultMap);
	}

	/**
	 * Tests the creation of mappings to build SNMP traps from file <b>Test2.snmp.xml</b> without <code>userData</code>, <code>sequenceNumber</code> and
	 * <code>message</code>.
	 * <p>
	 * Test method for {@link XmlMappingParser#newSnmpTrapMappingMap(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSnmpTrapMappingMapWithoutUserData () throws Exception {
		this.logger.info("Tests the creation of mappings to build SNMP traps from file Test2.snmp.xml without userData, sequenceNumber and message...");
		final XmlMappingParser parser = XmlMappingParser.newInstance(getClass().getResource("/net/sf/snmpadaptor4j/test/mbean/Test2.snmp.xml"));
		final String baseOid = "1.3.6.1.4.1.99.12.8.1";
		final Map<String, SnmpTrapMapping> resultMap = parser.newSnmpTrapMappingMap(baseOid);
		assertNotNull("The map of mappings is NULL", resultMap);
		assertEquals("The map of mappings is bad", newExpectedSnmpTrapMappingMap(baseOid, false, false, true, false), resultMap);
	}

	/**
	 * Tests the creation of mappings to build SNMP traps from file <b>Test3.snmp.xml</b> without <code>sequenceNumber</code> and <code>message</code>.
	 * <p>
	 * Test method for {@link XmlMappingParser#newSnmpTrapMappingMap(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSnmpTrapMappingMapWithoutSequenceNumberAndMessage () throws Exception {
		this.logger.info("Tests the creation of mappings to build SNMP traps from file Test3.snmp.xml without sequenceNumber and message...");
		final XmlMappingParser parser = XmlMappingParser.newInstance(getClass().getResource("/net/sf/snmpadaptor4j/test/mbean/Test3.snmp.xml"));
		final String baseOid = "1.3.6.1.4.1.99.12.8.1";
		final Map<String, SnmpTrapMapping> resultMap = parser.newSnmpTrapMappingMap(baseOid);
		assertNotNull("The map of mappings is NULL", resultMap);
		assertEquals("The map of mappings is bad", newExpectedSnmpTrapMappingMap(baseOid, false, false, true, true), resultMap);
	}

	/**
	 * Tests the creation of mappings to build SNMP traps from file <b>Example.snmp.xml</b>.
	 * <p>
	 * Test method for {@link XmlMappingParser#newSnmpTrapMappingMap(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSnmpTrapMappingMap () throws Exception {
		this.logger.info("Tests the creation of mappings to build SNMP traps from file Example.snmp.xml...");
		final XmlMappingParser parser = XmlMappingParser.newInstance(getClass().getResource("/net/sf/snmpadaptor4j/test/mbean/Example.snmp.xml"));
		final String baseOid = "1.3.6.1.4.1.99.12.8.1";
		final Map<String, SnmpTrapMapping> resultMap = parser.newSnmpTrapMappingMap(baseOid);
		assertNotNull("The map of mappings is NULL", resultMap);
		assertEquals("The map of mappings is bad", newExpectedSnmpTrapMappingMap(baseOid, true, true, true, true), resultMap);
	}

	/**
	 * Creates and returns the expected mappings to build SNMP traps.
	 * @param baseOid Base OID of MBean instance.
	 * @param hasSequenceNumber <code>TRUE</code> if we put a value in the <code>sequenceNumber</code> field.
	 * @param hasMessage <code>TRUE</code> if we put a value in the <code>message</code> field.
	 * @param hasSystemInfo Value of <code>hasSystemInfo</code> field.
	 * @param hasUserdata <code>TRUE</code> if we put a value in the <code>userdata</code> field.
	 * @return Expected mappings to build SNMP traps.
	 */
	private Map<String, SnmpTrapMapping> newExpectedSnmpTrapMappingMap (final String baseOid, final boolean hasSequenceNumber, final boolean hasMessage,
			final boolean hasSystemInfo, final boolean hasUserdata) {
		final SnmpTrapMappingFactory factory = SnmpTrapMappingFactory.getInstance();
		final SnmpOid sequenceNumberOid = (hasSequenceNumber ? SnmpOid.newInstance(baseOid, 20, 0) : null);
		final SnmpOid messageOid = (hasMessage ? SnmpOid.newInstance(baseOid, 21, 0) : null);
		final DataMapTrapMapping defaultDataMapTrapMapping = factory.newDataMapTrapMapping(sequenceNumberOid, messageOid, hasSystemInfo);
		DataMapTrapMapping dataMapTrapMappingSimple;
		DataMapTrapMapping dataMapTrapMappingMap;
		DataMapTrapMapping dataMapTrapMappingMap1;
		if (hasUserdata) {
			dataMapTrapMappingSimple = factory.newDataMapTrapMapping(sequenceNumberOid, messageOid, hasSystemInfo, SnmpDataType.octetString,
					SnmpOid.newInstance(baseOid, 22, 0));
			List<UserDataEntryDataMapTrapMapping> userDataEntryList = new ArrayList<UserDataEntryDataMapTrapMapping>();
			userDataEntryList.add(factory.newUserDataEntry("vvvvv", SnmpDataType.octetString, SnmpOid.newInstance(baseOid, 23, 0)));
			userDataEntryList.add(factory.newUserDataEntry("wwwww", SnmpDataType.octetString, SnmpOid.newInstance(baseOid, 24, 0)));
			dataMapTrapMappingMap = factory.newDataMapTrapMapping(sequenceNumberOid, messageOid, hasSystemInfo, userDataEntryList);
			userDataEntryList = new ArrayList<UserDataEntryDataMapTrapMapping>();
			userDataEntryList.add(factory.newUserDataEntry("vvvvv", SnmpDataType.octetString, SnmpOid.newInstance(baseOid, 25, 0)));
			userDataEntryList.add(factory.newUserDataEntry("wwwww", SnmpDataType.octetString, SnmpOid.newInstance(baseOid, 26, 0)));
			dataMapTrapMappingMap1 = factory.newDataMapTrapMapping(sequenceNumberOid, messageOid, hasSystemInfo, userDataEntryList);
		}
		else {
			dataMapTrapMappingSimple = defaultDataMapTrapMapping;
			dataMapTrapMappingMap = defaultDataMapTrapMapping;
			dataMapTrapMappingMap1 = defaultDataMapTrapMapping;
		}
		final Map<String, SnmpTrapMapping> expectedMap = new HashMap<String, SnmpTrapMapping>();
		SnmpOid source = SnmpOid.newInstance(baseOid, 7, 0);
		expectedMap.put("shortAttribute.level.up", factory.newSnmpTrapMapping(source, dataMapTrapMappingSimple, GenericSnmpTrapType.linkUp));
		expectedMap.put("shortAttribute.level.down", factory.newSnmpTrapMapping(source, dataMapTrapMappingMap, GenericSnmpTrapType.linkDown));
		expectedMap.put("shortAttribute.level.init", factory.newSnmpTrapMapping(source, dataMapTrapMappingSimple, GenericSnmpTrapType.coldStart));
		source = SnmpOid.newInstance(baseOid, 8, 0);
		expectedMap.put("intAttribute.level.critical", factory.newSnmpTrapMapping(source, dataMapTrapMappingMap, 0));
		expectedMap.put("intAttribute.level.error", factory.newSnmpTrapMapping(source, dataMapTrapMappingMap, 1));
		source = SnmpOid.newInstance(baseOid, 10, 0);
		expectedMap.put("byteArrayAttribute.level.critical", factory.newSnmpTrapMapping(source, defaultDataMapTrapMapping, GenericSnmpTrapType.warmStart));
		expectedMap.put("byteArrayAttribute.level.error", factory.newSnmpTrapMapping(source, dataMapTrapMappingMap1, 1));
		return expectedMap;
	}

	/**
	 * Tests of toString.
	 * <p>
	 * Test method for {@link XmlMappingParser#toString()}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testToString1 () throws Exception {
		this.logger.info("Tests of toString...");
		final URL url = getClass().getResource("/net/sf/snmpadaptor4j/test/mbean/Example.snmp.xml");
		final XmlMappingParser parser = XmlMappingParser.newInstance(url);
		assertEquals("The value returned by toString is bad", "XmlMappingParser[" + url + "]", parser.toString());
	}

}