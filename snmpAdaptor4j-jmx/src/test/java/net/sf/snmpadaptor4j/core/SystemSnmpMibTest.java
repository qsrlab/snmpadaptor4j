package net.sf.snmpadaptor4j.core;

import static org.junit.Assert.*;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;
import java.util.Map.Entry;
import net.sf.snmpadaptor4j.SnmpAppContext;
import net.sf.snmpadaptor4j.api.AttributeAccessor;
import net.sf.snmpadaptor4j.core.SystemSnmpMib;
import net.sf.snmpadaptor4j.mbean.SystemInfo;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import net.sf.snmpadaptor4j.test.mock.SnmpAppContextMock;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.core.SystemSnmpMib}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SystemSnmpMibTest {

	/**
	 * ATTRIBUTE VALUE: system.sysName.0
	 */
	private static final String SYSNAME_VALUE = "JUnit";

	/**
	 * ATTRIBUTE VALUE: system.sysDescr.0
	 */
	private static final String SYSDESCR_VALUE = "Unit test case";

	/**
	 * ATTRIBUTE VALUE: system.sysLocation.0
	 */
	private static final String SYSLOCATION_VALUE = "Somewhere";

	/**
	 * ATTRIBUTE VALUE: system.sysContact.0
	 */
	private static final String SYSCONTACT_VALUE = "Administrator";

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(SystemSnmpMibTest.class);

	/**
	 * MBean containing all informations on the system.
	 */
	private final SystemInfo systemInfo = new SystemInfo();

	/**
	 * Constructor.
	 * @throws Exception Exception if an error has occurred.
	 */
	public SystemSnmpMibTest () throws Exception {
		super();
	}

	/**
	 * Test initialization.
	 */
	@Before
	public void setUp () {
		this.systemInfo.setSysName(SystemSnmpMibTest.SYSNAME_VALUE);
		this.systemInfo.setSysDescr(SystemSnmpMibTest.SYSDESCR_VALUE);
		this.systemInfo.setSysLocation(SystemSnmpMibTest.SYSLOCATION_VALUE);
		this.systemInfo.setSysContact(SystemSnmpMibTest.SYSCONTACT_VALUE);

	}

	/**
	 * Tests the attribute initialization with none root OID.
	 * <p>
	 * Test method for {@link SystemSnmpMib#initSysObjectIDSet()}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testInitSysObjectIDSetWithNoneRootOid () throws Exception {
		this.logger.info("Tests the attribute initialization with none root OID...");
		final SnmpAppContext mainAppContext = new SnmpAppContextMock();
		final Map<ClassLoader, SnmpAppContext> appContextMap = new HashMap<ClassLoader, SnmpAppContext>();
		final SystemSnmpMib systemSnmpMib = new SystemSnmpMib(mainAppContext, appContextMap, this.systemInfo);

		// Test
		systemSnmpMib.initSysObjectIDSet();

		// Checking
		checksAttributes(systemSnmpMib.getMib(), new SnmpOid[] {});

	}

	/**
	 * Tests the attribute initialization with one root OID.
	 * <p>
	 * Test method for {@link SystemSnmpMib#initSysObjectIDSet()}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testInitSysObjectIDSetWithOneRootOid () throws Exception {
		this.logger.info("Tests the attribute initialization with one root OID...");
		final SnmpAppContext mainAppContext = new SnmpAppContextMock(".1.3.6.1.4.1.9999.2");
		final Map<ClassLoader, SnmpAppContext> appContextMap = new HashMap<ClassLoader, SnmpAppContext>();
		final SystemSnmpMib systemSnmpMib = new SystemSnmpMib(mainAppContext, appContextMap, this.systemInfo);

		// Test
		systemSnmpMib.initSysObjectIDSet();

		// Checking
		checksAttributes(systemSnmpMib.getMib(), new SnmpOid[] { SnmpOid.newInstance(".1.3.6.1.4.1.9999.2") });

	}

	/**
	 * Tests the attribute initialization.
	 * <p>
	 * Test method for {@link SystemSnmpMib#initSysObjectIDSet()}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testInitSysObjectIDSet () throws Exception {
		this.logger.info("Tests the attribute initialization...");
		final SnmpAppContext mainAppContext = new SnmpAppContextMock(".1.3.6.1.4.1.9999.2");
		mainAppContext.getRootOidMap().put("root1", ".1.3.6.1.4.1.9999.1.1");
		mainAppContext.getRootOidMap().put("root2", ".1.3.6.1.4.1.9999.1.2");
		mainAppContext.getRootOidMap().put("root3", ".1.3.6.1.4.1.9999.1.3");
		mainAppContext.getRootOidMap().put("default", ".1.3.6.1.4.1.9999.2");
		final Map<ClassLoader, SnmpAppContext> appContextMap = new HashMap<ClassLoader, SnmpAppContext>();
		SnmpAppContext appContext = new SnmpAppContextMock(".1.3.6.1.4.1.9999.3");
		appContext.getRootOidMap().put("root3", ".1.3.6.1.4.1.9999.1.3");
		appContext.getRootOidMap().put("root4", ".1.3.6.1.4.1.9999.1.4");
		appContextMap.put(this.getClass().getClassLoader(), appContext);
		appContext = new SnmpAppContextMock(".1.3.6.1.4.1.9999.2");
		appContext.getRootOidMap().put("root5", ".1.3.6.1.4.1.9999.1.5");
		appContext.getRootOidMap().put("root6", ".1.3.6.1.4.1.9999.1.6");
		appContextMap.put(new URLClassLoader(new URL[] {}), appContext);
		appContext = new SnmpAppContextMock();
		appContext.getRootOidMap().put("root6", ".1.3.6.1.4.1.9999.1.6");
		appContextMap.put(new URLClassLoader(new URL[] {}), appContext);
		final SystemSnmpMib systemSnmpMib = new SystemSnmpMib(mainAppContext, appContextMap, this.systemInfo);

		// Test
		systemSnmpMib.initSysObjectIDSet();

		// Checking
		checksAttributes(
				systemSnmpMib.getMib(),
				new SnmpOid[] { SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1"), SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.2"),
						SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.3"), SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.4"), SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.5"),
						SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.6"), SnmpOid.newInstance(".1.3.6.1.4.1.9999.2"), SnmpOid.newInstance(".1.3.6.1.4.1.9999.3") });

	}

	/**
	 * Tests a research of SnmpMibExternalNode not found.
	 * <p>
	 * Test method for {@link SystemSnmpMib#find(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testFindNotFound () {
		this.logger.info("Tests a research of SnmpMibExternalNode not found...");
		final SystemSnmpMib systemSnmpMib = new SystemSnmpMib(this.systemInfo);
		assertTrue("MBeans were loaded", systemSnmpMib.getMib().isEmpty());
		systemSnmpMib.getMib().clear();
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 5, 0 });
		systemSnmpMib.putExternalNode(0, oid);

		// Test
		final AttributeAccessor node = systemSnmpMib.find(SnmpOid.newInstance(".1.3.6.1.4.1.9999.99"));

		// Checking
		assertNull("A SnmpMibExternalNode was found", node);

	}

	/**
	 * Tests a research of SnmpMibExternalNode found.
	 * <p>
	 * Test method for {@link SystemSnmpMib#find(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testFindFound () {
		this.logger.info("Tests a research of SnmpMibExternalNode found...");
		final SystemSnmpMib systemSnmpMib = new SystemSnmpMib(this.systemInfo);
		assertTrue("MBeans were loaded", systemSnmpMib.getMib().isEmpty());
		systemSnmpMib.getMib().clear();
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 5, 0 });
		systemSnmpMib.putExternalNode(0, oid);

		// Test
		final AttributeAccessor node = systemSnmpMib.find(oid);

		// Checking
		assertNotNull("The SnmpMibExternalNode was not found", node);
		assertEquals("The SnmpMibExternalNode is bad", oid, node.getOid());

	}

	/**
	 * Tests a research of following SnmpMibExternalNode.
	 * <p>
	 * Test method for {@link SystemSnmpMib#next(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testNext () {
		this.logger.info("Tests a research of following SnmpMibExternalNode...");
		final SystemSnmpMib systemSnmpMib = new SystemSnmpMib(this.systemInfo);
		assertTrue("MBeans were loaded", systemSnmpMib.getMib().isEmpty());
		systemSnmpMib.getMib().clear();
		systemSnmpMib.putExternalNode(0, SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 5, 0 }));
		systemSnmpMib.putExternalNode(1, SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 1, 0 }));
		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.2.1.1.1.7");
		final SnmpOid expectedOid = SnmpOid.newInstance(".1.3.6.1.2.1.1.5.0");

		// Test
		final AttributeAccessor node = systemSnmpMib.next(oid);

		// Checking
		assertNotNull("The SnmpMibExternalNode was not found", node);
		assertEquals("The SnmpMibExternalNode is bad", expectedOid, node.getOid());

	}

	/**
	 * Tests a research of following SnmpMibExternalNode with end reached.
	 * <p>
	 * Test method for {@link SystemSnmpMib#next(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testNextWithEndReached () {
		this.logger.info("Tests a research of following SnmpMibExternalNode with end reached...");
		final SystemSnmpMib systemSnmpMib = new SystemSnmpMib(this.systemInfo);
		assertTrue("MBeans were loaded", systemSnmpMib.getMib().isEmpty());
		systemSnmpMib.getMib().clear();
		systemSnmpMib.putExternalNode(0, SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 5, 0 }));
		systemSnmpMib.putExternalNode(1, SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 1, 0 }));
		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.2.1.1.5.1.0");

		// Test
		final AttributeAccessor node = systemSnmpMib.next(oid);

		// Checking
		assertNull("A SnmpMibExternalNode was found", node);

	}

	/**
	 * Tests a research of following SnmpMibExternalNode list.
	 * <p>
	 * Test method for {@link SystemSnmpMib#nextSet(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testNextSet () {
		this.logger.info("Tests a research of following SnmpMibExternalNode list...");
		final SystemSnmpMib systemSnmpMib = new SystemSnmpMib(this.systemInfo);
		assertTrue("MBeans were loaded", systemSnmpMib.getMib().isEmpty());
		systemSnmpMib.getMib().clear();
		systemSnmpMib.putExternalNode(0, SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 5, 0 }));
		systemSnmpMib.putExternalNode(1, SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 1, 0 }));
		systemSnmpMib.putExternalNode(2, SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 6, 0 }));
		systemSnmpMib.putExternalNode(3, SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 4, 0 }));
		systemSnmpMib.putExternalNode(4, SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 3, 0 }));

		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.2.1.1.2.0");

		// Test
		final SortedMap<SnmpOid, AttributeAccessor> nodeMap = systemSnmpMib.nextSet(oid);

		// Checking
		assertNotNull("The SnmpMibExternalNode was not found", nodeMap);
		final Iterator<Entry<SnmpOid, AttributeAccessor>> entryIterator = nodeMap.entrySet().iterator();
		assertTrue("The SnmpMibExternalNode list is bad (1)", entryIterator.hasNext());
		assertEquals("The SnmpMibExternalNode is bad (1)", SnmpOid.newInstance(".1.3.6.1.2.1.1.3.0"), entryIterator.next().getKey());
		assertTrue("The SnmpMibExternalNode list is bad (2)", entryIterator.hasNext());
		assertEquals("The SnmpMibExternalNode is bad (2)", SnmpOid.newInstance(".1.3.6.1.2.1.1.4.0"), entryIterator.next().getKey());
		assertTrue("The SnmpMibExternalNode list is bad (3)", entryIterator.hasNext());
		assertEquals("The SnmpMibExternalNode is bad (3)", SnmpOid.newInstance(".1.3.6.1.2.1.1.5.0"), entryIterator.next().getKey());
		assertTrue("The SnmpMibExternalNode list is bad (4)", entryIterator.hasNext());
		assertEquals("The SnmpMibExternalNode is bad (4)", SnmpOid.newInstance(".1.3.6.1.2.1.1.6.0"), entryIterator.next().getKey());
		assertFalse("The SnmpMibExternalNode list is bad (5)", entryIterator.hasNext());

	}

	/**
	 * Tests the result returned by <code>getSnmpDataType</code> for an unknown attribute.
	 * <p>
	 * Test method for {@link SystemSnmpMib#getSnmpDataType(int)}.
	 * </p>
	 */
	@Test
	public void testGetSnmpDataTypeForUnknownAttribute () {
		this.logger.info("Tests the result returned by getSnmpDataType for an unknown attribute...");
		final SystemSnmpMib systemSnmpMib = new SystemSnmpMib(this.systemInfo);
		assertNull("The result is not null", systemSnmpMib.getSnmpDataType(-1));
	}

	/**
	 * Tests the result returned by <code>getJmxDataType</code> for an unknown attribute.
	 * <p>
	 * Test method for {@link SystemSnmpMib#getJmxDataType(int)}.
	 * </p>
	 */
	@Test
	public void testGetJmxDataTypeForUnknownAttribute () {
		this.logger.info("Tests the result returned by getJmxDataType for an unknown attribute...");
		final SystemSnmpMib systemSnmpMib = new SystemSnmpMib(this.systemInfo);
		assertNull("The result is not null", systemSnmpMib.getJmxDataType(-1));
	}

	/**
	 * Tests the result returned by <code>getValue</code> for an unknown attribute.
	 * <p>
	 * Test method for {@link SystemSnmpMib#getValue(int)}.
	 * </p>
	 */
	@Test
	public void testGetValueForUnknownAttribute () {
		this.logger.info("Tests the result returned by getValue for an unknown attribute...");
		final SystemSnmpMib systemSnmpMib = new SystemSnmpMib(this.systemInfo);
		assertNull("The result is not null", systemSnmpMib.getValue(-1));
	}

	/**
	 * Test method for {@link SystemSnmpMib#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests the toString...");
		final SystemSnmpMib systemSnmpMib = new SystemSnmpMib(this.systemInfo);
		assertEquals("The value returned by toString is bad", "SystemSnmpMib[]", systemSnmpMib.toString());
	}

	/**
	 * Checks all attributes.
	 * @param mibMap Attribute map.
	 * @param expectedSysObjectIDs Expected array of <code>system.sysObjectID</code>.
	 * @throws Exception Exception if an error has occurred.
	 */
	private void checksAttributes (final SortedMap<SnmpOid, AttributeAccessor> mibMap, final SnmpOid[] expectedSysObjectIDs) throws Exception {
		final Iterator<Entry<SnmpOid, AttributeAccessor>> mibIterator = mibMap.entrySet().iterator();
		checkAttribute(mibIterator, "system.sysDescr.0", SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 1, 0 }), SnmpDataType.octetString, String.class,
				SystemSnmpMibTest.SYSDESCR_VALUE, "NEW VALUE", ".1.3.6.1.2.1.1.1.0: (octetString) Unit test case");
		if (expectedSysObjectIDs.length == 1) {
			checkAttribute(mibIterator, "system.sysObjectID.0", SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 2, 0 }), SnmpDataType.objectIdentifier,
					SnmpOid.class, expectedSysObjectIDs[0], SnmpOid.newInstance(new int[] { 2 }), ".1.3.6.1.2.1.1.2.0: (objectIdentifier) "
							+ expectedSysObjectIDs[0]);
		}
		else if (expectedSysObjectIDs.length > 1) {
			for (int i = 0; i < expectedSysObjectIDs.length; i++) {
				checkAttribute(mibIterator, "system.sysObjectID." + i, SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 2, i + 1 }),
						SnmpDataType.objectIdentifier, SnmpOid.class, expectedSysObjectIDs[i], SnmpOid.newInstance(new int[] { 2 }), ".1.3.6.1.2.1.1.2." + (i + 1)
								+ ": (objectIdentifier) " + expectedSysObjectIDs[i]);
			}
		}
		checkAttribute(mibIterator, "system.sysUpTime.0", SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 3, 0 }), SnmpDataType.timeTicks, Long.class, null,
				new Long(10L), null);
		checkAttribute(mibIterator, "system.sysContact.0", SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 4, 0 }), SnmpDataType.octetString, String.class,
				SystemSnmpMibTest.SYSCONTACT_VALUE, "NEW VALUE", ".1.3.6.1.2.1.1.4.0: (octetString) Administrator");
		checkAttribute(mibIterator, "system.sysName.0", SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 5, 0 }), SnmpDataType.octetString, String.class,
				SystemSnmpMibTest.SYSNAME_VALUE, "NEW VALUE", ".1.3.6.1.2.1.1.5.0: (octetString) JUnit");
		checkAttribute(mibIterator, "system.sysLocation.0", SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 2, 1, 1, 6, 0 }), SnmpDataType.octetString, String.class,
				SystemSnmpMibTest.SYSLOCATION_VALUE, "NEW VALUE", ".1.3.6.1.2.1.1.6.0: (octetString) Somewhere");
		assertFalse("There is still attributes", mibIterator.hasNext());
	}

	/**
	 * Checks all properties of an attribute.
	 * @param mibIterator Iterator of attribute list.
	 * @param attributeName Attribute name (used for the messages).
	 * @param expectedOid Expected OID of attribute.
	 * @param expectedSnmpDataType Expected SNMP data type of attribute.
	 * @param expectedJmxDataType Expected JMX data type of attribute.
	 * @param expectedValue Expected value of attribute.
	 * @param newValue New value of attribute to set up.
	 * @param expectedToString Expected <code>toString</code> of attribute.
	 * @throws Exception Exception if an error has occurred.
	 */
	private void checkAttribute (final Iterator<Entry<SnmpOid, AttributeAccessor>> mibIterator, final String attributeName, final SnmpOid expectedOid,
			final SnmpDataType expectedSnmpDataType, final Class<?> expectedJmxDataType, final Object expectedValue, final Object newValue,
			final String expectedToString) throws Exception {
		assertTrue("There are more attributes", mibIterator.hasNext());
		final Entry<SnmpOid, AttributeAccessor> entry = mibIterator.next();
		assertEquals("Bad OID for " + attributeName + " at " + expectedOid, expectedOid, entry.getKey());
		assertEquals("Bad OID for " + attributeName + " at " + expectedOid, expectedOid, entry.getValue().getOid());
		assertEquals("Bad SNMP data type for " + attributeName + " at " + expectedOid, expectedSnmpDataType, entry.getValue().getSnmpDataType());
		assertEquals("Bad JMX data type for " + attributeName + " at " + expectedOid, expectedJmxDataType, entry.getValue().getJmxDataType());
		if (expectedValue != null) {
			assertEquals("Bad value for " + attributeName + " at " + expectedOid, expectedValue, entry.getValue().getValue());
			entry.getValue().setValue(newValue);
			assertEquals("Bad value for " + attributeName + " at " + expectedOid, expectedValue, entry.getValue().getValue());
		}
		else {
			assertEquals("Bad type of value for " + attributeName + " at " + expectedOid, Long.class, entry.getValue().getValue().getClass());
		}
		assertTrue("Bad readable status for " + attributeName + " at " + expectedOid, entry.getValue().isReadable());
		assertFalse("Bad writable status for " + attributeName + " at " + expectedOid, entry.getValue().isWritable());
		if (expectedToString != null) {
			assertEquals("Bad toString for " + attributeName + " at " + expectedOid, expectedToString, entry.getValue().toString());
		}
	}

}