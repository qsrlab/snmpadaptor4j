package net.sf.snmpadaptor4j.core.trap;

import static org.junit.Assert.*;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.Semaphore;
import net.sf.snmpadaptor4j.SnmpManagerConfiguration;
import net.sf.snmpadaptor4j.api.SnmpApiFactory;
import net.sf.snmpadaptor4j.api.SnmpTrapSender;
import net.sf.snmpadaptor4j.object.GenericSnmpTrapType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import net.sf.snmpadaptor4j.object.SnmpTrap;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.core.trap.SnmpManagers}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SnmpManagersTest {

	/**
	 * Thread of double-checked locking for {@link #testCloseWithDoubleCheckedLocking()}
	 */
	protected final class Locker
			implements Runnable {

		/**
		 * Semaphore.
		 */
		private final Semaphore semaphore = new Semaphore(0, true);

		/**
		 * Managers.
		 */
		private final SnmpManagers managers;

		/**
		 * Constructor.
		 * @param managers Managers.
		 */
		protected Locker (final SnmpManagers managers) {
			super();
			this.managers = managers;
		}

		/*
		 * {@inheritDoc}
		 * @see java.lang.Runnable#run()
		 */
		public void run () {
			synchronized (this.managers.senderList) {
				SnmpManagersTest.this.logger.info("Locking...");
				try {
					this.semaphore.acquire();
				}
				catch (final InterruptedException e) {
					SnmpManagersTest.this.logger.error(e);
				}
				SnmpManagersTest.this.logger.info("Unlocking");
			}
		}

		/**
		 * Releases the thread.
		 */
		public void release () {
			this.semaphore.release();
		}

	}

	/**
	 * Thread of stop for {@link #testCloseWithDoubleCheckedLocking()}
	 */
	protected final class Stopper
			implements Runnable {

		/**
		 * Managers.
		 */
		private final SnmpManagers managers;

		/**
		 * <code>TRUE</code> if the thread is finished.
		 */
		protected boolean finished = false;

		/**
		 * Constructor.
		 * @param managers Managers.
		 */
		protected Stopper (final SnmpManagers managers) {
			super();
			this.managers = managers;
		}

		/*
		 * {@inheritDoc}
		 * @see java.lang.Runnable#run()
		 */
		public void run () {
			SnmpManagersTest.this.logger.info("Before close");
			this.managers.close();
			this.finished = true;
			SnmpManagersTest.this.logger.info("After close");
		}

	}

	/**
	 * Logger.
	 */
	protected final Logger logger = Logger.getLogger(SnmpManagersTest.class);

	/**
	 * Tests the initialization of managers.
	 * <p>
	 * Test method for {@link SnmpManagers#initialize(List)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testInitialize () throws Exception {
		this.logger.info("Tests the initialization of managers...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpApiFactory apiFactory = mocksControl.createMock(SnmpApiFactory.class);
		final SnmpTrapSender sender1 = mocksControl.createMock(SnmpTrapSender.class);
		final SnmpTrapSender sender2 = mocksControl.createMock(SnmpTrapSender.class);
		final List<SnmpManagerConfiguration> managerList = new ArrayList<SnmpManagerConfiguration>();
		managerList.add(new SnmpManagerConfiguration("192.168.1.2", 162, 2, "public"));
		managerList.add(new SnmpManagerConfiguration("192.168.1.3", 1162, 1, "trap.public"));
		final SnmpManagers managers = new SnmpManagers(apiFactory);

		// Scenario
		apiFactory.newSnmpTrapSender(EasyMock.eq(InetAddress.getLocalHost()), EasyMock.eq("192.168.1.2"), EasyMock.eq(162), EasyMock.eq(2), EasyMock.eq("public"));
		EasyMock.expectLastCall().andReturn(sender1);
		apiFactory.newSnmpTrapSender(EasyMock.eq(InetAddress.getLocalHost()), EasyMock.eq("192.168.1.3"), EasyMock.eq(1162), EasyMock.eq(1),
				EasyMock.eq("trap.public"));
		EasyMock.expectLastCall().andReturn(sender2);
		mocksControl.replay();

		// Test
		managers.initialize(managerList);

		// Checking
		mocksControl.verify();
		assertEquals("The size of senderList is bad", 2, managers.senderList.size());
		assertEquals("The first sender of senderList is bad", sender1, managers.senderList.get(0));
		assertEquals("The second sender of senderList is bad", sender2, managers.senderList.get(1));

	}

	/**
	 * Tests the sending of SNMP traps with the closing management of connections to SNMP managers.
	 * <p>
	 * Test methods for:
	 * </p>
	 * <ul>
	 * <li>{@link SnmpManagers#send(SnmpTrap)},</li>
	 * <li>{@link SnmpManagers.ClosingTask#run()},</li>
	 * <li>{@link SnmpManagers#close()}.</li>
	 * </ul>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendAndClose () throws Exception {
		testSendAndClose(true);
		testSendAndClose(false);
	}

	/**
	 * Tests the sending of SNMP traps with the closing management of connections to SNMP managers.
	 * <p>
	 * Test methods for:
	 * </p>
	 * <ul>
	 * <li>{@link SnmpManagers#send(SnmpTrap)},</li>
	 * <li>{@link SnmpManagers.ClosingTask#run()},</li>
	 * <li>{@link SnmpManagers#close()}.</li>
	 * </ul>
	 * @param allLevel <code>TRUE</code> for trace all logs.
	 * @throws Exception Exception if an error has occurred.
	 */
	private void testSendAndClose (final boolean allLevel) throws Exception {
		this.logger.info("Tests the sending of SNMP traps with the closing management of connections to SNMP managers (log level = " + (allLevel ? "ALL" : "OFF")
				+ ")...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpApiFactory apiFactory = mocksControl.createMock(SnmpApiFactory.class);
		final SnmpManagers managers = new SnmpManagers(apiFactory, 1000);
		managers.logger.setLevel(allLevel ? Level.ALL : Level.OFF);
		final SnmpTrapSender sender1 = mocksControl.createMock(SnmpTrapSender.class);
		final SnmpTrapSender sender2 = mocksControl.createMock(SnmpTrapSender.class);
		managers.senderList.add(sender1);
		managers.senderList.add(sender2);
		final SnmpTrap trap1 = SnmpTrap.newInstance(0L, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0"), GenericSnmpTrapType.linkDown, null);
		final SnmpTrap trap2 = SnmpTrap.newInstance(0L, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0"), GenericSnmpTrapType.linkUp, null);

		// Scenario (first sending)
		sender1.isConnected();
		EasyMock.expectLastCall().andReturn(Boolean.FALSE);
		sender1.open();
		sender1.send(EasyMock.eq(trap1));
		sender2.isConnected();
		EasyMock.expectLastCall().andReturn(Boolean.FALSE);
		sender2.open();
		sender2.send(EasyMock.eq(trap1));

		// Scenario (second sending)
		sender1.isConnected();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		sender1.send(EasyMock.eq(trap2));
		sender2.isConnected();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		sender2.send(EasyMock.eq(trap2));

		// Scenario (closing of connections)
		sender1.isConnected();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		sender1.close();
		sender2.isConnected();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		sender2.close();

		// Scenario
		mocksControl.replay();

		// Test
		managers.send(trap1);
		Thread.sleep(600);
		managers.send(trap2);
		Thread.sleep(1500);

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the sending of SNMP traps with an exception.
	 * <p>
	 * Test methods for {@link SnmpManagers#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendWithException () throws Exception {
		this.logger.info("Tests the sending of SNMP traps with an exception...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpApiFactory apiFactory = mocksControl.createMock(SnmpApiFactory.class);
		final SnmpManagers managers = new SnmpManagers(apiFactory, 1000);
		managers.logger.setLevel(Level.ALL);
		final SnmpTrapSender sender1 = mocksControl.createMock(SnmpTrapSender.class);
		final SnmpTrapSender sender2 = mocksControl.createMock(SnmpTrapSender.class);
		managers.senderList.add(sender1);
		managers.senderList.add(sender2);
		managers.timer = new Timer();
		final SnmpTrap trap = SnmpTrap.newInstance(0L, SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.7.0"), GenericSnmpTrapType.linkDown, null);

		// Scenario
		sender1.isConnected();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		sender1.send(EasyMock.eq(trap));
		EasyMock.expectLastCall().andThrow(new Exception("MOCK EXCEPTION 1"));
		sender1.getName();
		EasyMock.expectLastCall().andReturn("localhost:162/public");
		sender2.isConnected();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		sender2.send(EasyMock.eq(trap));
		EasyMock.expectLastCall().andThrow(new Exception("MOCK EXCEPTION 2"));
		sender2.getName();
		EasyMock.expectLastCall().andReturn("192.168.1.1:162/public");
		mocksControl.replay();

		// Test
		managers.send(trap);

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the closing of connections to SNMP managers with an exception.
	 * <p>
	 * Test methods for {@link SnmpManagers#close()}.
	 * </p>
	 */
	@Test
	public void testCloseWithException () {
		this.logger.info("Tests the closing of connections to SNMP managers with an exception...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpApiFactory apiFactory = mocksControl.createMock(SnmpApiFactory.class);
		final SnmpManagers managers = new SnmpManagers(apiFactory);
		managers.logger.setLevel(Level.ALL);
		managers.closingTime = 0L;
		managers.timer = new Timer();
		final SnmpTrapSender sender1 = mocksControl.createMock(SnmpTrapSender.class);
		final SnmpTrapSender sender2 = mocksControl.createMock(SnmpTrapSender.class);
		final SnmpTrapSender sender3 = mocksControl.createMock(SnmpTrapSender.class);
		managers.senderList.add(sender1);
		managers.senderList.add(sender2);
		managers.senderList.add(sender3);

		// Scenario
		sender1.isConnected();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		sender1.close();
		EasyMock.expectLastCall().andThrow(new RuntimeException("MOCK EXCEPTION 1"));
		sender1.getName();
		EasyMock.expectLastCall().andReturn("localhost:162/public");
		sender2.isConnected();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		sender2.close();
		EasyMock.expectLastCall().andThrow(new RuntimeException("MOCK EXCEPTION 2"));
		sender2.getName();
		EasyMock.expectLastCall().andReturn("192.168.1.1:162/public");
		sender3.isConnected();
		EasyMock.expectLastCall().andReturn(Boolean.FALSE);
		mocksControl.replay();

		// Test
		managers.close();

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the double-checked locking of connection closing.
	 * <p>
	 * Test methods for {@link SnmpManagers#close()}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testCloseWithDoubleCheckedLocking () throws Exception {
		this.logger.info("Tests the double-checked locking of connection closing...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpApiFactory apiFactory = mocksControl.createMock(SnmpApiFactory.class);
		final SnmpManagers managers = new SnmpManagers(apiFactory);
		managers.logger.setLevel(Level.ALL);
		managers.closingTime = 0L;
		managers.timer = new Timer();
		final SnmpTrapSender sender1 = mocksControl.createMock(SnmpTrapSender.class);
		final SnmpTrapSender sender2 = mocksControl.createMock(SnmpTrapSender.class);
		managers.senderList.add(sender1);
		managers.senderList.add(sender2);

		// Scenario
		mocksControl.replay();

		// Locker
		final Locker locker = new Locker(managers);
		final Thread lockerThread = new Thread(locker);

		// Stopper
		final Stopper stopper = new Stopper(managers);
		final Thread stopperThread = new Thread(stopper);

		// Test
		lockerThread.start();
		stopperThread.start();
		Thread.sleep(500);
		assertFalse("The stop is finished", stopper.finished);
		managers.closingTime = Long.MAX_VALUE;
		locker.release();
		Thread.sleep(500);

		// Checking
		mocksControl.verify();
		assertTrue("The stop is not finished", stopper.finished);

	}

	/**
	 * Test method for {@link SnmpManagers#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests of toString...");
		final SnmpManagers managers = new SnmpManagers(null);
		assertEquals("toString is bad", "SnmpManagers", managers.toString());
	}

}