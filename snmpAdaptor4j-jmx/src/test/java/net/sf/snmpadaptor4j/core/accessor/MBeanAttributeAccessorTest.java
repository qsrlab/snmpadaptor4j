package net.sf.snmpadaptor4j.core.accessor;

import static org.junit.Assert.*;
import java.lang.management.ManagementFactory;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import net.sf.snmpadaptor4j.core.accessor.MBeanAttributeAccessor;
import net.sf.snmpadaptor4j.core.mapping.MBeanAttributeMapping;
import net.sf.snmpadaptor4j.core.mapping.MBeanAttributeMappingFactory;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import net.sf.snmpadaptor4j.test.mbean.Example;
import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.core.accessor.MBeanAttributeAccessor}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class MBeanAttributeAccessorTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(MBeanAttributeAccessorTest.class);

	/**
	 * MBean instance.
	 */
	private final Example mBean = new Example();

	/**
	 * MBean name.
	 */
	private final ObjectName mBeanName = new ObjectName("net.sf.snmpadaptor4j.test:type=Example,id=1");

	/**
	 * Constructor.
	 * @throws Exception Exception if an error has occurred.
	 */
	public MBeanAttributeAccessorTest () throws Exception {
		super();
	}

	/**
	 * Test initialization.
	 * @throws Exception Exception if an error has occurred.
	 */
	@Before
	public void setUp () throws Exception {
		ManagementFactory.getPlatformMBeanServer().registerMBean(this.mBean, this.mBeanName);
		this.mBean.setLongAttribute(43);
	}

	/**
	 * Test finalization.
	 * @throws Exception Exception if an error has occurred.
	 */
	@After
	public void tearDown () throws Exception {
		ManagementFactory.getPlatformMBeanServer().unregisterMBean(this.mBeanName);
	}

	/**
	 * Test method for {@link MBeanAttributeAccessor#getMBeanName()}.
	 */
	@Test
	public void testGetMBeanName () {
		this.logger.info("Tests the getMBeanName...");
		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.1.1.0");
		final MBeanAttributeMapping mapping = MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(oid, "LongAttribute", SnmpDataType.counter64,
				long.class, true, true);
		final MBeanAttributeAccessor node = new MBeanAttributeAccessor(ManagementFactory.getPlatformMBeanServer(), this.mBeanName, mapping);
		assertEquals("The value returned by getMBeanName is bad", this.mBeanName, node.getMBeanName());
	}

	/**
	 * Test method for {@link MBeanAttributeAccessor#getOid()}.
	 */
	@Test
	public void testGetOid () {
		this.logger.info("Tests the getOid...");
		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.1.1.0");
		final MBeanAttributeMapping mapping = MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(oid, "LongAttribute", SnmpDataType.counter64,
				long.class, true, true);
		final MBeanAttributeAccessor node = new MBeanAttributeAccessor(ManagementFactory.getPlatformMBeanServer(), this.mBeanName, mapping);
		assertEquals("The value returned by getOid is bad", oid, node.getOid());
	}

	/**
	 * Test method for {@link MBeanAttributeAccessor#getSnmpDataType()}.
	 */
	@Test
	public void testGetSnmpDataType () {
		this.logger.info("Tests the getSnmpDataType...");
		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.1.1.0");
		final MBeanAttributeMapping mapping = MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(oid, "LongAttribute", SnmpDataType.counter64,
				long.class, true, true);
		final MBeanAttributeAccessor node = new MBeanAttributeAccessor(ManagementFactory.getPlatformMBeanServer(), this.mBeanName, mapping);
		assertEquals("The value returned by getSnmpDataType is bad", SnmpDataType.counter64, node.getSnmpDataType());
	}

	/**
	 * Test method for {@link MBeanAttributeAccessor#getJmxDataType()}.
	 */
	@Test
	public void testGetJmxDataType () {
		this.logger.info("Tests the getJmxDataType...");
		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.1.1.0");
		final MBeanAttributeMapping mapping = MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(oid, "LongAttribute", SnmpDataType.counter64,
				long.class, true, true);
		final MBeanAttributeAccessor node = new MBeanAttributeAccessor(ManagementFactory.getPlatformMBeanServer(), this.mBeanName, mapping);
		assertEquals("The value returned by getJmxDataType is bad", long.class, node.getJmxDataType());
	}

	/**
	 * Test method for {@link MBeanAttributeAccessor#getValue()}.
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testGetValue () throws Exception {
		this.logger.info("Tests the getValue...");
		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.1.1.0");
		final MBeanAttributeMapping mapping = MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(oid, "LongAttribute", SnmpDataType.counter64,
				long.class, true, true);
		final MBeanAttributeAccessor node = new MBeanAttributeAccessor(ManagementFactory.getPlatformMBeanServer(), this.mBeanName, mapping);
		assertEquals("The value returned by getValue is bad", new Long(43), node.getValue());
	}

	/**
	 * Test method for {@link MBeanAttributeAccessor#setValue(java.lang.Object)}.
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetValue () throws Exception {
		this.logger.info("Tests the setValue...");
		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.1.1.0");
		final MBeanAttributeMapping mapping = MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(oid, "LongAttribute", SnmpDataType.counter64,
				long.class, true, true);
		final MBeanAttributeAccessor node = new MBeanAttributeAccessor(ManagementFactory.getPlatformMBeanServer(), this.mBeanName, mapping);
		node.setValue(new Long(25));
		assertEquals("The value returned by setValue is bad", 25, this.mBean.getLongAttribute());
	}

	/**
	 * Test method for {@link MBeanAttributeAccessor#isReadable()}.
	 */
	@Test
	public void testIsReadable () {
		this.logger.info("Tests the isReadable...");
		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.1.1.0");
		final MBeanAttributeMapping mapping = MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(oid, "LongAttribute", SnmpDataType.counter64,
				long.class, true, true);
		final MBeanAttributeAccessor node = new MBeanAttributeAccessor(ManagementFactory.getPlatformMBeanServer(), this.mBeanName, mapping);
		assertTrue("The value returned by isReadable is bad", node.isReadable());
	}

	/**
	 * Test method for {@link MBeanAttributeAccessor#isWritable()}.
	 */
	@Test
	public void testIsWritable () {
		this.logger.info("Tests the isWritable...");
		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.1.1.0");
		final MBeanAttributeMapping mapping = MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(oid, "LongAttribute", SnmpDataType.counter64,
				long.class, true, true);
		final MBeanAttributeAccessor node = new MBeanAttributeAccessor(ManagementFactory.getPlatformMBeanServer(), this.mBeanName, mapping);
		assertTrue("The value returned by isWritable is bad", node.isWritable());
	}

	/**
	 * Nominal test.
	 * <p>
	 * Test method for {@link MBeanAttributeAccessor#toString()}.
	 * </p>
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests the toString (nominal test)...");
		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.1.1.0");
		final MBeanAttributeMapping mapping = MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(oid, "LongAttribute", SnmpDataType.counter64,
				long.class, true, true);
		final MBeanAttributeAccessor node = new MBeanAttributeAccessor(ManagementFactory.getPlatformMBeanServer(), this.mBeanName, mapping);
		assertEquals("The value returned by toString is bad", ".1.3.6.1.4.1.9999.1.1.1.1.0: (counter64) 43", node.toString());
	}

	/**
	 * Error test 1.
	 * <p>
	 * Test method for {@link MBeanAttributeAccessor#toString()}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testToStringWithError1 () throws Exception {
		this.logger.info("Tests the toString (error test 1)...");
		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.1.1.0");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final MBeanServer mBeanServer = mocksControl.createMock(MBeanServer.class);
		final MBeanAttributeMapping mapping = MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(oid, "LongAttribute", SnmpDataType.counter64,
				long.class, true, true);
		final MBeanAttributeAccessor node = new MBeanAttributeAccessor(mBeanServer, this.mBeanName, mapping);

		// Scenario
		mBeanServer.getAttribute(EasyMock.eq(this.mBeanName), EasyMock.eq("LongAttribute"));
		EasyMock.expectLastCall().andThrow(new RuntimeException("MOCK EXCEPTION"));
		mocksControl.replay();

		// Test
		final String result = node.toString();

		// Checking
		mocksControl.verify();
		assertEquals("The value returned by toString is bad", ".1.3.6.1.4.1.9999.1.1.1.1.0: (counter64) ERROR: MOCK EXCEPTION", result);

	}

	/**
	 * Error test 2.
	 * <p>
	 * Test method for {@link MBeanAttributeAccessor#toString()}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testToStringWithError2 () throws Exception {
		this.logger.info("Tests the toString (error test 2)...");
		final SnmpOid oid = SnmpOid.newInstance(".1.3.6.1.4.1.9999.1.1.1.1.0");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final MBeanServer mBeanServer = mocksControl.createMock(MBeanServer.class);
		final MBeanAttributeMapping mapping = MBeanAttributeMappingFactory.getInstance().newMBeanAttributeMapping(oid, "LongAttribute", SnmpDataType.counter64,
				long.class, true, true);
		final MBeanAttributeAccessor node = new MBeanAttributeAccessor(mBeanServer, this.mBeanName, mapping);

		// Scenario
		mBeanServer.getAttribute(EasyMock.eq(this.mBeanName), EasyMock.eq("LongAttribute"));
		EasyMock.expectLastCall().andThrow(new RuntimeException());
		mocksControl.replay();

		// Test
		final String result = node.toString();

		// Checking
		mocksControl.verify();
		assertEquals("The value returned by toString is bad", ".1.3.6.1.4.1.9999.1.1.1.1.0: (counter64) ERROR: java.lang.RuntimeException", result);

	}

}