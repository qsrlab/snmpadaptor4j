package net.sf.snmpadaptor4j.core;

import static org.junit.Assert.*;
import java.util.HashMap;
import java.util.Map;
import javax.management.MBeanServer;
import javax.management.Notification;
import javax.management.NotificationFilter;
import javax.management.ObjectName;
import net.sf.snmpadaptor4j.core.mapping.SnmpTrapMapping;
import net.sf.snmpadaptor4j.core.mapping.SnmpTrapMappingFactory;
import net.sf.snmpadaptor4j.core.trap.SnmpManagers;
import net.sf.snmpadaptor4j.core.trap.SnmpTrapBuilder;
import net.sf.snmpadaptor4j.object.GenericSnmpTrapType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import net.sf.snmpadaptor4j.object.SnmpTrap;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.core.JmxNotificationManager}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class JmxNotificationManagerTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(JmxNotificationManagerTest.class);

	/**
	 * MBean name.
	 */
	private final ObjectName mBeanName1 = new ObjectName("net.sf.snmpadaptor4j.test:type=Example,id=1");

	/**
	 * MBean name.
	 */
	private final ObjectName mBeanName2 = new ObjectName("net.sf.snmpadaptor4j.test:type=Example,id=2");

	/**
	 * Constructor.
	 * @throws Exception Exception if an error has occurred.
	 */
	public JmxNotificationManagerTest () throws Exception {
		super();
	}

	/**
	 * Tests the registering of notification listener with an empty map of mappings.
	 * <p>
	 * Test method for {@link JmxNotificationManager#register(MBeanServer, ObjectName, Map)}.
	 * </p>
	 */
	@Test
	public void testRegisterWithEmptyMappingMap () {
		testRegisterWithEmptyMappingMap(true);
		testRegisterWithEmptyMappingMap(false);
	}

	/**
	 * Tests the registering of notification listener with an empty map of mappings.
	 * <p>
	 * Test method for {@link JmxNotificationManager#register(MBeanServer, ObjectName, Map)}.
	 * </p>
	 * @param allLevel <code>TRUE</code> for trace all logs.
	 */
	private void testRegisterWithEmptyMappingMap (final boolean allLevel) {
		this.logger.info("Tests the registering of notification listener with an empty map of mappings (log level = " + (allLevel ? "ALL" : "OFF") + ")...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final MBeanServer server = mocksControl.createMock(MBeanServer.class);
		final JmxNotificationManager notificationManager = new JmxNotificationManager(null, null);
		notificationManager.logger.setLevel(allLevel ? Level.ALL : Level.OFF);
		assertTrue("The manager already contains listeners", notificationManager.listenerMap.isEmpty());

		// Scenario
		mocksControl.replay();

		// Test
		notificationManager.register(server, this.mBeanName1, new HashMap<String, SnmpTrapMapping>());

		// Checking
		mocksControl.verify();
		assertTrue("The manager contains listeners", notificationManager.listenerMap.isEmpty());

	}

	/**
	 * Tests the registering of notification listener with an exception.
	 * <p>
	 * Test method for {@link JmxNotificationManager#register(MBeanServer, ObjectName, Map)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testRegisterWithException () throws Exception {
		this.logger.info("Tests the registering of notification listener with an exception...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final MBeanServer server = mocksControl.createMock(MBeanServer.class);
		final JmxNotificationManager notificationManager = new JmxNotificationManager(null, null);
		notificationManager.logger.setLevel(Level.ALL);
		assertTrue("The manager already contains listeners", notificationManager.listenerMap.isEmpty());
		final Map<String, SnmpTrapMapping> trapMappingMap = SnmpTrapMappingFactory.getInstance().newExpectedSnmpTrapMappingFromExample("1.3.6.1.4.1.99.12.8.1.1");

		// Scenario
		server.addNotificationListener(EasyMock.eq(this.mBeanName1), EasyMock.anyObject(JmxNotificationManager.Listener.class),
				(NotificationFilter) EasyMock.eq(null), EasyMock.eq(null));
		EasyMock.expectLastCall().andThrow(new RuntimeException("MOCK EXCEPTION"));
		mocksControl.replay();

		// Test
		notificationManager.register(server, this.mBeanName1, trapMappingMap);

		// Checking
		mocksControl.verify();
		assertTrue("The manager contains listeners", notificationManager.listenerMap.isEmpty());

	}

	/**
	 * Tests the registering of notification listener.
	 * <p>
	 * Test method for {@link JmxNotificationManager#register(MBeanServer, ObjectName, Map)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testRegister () throws Exception {
		testRegister(true);
		testRegister(false);
	}

	/**
	 * Tests the registering of notification listener.
	 * <p>
	 * Test method for {@link JmxNotificationManager#register(MBeanServer, ObjectName, Map)}.
	 * </p>
	 * @param allLevel <code>TRUE</code> for trace all logs.
	 * @throws Exception Exception if an error has occurred.
	 */
	private void testRegister (final boolean allLevel) throws Exception {
		this.logger.info("Tests the registering of notification listener (log level = " + (allLevel ? "ALL" : "OFF") + ")...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final MBeanServer server = mocksControl.createMock(MBeanServer.class);
		final JmxNotificationManager notificationManager = new JmxNotificationManager(null, null);
		notificationManager.logger.setLevel(allLevel ? Level.ALL : Level.OFF);
		assertTrue("The manager already contains listeners", notificationManager.listenerMap.isEmpty());
		final Map<String, SnmpTrapMapping> trapMappingMap = SnmpTrapMappingFactory.getInstance().newExpectedSnmpTrapMappingFromExample("1.3.6.1.4.1.99.12.8.1.1");

		// Scenario
		server.addNotificationListener(EasyMock.eq(this.mBeanName1), EasyMock.anyObject(JmxNotificationManager.Listener.class),
				(NotificationFilter) EasyMock.eq(null), EasyMock.eq(null));
		mocksControl.replay();

		// Test
		notificationManager.register(server, this.mBeanName1, trapMappingMap);

		// Checking
		mocksControl.verify();
		assertEquals("The list of listeners is bad", 1, notificationManager.listenerMap.size());

	}

	/**
	 * Tests the unregistering of all notification listeners.
	 * <p>
	 * Test method for {@link JmxNotificationManager#unregisterAll(MBeanServer)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testUnregisterAll () throws Exception {
		testUnregisterAll(true);
		testUnregisterAll(false);
	}

	/**
	 * Tests the unregistering of all notification listeners.
	 * <p>
	 * Test method for {@link JmxNotificationManager#unregisterAll(MBeanServer)}.
	 * </p>
	 * @param allLevel <code>TRUE</code> for trace all logs.
	 * @throws Exception Exception if an error has occurred.
	 */
	private void testUnregisterAll (final boolean allLevel) throws Exception {
		this.logger.info("Tests the unregistering of all notification listeners (log level = " + (allLevel ? "ALL" : "OFF") + ")...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final MBeanServer server = mocksControl.createMock(MBeanServer.class);
		final SnmpTrapBuilder snmpTrapBuilder = mocksControl.createMock(SnmpTrapBuilder.class);
		final JmxNotificationManager notificationManager = new JmxNotificationManager(null, null);
		notificationManager.logger.setLevel(allLevel ? Level.ALL : Level.OFF);
		final JmxNotificationManager.Listener listener1 = notificationManager.new Listener(snmpTrapBuilder);
		final JmxNotificationManager.Listener listener2 = notificationManager.new Listener(snmpTrapBuilder);
		notificationManager.listenerMap.put(this.mBeanName1, listener1);
		notificationManager.listenerMap.put(this.mBeanName2, listener2);
		assertEquals("The list of listeners is bad", 2, notificationManager.listenerMap.size());
		final Map<String, SnmpTrapMapping> trapMappingMap = SnmpTrapMappingFactory.getInstance().newExpectedSnmpTrapMappingFromExample("1.3.6.1.4.1.99.12.8.1.1");

		// Scenario
		if (allLevel) {
			snmpTrapBuilder.getMappingMap();
			EasyMock.expectLastCall().andReturn(trapMappingMap);
		}
		server.isRegistered(EasyMock.eq(this.mBeanName2));
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		server.removeNotificationListener(EasyMock.eq(this.mBeanName2), EasyMock.eq(listener2));
		if (allLevel) {
			snmpTrapBuilder.getMappingMap();
			EasyMock.expectLastCall().andReturn(trapMappingMap);
		}
		server.isRegistered(EasyMock.eq(this.mBeanName1));
		EasyMock.expectLastCall().andReturn(Boolean.FALSE);
		mocksControl.replay();

		// Test
		notificationManager.unregisterAll(server);

		// Checking
		mocksControl.verify();
		assertEquals("The list of listeners is bad", 0, notificationManager.listenerMap.size());

	}

	/**
	 * Tests the unregistering of an MBean without notification listener.
	 * <p>
	 * Test method for {@link JmxNotificationManager#unregister(MBeanServer, ObjectName)}.
	 * </p>
	 */
	@Test
	public void testUnregisterMBeanWithoutNotificationListener () {
		this.logger.info("Tests the unregistering of an MBean without notification listener...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final MBeanServer server = mocksControl.createMock(MBeanServer.class);
		final JmxNotificationManager notificationManager = new JmxNotificationManager(null, null);
		notificationManager.logger.setLevel(Level.ALL);
		final JmxNotificationManager.Listener listener1 = notificationManager.new Listener(null);
		notificationManager.listenerMap.put(this.mBeanName1, listener1);
		assertEquals("The list of listeners is bad", 1, notificationManager.listenerMap.size());

		// Scenario
		mocksControl.replay();

		// Test
		notificationManager.unregister(server, this.mBeanName2);

		// Checking
		mocksControl.verify();
		assertEquals("The list of listeners is bad", 1, notificationManager.listenerMap.size());

	}

	/**
	 * Tests the unregistering of an MBean with an exception.
	 * <p>
	 * Test method for {@link JmxNotificationManager#unregister(MBeanServer, ObjectName)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testUnregisterWithException () throws Exception {
		this.logger.info("Tests the unregistering of an MBean with an exception...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final MBeanServer server = mocksControl.createMock(MBeanServer.class);
		final SnmpTrapBuilder snmpTrapBuilder = mocksControl.createMock(SnmpTrapBuilder.class);
		final JmxNotificationManager notificationManager = new JmxNotificationManager(null, null);
		notificationManager.logger.setLevel(Level.ALL);
		final JmxNotificationManager.Listener listener1 = notificationManager.new Listener(snmpTrapBuilder);
		final JmxNotificationManager.Listener listener2 = notificationManager.new Listener(snmpTrapBuilder);
		notificationManager.listenerMap.put(this.mBeanName1, listener1);
		notificationManager.listenerMap.put(this.mBeanName2, listener2);
		assertEquals("The list of listeners is bad", 2, notificationManager.listenerMap.size());

		// Scenario
		final Map<String, SnmpTrapMapping> trapMappingMap = SnmpTrapMappingFactory.getInstance().newExpectedSnmpTrapMappingFromExample("1.3.6.1.4.1.99.12.8.1.1");
		snmpTrapBuilder.getMappingMap();
		EasyMock.expectLastCall().andReturn(trapMappingMap);
		server.isRegistered(EasyMock.eq(this.mBeanName2));
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		server.removeNotificationListener(EasyMock.eq(this.mBeanName2), EasyMock.eq(listener2));
		EasyMock.expectLastCall().andThrow(new RuntimeException("MOCK EXCEPTION"));
		mocksControl.replay();

		// Test
		notificationManager.unregister(server, this.mBeanName2);

		// Checking
		mocksControl.verify();
		assertEquals("The list of listeners is bad", 2, notificationManager.listenerMap.size());

	}

	/**
	 * Tests the unregistering of an MBean.
	 * <p>
	 * Test method for {@link JmxNotificationManager#unregister(MBeanServer, ObjectName)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testUnregister () throws Exception {
		testUnregister(true);
		testUnregister(false);
	}

	/**
	 * Tests the unregistering of an MBean.
	 * <p>
	 * Test method for {@link JmxNotificationManager#unregister(MBeanServer, ObjectName)}.
	 * </p>
	 * @param allLevel <code>TRUE</code> for trace all logs.
	 * @throws Exception Exception if an error has occurred.
	 */
	private void testUnregister (final boolean allLevel) throws Exception {
		this.logger.info("Tests the unregistering of an MBean (log level = " + (allLevel ? "ALL" : "OFF") + ")...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final MBeanServer server = mocksControl.createMock(MBeanServer.class);
		final SnmpTrapBuilder snmpTrapBuilder = mocksControl.createMock(SnmpTrapBuilder.class);
		final JmxNotificationManager notificationManager = new JmxNotificationManager(null, null);
		notificationManager.logger.setLevel(allLevel ? Level.ALL : Level.OFF);
		final JmxNotificationManager.Listener listener1 = notificationManager.new Listener(snmpTrapBuilder);
		final JmxNotificationManager.Listener listener2 = notificationManager.new Listener(snmpTrapBuilder);
		notificationManager.listenerMap.put(this.mBeanName1, listener1);
		notificationManager.listenerMap.put(this.mBeanName2, listener2);
		assertEquals("The list of listeners is bad", 2, notificationManager.listenerMap.size());

		// Scenario
		if (allLevel) {
			final Map<String, SnmpTrapMapping> trapMappingMap = SnmpTrapMappingFactory.getInstance()
					.newExpectedSnmpTrapMappingFromExample("1.3.6.1.4.1.99.12.8.1.1");
			snmpTrapBuilder.getMappingMap();
			EasyMock.expectLastCall().andReturn(trapMappingMap);
		}
		server.isRegistered(EasyMock.eq(this.mBeanName2));
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		server.removeNotificationListener(EasyMock.eq(this.mBeanName2), EasyMock.eq(listener2));
		mocksControl.replay();

		// Test
		notificationManager.unregister(server, this.mBeanName2);

		// Checking
		mocksControl.verify();
		assertEquals("The list of listeners is bad", 1, notificationManager.listenerMap.size());

	}

	/**
	 * Tests the notification handling in the disabled status.
	 * <p>
	 * Test method for {@link JmxNotificationManager#handleNotification(SnmpTrapBuilder, Notification)}.
	 * </p>
	 */
	@Test
	public void testHandleNotificationInDisabledStatus () {
		this.logger.info("Tests the notification handling in the disabled status...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpTrapBuilder trapBuilder = mocksControl.createMock(SnmpTrapBuilder.class);
		final JmxNotificationManager notificationManager = new JmxNotificationManager(null, null);
		notificationManager.logger.setLevel(Level.ALL);
		assertFalse("The notification manager is enabled", notificationManager.isEnabled());
		final JmxNotificationManager.Listener listener = notificationManager.new Listener(trapBuilder);
		final Notification notification = new Notification("type", this.mBeanName1, 1L, 100L, "MOCK NOTIFICATION");
		final Map<String, String> userDataMap = new HashMap<String, String>();
		userDataMap.put("key1", "value1");
		userDataMap.put("key2", "value2");
		userDataMap.put("key3", "value3");
		notification.setUserData(userDataMap);

		// Scenario
		mocksControl.replay();

		// Test
		listener.handleNotification(notification, null);

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the notification handling without SNMP trap to send.
	 * <p>
	 * Test method for {@link JmxNotificationManager#handleNotification(SnmpTrapBuilder, Notification)}.
	 * </p>
	 */
	@Test
	public void testHandleNotificationWithoutTrap () {
		testHandleNotificationWithoutTrap(true);
		testHandleNotificationWithoutTrap(false);
	}

	/**
	 * Tests the notification handling without SNMP trap to send.
	 * <p>
	 * Test method for {@link JmxNotificationManager#handleNotification(SnmpTrapBuilder, Notification)}.
	 * </p>
	 * @param allLevel <code>TRUE</code> for trace all logs.
	 */
	private void testHandleNotificationWithoutTrap (final boolean allLevel) {
		this.logger.info("Tests the notification handling without SNMP trap to send (log level = " + (allLevel ? "ALL" : "OFF") + ")...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpTrapBuilder trapBuilder = mocksControl.createMock(SnmpTrapBuilder.class);
		final JmxNotificationManager notificationManager = new JmxNotificationManager(null, null);
		notificationManager.logger.setLevel(allLevel ? Level.ALL : Level.OFF);
		notificationManager.setEnabled(true);
		assertTrue("The notification manager isn't enabled", notificationManager.isEnabled());
		final JmxNotificationManager.Listener listener = notificationManager.new Listener(trapBuilder);
		final Notification notification = new Notification("type", this.mBeanName1, 1L, 100L, "MOCK NOTIFICATION");
		final Map<String, String> userDataMap = new HashMap<String, String>();
		userDataMap.put("key1", "value1");
		userDataMap.put("key2", "value2");
		userDataMap.put("key3", "value3");
		notification.setUserData(userDataMap);

		// Scenario
		trapBuilder.newTrap(EasyMock.eq(notification));
		EasyMock.expectLastCall().andReturn(null);
		mocksControl.replay();

		// Test
		listener.handleNotification(notification, null);

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the notification handling with an exception.
	 * <p>
	 * Test method for {@link JmxNotificationManager#handleNotification(SnmpTrapBuilder, Notification)}.
	 * </p>
	 */
	@Test
	public void testHandleNotificationWithException () {
		testHandleNotificationWithException(true);
		testHandleNotificationWithException(false);
	}

	/**
	 * Tests the notification handling with an exception.
	 * <p>
	 * Test method for {@link JmxNotificationManager#handleNotification(SnmpTrapBuilder, Notification)}.
	 * </p>
	 * @param allLevel <code>TRUE</code> for trace all logs.
	 */
	private void testHandleNotificationWithException (final boolean allLevel) {
		this.logger.info("Tests the notification handling with an exception (log level = " + (allLevel ? "ALL" : "OFF") + ")...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpTrapBuilder trapBuilder = mocksControl.createMock(SnmpTrapBuilder.class);
		final SnmpManagers managers = mocksControl.createMock(SnmpManagers.class);
		final JmxNotificationManager notificationManager = new JmxNotificationManager(managers, null);
		notificationManager.logger.setLevel(allLevel ? Level.ALL : Level.OFF);
		notificationManager.setEnabled(true);
		assertTrue("The notification manager isn't enabled", notificationManager.isEnabled());
		final JmxNotificationManager.Listener listener = notificationManager.new Listener(trapBuilder);
		final Notification notification = new Notification("type", this.mBeanName1, 1L, 100L, "MOCK NOTIFICATION");
		notification.setUserData("USER DATA");
		final SnmpTrap trap = SnmpTrap.newInstance(10L, SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.7.0"), GenericSnmpTrapType.linkDown, null);

		// Scenario
		trapBuilder.newTrap(EasyMock.eq(notification));
		EasyMock.expectLastCall().andReturn(trap);
		managers.send(EasyMock.eq(trap));
		EasyMock.expectLastCall().andThrow(new RuntimeException("MOCK EXCEPTION"));
		mocksControl.replay();

		// Test
		listener.handleNotification(notification, null);

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the notification handling.
	 * <p>
	 * Test method for {@link JmxNotificationManager#handleNotification(SnmpTrapBuilder, Notification)}.
	 * </p>
	 */
	@Test
	public void testHandleNotification () {
		testHandleNotification(true);
		testHandleNotification(false);
	}

	/**
	 * Tests the notification handling.
	 * <p>
	 * Test method for {@link JmxNotificationManager#handleNotification(SnmpTrapBuilder, Notification)}.
	 * </p>
	 * @param allLevel <code>TRUE</code> for trace all logs.
	 */
	private void testHandleNotification (final boolean allLevel) {
		this.logger.info("Tests the notification handling (log level = " + (allLevel ? "ALL" : "OFF") + ")...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpTrapBuilder trapBuilder = mocksControl.createMock(SnmpTrapBuilder.class);
		final SnmpManagers managers = mocksControl.createMock(SnmpManagers.class);
		final JmxNotificationManager notificationManager = new JmxNotificationManager(managers, null);
		notificationManager.logger.setLevel(allLevel ? Level.ALL : Level.OFF);
		notificationManager.setEnabled(true);
		assertTrue("The notification manager isn't enabled", notificationManager.isEnabled());
		final JmxNotificationManager.Listener listener = notificationManager.new Listener(trapBuilder);
		final Notification notification = new Notification("type", this.mBeanName1, 1L, 100L, "MOCK NOTIFICATION");
		final SnmpTrap trap = SnmpTrap.newInstance(10L, SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.7.0"), GenericSnmpTrapType.linkDown, null);

		// Scenario
		trapBuilder.newTrap(EasyMock.eq(notification));
		EasyMock.expectLastCall().andReturn(trap);
		managers.send(EasyMock.eq(trap));
		mocksControl.replay();

		// Test
		listener.handleNotification(notification, null);

		// Checking
		mocksControl.verify();

	}

	/**
	 * Test method for {@link JmxNotificationManager#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests of toString...");
		final JmxNotificationManager notificationManager = new JmxNotificationManager(null, null);
		assertEquals("toString is bad (JmxNotificationManager)", "JmxNotificationManager[managers=null; systemInfo=null; listenerMap={}]",
				notificationManager.toString());
		final JmxNotificationManager.Listener listener = notificationManager.new Listener(null);
		assertEquals("toString is bad (JmxNotificationManager.Listener)", "Listener[trapBuilder=null]", listener.toString());
	}

}