package net.sf.snmpadaptor4j.core.mapping;

import static org.junit.Assert.*;
import net.sf.snmpadaptor4j.object.SnmpOid;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.core.mapping.DefaultDataMapTrapMapping}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class DefaultDataMapTrapMappingTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(DefaultDataMapTrapMappingTest.class);

	/**
	 * Tests the constructor.
	 * <p>
	 * Test method for {@link DefaultDataMapTrapMapping#DefaultDataMapTrapMapping(SnmpOid, SnmpOid, boolean)}.
	 * </p>
	 */
	@Test
	public void testDefaultDataMapTrapMapping () {
		this.logger.info("Tests the constructor...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final DefaultDataMapTrapMapping mapping = new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, true);
		assertEquals("sequenceNumberOid is bad", sequenceNumberOid, mapping.getSequenceNumberOid());
		assertEquals("messageOid is bad", messageOid, mapping.getMessageOid());
		assertTrue("hasSystemInfo is bad", mapping.isHasSystemInfo());
	}

	/**
	 * Test method for {@link DefaultDataMapTrapMapping#hashCode()}.
	 */
	@Test
	public void testHashCode () {
		this.logger.info("Tests the hashCode...");
		final DefaultDataMapTrapMapping mapping1 = new DefaultDataMapTrapMapping(null, null, false);
		final DefaultDataMapTrapMapping mapping2 = new DefaultDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"), true);
		final DefaultDataMapTrapMapping mapping3 = new DefaultDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0"), true);
		final DefaultDataMapTrapMapping mapping4 = new DefaultDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.23.0"), true);
		final DefaultDataMapTrapMapping mapping5 = new DefaultDataMapTrapMapping(SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0"),
				SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.23.0"), false);
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping2", mapping1.hashCode() != mapping2.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping3", mapping1.hashCode() != mapping3.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping4", mapping1.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping1 and mapping5", mapping1.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping3", mapping2.hashCode() != mapping3.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping4", mapping2.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping2 and mapping5", mapping2.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping4", mapping3.hashCode() != mapping4.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping3 and mapping5", mapping3.hashCode() != mapping5.hashCode());
		assertTrue("The value returned by hashCode is bad between mapping4 and mapping5", mapping4.hashCode() != mapping5.hashCode());
	}

	/**
	 * Tests the equality with <code>NULL</code>.
	 * <p>
	 * Test method for {@link DefaultDataMapTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithNull () {
		this.logger.info("Tests the equality with NULL...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final DefaultDataMapTrapMapping mapping = new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, true);
		assertFalse("The equality is bad", mapping.equals(null));
	}

	/**
	 * Tests the equality with another object.
	 * <p>
	 * Test method for {@link DefaultDataMapTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithAnotherObject () {
		this.logger.info("Tests the equality with another object...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final DefaultDataMapTrapMapping mapping = new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, true);
		assertFalse("The equality is bad", mapping.equals(new Object()));
	}

	/**
	 * Tests the equality with itself.
	 * <p>
	 * Test method for {@link DefaultDataMapTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithItself () {
		this.logger.info("Tests the equality with itself...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final DefaultDataMapTrapMapping mapping = new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, true);
		assertTrue("The equality is bad", mapping.equals(mapping));
	}

	/**
	 * Tests the equality with the same {@link DefaultDataMapTrapMapping}.
	 * <p>
	 * Test method for {@link DefaultDataMapTrapMapping#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithSameDefaultDataMapTrapMapping () {
		this.logger.info("Tests the equality with the same DefaultDataMapTrapMapping...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final DefaultDataMapTrapMapping mapping1 = new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, true);
		final DefaultDataMapTrapMapping mapping2 = new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, true);
		assertTrue("The equality is bad", mapping1.equals(mapping2));
		assertTrue("The equality with NULL values is bad", new DefaultDataMapTrapMapping(null, null, true).equals(new DefaultDataMapTrapMapping(null, null, true)));
	}

	/**
	 * Tests the equality with a different sequenceNumberOid.
	 * <p>
	 * Test method for {@link DefaultDataMapTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentSequenceNumberOid () {
		this.logger.info("Tests the equality with a different sequenceNumberOid...");
		final SnmpOid sequenceNumberOid1 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid sequenceNumberOid2 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final DefaultDataMapTrapMapping mapping1 = new DefaultDataMapTrapMapping(sequenceNumberOid1, messageOid, true);
		final DefaultDataMapTrapMapping mapping2 = new DefaultDataMapTrapMapping(sequenceNumberOid2, messageOid, true);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)", mapping1.equals(new DefaultDataMapTrapMapping(null, messageOid, true)));
		assertFalse("The equality is bad (NULL value)", new DefaultDataMapTrapMapping(null, messageOid, true).equals(mapping2));
	}

	/**
	 * Tests the equality with a different messageOid.
	 * <p>
	 * Test method for {@link DefaultDataMapTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentMessageOid () {
		this.logger.info("Tests the equality with a different messageOid...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid1 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final SnmpOid messageOid2 = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.22.0");
		final DefaultDataMapTrapMapping mapping1 = new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid1, true);
		final DefaultDataMapTrapMapping mapping2 = new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid2, true);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
		assertFalse("The equality is bad (NULL value)", mapping1.equals(new DefaultDataMapTrapMapping(sequenceNumberOid, null, true)));
		assertFalse("The equality is bad (NULL value)", new DefaultDataMapTrapMapping(sequenceNumberOid, null, true).equals(mapping2));
	}

	/**
	 * Tests the equality with a different hasSystemInfo.
	 * <p>
	 * Test method for {@link DefaultDataMapTrapMapping#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentHasSystemInfo () {
		this.logger.info("Tests the equality with a different hasSystemInfo...");
		final SnmpOid sequenceNumberOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.20.0");
		final SnmpOid messageOid = SnmpOid.newInstance("1.3.6.1.4.1.9999.1.1.1.21.0");
		final DefaultDataMapTrapMapping mapping1 = new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, true);
		final DefaultDataMapTrapMapping mapping2 = new DefaultDataMapTrapMapping(sequenceNumberOid, messageOid, false);
		assertFalse("The equality is bad", mapping1.equals(mapping2));
	}

	/**
	 * Test method for {@link DefaultDataMapTrapMapping#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests the toString...");
		final DefaultDataMapTrapMapping mapping = new DefaultDataMapTrapMapping(null, null, false);
		assertEquals("The value returned by toString is bad", "DefaultDataMapTrapMapping[sequenceNumberOid=null; messageOid=null; hasSystemInfo=false]",
				mapping.toString());
	}

}