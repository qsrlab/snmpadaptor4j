package net.sf.snmpadaptor4j.core.trap;

import static org.junit.Assert.*;
import java.util.HashMap;
import java.util.Map;
import javax.management.Notification;
import javax.management.ObjectName;
import net.sf.snmpadaptor4j.core.mapping.DataMapTrapMapping;
import net.sf.snmpadaptor4j.core.mapping.SnmpTrapMapping;
import net.sf.snmpadaptor4j.core.mapping.SnmpTrapMappingFactory;
import net.sf.snmpadaptor4j.mbean.SystemInfo;
import net.sf.snmpadaptor4j.object.GenericSnmpTrapType;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import net.sf.snmpadaptor4j.object.SnmpTrap;
import net.sf.snmpadaptor4j.object.SnmpTrapData;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.core.trap.SnmpTrapBuilder}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SnmpTrapBuilderTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(SnmpTrapBuilderTest.class);

	/**
	 * MBean name of notification source.
	 */
	private final ObjectName source = new ObjectName("net.sf.snmpadaptor4j.test:type=Example,id=1");

	/**
	 * Constructor.
	 * @throws Exception Exception if an error has occurred.
	 */
	public SnmpTrapBuilderTest () throws Exception {
		super();
	}

	/**
	 * Test method for {@link SnmpTrapBuilder#getMappingMap()}.
	 */
	@Test
	public void testGetMappingMap () {
		this.logger.info("Tests of getMappingMap...");
		final String rootOid = "1.3.6.1.4.1.99.12.8.1.1.1";
		final Map<String, SnmpTrapMapping> mappingMap = SnmpTrapMappingFactory.getInstance().newExpectedSnmpTrapMappingFromTest1(rootOid);
		final SnmpTrapBuilder builder = new SnmpTrapBuilder(mappingMap, null);
		assertEquals("getMappingMap is bad", mappingMap, builder.getMappingMap());
	}

	/**
	 * Tests the building of generic SNMP trap.
	 * <p>
	 * Test method for {@link SnmpTrapBuilder#newTrap(Notification)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewGenericTrap () throws Exception {
		this.logger.info("Tests the building of generic SNMP trap...");
		final String rootOid = "1.3.6.1.4.1.99.12.8.1.1.1";
		final Map<String, SnmpTrapMapping> mappingMap = SnmpTrapMappingFactory.getInstance().newExpectedSnmpTrapMappingFromTest1(rootOid);
		final SnmpTrapBuilder builder = new SnmpTrapBuilder(mappingMap, new SystemInfo());
		final Notification notification = new Notification("shortAttribute.level.down", this.source, 1L, 1000L, "MOCK NOTIFICATION");

		// Test
		final SnmpTrap trap = builder.newTrap(notification);

		// Checking
		final SnmpTrap expectedTrap = SnmpTrap.newInstance(100L, SnmpOid.newInstance(rootOid, 7, 0), GenericSnmpTrapType.linkDown, null);
		assertEquals("The SNMP trap is bad", expectedTrap, trap);

	}

	/**
	 * Tests the building of specific SNMP trap.
	 * <p>
	 * Test method for {@link SnmpTrapBuilder#newTrap(Notification)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSpecificTrap () throws Exception {
		this.logger.info("Tests the building of specific SNMP trap...");
		final String rootOid = "1.3.6.1.4.1.99.12.8.1.1.1";
		final Map<String, SnmpTrapMapping> mappingMap = SnmpTrapMappingFactory.getInstance().newExpectedSnmpTrapMappingFromTest1(rootOid);
		final SnmpTrapBuilder builder = new SnmpTrapBuilder(mappingMap, new SystemInfo());
		final Notification notification = new Notification("intAttribute.level.error", this.source, 2L, 1200L, "MOCK NOTIFICATION");

		// Test
		final SnmpTrap trap = builder.newTrap(notification);

		// Checking
		final SnmpTrap expectedTrap = SnmpTrap.newInstance(120L, SnmpOid.newInstance(rootOid, 8, 0), 1, null);
		assertEquals("The SNMP trap is bad", expectedTrap, trap);

	}

	/**
	 * Tests the building of missing SNMP trap.
	 * <p>
	 * Test method for {@link SnmpTrapBuilder#newTrap(Notification)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewMissingTrap () throws Exception {
		this.logger.info("Tests the building of missing SNMP trap...");
		final String rootOid = "1.3.6.1.4.1.99.12.8.1.1.1";
		final Map<String, SnmpTrapMapping> mappingMap = SnmpTrapMappingFactory.getInstance().newExpectedSnmpTrapMappingFromTest1(rootOid);
		final SnmpTrapBuilder builder = new SnmpTrapBuilder(mappingMap, new SystemInfo());
		final Notification notification = new Notification("notification.missing", this.source, 3L, 1400L, "MOCK NOTIFICATION");

		// Test
		final SnmpTrap trap = builder.newTrap(notification);

		// Checking
		assertNull("The SNMP trap is bad", trap);

	}

	/**
	 * Tests the building of default <code>dataMap</code> of {@link SnmpTrap}.
	 * <p>
	 * Test method for {@link SnmpTrapBuilder#newDataMap(DataMapTrapMapping, Notification)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewDefaultDataMap () throws Exception {
		this.logger.info("Tests the building of default dataMap of SnmpTrap...");
		final String rootOid = "1.3.6.1.4.1.99.12.8.1.1.1";
		final Map<String, SnmpTrapMapping> mappingMap = SnmpTrapMappingFactory.getInstance().newExpectedSnmpTrapMappingFromExample(rootOid);
		final SystemInfo systemInfo = new SystemInfo("Name", "Description", "Location", "Contact");
		final SnmpTrapBuilder builder = new SnmpTrapBuilder(mappingMap, systemInfo);
		final Notification notification = new Notification("byteArrayAttribute.level.critical", this.source, 4L, 1600L, "MOCK NOTIFICATION");
		final SnmpTrapMapping mapping = mappingMap.get(notification.getType());

		// Test
		final Map<SnmpOid, SnmpTrapData> dataMap = builder.newDataMap(mapping.getDataMap(), notification);

		// Checking
		final Map<SnmpOid, SnmpTrapData> expectedDataMap = new HashMap<SnmpOid, SnmpTrapData>();
		expectedDataMap.put(SnmpOid.newInstance(rootOid, 20, 0), new SnmpTrapData(SnmpDataType.unsigned32, new Long(4L)));
		expectedDataMap.put(SnmpOid.newInstance(rootOid, 21, 0), new SnmpTrapData(SnmpDataType.octetString, "MOCK NOTIFICATION"));
		expectedDataMap.put(SnmpOid.SYSNAME_OID, new SnmpTrapData(SnmpDataType.octetString, systemInfo.getSysName()));
		expectedDataMap.put(SnmpOid.SYSDESCR_OID, new SnmpTrapData(SnmpDataType.octetString, systemInfo.getSysDescr()));
		expectedDataMap.put(SnmpOid.SYSLOCATION_OID, new SnmpTrapData(SnmpDataType.octetString, systemInfo.getSysLocation()));
		expectedDataMap.put(SnmpOid.SYSCONTACT_OID, new SnmpTrapData(SnmpDataType.octetString, systemInfo.getSysContact()));
		assertEquals("The dataMap field of SNMP trap is bad", expectedDataMap, dataMap);

	}

	/**
	 * Tests the building of <code>dataMap</code> of {@link SnmpTrap} with a simple userData as <code>NULL</code> value.
	 * <p>
	 * Test method for {@link SnmpTrapBuilder#newDataMap(DataMapTrapMapping, Notification)}.
	 * </p>
	 */
	@Test
	public void testNewDataMapWithSimpleUserDataAsNull () {
		this.logger.info("Tests the building of dataMap of SnmpTrap with a simple userData as NULL value...");
		final String rootOid = "1.3.6.1.4.1.99.12.8.1.1.1";
		final Map<String, SnmpTrapMapping> mappingMap = SnmpTrapMappingFactory.getInstance().newExpectedSnmpTrapMappingFromExample(rootOid);
		final SnmpTrapBuilder builder = new SnmpTrapBuilder(mappingMap, null);
		final Notification notification = new Notification("shortAttribute.level.init", this.source, 5L, 2000L, "MOCK NOTIFICATION");
		final SnmpTrapMapping mapping = mappingMap.get(notification.getType());

		// Test
		final Map<SnmpOid, SnmpTrapData> dataMap = builder.newDataMap(mapping.getDataMap(), notification);

		// Checking
		final Map<SnmpOid, SnmpTrapData> expectedDataMap = new HashMap<SnmpOid, SnmpTrapData>();
		expectedDataMap.put(SnmpOid.newInstance(rootOid, 20, 0), new SnmpTrapData(SnmpDataType.unsigned32, new Long(5L)));
		expectedDataMap.put(SnmpOid.newInstance(rootOid, 21, 0), new SnmpTrapData(SnmpDataType.octetString, "MOCK NOTIFICATION"));
		expectedDataMap.put(SnmpOid.newInstance(rootOid, 22, 0), new SnmpTrapData(SnmpDataType.octetString, null));
		assertEquals("The dataMap field of SNMP trap is bad", expectedDataMap, dataMap);

	}

	/**
	 * Tests the building of <code>dataMap</code> of {@link SnmpTrap} with a simple userData as Map.
	 * <p>
	 * Test method for {@link SnmpTrapBuilder#newDataMap(DataMapTrapMapping, Notification)}.
	 * </p>
	 */
	@Test
	public void testNewDataMapWithSimpleUserDataAsMap () {
		this.logger.info("Tests the building of dataMap of SnmpTrap with a simple userData as Map...");
		final String rootOid = "1.3.6.1.4.1.99.12.8.1.1.1";
		final Map<String, SnmpTrapMapping> mappingMap = SnmpTrapMappingFactory.getInstance().newExpectedSnmpTrapMappingFromExample(rootOid);
		final SnmpTrapBuilder builder = new SnmpTrapBuilder(mappingMap, null);
		final Notification notification = new Notification("shortAttribute.level.init", this.source, 6L, 1800L, "MOCK NOTIFICATION");
		notification.setUserData(new HashMap<Object, Object>());
		final SnmpTrapMapping mapping = mappingMap.get(notification.getType());

		// Test & checking
		try {
			builder.newDataMap(mapping.getDataMap(), notification);
			fail("No exception");
		}
		catch (final RuntimeException e) {
			assertEquals("The message of exception is bad", "UserData field of notification \"" + notification.getType()
					+ "\" contains map - see mapping file of MBean", e.getMessage());
		}

	}

	/**
	 * Tests the building of <code>dataMap</code> of {@link SnmpTrap} with a simple userData.
	 * <p>
	 * Test method for {@link SnmpTrapBuilder#newDataMap(DataMapTrapMapping, Notification)}.
	 * </p>
	 */
	@Test
	public void testNewDataMapWithSimpleUserData () {
		this.logger.info("Tests the building of dataMap of SnmpTrap with a simple userData...");
		final String rootOid = "1.3.6.1.4.1.99.12.8.1.1.1";
		final Map<String, SnmpTrapMapping> mappingMap = SnmpTrapMappingFactory.getInstance().newExpectedSnmpTrapMappingFromExample(rootOid);
		final SnmpTrapBuilder builder = new SnmpTrapBuilder(mappingMap, null);
		final Notification notification = new Notification("shortAttribute.level.init", this.source, 7L, 1800L, "MOCK NOTIFICATION");
		notification.setUserData("USER DATA");
		final SnmpTrapMapping mapping = mappingMap.get(notification.getType());

		// Test
		final Map<SnmpOid, SnmpTrapData> dataMap = builder.newDataMap(mapping.getDataMap(), notification);

		// Checking
		final Map<SnmpOid, SnmpTrapData> expectedDataMap = new HashMap<SnmpOid, SnmpTrapData>();
		expectedDataMap.put(SnmpOid.newInstance(rootOid, 20, 0), new SnmpTrapData(SnmpDataType.unsigned32, new Long(7L)));
		expectedDataMap.put(SnmpOid.newInstance(rootOid, 21, 0), new SnmpTrapData(SnmpDataType.octetString, "MOCK NOTIFICATION"));
		expectedDataMap.put(SnmpOid.newInstance(rootOid, 22, 0), new SnmpTrapData(SnmpDataType.octetString, notification.getUserData()));
		assertEquals("The dataMap field of SNMP trap is bad", expectedDataMap, dataMap);

	}

	/**
	 * Tests the building of <code>dataMap</code> of {@link SnmpTrap} with a map userData as <code>NULL</code> value.
	 * <p>
	 * Test method for {@link SnmpTrapBuilder#newDataMap(DataMapTrapMapping, Notification)}.
	 * </p>
	 */
	@Test
	public void testNewDataMapWithMapUserDataAsNull () {
		this.logger.info("Tests the building of dataMap of SnmpTrap with a map userData as NULL value...");
		final String rootOid = "1.3.6.1.4.1.99.12.8.1.1.1";
		final Map<String, SnmpTrapMapping> mappingMap = SnmpTrapMappingFactory.getInstance().newExpectedSnmpTrapMappingFromExample(rootOid);
		final SnmpTrapBuilder builder = new SnmpTrapBuilder(mappingMap, null);
		final Notification notification = new Notification("intAttribute.level.error", this.source, 8L, 2200L, "MOCK NOTIFICATION");
		final SnmpTrapMapping mapping = mappingMap.get(notification.getType());

		// Test
		final Map<SnmpOid, SnmpTrapData> dataMap = builder.newDataMap(mapping.getDataMap(), notification);

		// Checking
		final Map<SnmpOid, SnmpTrapData> expectedDataMap = new HashMap<SnmpOid, SnmpTrapData>();
		expectedDataMap.put(SnmpOid.newInstance(rootOid, 20, 0), new SnmpTrapData(SnmpDataType.unsigned32, new Long(8L)));
		expectedDataMap.put(SnmpOid.newInstance(rootOid, 21, 0), new SnmpTrapData(SnmpDataType.octetString, "MOCK NOTIFICATION"));
		assertEquals("The dataMap field of SNMP trap is bad", expectedDataMap, dataMap);

	}

	/**
	 * Tests the building of <code>dataMap</code> of {@link SnmpTrap} with a map userData as another object than map.
	 * <p>
	 * Test method for {@link SnmpTrapBuilder#newDataMap(DataMapTrapMapping, Notification)}.
	 * </p>
	 */
	@Test
	public void testNewDataMapWithMapUserDataAsNoMap () {
		this.logger.info("Tests the building of dataMap of SnmpTrap with a map userData as another object than map...");
		final String rootOid = "1.3.6.1.4.1.99.12.8.1.1.1";
		final Map<String, SnmpTrapMapping> mappingMap = SnmpTrapMappingFactory.getInstance().newExpectedSnmpTrapMappingFromExample(rootOid);
		final SnmpTrapBuilder builder = new SnmpTrapBuilder(mappingMap, null);
		final Notification notification = new Notification("intAttribute.level.error", this.source, 9L, 2400L, "MOCK NOTIFICATION");
		notification.setUserData("USER DATA");
		final SnmpTrapMapping mapping = mappingMap.get(notification.getType());

		// Test & checking
		try {
			builder.newDataMap(mapping.getDataMap(), notification);
			fail("No exception");
		}
		catch (final RuntimeException e) {
			assertEquals("UserData field of notification \"" + notification.getType() + "\" doesn't contain map - see mapping file of MBean", e.getMessage());
		}

	}

	/**
	 * Tests the building of <code>dataMap</code> of {@link SnmpTrap} with a map userData as empty.
	 * <p>
	 * Test method for {@link SnmpTrapBuilder#newDataMap(DataMapTrapMapping, Notification)}.
	 * </p>
	 */
	@Test
	public void testNewDataMapWithMapUserDataAsEmpty () {
		this.logger.info("Tests the building of dataMap of SnmpTrap with a map userData as empty...");
		final String rootOid = "1.3.6.1.4.1.99.12.8.1.1.1";
		final Map<String, SnmpTrapMapping> mappingMap = SnmpTrapMappingFactory.getInstance().newExpectedSnmpTrapMappingFromExample(rootOid);
		final SnmpTrapBuilder builder = new SnmpTrapBuilder(mappingMap, null);
		final Notification notification = new Notification("intAttribute.level.error", this.source, 10L, 2600L, "MOCK NOTIFICATION");
		final SnmpTrapMapping mapping = mappingMap.get(notification.getType());

		// Test
		final Map<SnmpOid, SnmpTrapData> dataMap = builder.newDataMap(mapping.getDataMap(), notification);

		// Checking
		final Map<SnmpOid, SnmpTrapData> expectedDataMap = new HashMap<SnmpOid, SnmpTrapData>();
		expectedDataMap.put(SnmpOid.newInstance(rootOid, 20, 0), new SnmpTrapData(SnmpDataType.unsigned32, new Long(10L)));
		expectedDataMap.put(SnmpOid.newInstance(rootOid, 21, 0), new SnmpTrapData(SnmpDataType.octetString, "MOCK NOTIFICATION"));
		assertEquals("The dataMap field of SNMP trap is bad", expectedDataMap, dataMap);

	}

	/**
	 * Tests the building of <code>dataMap</code> of {@link SnmpTrap} with a map userData (case 1).
	 * <p>
	 * Test method for {@link SnmpTrapBuilder#newDataMap(DataMapTrapMapping, Notification)}.
	 * </p>
	 */
	@Test
	public void testNewDataMapWithMapUserData1 () {
		this.logger.info("Tests the building of dataMap of SnmpTrap with a map userData (case 1)...");
		final String rootOid = "1.3.6.1.4.1.99.12.8.1.1.1";
		final Map<String, SnmpTrapMapping> mappingMap = SnmpTrapMappingFactory.getInstance().newExpectedSnmpTrapMappingFromExample(rootOid);
		final SnmpTrapBuilder builder = new SnmpTrapBuilder(mappingMap, null);
		final Notification notification = new Notification("intAttribute.level.error", this.source, 11L, 2700L, "MOCK NOTIFICATION");
		final Map<String, String> map = new HashMap<String, String>();
		map.put("vvvvv", null);
		map.put("wwwww", "USER DATA wwwww");
		map.put("missing", "USER DATA missing");
		notification.setUserData(map);
		final SnmpTrapMapping mapping = mappingMap.get(notification.getType());

		// Test
		final Map<SnmpOid, SnmpTrapData> dataMap = builder.newDataMap(mapping.getDataMap(), notification);

		// Checking
		final Map<SnmpOid, SnmpTrapData> expectedDataMap = new HashMap<SnmpOid, SnmpTrapData>();
		expectedDataMap.put(SnmpOid.newInstance(rootOid, 20, 0), new SnmpTrapData(SnmpDataType.unsigned32, new Long(11L)));
		expectedDataMap.put(SnmpOid.newInstance(rootOid, 21, 0), new SnmpTrapData(SnmpDataType.octetString, "MOCK NOTIFICATION"));
		expectedDataMap.put(SnmpOid.newInstance(rootOid, 23, 0), new SnmpTrapData(SnmpDataType.octetString, null));
		expectedDataMap.put(SnmpOid.newInstance(rootOid, 24, 0), new SnmpTrapData(SnmpDataType.octetString, "USER DATA wwwww"));
		assertEquals("The dataMap field of SNMP trap is bad", expectedDataMap, dataMap);

	}

	/**
	 * Tests the building of <code>dataMap</code> of {@link SnmpTrap} with a map userData (case 2).
	 * <p>
	 * Test method for {@link SnmpTrapBuilder#newDataMap(DataMapTrapMapping, Notification)}.
	 * </p>
	 */
	@Test
	public void testNewDataMapWithMapUserData2 () {
		this.logger.info("Tests the building of dataMap of SnmpTrap with a map userData (case 2)...");
		final String rootOid = "1.3.6.1.4.1.99.12.8.1.1.1";
		final Map<String, SnmpTrapMapping> mappingMap = SnmpTrapMappingFactory.getInstance().newExpectedSnmpTrapMappingFromExample(rootOid);
		final SnmpTrapBuilder builder = new SnmpTrapBuilder(mappingMap, null);
		final Notification notification = new Notification("intAttribute.level.error", this.source, 11L, 2700L, "MOCK NOTIFICATION");
		final Map<String, String> map = new HashMap<String, String>();
		map.put("wwwww", "USER DATA wwwww");
		map.put("missing", "USER DATA missing");
		notification.setUserData(map);
		final SnmpTrapMapping mapping = mappingMap.get(notification.getType());

		// Test
		final Map<SnmpOid, SnmpTrapData> dataMap = builder.newDataMap(mapping.getDataMap(), notification);

		// Checking
		final Map<SnmpOid, SnmpTrapData> expectedDataMap = new HashMap<SnmpOid, SnmpTrapData>();
		expectedDataMap.put(SnmpOid.newInstance(rootOid, 20, 0), new SnmpTrapData(SnmpDataType.unsigned32, new Long(11L)));
		expectedDataMap.put(SnmpOid.newInstance(rootOid, 21, 0), new SnmpTrapData(SnmpDataType.octetString, "MOCK NOTIFICATION"));
		expectedDataMap.put(SnmpOid.newInstance(rootOid, 24, 0), new SnmpTrapData(SnmpDataType.octetString, "USER DATA wwwww"));
		assertEquals("The dataMap field of SNMP trap is bad", expectedDataMap, dataMap);

	}

	/**
	 * Test method for {@link SnmpTrapBuilder#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests of toString...");
		final SnmpTrapBuilder builder = new SnmpTrapBuilder(null, null);
		assertEquals("toString is bad", "SnmpTrapBuilder:null", builder.toString());
	}

}