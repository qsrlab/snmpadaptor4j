package net.sf.snmpadaptor4j;

import static org.junit.Assert.*;
import net.sf.snmpadaptor4j.SnmpManagerConfiguration;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.SnmpManagerConfiguration}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SnmpManagerConfigurationTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(SnmpManagerConfigurationTest.class);

	/**
	 * Tests the constructor.
	 * <p>
	 * Test method for {@link SnmpManagerConfiguration#SnmpManagerConfiguration(String, int, int, String)}.
	 * </p>
	 */
	@Test
	public void testSnmpManager () {
		this.logger.info("Tests the constructor...");
		final SnmpManagerConfiguration snmpManager = new SnmpManagerConfiguration("localhost", 162, 2, "public");
		assertEquals("address is bad", "localhost", snmpManager.getAddress());
		assertEquals("port is bad", 162, snmpManager.getPort());
		assertEquals("version is bad", 2, snmpManager.getVersion());
		assertEquals("community is bad", "public", snmpManager.getCommunity());
	}

	/**
	 * Test method for {@link SnmpManagerConfiguration#hashCode()}.
	 */
	@Test
	public void testHashCode () {
		this.logger.info("Tests the hashCode...");
		final SnmpManagerConfiguration manager1 = new SnmpManagerConfiguration(null, 162, 2, null);
		final SnmpManagerConfiguration manager2 = new SnmpManagerConfiguration("localhost", 162, 2, "public");
		final SnmpManagerConfiguration manager3 = new SnmpManagerConfiguration("127.0.0.1", 162, 2, "public");
		final SnmpManagerConfiguration manager4 = new SnmpManagerConfiguration("localhost", 1162, 2, "public");
		final SnmpManagerConfiguration manager5 = new SnmpManagerConfiguration("localhost", 162, 1, "public");
		final SnmpManagerConfiguration manager6 = new SnmpManagerConfiguration("localhost", 162, 2, "private");
		assertTrue("The value returned by hashCode is bad between manager1 and manager2", manager1.hashCode() != manager2.hashCode());
		assertTrue("The value returned by hashCode is bad between manager1 and manager3", manager1.hashCode() != manager3.hashCode());
		assertTrue("The value returned by hashCode is bad between manager1 and manager4", manager1.hashCode() != manager4.hashCode());
		assertTrue("The value returned by hashCode is bad between manager1 and manager5", manager1.hashCode() != manager5.hashCode());
		assertTrue("The value returned by hashCode is bad between manager1 and manager6", manager1.hashCode() != manager6.hashCode());
		assertTrue("The value returned by hashCode is bad between manager2 and manager3", manager2.hashCode() != manager3.hashCode());
		assertTrue("The value returned by hashCode is bad between manager2 and manager4", manager2.hashCode() != manager4.hashCode());
		assertTrue("The value returned by hashCode is bad between manager2 and manager5", manager2.hashCode() != manager5.hashCode());
		assertTrue("The value returned by hashCode is bad between manager2 and manager6", manager2.hashCode() != manager6.hashCode());
		assertTrue("The value returned by hashCode is bad between manager3 and manager4", manager3.hashCode() != manager4.hashCode());
		assertTrue("The value returned by hashCode is bad between manager3 and manager5", manager3.hashCode() != manager5.hashCode());
		assertTrue("The value returned by hashCode is bad between manager3 and manager6", manager3.hashCode() != manager6.hashCode());
		assertTrue("The value returned by hashCode is bad between manager4 and manager5", manager4.hashCode() != manager5.hashCode());
		assertTrue("The value returned by hashCode is bad between manager4 and manager6", manager4.hashCode() != manager6.hashCode());
		assertTrue("The value returned by hashCode is bad between manager5 and manager6", manager5.hashCode() != manager6.hashCode());
	}

	/**
	 * Tests the equality with <code>NULL</code>.
	 * <p>
	 * Test method for {@link SnmpManagerConfiguration#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithNull () {
		this.logger.info("Tests the equality with NULL...");
		final SnmpManagerConfiguration snmpManager = new SnmpManagerConfiguration("localhost", 162, 2, "public");
		assertFalse("The equality is bad", snmpManager.equals(null));
	}

	/**
	 * Tests the equality with another object.
	 * <p>
	 * Test method for {@link SnmpManagerConfiguration#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithAnotherObject () {
		this.logger.info("Tests the equality with another object...");
		final SnmpManagerConfiguration snmpManager = new SnmpManagerConfiguration("localhost", 162, 2, "public");
		assertFalse("The equality is bad", snmpManager.equals(new Object()));
	}

	/**
	 * Tests the equality with itself.
	 * <p>
	 * Test method for {@link SnmpManagerConfiguration#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithItself () {
		this.logger.info("Tests the equality with itself...");
		final SnmpManagerConfiguration snmpManager = new SnmpManagerConfiguration("localhost", 162, 2, "public");
		assertTrue("The equality is bad", snmpManager.equals(snmpManager));
	}

	/**
	 * Tests the equality with the same manager.
	 * <p>
	 * Test method for {@link SnmpManagerConfiguration#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithSameManager () {
		this.logger.info("Tests the equality with the same manager...");
		assertTrue("The equality is bad",
				new SnmpManagerConfiguration("localhost", 162, 2, "public").equals(new SnmpManagerConfiguration("localhost", 162, 2, "public")));
		assertTrue("The equality with NULL values is bad", new SnmpManagerConfiguration(null, 162, 2, null).equals(new SnmpManagerConfiguration(null, 162, 2, null)));
	}

	/**
	 * Tests the equality with a different address.
	 * <p>
	 * Test method for {@link SnmpManagerConfiguration#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentAddress () {
		this.logger.info("Tests the equality with a different address...");
		assertFalse("The equality is bad",
				new SnmpManagerConfiguration("localhost", 162, 2, "public").equals(new SnmpManagerConfiguration("127.0.0.1", 162, 2, "public")));
		assertFalse("The equality is bad (NULL value)",
				new SnmpManagerConfiguration("localhost", 162, 2, "public").equals(new SnmpManagerConfiguration(null, 162, 2, "public")));
		assertFalse("The equality is bad (NULL value)",
				new SnmpManagerConfiguration(null, 162, 2, "public").equals(new SnmpManagerConfiguration("localhost", 162, 2, "public")));
	}

	/**
	 * Tests the equality with a different port.
	 * <p>
	 * Test method for {@link SnmpManagerConfiguration#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentPort () {
		this.logger.info("Tests the equality with a different port...");
		assertFalse("The equality is bad",
				new SnmpManagerConfiguration("localhost", 162, 2, "public").equals(new SnmpManagerConfiguration("localhost", 1162, 2, "public")));
	}

	/**
	 * Tests the equality with a different version.
	 * <p>
	 * Test method for {@link SnmpManagerConfiguration#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentVersion () {
		this.logger.info("Tests the equality with a different version...");
		assertFalse("The equality is bad",
				new SnmpManagerConfiguration("localhost", 162, 2, "public").equals(new SnmpManagerConfiguration("localhost", 162, 1, "public")));
	}

	/**
	 * Tests the equality with a different community.
	 * <p>
	 * Test method for {@link SnmpManagerConfiguration#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentCommunity () {
		this.logger.info("Tests the equality with a different community...");
		assertFalse("The equality is bad",
				new SnmpManagerConfiguration("localhost", 162, 2, "public").equals(new SnmpManagerConfiguration("localhost", 162, 2, "other")));
		assertFalse("The equality is bad (NULL value)",
				new SnmpManagerConfiguration("localhost", 162, 2, "public").equals(new SnmpManagerConfiguration("localhost", 162, 2, null)));
		assertFalse("The equality is bad (NULL value)",
				new SnmpManagerConfiguration("localhost", 162, 2, null).equals(new SnmpManagerConfiguration("localhost", 162, 2, "public")));
	}

	/**
	 * Test method for {@link SnmpManagerConfiguration#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests the toString...");
		final SnmpManagerConfiguration snmpManager = new SnmpManagerConfiguration("localhost", 162, 2, "public");
		assertEquals("The value returned by toString is bad", "localhost:162/public", snmpManager.toString());
	}

}