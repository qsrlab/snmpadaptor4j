package net.sf.snmpadaptor4j.test.mbean;

/**
 * MBean example.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public interface ExampleBMBean {

	/**
	 * Returns the <code>long</code> attribute.
	 * @return <code>Long</code> attribute.
	 */
	long getLongAttribute ();

	/**
	 * Sets the <code>long</code> attribute.
	 * @param longAttribute <code>Long</code> attribute.
	 */
	void setLongAttribute (long longAttribute);

}