package net.sf.snmpadaptor4j.test.mbean;

import java.net.InetAddress;

/**
 * MBean example.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public interface ExampleMBean {

	/**
	 * Returns the <code>long</code> attribute.
	 * @return <code>Long</code> attribute.
	 */
	long getLongAttribute ();

	/**
	 * Sets the <code>long</code> attribute.
	 * @param longAttribute <code>Long</code> attribute.
	 */
	void setLongAttribute (long longAttribute);

	/**
	 * Returns the <code>String</code> attribute.
	 * @return <code>String</code> attribute.
	 */
	String getStringAttribute ();

	/**
	 * Sets the <code>String</code> attribute.
	 * @param stringAttribute <code>String</code> attribute.
	 */
	void setStringAttribute (String stringAttribute);

	/**
	 * Returns the hidden attribute.
	 * @return Hidden attribute.
	 */
	Object getHiddenAttribute ();

	/**
	 * Sets the hidden attribute.
	 * @param hiddenAttribute Hidden attribute.
	 */
	void setHiddenAttribute (Object hiddenAttribute);

	/**
	 * Returns the <code>InetAddress</code> attribute.
	 * @return <code>InetAddress</code> attribute.
	 */
	InetAddress getInetAddressAttribute ();

	/**
	 * Returns the <code>timesticks</code> attribute.
	 * @return <code>timesticks</code> attribute.
	 */
	long getTimesticksAttribute ();

	/**
	 * Sets the <code>timesticks</code> attribute.
	 * @param timesticksAttribute <code>timesticks</code> attribute.
	 */
	void setTimesticksAttribute (long timesticksAttribute);

	/**
	 * Returns the <code>byte</code> attribute.
	 * @return <code>byte</code> attribute.
	 */
	byte getByteAttribute ();

	/**
	 * Sets the <code>byte</code> attribute.
	 * @param byteAttribute <code>byte</code> attribute.
	 */
	void setByteAttribute (byte byteAttribute);

	/**
	 * Returns the <code>short</code> attribute.
	 * @return <code>short</code> attribute.
	 */
	short getShortAttribute ();

	/**
	 * Sets the <code>short</code> attribute.
	 * @param shortAttribute <code>short</code> attribute.
	 */
	void setShortAttribute (short shortAttribute);

	/**
	 * Returns the <code>int</code> attribute.
	 * @return <code>int</code> attribute.
	 */
	int getIntAttribute ();

	/**
	 * Sets the <code>int</code> attribute.
	 * @param intAttribute <code>int</code> attribute.
	 */
	void setIntAttribute (int intAttribute);

	/**
	 * Returns the <code>boolean</code> attribute.
	 * @return <code>boolean</code> attribute.
	 */
	boolean isBooleanAttribute ();

	/**
	 * Sets the <code>boolean</code> attribute.
	 * @param booleanAttribute <code>boolean</code> attribute.
	 */
	void setBooleanAttribute (boolean booleanAttribute);

	/**
	 * Returns the <code>byte</code> array attribute.
	 * @return <code>byte</code> array attribute.
	 */
	byte[] getByteArrayAttribute ();

	/**
	 * Sets the <code>byte</code> array attribute.
	 * @param byteArrayAttribute <code>byte</code> array attribute.
	 */
	void setByteArrayAttribute (byte[] byteArrayAttribute);

	/**
	 * Returns the <code>short</code> array attribute.
	 * @return <code>short</code> array attribute.
	 */
	short[] getShortArrayAttribute ();

	/**
	 * Sets the <code>short</code> array attribute.
	 * @param shortArrayAttribute <code>short</code> array attribute.
	 */
	void setShortArrayAttribute (short[] shortArrayAttribute);

	/**
	 * Returns the <code>int</code> array attribute.
	 * @return <code>int</code> array attribute.
	 */
	int[] getIntArrayAttribute ();

	/**
	 * Sets the <code>int</code> array attribute.
	 * @param intArrayAttribute <code>int</code> array attribute.
	 */
	void setIntArrayAttribute (int[] intArrayAttribute);

	/**
	 * Returns the <code>long</code> array attribute.
	 * @return <code>long</code> array attribute.
	 */
	long[] getLongArrayAttribute ();

	/**
	 * Sets the <code>long</code> array attribute.
	 * @param longArrayAttribute <code>long</code> array attribute.
	 */
	void setLongArrayAttribute (long[] longArrayAttribute);

}