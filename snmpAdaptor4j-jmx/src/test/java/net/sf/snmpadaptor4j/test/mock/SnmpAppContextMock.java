package net.sf.snmpadaptor4j.test.mock;

import java.util.HashMap;
import java.util.Map;
import javax.management.ObjectName;
import net.sf.snmpadaptor4j.SnmpAppContext;

/**
 * Mock object for {@link SnmpAppContext}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SnmpAppContextMock
		implements SnmpAppContext {

	/**
	 * Default root OID containing the attributes of the application.
	 */
	private final String defaultRootOid;

	/**
	 * Map of root OIDs where the attributes of the application will stay.
	 */
	private final Map<String, String> rootOidMap = new HashMap<String, String>();

	/**
	 * Map of MBean OIDs (never <code>NULL</code> but can be empty).
	 */
	private final Map<ObjectName, String> mBeanOidMap = new HashMap<ObjectName, String>();

	/**
	 * Constructor.
	 */
	public SnmpAppContextMock () {
		super();
		this.defaultRootOid = null;
	}

	/**
	 * Constructor.
	 * @param defaultRootOid Default root OID containing the attributes of the application.
	 */
	public SnmpAppContextMock (final String defaultRootOid) {
		super();
		this.defaultRootOid = defaultRootOid;
	}

	/**
	 * Constructor.
	 * @param mBeanName MBean name.
	 * @param mBeanOid MBean OID.
	 */
	public SnmpAppContextMock (final ObjectName mBeanName, final String mBeanOid) {
		super();
		this.defaultRootOid = null;
		this.mBeanOidMap.put(mBeanName, mBeanOid);
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpAppContext#getDefaultRootOid()
	 */
	public String getDefaultRootOid () {
		return this.defaultRootOid;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpAppContext#getRootOidMap()
	 */
	public Map<String, String> getRootOidMap () {
		return this.rootOidMap;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpAppContext#getMBeanOidMap()
	 */
	public Map<ObjectName, String> getMBeanOidMap () {
		return this.mBeanOidMap;
	}

}