package net.sf.snmpadaptor4j.test;

import static org.junit.Assert.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import net.sf.snmpadaptor4j.object.SnmpTrap;
import net.sf.snmpadaptor4j.object.SnmpTrapData;
import net.sf.snmpadaptor4j.object.GenericSnmpTrapType;
import org.apache.log4j.Logger;
import org.opennms.protocols.snmp.SnmpCounter32;
import org.opennms.protocols.snmp.SnmpCounter64;
import org.opennms.protocols.snmp.SnmpGauge32;
import org.opennms.protocols.snmp.SnmpIPAddress;
import org.opennms.protocols.snmp.SnmpInt32;
import org.opennms.protocols.snmp.SnmpObjectId;
import org.opennms.protocols.snmp.SnmpOctetString;
import org.opennms.protocols.snmp.SnmpOpaque;
import org.opennms.protocols.snmp.SnmpPduPacket;
import org.opennms.protocols.snmp.SnmpPduRequest;
import org.opennms.protocols.snmp.SnmpPduTrap;
import org.opennms.protocols.snmp.SnmpSyntax;
import org.opennms.protocols.snmp.SnmpTimeTicks;
import org.opennms.protocols.snmp.SnmpTrapHandler;
import org.opennms.protocols.snmp.SnmpTrapSession;
import org.opennms.protocols.snmp.SnmpUInt32;
import org.opennms.protocols.snmp.SnmpVarBind;

/**
 * SNMP manager for the trap receiving. Listen on UDP 1162 port.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SnmpTrapManager
		implements SnmpTrapHandler {

	/**
	 * Informations on the SNMP trap request.
	 */
	protected final class TrapRequestInfo {

		/**
		 * IP address of trap sender.
		 */
		private final InetAddress agentAddress;

		/**
		 * Community name given by the trap sender.
		 */
		private final String community;

		/**
		 * Trap sent by by the trap sender.
		 */
		private final SnmpTrap trap;

		/**
		 * Constructor.
		 * @param agentAddress IP address of trap sender.
		 * @param community Community name given by the trap sender.
		 * @param trap Trap sent by by the trap sender.
		 */
		protected TrapRequestInfo (final InetAddress agentAddress, final String community, final SnmpTrap trap) {
			super();
			this.agentAddress = agentAddress;
			this.community = community;
			this.trap = trap;
		}

		/**
		 * Returns the IP address of trap sender.
		 * @return IP address of trap sender.
		 */
		protected InetAddress getAgentAddress () {
			return this.agentAddress;
		}

		/**
		 * Returns the community name given by the trap sender.
		 * @return Community name given by the trap sender.
		 */
		protected String getCommunity () {
			return this.community;
		}

		/**
		 * Returns the trap sent by by the trap sender.
		 * @return Trap sent by by the trap sender.
		 */
		protected SnmpTrap getTrap () {
			return this.trap;
		}

	}

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(SnmpTrapManager.class);

	/**
	 * Daemon session.
	 */
	private SnmpTrapSession daemonSession = null;

	/**
	 * List of informations on the SNMP trap request.
	 */
	private final List<TrapRequestInfo> trapRequestInfoList = new ArrayList<SnmpTrapManager.TrapRequestInfo>();

	/**
	 * Starts the manager.
	 * @throws Exception Exception if an error has occurred.
	 */
	public synchronized void start () throws Exception {
		if (this.daemonSession == null) {
			this.logger.info("SNMP trap manager starting...");
			this.daemonSession = new SnmpTrapSession(this, 1162);
		}
	}

	/**
	 * Stops the manager.
	 */
	public synchronized void stop () {
		if (this.daemonSession != null) {
			this.logger.info("SNMP trap manager stopping...");
			this.daemonSession.close();
		}
	}

	/**
	 * Returns <code>TRUE</code> if the manager is started.
	 * @return <code>TRUE</code> if the manager is started.
	 */
	public boolean isStarted () {
		return (this.daemonSession != null);
	}

	/*
	 * {@inheritDoc}
	 * @see org.opennms.protocols.snmp.SnmpTrapHandler#snmpReceivedTrap(org.opennms.protocols.snmp.SnmpTrapSession, java.net.InetAddress, int,
	 * org.opennms.protocols.snmp.SnmpOctetString, org.opennms.protocols.snmp.SnmpPduPacket)
	 */
	public void snmpReceivedTrap (final SnmpTrapSession session, final InetAddress agent, final int port, final SnmpOctetString community, final SnmpPduPacket pdu) {
		if (pdu.getCommand() == SnmpPduPacket.V2TRAP) {
			this.logger.info("V2 trap received from agent " + agent + " on port " + port);
			if ((pdu instanceof SnmpPduRequest) && (((SnmpPduRequest) pdu).getErrorStatus() != 0)) {
				this.logger.error("V2 trap PDU error " + ((SnmpPduRequest) pdu).getErrorStatus() + " at index " + ((SnmpPduRequest) pdu).getErrorIndex());
			}
			else {
				final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
				long timeStamp = 0L;
				SnmpOid enterprise = null;
				GenericSnmpTrapType generic = null;
				SnmpOid specificOid = null;
				int specific = -1;
				InetAddress agentAddress = null;
				String agentCommunity = null;
				for (final SnmpVarBind varBind : pdu.toVarBindArray()) {
					final SnmpTrapData value = toSnmpTrapData(varBind.getValue());
					final SnmpOid oid = SnmpOid.newInstance(varBind.getName().getIdentifiers());
					if (SnmpOid.newInstance("1.3.6.1.2.1.1.3.0").equals(oid) && (value.getType() == SnmpDataType.timeTicks)) {
						timeStamp = ((Long) (value.getValue())).longValue();
					}
					else if (SnmpOid.newInstance("1.3.6.1.6.3.1.1.4.1.0").equals(oid) && (value.getType() == SnmpDataType.objectIdentifier)) {
						final SnmpOid oidTrapType = (SnmpOid) (value.getValue());
						if (SnmpOid.newInstance("1.3.6.1.6.3.1.1.5.1").equals(oidTrapType)) {
							generic = GenericSnmpTrapType.coldStart;
							specificOid = null;
						}
						else if (SnmpOid.newInstance("1.3.6.1.6.3.1.1.5.2").equals(oidTrapType)) {
							generic = GenericSnmpTrapType.warmStart;
							specificOid = null;
						}
						else if (SnmpOid.newInstance("1.3.6.1.6.3.1.1.5.3").equals(oidTrapType)) {
							generic = GenericSnmpTrapType.linkDown;
							specificOid = null;
						}
						else if (SnmpOid.newInstance("1.3.6.1.6.3.1.1.5.4").equals(oidTrapType)) {
							generic = GenericSnmpTrapType.linkUp;
							specificOid = null;
						}
						else if (SnmpOid.newInstance("1.3.6.1.6.3.1.1.5.5").equals(oidTrapType)) {
							generic = GenericSnmpTrapType.authenticationFailure;
							specificOid = null;
						}
						else if (SnmpOid.newInstance("1.3.6.1.6.3.1.1.5.6").equals(oidTrapType)) {
							generic = GenericSnmpTrapType.egpNeighborLoss;
							specificOid = null;
						}
						else {
							generic = null;
							specificOid = oidTrapType;
						}
					}
					else if (SnmpOid.newInstance("1.3.6.1.6.3.18.1.3.0").equals(oid) && (value.getType() == SnmpDataType.ipAddress)) {
						agentAddress = (InetAddress) (value.getValue());
					}
					else if (SnmpOid.newInstance("1.3.6.1.6.3.18.1.4.0").equals(oid) && (value.getType() == SnmpDataType.octetString)) {
						agentCommunity = (String) (value.getValue());
					}
					else if (SnmpOid.newInstance("1.3.6.1.6.3.1.1.4.3.0").equals(oid) && (value.getType() == SnmpDataType.objectIdentifier)) {
						enterprise = (SnmpOid) (value.getValue());
					}
					else {
						dataMap.put(oid, value);
					}
				}
				if ((specificOid != null) && (enterprise != null)) {
					if (((enterprise.getOid().length + 2) == specificOid.getOid().length) && enterprise.isRootOf(specificOid)) {
						if (specificOid.getOid()[specificOid.getOid().length - 2] == 0) {
							specific = specificOid.getOid()[specificOid.getOid().length - 1] - 1;
						}
					}
				}
				SnmpTrap trap;
				if (generic != null) {
					trap = SnmpTrap.newInstance(timeStamp, enterprise, generic, dataMap);
				}
				else {
					trap = SnmpTrap.newInstance(timeStamp, enterprise, specific, dataMap);
				}

				this.trapRequestInfoList.add(new TrapRequestInfo(agentAddress, agentCommunity, trap));
			}
		}
		else {
			this.logger.info("Another command received from agent " + agent + " on port " + port);
		}
	}

	/*
	 * {@inheritDoc}
	 * @see org.opennms.protocols.snmp.SnmpTrapHandler#snmpReceivedTrap(org.opennms.protocols.snmp.SnmpTrapSession, java.net.InetAddress, int,
	 * org.opennms.protocols.snmp.SnmpOctetString, org.opennms.protocols.snmp.SnmpPduTrap)
	 */
	public void snmpReceivedTrap (final SnmpTrapSession session, final InetAddress agent, final int port, final SnmpOctetString community, final SnmpPduTrap pdu) {
		this.logger.info("V1 trap received from agent " + agent + " on port " + port);
		final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
		for (final SnmpVarBind varBind : pdu.toVarBindArray()) {
			final SnmpSyntax value = varBind.getValue();
			dataMap.put(SnmpOid.newInstance(varBind.getName().getIdentifiers()), toSnmpTrapData(value));
		}
		SnmpTrap trap;
		if (pdu.getGeneric() == SnmpPduTrap.GenericColdStart) {
			trap = SnmpTrap.newInstance(pdu.getTimeStamp(), SnmpOid.newInstance(pdu.getEnterprise().getIdentifiers()), GenericSnmpTrapType.coldStart, dataMap);
		}
		else if (pdu.getGeneric() == SnmpPduTrap.GenericWarmStart) {
			trap = SnmpTrap.newInstance(pdu.getTimeStamp(), SnmpOid.newInstance(pdu.getEnterprise().getIdentifiers()), GenericSnmpTrapType.warmStart, dataMap);
		}
		else if (pdu.getGeneric() == SnmpPduTrap.GenericLinkDown) {
			trap = SnmpTrap.newInstance(pdu.getTimeStamp(), SnmpOid.newInstance(pdu.getEnterprise().getIdentifiers()), GenericSnmpTrapType.linkDown, dataMap);
		}
		else if (pdu.getGeneric() == SnmpPduTrap.GenericLinkUp) {
			trap = SnmpTrap.newInstance(pdu.getTimeStamp(), SnmpOid.newInstance(pdu.getEnterprise().getIdentifiers()), GenericSnmpTrapType.linkUp, dataMap);
		}
		else if (pdu.getGeneric() == SnmpPduTrap.GenericAuthenticationFailure) {
			trap = SnmpTrap.newInstance(pdu.getTimeStamp(), SnmpOid.newInstance(pdu.getEnterprise().getIdentifiers()), GenericSnmpTrapType.authenticationFailure,
					dataMap);
		}
		else if (pdu.getGeneric() == SnmpPduTrap.GenericEgpNeighborLoss) {
			trap = SnmpTrap.newInstance(pdu.getTimeStamp(), SnmpOid.newInstance(pdu.getEnterprise().getIdentifiers()), GenericSnmpTrapType.egpNeighborLoss, dataMap);
		}
		else {
			trap = SnmpTrap.newInstance(pdu.getTimeStamp(), SnmpOid.newInstance(pdu.getEnterprise().getIdentifiers()), pdu.getSpecific(), dataMap);
		}
		try {
			this.trapRequestInfoList.add(new TrapRequestInfo(InetAddress.getByAddress(pdu.getAgentAddress().getString()), new String(community.getString()), trap));
		}
		catch (final UnknownHostException e) {
			this.logger.error(e);
		}
	}

	/*
	 * {@inheritDoc}
	 * @see org.opennms.protocols.snmp.SnmpTrapHandler#snmpTrapSessionError(org.opennms.protocols.snmp.SnmpTrapSession, int, java.lang.Object)
	 */
	public void snmpTrapSessionError (final SnmpTrapSession session, final int error, final Object ref) {
		this.logger.error("An error n°" + error + " occurred" + (ref != null ? " (" + ref + ")" : ""));
	}

	/**
	 * Converts a {@link SnmpSyntax} to a {@link SnmpTrapData}.
	 * @param syntax Value as {@link SnmpSyntax}.
	 * @return Value as {@link SnmpTrapData}.
	 */
	private SnmpTrapData toSnmpTrapData (final SnmpSyntax syntax) {
		Object value;
		SnmpDataType type;
		if (syntax instanceof SnmpInt32) {
			value = new Integer(((SnmpInt32) syntax).getValue());
			type = SnmpDataType.integer32;
		}
		else if (syntax instanceof SnmpGauge32) {
			value = new Long(((SnmpGauge32) syntax).getValue());
			type = SnmpDataType.gauge32;
		}
		else if (syntax instanceof SnmpCounter32) {
			value = new Long(((SnmpCounter32) syntax).getValue());
			type = SnmpDataType.counter32;
		}
		else if (syntax instanceof SnmpTimeTicks) {
			value = new Long(((SnmpTimeTicks) syntax).getValue());
			type = SnmpDataType.timeTicks;
		}
		else if (syntax instanceof SnmpUInt32) {
			value = new Long(((SnmpUInt32) syntax).getValue());
			type = SnmpDataType.unsigned32;
		}
		else if (syntax instanceof SnmpCounter64) {
			value = ((SnmpCounter64) syntax).getValue();
			type = SnmpDataType.counter64;
		}
		else if (syntax instanceof SnmpIPAddress) {
			try {
				value = InetAddress.getByAddress(((SnmpIPAddress) syntax).getString());
			}
			catch (final UnknownHostException e) {
				this.logger.error(e);
				value = null;
			}
			type = SnmpDataType.ipAddress;
		}
		else if (syntax instanceof SnmpOpaque) {
			value = new String(((SnmpOpaque) syntax).getString());
			type = SnmpDataType.opaque;
		}
		else if (syntax instanceof SnmpOctetString) {
			value = new String(((SnmpOctetString) syntax).getString());
			type = SnmpDataType.octetString;
		}
		else if (syntax instanceof SnmpObjectId) {
			value = SnmpOid.newInstance(((SnmpObjectId) syntax).getIdentifiers());
			type = SnmpDataType.objectIdentifier;
		}
		else {
			if (syntax != null) {
				this.logger.error(syntax.getClass().getName() + " isn't handled");
			}
			value = null;
			type = null;
		}
		return new SnmpTrapData(type, value);
	}

	/**
	 * Checks the next received SNMP trap.
	 * @param agentAddress IP address of trap sender.
	 * @param community Community name given by the trap sender.
	 * @param trap Trap sent by by the trap sender.
	 */
	@SuppressWarnings("null")
	public void assertTrap (final InetAddress agentAddress, final String community, final SnmpTrap trap) {
		TrapRequestInfo trapRequestInfo = null;
		synchronized (this.trapRequestInfoList) {
			if (this.trapRequestInfoList.size() > 0) {
				trapRequestInfo = this.trapRequestInfoList.remove(0);
			}
		}
		if (trapRequestInfo == null) {
			fail("None SNMP trap received");
		}
		assertEquals("Bad IP address of trap sender", agentAddress, trapRequestInfo.getAgentAddress());
		assertEquals("Bad community name given by the trap sender", community, trapRequestInfo.getCommunity());
		assertEquals("Bad trap", trap, trapRequestInfo.getTrap());
	}

	/**
	 * Checks the absence of SNMP trap.
	 */
	public void assertNoTrap () {
		if (this.trapRequestInfoList.size() > 0) {
			fail("SNMP trap received");
		}
	}

	/*
	 * {@inheritDoc}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "SnmpTrapManager";
	}

}