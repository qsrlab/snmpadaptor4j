package net.sf.snmpadaptor4j.test.mbean;

/**
 * MBean example.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class ExampleA
		implements ExampleAMBean {

	/**
	 * <code>Long</code> attribute.
	 */
	private long longAttribute;

	/**
	 * Constructor.
	 */
	public ExampleA () {
		super();
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleAMBean#getLongAttribute()
	 */
	public long getLongAttribute () {
		return this.longAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleAMBean#setLongAttribute(long)
	 */
	public void setLongAttribute (final long longAttribute) {
		this.longAttribute = longAttribute;
	}

}