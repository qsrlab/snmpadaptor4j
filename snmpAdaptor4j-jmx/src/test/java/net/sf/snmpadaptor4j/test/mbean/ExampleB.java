package net.sf.snmpadaptor4j.test.mbean;

/**
 * MBean example.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class ExampleB
		implements ExampleBMBean {

	/**
	 * <code>Long</code> attribute.
	 */
	private long longAttribute;

	/**
	 * Constructor.
	 */
	public ExampleB () {
		super();
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleBMBean#getLongAttribute()
	 */
	public long getLongAttribute () {
		return this.longAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleBMBean#setLongAttribute(long)
	 */
	public void setLongAttribute (final long longAttribute) {
		this.longAttribute = longAttribute;
	}

}