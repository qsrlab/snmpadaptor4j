package net.sf.snmpadaptor4j.test.mock;

import java.util.List;
import net.sf.snmpadaptor4j.SnmpConfiguration;
import net.sf.snmpadaptor4j.SnmpManagerConfiguration;

/**
 * Mock object for {@link SnmpConfiguration}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SnmpConfigurationMock
		implements SnmpConfiguration {

	/**
	 * Protocol version of SNMP daemon.
	 */
	private final Integer snmpVersion;

	/**
	 * Constructor.
	 */
	public SnmpConfigurationMock () {
		this(2);
	}

	/**
	 * Constructor.
	 * @param snmpVersion Protocol version of SNMP daemon.
	 */
	public SnmpConfigurationMock (final int snmpVersion) {
		super();
		this.snmpVersion = new Integer(snmpVersion);
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerAddress()
	 */
	public String getListenerAddress () {
		return "127.0.0.1";
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerPort()
	 */
	public Integer getListenerPort () {
		return new Integer(161);
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerSnmpVersion()
	 */
	public Integer getListenerSnmpVersion () {
		return this.snmpVersion;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerReadCommunity()
	 */
	public String getListenerReadCommunity () {
		return "public";
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerWriteCommunity()
	 */
	public String getListenerWriteCommunity () {
		return "private";
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.SnmpConfiguration#getManagerList()
	 */
	public List<SnmpManagerConfiguration> getManagerList () {
		return null;
	}

}