package net.sf.snmpadaptor4j.test.mbean;

import java.net.InetAddress;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanConstructorInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanNotificationInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanParameterInfo;
import javax.management.NotCompliantMBeanException;
import javax.management.NotificationBroadcaster;
import javax.management.NotificationBroadcasterSupport;
import javax.management.NotificationFilter;
import javax.management.NotificationListener;
import javax.management.StandardMBean;

/**
 * MBean example.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class Example
		extends StandardMBean
		implements ExampleMBean, NotificationBroadcaster {

	/**
	 * Implementation of {@link NotificationBroadcaster}.
	 */
	private final NotificationBroadcasterSupport notificationBroadcaster = new NotificationBroadcasterSupport();

	/**
	 * <code>Long</code> attribute.
	 */
	private long longAttribute;

	/**
	 * <code>String</code> attribute.
	 */
	private String stringAttribute;

	/**
	 * Hidden attribute.
	 */
	private Object hiddenAttribute;

	/**
	 * <code>InetAddress</code> attribute.
	 */
	private InetAddress inetAddressAttribute;

	/**
	 * <code>timesticks</code> attribute.
	 */
	private long timesticksAttribute;

	/**
	 * <code>byte</code> attribute.
	 */
	private byte byteAttribute;

	/**
	 * <code>short</code> attribute.
	 */
	private short shortAttribute;

	/**
	 * <code>int</code> attribute.
	 */
	private int intAttribute;

	/**
	 * <code>boolean</code> attribute.
	 */
	private boolean booleanAttribute = false;

	/**
	 * <code>byte</code> array attribute.
	 */
	private byte[] byteArrayAttribute;

	/**
	 * <code>short</code> array attribute.
	 */
	private short[] shortArrayAttribute;

	/**
	 * <code>int</code> array attribute.
	 */
	private int[] intArrayAttribute;

	/**
	 * <code>long</code> array attribute.
	 */
	private long[] longArrayAttribute;

	/**
	 * Constructor.
	 * @throws NotCompliantMBeanException if the <var>mbeanInterface</var> does not follow JMX design patterns for Management Interfaces, or if <var>this</var> does
	 *             not implement the specified interface.
	 */
	public Example () throws NotCompliantMBeanException {
		super(ExampleMBean.class);
		final MBeanAttributeInfo[] attributes = new MBeanAttributeInfo[] {
				new MBeanAttributeInfo("LongAttribute", long.class.getName(), "Long attribute.", true, true, false),
				new MBeanAttributeInfo("StringAttribute", String.class.getName(), "String attribute", true, true, false),
				new MBeanAttributeInfo("HiddenAttribute", Object.class.getName(), "Hidden attribute", false, false, false),
				new MBeanAttributeInfo("InetAddressAttribute", InetAddress.class.getName(), "InetAddress attribute", true, false, false),
				new MBeanAttributeInfo("TimesticksAttribute", long.class.getName(), "timesticks attribute", true, true, false),
				new MBeanAttributeInfo("ByteAttribute", byte.class.getName(), "byte attribute", true, true, false),
				new MBeanAttributeInfo("ShortAttribute", short.class.getName(), "short attribute", true, true, false),
				new MBeanAttributeInfo("IntAttribute", int.class.getName(), "int attribute", true, true, false),
				new MBeanAttributeInfo("BooleanAttribute", boolean.class.getName(), "boolean attribute", true, true, true),
				new MBeanAttributeInfo("ByteArrayAttribute", byte[].class.getName(), "byte array attribute", true, true, false),
				new MBeanAttributeInfo("ShortArrayAttribute", short[].class.getName(), "short array attribute", true, true, false),
				new MBeanAttributeInfo("IntArrayAttribute", int[].class.getName(), "int array attribute", true, true, false),
				new MBeanAttributeInfo("LongArrayAttribute", long[].class.getName(), "long array attribute", true, true, false) };
		final MBeanConstructorInfo[] constructors = new MBeanConstructorInfo[] { new MBeanConstructorInfo(getClass().getName(), "Constructor",
				new MBeanParameterInfo[] {}) };
		cacheMBeanInfo(new MBeanInfo(getClass().getName(), "Example MBean for tests", attributes, constructors, new MBeanOperationInfo[] {},
				new MBeanNotificationInfo[] {}));
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#getLongAttribute()
	 */
	public long getLongAttribute () {
		return this.longAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#setLongAttribute(long)
	 */
	public void setLongAttribute (final long longAttribute) {
		this.longAttribute = longAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#getStringAttribute()
	 */
	public String getStringAttribute () {
		return this.stringAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#setStringAttribute(java.lang.String)
	 */
	public void setStringAttribute (final String stringAttribute) {
		this.stringAttribute = stringAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#getHiddenAttribute()
	 */
	public Object getHiddenAttribute () {
		return this.hiddenAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#setHiddenAttribute(java.lang.Object)
	 */
	public void setHiddenAttribute (final Object hiddenAttribute) {
		this.hiddenAttribute = hiddenAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#getInetAddressAttribute()
	 */
	public InetAddress getInetAddressAttribute () {
		return this.inetAddressAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#getTimesticksAttribute()
	 */
	public long getTimesticksAttribute () {
		return this.timesticksAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#setTimesticksAttribute(long)
	 */
	public void setTimesticksAttribute (final long timesticksAttribute) {
		this.timesticksAttribute = timesticksAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#getByteAttribute()
	 */
	public byte getByteAttribute () {
		return this.byteAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#setByteAttribute(byte)
	 */
	public void setByteAttribute (final byte byteAttribute) {
		this.byteAttribute = byteAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#getShortAttribute()
	 */
	public short getShortAttribute () {
		return this.shortAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#setShortAttribute(short)
	 */
	public void setShortAttribute (final short shortAttribute) {
		this.shortAttribute = shortAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#getIntAttribute()
	 */
	public int getIntAttribute () {
		return this.intAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#setIntAttribute(int)
	 */
	public void setIntAttribute (final int intAttribute) {
		this.intAttribute = intAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#isBooleanAttribute()
	 */
	public boolean isBooleanAttribute () {
		return this.booleanAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#setBooleanAttribute(boolean)
	 */
	public void setBooleanAttribute (final boolean booleanAttribute) {
		this.booleanAttribute = booleanAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#getByteArrayAttribute()
	 */
	public byte[] getByteArrayAttribute () {
		return this.byteArrayAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#setByteArrayAttribute(byte[])
	 */
	public void setByteArrayAttribute (final byte[] byteArrayAttribute) {
		this.byteArrayAttribute = byteArrayAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#getShortArrayAttribute()
	 */
	public short[] getShortArrayAttribute () {
		return this.shortArrayAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#setShortArrayAttribute(short[])
	 */
	public void setShortArrayAttribute (final short[] shortArrayAttribute) {
		this.shortArrayAttribute = shortArrayAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#getIntArrayAttribute()
	 */
	public int[] getIntArrayAttribute () {
		return this.intArrayAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#setIntArrayAttribute(int[])
	 */
	public void setIntArrayAttribute (final int[] intArrayAttribute) {
		this.intArrayAttribute = intArrayAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#getLongArrayAttribute()
	 */
	public long[] getLongArrayAttribute () {
		return this.longArrayAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see net.sf.snmpadaptor4j.test.mbean.ExampleMBean#setLongArrayAttribute(long[])
	 */
	public void setLongArrayAttribute (final long[] longArrayAttribute) {
		this.longArrayAttribute = longArrayAttribute;
	}

	/*
	 * {@inheritDoc}
	 * @see javax.management.NotificationBroadcaster#addNotificationListener(javax.management.NotificationListener, javax.management.NotificationFilter,
	 * java.lang.Object)
	 */
	public void addNotificationListener (final NotificationListener listener, final NotificationFilter filter, final Object handback)
			throws IllegalArgumentException {
		this.notificationBroadcaster.addNotificationListener(listener, filter, handback);
	}

	/*
	 * {@inheritDoc}
	 * @see javax.management.NotificationBroadcaster#removeNotificationListener(javax.management.NotificationListener)
	 */
	public void removeNotificationListener (final NotificationListener listener) throws ListenerNotFoundException {
		this.notificationBroadcaster.removeNotificationListener(listener);
	}

	/*
	 * {@inheritDoc}
	 * @see javax.management.NotificationBroadcaster#getNotificationInfo()
	 */
	public MBeanNotificationInfo[] getNotificationInfo () {
		return null;
	}

}