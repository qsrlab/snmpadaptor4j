package net.sf.snmpadaptor4j;

import static org.junit.Assert.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.ObjectName;
import net.sf.snmpadaptor4j.api.SnmpDaemon;
import net.sf.snmpadaptor4j.core.JmxListener;
import net.sf.snmpadaptor4j.core.JmxNotificationManager;
import net.sf.snmpadaptor4j.core.JvmSnmpMib;
import net.sf.snmpadaptor4j.core.SystemSnmpMib;
import net.sf.snmpadaptor4j.mbean.SystemInfo;
import net.sf.snmpadaptor4j.test.mock.SnmpAppContextMock;
import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.SnmpAdaptor}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SnmpAdaptorTest {

	/**
	 * Mock object for {@link SnmpConfiguration}.
	 */
	protected final class SnmpConfigurationMock
			implements SnmpConfiguration {

		/**
		 * List of managers where to send all notifications (SNMP traps).
		 */
		private final List<SnmpManagerConfiguration> managerList;

		/**
		 * Constructor.
		 * @param nullManagerList <code>TRUE</code> if the <code>managerList</code> must be <code>NULL</code>.
		 */
		protected SnmpConfigurationMock (final boolean nullManagerList) {
			super();
			if (nullManagerList) {
				this.managerList = null;
			}
			else {
				this.managerList = new ArrayList<SnmpManagerConfiguration>();
				this.managerList.add(new SnmpManagerConfiguration("192.168.1.4", 162, 2, "public"));
				this.managerList.add(new SnmpManagerConfiguration("192.168.1.5", 1162, 1, "private"));
			}
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerAddress()
		 */
		public String getListenerAddress () {
			return "192.168.1.2";
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerPort()
		 */
		public Integer getListenerPort () {
			return new Integer(2161);
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerSnmpVersion()
		 */
		public Integer getListenerSnmpVersion () {
			return new Integer(1);
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerReadCommunity()
		 */
		public String getListenerReadCommunity () {
			return "unittest.public";
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration#getListenerWriteCommunity()
		 */
		public String getListenerWriteCommunity () {
			return "unittest.private";
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.SnmpConfiguration#getManagerList()
		 */
		public List<SnmpManagerConfiguration> getManagerList () {
			return this.managerList;
		}

	}

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(SnmpAdaptorTest.class);

	/**
	 * Tests the constructor with a minimum of parameters.
	 * <p>
	 * Test method for {@link SnmpAdaptor#SnmpAdaptor(boolean,String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpAdaptorWithMinimumParameters () throws Exception {
		this.logger.info("Tests the constructor with a minimum of parameters...");
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor(true,null);
		assertEquals("listenerAddress is bad", "localhost", snmpAdaptor.getListenerAddress());
		assertEquals("listenerPort is bad", new Integer(161), snmpAdaptor.getListenerPort());
		assertEquals("listenerSnmpVersion is bad", new Integer(2), snmpAdaptor.getListenerSnmpVersion());
		assertEquals("listenerReadCommunity is bad", "public", snmpAdaptor.getListenerReadCommunity());
		assertEquals("listenerWriteCommunity is bad", "private", snmpAdaptor.getListenerWriteCommunity());
		assertTrue("managerList is not empty", snmpAdaptor.getManagerList().isEmpty());
		assertEquals("defaultRootOid is bad", "1.3.6.1.4.1.99.12.8.1", snmpAdaptor.getDefaultRootOid());
		assertTrue("rootOidMap is not empty", snmpAdaptor.getRootOidMap().isEmpty());
		assertTrue("mBeanOidMap is not empty", snmpAdaptor.getMBeanOidMap().isEmpty());
		final SystemInfo expectedSystemInfo = new SystemInfo();
		assertEquals("systemInfo.sysName is bad", expectedSystemInfo.getSysName(), snmpAdaptor.getSystemInfo().getSysName());
		assertEquals("systemInfo.sysDescr is bad", expectedSystemInfo.getSysDescr(), snmpAdaptor.getSystemInfo().getSysDescr());
		assertEquals("systemInfo.sysLocation is bad", expectedSystemInfo.getSysLocation(), snmpAdaptor.getSystemInfo().getSysLocation());
		assertEquals("systemInfo.sysContact is bad", expectedSystemInfo.getSysContact(), snmpAdaptor.getSystemInfo().getSysContact());
	}

	/**
	 * Tests the constructor with an application context.
	 * <p>
	 * Test method for {@link SnmpAdaptor#SnmpAdaptor(SnmpAppContext, boolean,String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpAdaptorWithSnmpAppContext () throws Exception {
		this.logger.info("Tests the constructor with an application context...");
		final SnmpAppContextMock appContext = new SnmpAppContextMock("1.3.6.1.4.1.999.1");
		appContext.getRootOidMap().put("root1", "1.3.6.1.4.1.999.2");
		appContext.getRootOidMap().put("root2", "1.3.6.1.4.1.999.3");
		appContext.getRootOidMap().put("root3", "1.3.6.1.4.1.999.4");
		appContext.getMBeanOidMap().put(new ObjectName("net.sf.snmpadaptor4j.example:type=Example,id=1"), "1.3.6.1.4.1.999.1.1.1");
		appContext.getMBeanOidMap().put(new ObjectName("net.sf.snmpadaptor4j.example:type=Example,id=2"), "1.3.6.1.4.1.999.1.1.2");
		appContext.getMBeanOidMap().put(new ObjectName("net.sf.snmpadaptor4j.example:type=Example,id=3"), "1.3.6.1.4.1.999.1.1.3");
		appContext.getMBeanOidMap().put(new ObjectName("net.sf.snmpadaptor4j.example:type=Example,id=4"), "1.3.6.1.4.1.999.1.1.4");
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor(appContext, true,null);
		assertEquals("listenerAddress is bad", "localhost", snmpAdaptor.getListenerAddress());
		assertEquals("listenerPort is bad", new Integer(161), snmpAdaptor.getListenerPort());
		assertEquals("listenerSnmpVersion is bad", new Integer(2), snmpAdaptor.getListenerSnmpVersion());
		assertEquals("listenerReadCommunity is bad", "public", snmpAdaptor.getListenerReadCommunity());
		assertEquals("listenerWriteCommunity is bad", "private", snmpAdaptor.getListenerWriteCommunity());
		assertTrue("managerList is not empty", snmpAdaptor.getManagerList().isEmpty());
		assertEquals("defaultRootOid is bad", "1.3.6.1.4.1.999.1", snmpAdaptor.getDefaultRootOid());
		assertEquals("rootOidMap is bad", appContext.getRootOidMap(), snmpAdaptor.getRootOidMap());
		assertEquals("mBeanOidMap is bad", appContext.getMBeanOidMap(), snmpAdaptor.getMBeanOidMap());
		final SystemInfo expectedSystemInfo = new SystemInfo();
		assertEquals("systemInfo.sysName is bad", expectedSystemInfo.getSysName(), snmpAdaptor.getSystemInfo().getSysName());
		assertEquals("systemInfo.sysDescr is bad", expectedSystemInfo.getSysDescr(), snmpAdaptor.getSystemInfo().getSysDescr());
		assertEquals("systemInfo.sysLocation is bad", expectedSystemInfo.getSysLocation(), snmpAdaptor.getSystemInfo().getSysLocation());
		assertEquals("systemInfo.sysContact is bad", expectedSystemInfo.getSysContact(), snmpAdaptor.getSystemInfo().getSysContact());
	}

	/**
	 * Tests the constructor with an {@link URL} to SNMP configuration file (XML).
	 * <p>
	 * Test method for {@link SnmpAdaptor#SnmpAdaptor(URL, boolean, String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpAdaptorWithUrl () throws Exception {
		this.logger.info("Tests the constructor with an URL to SNMP configuration file (XML)...");
		final URL url = getClass().getResource("/snmp1.xml");
		assertNotNull("SNMP configuration file not found", url);
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor(url, false,null);
		assertEquals("listenerAddress is bad", "192.168.1.1", snmpAdaptor.getListenerAddress());
		assertEquals("listenerPort is bad", new Integer(1161), snmpAdaptor.getListenerPort());
		assertEquals("listenerSnmpVersion is bad", new Integer(1), snmpAdaptor.getListenerSnmpVersion());
		assertEquals("listenerReadCommunity is bad", "test.public", snmpAdaptor.getListenerReadCommunity());
		assertEquals("listenerWriteCommunity is bad", "test.private", snmpAdaptor.getListenerWriteCommunity());
		final List<SnmpManagerConfiguration> expectedManagerList = new ArrayList<SnmpManagerConfiguration>();
		expectedManagerList.add(new SnmpManagerConfiguration("192.168.1.2", 162, 2, "public"));
		expectedManagerList.add(new SnmpManagerConfiguration("192.168.1.3", 1162, 1, "trap.public"));
		assertEquals("managerList is bad", expectedManagerList, snmpAdaptor.getManagerList());
		assertEquals("defaultRootOid is bad", "1.3.6.1.4.1.99.12.8.1", snmpAdaptor.getDefaultRootOid());
		final Map<String, String> rootOidMap = new HashMap<String, String>();
		rootOidMap.put("framework", "1.3.6.1.4.1.9999.1");
		rootOidMap.put("example", "1.3.6.1.4.1.9999.50");
		assertEquals("rootOidMap is bad", rootOidMap, snmpAdaptor.getRootOidMap());
		final Map<ObjectName, String> mBeanOidMap = new HashMap<ObjectName, String>();
		mBeanOidMap.put(new ObjectName("net.sf.snmpadaptor4j.example:type=Example,id=1"), "1.3.6.1.4.1.9999.50.1.1");
		mBeanOidMap.put(new ObjectName("net.sf.snmpadaptor4j.example:type=Example,id=2"), "1.3.6.1.4.1.9999.50.1.2");
		mBeanOidMap.put(new ObjectName("net.sf.snmpadaptor4j.example:type=Example,id=3"), "1.3.6.1.4.1.99.12.8.1.1.3");
		mBeanOidMap.put(new ObjectName("net.sf.snmpadaptor4j.example:type=Example,id=4"), "1.3.6.1.4.1.99.12.8.1.1.4");
		assertEquals("mBeanOidMap is bad", mBeanOidMap, snmpAdaptor.getMBeanOidMap());
		final SystemInfo expectedSystemInfo = new SystemInfo();
		assertEquals("systemInfo.sysName is bad", expectedSystemInfo.getSysName(), snmpAdaptor.getSystemInfo().getSysName());
		assertEquals("systemInfo.sysDescr is bad", expectedSystemInfo.getSysDescr(), snmpAdaptor.getSystemInfo().getSysDescr());
		assertEquals("systemInfo.sysLocation is bad", expectedSystemInfo.getSysLocation(), snmpAdaptor.getSystemInfo().getSysLocation());
		assertEquals("systemInfo.sysContact is bad", expectedSystemInfo.getSysContact(), snmpAdaptor.getSystemInfo().getSysContact());
	}

	/**
	 * Tests the constructor with an {@link URL} to SNMP configuration file (XML) and a {@link SystemInfo}.
	 * <p>
	 * Test method for {@link SnmpAdaptor#SnmpAdaptor(URL, SystemInfo, boolean, String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpAdaptorWithUrlAndSystemInfo () throws Exception {
		this.logger.info("Tests the constructor with an URL to SNMP configuration file (XML) and a SystemInfo...");
		final URL url = getClass().getResource("/snmp1.xml");
		assertNotNull("SNMP configuration file not found", url);
		final SystemInfo systemInfo = new SystemInfo();
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor(url, systemInfo, false, null);
		assertEquals("listenerAddress is bad", "192.168.1.1", snmpAdaptor.getListenerAddress());
		assertEquals("listenerPort is bad", new Integer(1161), snmpAdaptor.getListenerPort());
		assertEquals("listenerSnmpVersion is bad", new Integer(1), snmpAdaptor.getListenerSnmpVersion());
		assertEquals("listenerReadCommunity is bad", "test.public", snmpAdaptor.getListenerReadCommunity());
		assertEquals("listenerWriteCommunity is bad", "test.private", snmpAdaptor.getListenerWriteCommunity());
		final List<SnmpManagerConfiguration> expectedManagerList = new ArrayList<SnmpManagerConfiguration>();
		expectedManagerList.add(new SnmpManagerConfiguration("192.168.1.2", 162, 2, "public"));
		expectedManagerList.add(new SnmpManagerConfiguration("192.168.1.3", 1162, 1, "trap.public"));
		assertEquals("managerList is bad", expectedManagerList, snmpAdaptor.getManagerList());
		assertEquals("defaultRootOid is bad", "1.3.6.1.4.1.99.12.8.1", snmpAdaptor.getDefaultRootOid());
		final Map<String, String> rootOidMap = new HashMap<String, String>();
		rootOidMap.put("framework", "1.3.6.1.4.1.9999.1");
		rootOidMap.put("example", "1.3.6.1.4.1.9999.50");
		assertEquals("rootOidMap is bad", rootOidMap, snmpAdaptor.getRootOidMap());
		final Map<ObjectName, String> mBeanOidMap = new HashMap<ObjectName, String>();
		mBeanOidMap.put(new ObjectName("net.sf.snmpadaptor4j.example:type=Example,id=1"), "1.3.6.1.4.1.9999.50.1.1");
		mBeanOidMap.put(new ObjectName("net.sf.snmpadaptor4j.example:type=Example,id=2"), "1.3.6.1.4.1.9999.50.1.2");
		mBeanOidMap.put(new ObjectName("net.sf.snmpadaptor4j.example:type=Example,id=3"), "1.3.6.1.4.1.99.12.8.1.1.3");
		mBeanOidMap.put(new ObjectName("net.sf.snmpadaptor4j.example:type=Example,id=4"), "1.3.6.1.4.1.99.12.8.1.1.4");
		assertEquals("mBeanOidMap is bad", mBeanOidMap, snmpAdaptor.getMBeanOidMap());
		systemInfo.setSysName("test");
		assertEquals("systemInfo.sysName is bad", "test", snmpAdaptor.getSystemInfo().getSysName());
		systemInfo.setSysDescr("Unit test of SnmpAdaptor");
		assertEquals("systemInfo.sysDescr is bad", "Unit test of SnmpAdaptor", snmpAdaptor.getSystemInfo().getSysDescr());
		systemInfo.setSysLocation("somewhere");
		assertEquals("systemInfo.sysLocation is bad", "somewhere", snmpAdaptor.getSystemInfo().getSysLocation());
		systemInfo.setSysContact("my");
		assertEquals("systemInfo.sysContact is bad", "my", snmpAdaptor.getSystemInfo().getSysContact());
	}

	/**
	 * Tests the constructor with all parameters.
	 * <p>
	 * Test method for {@link SnmpAdaptor#SnmpAdaptor(SnmpConfiguration, SnmpAppContext, SystemInfo, boolean, String)} .
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpAdaptorWithAllParameters () throws Exception {
		SnmpConfiguration configuration = new SnmpConfigurationMock(false);
		testSnmpAdaptorWithAllParameters(configuration, configuration.getManagerList());
		testSnmpAdaptorWithAllParameters(new SnmpConfigurationMock(true), new ArrayList<SnmpManagerConfiguration>());
	}

	/**
	 * Tests the constructor with all parameters.
	 * <p>
	 * Test method for {@link SnmpAdaptor#SnmpAdaptor(SnmpConfiguration, SnmpAppContext, SystemInfo, boolean, String)} .
	 * </p>
	 * @param configuration SNMP configuration settings.
	 * @param expectedManagerList Expected list of SNMP managers.
	 * @throws Exception Exception if an error has occurred.
	 */
	private void testSnmpAdaptorWithAllParameters (final SnmpConfiguration configuration, final List<SnmpManagerConfiguration> expectedManagerList) throws Exception {
		this.logger.info("Tests the constructor with all parameters...");
		final SystemInfo systemInfo = new SystemInfo();
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor(configuration, new SnmpAppContextMock(), systemInfo, false,null);
		assertEquals("listenerAddress is bad", "192.168.1.2", snmpAdaptor.getListenerAddress());
		assertEquals("listenerPort is bad", new Integer(2161), snmpAdaptor.getListenerPort());
		assertEquals("listenerSnmpVersion is bad", new Integer(1), snmpAdaptor.getListenerSnmpVersion());
		assertEquals("listenerReadCommunity is bad", "unittest.public", snmpAdaptor.getListenerReadCommunity());
		assertEquals("listenerWriteCommunity is bad", "unittest.private", snmpAdaptor.getListenerWriteCommunity());
		assertEquals("managerList is bad", expectedManagerList, snmpAdaptor.getManagerList());
		assertEquals("defaultRootOid is bad", "1.3.6.1.4.1.99.12.8.1", snmpAdaptor.getDefaultRootOid());
		assertTrue("rootOidMap is not empty", snmpAdaptor.getRootOidMap().isEmpty());
		assertTrue("mBeanOidMap is not empty", snmpAdaptor.getMBeanOidMap().isEmpty());
		systemInfo.setSysName("test");
		assertEquals("systemInfo.sysName is bad", "test", snmpAdaptor.getSystemInfo().getSysName());
		systemInfo.setSysDescr("Unit test of SnmpAdaptor");
		assertEquals("systemInfo.sysDescr is bad", "Unit test of SnmpAdaptor", snmpAdaptor.getSystemInfo().getSysDescr());
		systemInfo.setSysLocation("somewhere");
		assertEquals("systemInfo.sysLocation is bad", "somewhere", snmpAdaptor.getSystemInfo().getSysLocation());
		systemInfo.setSysContact("my");
		assertEquals("systemInfo.sysContact is bad", "my", snmpAdaptor.getSystemInfo().getSysContact());
	}

	/**
	 * Tests the adding of a new application context.
	 * <p>
	 * Test method for {@link SnmpAdaptor#addAppContext(ClassLoader, SnmpAppContext)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testAddAppContext () throws Exception {
		this.logger.info("Tests the adding of a new application context...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final SystemSnmpMib systemMib = mocksControl.createMock(SystemSnmpMib.class);
		final SnmpAppContext appContext = new SnmpAppContextMock();
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		systemMib.initSysObjectIDSet();
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, systemMib, null, null, jmxNotificationManager, daemon);
		snmpAdaptor.addAppContext(getClass().getClassLoader(), appContext);

		// Checking
		mocksControl.verify();
		assertEquals("The application context has not been added", appContext, snmpAdaptor.appContextMap.get(getClass().getClassLoader()));

	}

	/**
	 * Tests the removing of an application context.
	 * <p>
	 * Test method for {@link SnmpAdaptor#removeAppContext(ClassLoader)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testRemoveAppContext () throws Exception {
		this.logger.info("Tests the removing of an application context...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final SystemSnmpMib systemMib = mocksControl.createMock(SystemSnmpMib.class);
		final SnmpAppContext appContext = new SnmpAppContextMock();
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		systemMib.initSysObjectIDSet();
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, systemMib, null, null, jmxNotificationManager, daemon);
		snmpAdaptor.appContextMap.put(getClass().getClassLoader(), appContext);
		assertEquals("The application context has not been initialized", appContext, snmpAdaptor.appContextMap.get(getClass().getClassLoader()));
		snmpAdaptor.removeAppContext(getClass().getClassLoader());

		// Checking
		mocksControl.verify();
		assertNull("The application context has not been removed", snmpAdaptor.appContextMap.get(getClass().getClassLoader()));

	}

	/**
	 * Tests the <code>listenerAddress</code> setting without changing.
	 * <p>
	 * Test method for {@link SnmpAdaptor#setListenerAddress(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetListenerAddressWithoutChanging () throws Exception {
		this.logger.info("Tests the listenerAddress setting without changing...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		final String listenerAddress = snmpAdaptor.getListenerAddress();
		snmpAdaptor.setListenerAddress(listenerAddress);

		// Checking
		mocksControl.verify();
		assertEquals("listenerAddress is bad", listenerAddress, snmpAdaptor.getListenerAddress());

	}

	/**
	 * Tests the <code>listenerAddress</code> setting with changing.
	 * <p>
	 * Test method for {@link SnmpAdaptor#setListenerAddress(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetListenerAddressWithChanging () throws Exception {
		this.logger.info("Tests the listenerAddress setting with changing...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		snmpAdaptor.setListenerAddress("192.168.1.1");

		// Checking
		mocksControl.verify();
		assertEquals("listenerAddress is bad", "192.168.1.1", snmpAdaptor.getListenerAddress());

	}

	/**
	 * Tests the <code>listenerAddress</code> setting with an exception.
	 * <p>
	 * Test method for {@link SnmpAdaptor#setListenerAddress(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetListenerAddressWithException () throws Exception {
		this.logger.info("Tests the listenerAddress setting with an exception...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		EasyMock.expectLastCall().andThrow(new Exception("MOCK EXCEPTION"));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		final String listenerAddress = snmpAdaptor.getListenerAddress();
		snmpAdaptor.setListenerAddress("192.168.1.1");

		// Checking
		mocksControl.verify();
		assertEquals("listenerAddress is bad", listenerAddress, snmpAdaptor.getListenerAddress());

	}

	/**
	 * Tests the <code>listenerPort</code> setting without changing.
	 * <p>
	 * Test method for {@link SnmpAdaptor#setListenerPort(Integer)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetListenerPortWithoutChanging () throws Exception {
		this.logger.info("Tests the listenerPort setting without changing...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		final Integer listenerPort = snmpAdaptor.getListenerPort();
		snmpAdaptor.setListenerPort(listenerPort);

		// Checking
		mocksControl.verify();
		assertEquals("listenerPort is bad", listenerPort, snmpAdaptor.getListenerPort());

	}

	/**
	 * Tests the <code>listenerPort</code> setting with changing.
	 * <p>
	 * Test method for {@link SnmpAdaptor#setListenerPort(Integer)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetListenerPortWithChanging () throws Exception {
		this.logger.info("Tests the listenerPort setting with changing...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		snmpAdaptor.setListenerPort(new Integer(1161));

		// Checking
		mocksControl.verify();
		assertEquals("listenerPort is bad", new Integer(1161), snmpAdaptor.getListenerPort());

	}

	/**
	 * Tests the <code>listenerPort</code> setting with an exception.
	 * <p>
	 * Test method for {@link SnmpAdaptor#setListenerPort(Integer)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetListenerPortWithException () throws Exception {
		this.logger.info("Tests the listenerPort setting with an exception...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		EasyMock.expectLastCall().andThrow(new Exception("MOCK EXCEPTION"));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		final Integer listenerPort = snmpAdaptor.getListenerPort();
		snmpAdaptor.setListenerPort(new Integer(1161));

		// Checking
		mocksControl.verify();
		assertEquals("listenerPort is bad", listenerPort, snmpAdaptor.getListenerPort());

	}

	/**
	 * Tests the <code>listenerSnmpVersion</code> setting without changing.
	 * <p>
	 * Test method for {@link SnmpAdaptor#setListenerSnmpVersion(Integer)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetListenerSnmpVersionWithoutChanging () throws Exception {
		this.logger.info("Tests the listenerSnmpVersion setting without changing...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		final Integer listenerSnmpVersion = snmpAdaptor.getListenerSnmpVersion();
		snmpAdaptor.setListenerSnmpVersion(listenerSnmpVersion);

		// Checking
		mocksControl.verify();
		assertEquals("listenerSnmpVersion is bad", listenerSnmpVersion, snmpAdaptor.getListenerSnmpVersion());

	}

	/**
	 * Tests the <code>listenerSnmpVersion</code> setting with changing.
	 * <p>
	 * Test method for {@link SnmpAdaptor#setListenerSnmpVersion(Integer)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetListenerSnmpVersionWithChanging () throws Exception {
		this.logger.info("Tests the listenerSnmpVersion setting with changing...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		snmpAdaptor.setListenerSnmpVersion(new Integer(1));

		// Checking
		mocksControl.verify();
		assertEquals("listenerSnmpVersion is bad", new Integer(1), snmpAdaptor.getListenerSnmpVersion());

	}

	/**
	 * Tests the <code>listenerSnmpVersion</code> setting with an exception.
	 * <p>
	 * Test method for {@link SnmpAdaptor#setListenerSnmpVersion(Integer)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetListenerSnmpVersionWithException () throws Exception {
		this.logger.info("Tests the listenerSnmpVersion setting with an exception...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		EasyMock.expectLastCall().andThrow(new Exception("MOCK EXCEPTION"));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		final Integer listenerSnmpVersion = snmpAdaptor.getListenerSnmpVersion();
		snmpAdaptor.setListenerSnmpVersion(new Integer(1));

		// Checking
		mocksControl.verify();
		assertEquals("listenerSnmpVersion is bad", listenerSnmpVersion, snmpAdaptor.getListenerSnmpVersion());

	}

	/**
	 * Tests the <code>listenerSnmpVersion</code> setting with a minimum overlimit.
	 * <p>
	 * Test method for {@link SnmpAdaptor#setListenerSnmpVersion(Integer)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetListenerSnmpVersionWithMinimumOverlimit () throws Exception {
		this.logger.info("Tests the listenerSnmpVersion setting with a minimum overlimit...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", new SnmpConfigurationMock(false), true, null, null, null, jmxNotificationManager,
				daemon);
		snmpAdaptor.setListenerSnmpVersion(new Integer(0));

		// Checking
		mocksControl.verify();
		assertEquals("listenerSnmpVersion is bad", new Integer(2), snmpAdaptor.getListenerSnmpVersion());

	}

	/**
	 * Tests the <code>listenerSnmpVersion</code> setting with a maximum overlimit.
	 * <p>
	 * Test method for {@link SnmpAdaptor#setListenerSnmpVersion(Integer)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetListenerSnmpVersionWithMaximumOverlimit () throws Exception {
		this.logger.info("Tests the listenerSnmpVersion setting with a maximum overlimit...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", new SnmpConfigurationMock(true), true, null, null, null, jmxNotificationManager,
				daemon);
		snmpAdaptor.setListenerSnmpVersion(new Integer(3));

		// Checking
		mocksControl.verify();
		assertEquals("listenerSnmpVersion is bad", new Integer(2), snmpAdaptor.getListenerSnmpVersion());

	}

	/**
	 * Tests the <code>listenerReadCommunity</code> setting without changing.
	 * <p>
	 * Test method for {@link SnmpAdaptor#setListenerReadCommunity(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetListenerReadCommunityWithoutChanging () throws Exception {
		this.logger.info("Tests the listenerReadCommunity setting without changing...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		final String listenerReadCommunity = snmpAdaptor.getListenerReadCommunity();
		snmpAdaptor.setListenerReadCommunity(listenerReadCommunity);

		// Checking
		mocksControl.verify();
		assertEquals("listenerReadCommunity is bad", listenerReadCommunity, snmpAdaptor.getListenerReadCommunity());

	}

	/**
	 * Tests the <code>listenerReadCommunity</code> setting with changing.
	 * <p>
	 * Test method for {@link SnmpAdaptor#setListenerReadCommunity(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetListenerReadCommunityWithChanging () throws Exception {
		this.logger.info("Tests the listenerReadCommunity setting with changing...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		snmpAdaptor.setListenerReadCommunity("readCommunity");

		// Checking
		mocksControl.verify();
		assertEquals("listenerReadCommunity is bad", "readCommunity", snmpAdaptor.getListenerReadCommunity());

	}

	/**
	 * Tests the <code>listenerReadCommunity</code> setting with an exception.
	 * <p>
	 * Test method for {@link SnmpAdaptor#setListenerReadCommunity(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetListenerReadCommunityWithException () throws Exception {
		this.logger.info("Tests the listenerReadCommunity setting with an exception...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		EasyMock.expectLastCall().andThrow(new Exception("MOCK EXCEPTION"));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		final String listenerReadCommunity = snmpAdaptor.getListenerReadCommunity();
		snmpAdaptor.setListenerReadCommunity("readCommunity");

		// Checking
		mocksControl.verify();
		assertEquals("listenerReadCommunity is bad", listenerReadCommunity, snmpAdaptor.getListenerReadCommunity());

	}

	/**
	 * Tests the <code>listenerWriteCommunity</code> setting without changing.
	 * <p>
	 * Test method for {@link SnmpAdaptor#setListenerWriteCommunity(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetListenerWriteCommunityWithoutChanging () throws Exception {
		this.logger.info("Tests the listenerWriteCommunity setting without changing...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		final String listenerWriteCommunity = snmpAdaptor.getListenerWriteCommunity();
		snmpAdaptor.setListenerWriteCommunity(listenerWriteCommunity);

		// Checking
		mocksControl.verify();
		assertEquals("listenerWriteCommunity is bad", listenerWriteCommunity, snmpAdaptor.getListenerWriteCommunity());

	}

	/**
	 * Tests the <code>listenerWriteCommunity</code> setting with changing.
	 * <p>
	 * Test method for {@link SnmpAdaptor#setListenerWriteCommunity(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetListenerWriteCommunityWithChanging () throws Exception {
		this.logger.info("Tests the listenerWriteCommunity setting with changing...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		snmpAdaptor.setListenerWriteCommunity("writeCommunity");

		// Checking
		mocksControl.verify();
		assertEquals("listenerWriteCommunity is bad", "writeCommunity", snmpAdaptor.getListenerWriteCommunity());

	}

	/**
	 * Tests the <code>listenerWriteCommunity</code> setting with an exception.
	 * <p>
	 * Test method for {@link SnmpAdaptor#setListenerWriteCommunity(String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetListenerWriteCommunityWithException () throws Exception {
		this.logger.info("Tests the listenerWriteCommunity setting with an exception...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		EasyMock.expectLastCall().andThrow(new Exception("MOCK EXCEPTION"));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		final String listenerWriteCommunity = snmpAdaptor.getListenerWriteCommunity();
		snmpAdaptor.setListenerWriteCommunity("writeCommunity");

		// Checking
		mocksControl.verify();
		assertEquals("listenerWriteCommunity is bad", listenerWriteCommunity, snmpAdaptor.getListenerWriteCommunity());

	}

	/**
	 * Tests the daemon startup successfully.
	 * <p>
	 * Test method for {@link SnmpAdaptor#start()}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testStartSuccessfully () throws Exception {
		this.logger.info("Tests the daemon startup successfully...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.start();
		daemon.isStarted();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		jmxNotificationManager.setEnabled(EasyMock.eq(true));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		snmpAdaptor.start();

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the daemon startup failure.
	 * <p>
	 * Test method for {@link SnmpAdaptor#start()}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testStartFailure () throws Exception {
		this.logger.info("Tests the daemon startup failure...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.start();
		daemon.isStarted();
		EasyMock.expectLastCall().andReturn(Boolean.FALSE);
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		try {
			snmpAdaptor.start();
			fail("None exception !");
		}
		catch (final Exception e) {
			assertEquals("Error message is bad", "The SNMP daemon did not start", e.getMessage());
		}

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the daemon shutdown successfully.
	 * <p>
	 * Test method for {@link SnmpAdaptor#stop()}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testStopSuccessfully () throws Exception {
		this.logger.info("Tests the daemon shutdown successfully...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		daemon.isStarted();
		EasyMock.expectLastCall().andReturn(Boolean.FALSE);
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		snmpAdaptor.stop();

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the daemon shutdown failure.
	 * <p>
	 * Test method for {@link SnmpAdaptor#stop()}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testStopFailure () throws Exception {
		this.logger.info("Tests the daemon shutdown failure...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		daemon.isStarted();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		try {
			snmpAdaptor.stop();
			fail("None exception !");
		}
		catch (final Exception e) {
			assertEquals("Error message is bad", "The SNMP daemon has not been stopped", e.getMessage());
		}

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests the daemon status.
	 * <p>
	 * Test method for {@link SnmpAdaptor#isStarted()}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testIsStarted () throws Exception {
		this.logger.info("Tests the daemon status...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.isStarted();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, null, null, jmxNotificationManager, daemon);
		final boolean started = snmpAdaptor.isStarted();

		// Checking
		mocksControl.verify();
		assertTrue("The daemon status is bad", started);

	}

	/**
	 * Tests the attribute formatting with the <code>NULL</code> value.
	 * <p>
	 * Test method for {@link SnmpAdaptor#formatAttribute(String)}.
	 * </p>
	 */
	@Test
	public void testFormatAttributeWithNullValue () {
		this.logger.info("Tests the attribute formatting with the NULL value...");
		final String value = SnmpAdaptor.formatAttribute(null);
		assertNull("The result is not NULL", value);
	}

	/**
	 * Tests the attribute formatting with an empty String.
	 * <p>
	 * Test method for {@link SnmpAdaptor#formatAttribute(String)}.
	 * </p>
	 */
	@Test
	public void testFormatAttributeWithEmptyString () {
		this.logger.info("Tests the attribute formatting with an empty String...");
		final String value = SnmpAdaptor.formatAttribute("  ");
		assertNull("The result is not NULL", value);
	}

	/**
	 * Tests the attribute formatting.
	 * <p>
	 * Test method for {@link SnmpAdaptor#formatAttribute(String)}.
	 * </p>
	 */
	@Test
	public void testFormatAttribute () {
		this.logger.info("Tests the attribute formatting...");
		final String value = SnmpAdaptor.formatAttribute("  VALUE  ");
		assertEquals("The result is bad", "VALUE", value);
	}

	/**
	 * Tests a MBean registration.
	 * <p>
	 * Test method for {@link SnmpAdaptor#preRegister(MBeanServer, ObjectName)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testPreRegister () throws Exception {
		this.logger.info("Tests a MBean registration...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxListener jmxListener = mocksControl.createMock(JmxListener.class);
		final JvmSnmpMib jvmMib = mocksControl.createMock(JvmSnmpMib.class);
		final MBeanServer server = MBeanServerFactory.createMBeanServer();
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		jmxListener.open(EasyMock.eq(server));
		jvmMib.open(EasyMock.eq(server));
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, jvmMib, jmxListener, jmxNotificationManager, daemon);
		final ObjectName result = snmpAdaptor.preRegister(server, new ObjectName("net.sf.snmpadaptor4j.example:type=Example"));

		// Checking
		mocksControl.verify();
		assertEquals("The result is bad", new ObjectName("net.sf.snmpadaptor4j.example:type=Example"), result);

	}

	/**
	 * Tests a MBean deregistration.
	 * <p>
	 * Test method for {@link SnmpAdaptor#preDeregister()}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testPreDeregister () throws Exception {
		this.logger.info("Tests a MBean deregistration...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final SnmpDaemon daemon = mocksControl.createMock(SnmpDaemon.class);
		final JmxListener jmxListener = mocksControl.createMock(JmxListener.class);
		final JvmSnmpMib jvmMib = mocksControl.createMock(JvmSnmpMib.class);
		final JmxNotificationManager jmxNotificationManager = mocksControl.createMock(JmxNotificationManager.class);

		// Scenario
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		daemon.stop();
		jmxNotificationManager.setEnabled(EasyMock.eq(false));
		jmxListener.close();
		jvmMib.close();
		mocksControl.replay();

		// Test
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor("1.3.6.1.4.1.99.12.8.1", null, true, null, jvmMib, jmxListener, jmxNotificationManager, daemon);
		snmpAdaptor.preDeregister();

		// Checking
		mocksControl.verify();

	}

	/**
	 * Test method for {@link SnmpAdaptor#toString()}.
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testToString () throws Exception {
		this.logger.info("Tests the toString...");
		final SnmpAdaptor snmpAdaptor = new SnmpAdaptor(null, null, null, true,null);
		assertEquals("The value returned by toString is bad", "SnmpAdaptor[localhost:161]", snmpAdaptor.toString());
	}

}