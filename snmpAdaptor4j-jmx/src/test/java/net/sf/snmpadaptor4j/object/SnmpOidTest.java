package net.sf.snmpadaptor4j.object;

import static org.junit.Assert.*;
import net.sf.snmpadaptor4j.object.SnmpOid;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.object.SnmpOid}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SnmpOidTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(SnmpOidTest.class);

	/**
	 * Tests the OID creation with a string value, a node number and an index.
	 * <p>
	 * Test method for {@link SnmpOid#newInstance(String, int, int)}.
	 * </p>
	 */
	@Test
	public void testNewInstanceWithStringValueAndNodeNumber () {
		this.logger.info("Tests the OID creation with a string value, a node number and an index...");
		final SnmpOid oid = SnmpOid.newInstance("1.3.6.1.4.1.9999", 9, 0);
		assertArrayEquals("The OID is bad", new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 }, oid.getOid());
		assertEquals("The index is bad", 0, oid.getIndex());
	}

	/**
	 * Tests the OID creation with a string value.
	 * <p>
	 * Test method for {@link SnmpOid#newInstance(String)}.
	 * </p>
	 */
	@Test
	public void testNewInstanceWithStringValue () {
		this.logger.info("Tests the OID creation with a string value...");
		final SnmpOid oid = SnmpOid.newInstance("1.3.6.1.4.1.9999.9.0");
		assertArrayEquals("The OID is bad", new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 }, oid.getOid());
		assertEquals("The index is bad", 0, oid.getIndex());
	}

	/**
	 * Tests the OID creation with a numeric sequence and an index.
	 * <p>
	 * Test method for {@link SnmpOid#newInstance(int[], int)}.
	 * </p>
	 */
	@Test
	public void testNewInstanceWithNumericSequenceAndIndex () {
		this.logger.info("Tests the OID creation with a numeric sequence and an index...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9 }, 0);
		assertArrayEquals("The OID is bad", new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 }, oid.getOid());
		assertEquals("The index is bad", 0, oid.getIndex());
	}

	/**
	 * Tests the OID creation with a numeric sequence.
	 * <p>
	 * Test method for {@link SnmpOid#newInstance(int[])}.
	 * </p>
	 */
	@Test
	public void testNewInstanceWithNumericSequence () {
		this.logger.info("Tests the OID creation with a numeric sequence...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 });
		assertArrayEquals("The OID is bad", new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 }, oid.getOid());
		assertEquals("The index is bad", 0, oid.getIndex());
	}

	/**
	 * Test method for {@link SnmpOid#hashCode()}.
	 */
	@Test
	public void testHashCode () {
		this.logger.info("Tests the hashCode...");
		final SnmpOid oid0 = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 33333 });
		final SnmpOid oid1 = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 33333, 9, 3 });
		final SnmpOid oid2 = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 33333, 9, 4 });
		final SnmpOid oid3 = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 33333, 9, 4, 10 });
		final SnmpOid oid4 = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 33333, 10, 0 });
		assertTrue("The value returned by hashCode is bad between oid0 and oid1", oid0.hashCode() != oid1.hashCode());
		assertTrue("The value returned by hashCode is bad between oid0 and oid2", oid0.hashCode() != oid2.hashCode());
		assertTrue("The value returned by hashCode is bad between oid0 and oid3", oid0.hashCode() != oid3.hashCode());
		assertTrue("The value returned by hashCode is bad between oid0 and oid4", oid0.hashCode() != oid4.hashCode());
		assertTrue("The value returned by hashCode is bad between oid1 and oid2", oid1.hashCode() != oid2.hashCode());
		assertTrue("The value returned by hashCode is bad between oid1 and oid3", oid1.hashCode() != oid3.hashCode());
		assertTrue("The value returned by hashCode is bad between oid1 and oid4", oid1.hashCode() != oid4.hashCode());
		assertTrue("The value returned by hashCode is bad between oid2 and oid3", oid2.hashCode() != oid3.hashCode());
		assertTrue("The value returned by hashCode is bad between oid2 and oid4", oid2.hashCode() != oid4.hashCode());
		assertTrue("The value returned by hashCode is bad between oid3 and oid4", oid3.hashCode() != oid4.hashCode());
	}

	/**
	 * Tests the equality with <code>NULL</code>.
	 * <p>
	 * Test method for {@link SnmpOid#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithNull () {
		this.logger.info("Tests the equality with NULL...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 });
		assertFalse("The equality is bad", oid.equals(null));
	}

	/**
	 * Tests the equality with another object.
	 * <p>
	 * Test method for {@link SnmpOid#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithAnotherObject () {
		this.logger.info("Tests the equality with another object...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 });
		assertFalse("The equality is bad", oid.equals(new Object()));
	}

	/**
	 * Tests the equality with itself.
	 * <p>
	 * Test method for {@link SnmpOid#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithItself () {
		this.logger.info("Tests the equality with itself...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 });
		assertTrue("The equality is bad", oid.equals(oid));
	}

	/**
	 * Tests the equality with the same OID.
	 * <p>
	 * Test method for {@link SnmpOid#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithSameOid () {
		this.logger.info("Tests the equality with the same OID...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 });
		assertTrue("The equality is bad", oid.equals(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 })));
	}

	/**
	 * Tests the equality with a different OID.
	 * <p>
	 * Test method for {@link SnmpOid#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentOid () {
		this.logger.info("Tests the equality with a different OID...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 });
		assertFalse("The equality is bad", oid.equals(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 8888, 9, 0 })));
	}

	/**
	 * Tests the equality with an OID in the array.
	 * <p>
	 * Test method for {@link SnmpOid#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithOidInArray () {
		this.logger.info("Tests the equality with an OID in the array...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 5 });
		assertFalse("The equality is bad", oid.equals(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9 })));
	}

	/**
	 * Tests the equality without index and with an OID outside the array.
	 * <p>
	 * Test method for {@link SnmpOid#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithOidOutsideArray () {
		this.logger.info("Tests the equality with an OID outside the array...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 5 });
		assertFalse("The equality is bad", oid.equals(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 8888, 9 })));
	}

	/**
	 * Tests the equality with a child OID.
	 * <p>
	 * Test method for {@link SnmpOid#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithChildOid () {
		this.logger.info("Tests the equality with a child OID...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999 });
		assertFalse("The equality is bad", oid.equals(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 })));
	}

	/**
	 * Tests the equality with a non-child OID.
	 * <p>
	 * Test method for {@link SnmpOid#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithNonChildOid () {
		this.logger.info("Tests the equality with a non-child OID...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999 });
		assertFalse("The equality is bad", oid.equals(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 8888, 9, 0 })));
	}

	/**
	 * Tests the equality without index and with <code>NULL</code>.
	 * <p>
	 * Test method for {@link SnmpOid#equalsWithoutIndex(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithoutIndexWithNull () {
		this.logger.info("Tests the equality without index and with NULL...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 });
		assertFalse("The equality is bad", oid.equalsWithoutIndex(null));
	}

	/**
	 * Tests the equality without index and with itself.
	 * <p>
	 * Test method for {@link SnmpOid#equalsWithoutIndex(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithoutIndexWithItself () {
		this.logger.info("Tests the equality without index and with itself...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 });
		assertFalse("The equality is bad", oid.equalsWithoutIndex(oid));
	}

	/**
	 * Tests the equality without index and with the same OID.
	 * <p>
	 * Test method for {@link SnmpOid#equalsWithoutIndex(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithoutIndexWithSameOid () {
		this.logger.info("Tests the equality without index and with the same OID...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 });
		assertFalse("The equality is bad", oid.equalsWithoutIndex(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 })));
	}

	/**
	 * Tests the equality without index and with a different OID.
	 * <p>
	 * Test method for {@link SnmpOid#equalsWithoutIndex(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithoutIndexWithDifferentOid () {
		this.logger.info("Tests the equality without index and with a different OID...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 });
		assertFalse("The equality is bad", oid.equalsWithoutIndex(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 8888, 9, 0 })));
	}

	/**
	 * Tests the equality without index and with an OID in the array.
	 * <p>
	 * Test method for {@link SnmpOid#equalsWithoutIndex(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithoutIndexWithOidInArray () {
		this.logger.info("Tests the equality without index and with an OID in the array...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 5 });
		assertTrue("The equality is bad", oid.equalsWithoutIndex(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9 })));
	}

	/**
	 * Tests the equality without index and with an OID outside the array.
	 * <p>
	 * Test method for {@link SnmpOid#equalsWithoutIndex(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithoutIndexWithOidOutsideArray () {
		this.logger.info("Tests the equality without index and with an OID outside the array...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 5 });
		assertFalse("The equality is bad", oid.equalsWithoutIndex(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 8888, 9 })));
	}

	/**
	 * Tests the equality without index and with a child OID.
	 * <p>
	 * Test method for {@link SnmpOid#equalsWithoutIndex(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithoutIndexWithChildOid () {
		this.logger.info("Tests the equality without index and with a child OID...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999 });
		assertFalse("The equality is bad", oid.equalsWithoutIndex(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 })));
	}

	/**
	 * Tests the equality without index and with a non-child OID.
	 * <p>
	 * Test method for {@link SnmpOid#equalsWithoutIndex(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithoutIndexWithNonChildOid () {
		this.logger.info("Tests the equality without index and with a non-child OID...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999 });
		assertFalse("The equality is bad", oid.equalsWithoutIndex(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 8888, 9, 0 })));
	}

	/**
	 * Tests the membership of the OID root with <code>NULL</code>.
	 * <p>
	 * Test method for {@link SnmpOid#isRootOf(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testIsRootOfWithNull () {
		this.logger.info("Tests the membership of the OID root with NULL...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 });
		assertFalse("The membership of the OID root is bad", oid.isRootOf(null));
	}

	/**
	 * Tests the membership of the OID root with itself.
	 * <p>
	 * Test method for {@link SnmpOid#isRootOf(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testIsRootOfWithItself () {
		this.logger.info("Tests the membership of the OID root with itself...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 });
		assertTrue("The membership of the OID root is bad", oid.isRootOf(oid));
	}

	/**
	 * Tests the membership of the OID root with the same OID.
	 * <p>
	 * Test method for {@link SnmpOid#isRootOf(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testIsRootOfWithSameOid () {
		this.logger.info("Tests the membership of the OID root with the same OID...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 });
		assertTrue("The membership of the OID root is bad", oid.isRootOf(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 })));
	}

	/**
	 * Tests the membership of the OID root with a different OID.
	 * <p>
	 * Test method for {@link SnmpOid#isRootOf(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testIsRootOfWithDifferentOid () {
		this.logger.info("Tests the membership of the OID root with a different OID...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 });
		assertFalse("The membership of the OID root is bad", oid.isRootOf(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 8888, 9, 0 })));
	}

	/**
	 * Tests the membership of the OID root with an OID in the array.
	 * <p>
	 * Test method for {@link SnmpOid#isRootOf(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testIsRootOfWithOidInArray () {
		this.logger.info("Tests the membership of the OID root with an OID in the array...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 5 });
		assertFalse("The membership of the OID root is bad", oid.isRootOf(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9 })));
	}

	/**
	 * Tests the membership of the OID root with an OID outside the array.
	 * <p>
	 * Test method for {@link SnmpOid#isRootOf(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testIsRootOfWithOidOutsideArray () {
		this.logger.info("Tests the membership of the OID root with an OID outside the array...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 5 });
		assertFalse("The membership of the OID root is bad", oid.isRootOf(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 8888, 9 })));
	}

	/**
	 * Tests the membership of the OID root with a child OID.
	 * <p>
	 * Test method for {@link SnmpOid#isRootOf(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testIsRootOfWithChildOid () {
		this.logger.info("Tests the membership of the OID root with a child OID...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999 });
		assertTrue("The membership of the OID root is bad", oid.isRootOf(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 })));
	}

	/**
	 * Tests the membership of the OID root with a non-child OID.
	 * <p>
	 * Test method for {@link SnmpOid#isRootOf(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testIsRootOfWithNonChildOid () {
		this.logger.info("Tests the membership of the OID root with a non-child OID...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999 });
		assertFalse("The membership of the OID root is bad", oid.isRootOf(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 8888, 9, 0 })));
	}

	/**
	 * Tests the comparison with two equal OID.
	 * <p>
	 * Test method for {@link SnmpOid#compareTo(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testCompareToWithEqualOid () {
		this.logger.info("Tests the comparison with two equal OID...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 });
		assertEquals("The comparison is bad", 0, oid.compareTo(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 })));
	}

	/**
	 * Tests the comparison with a smaller OID (case 1).
	 * <p>
	 * Test method for {@link SnmpOid#compareTo(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testCompareToWithSmallerOid1 () {
		this.logger.info("Tests the comparison with a smaller OID (case 1)...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 2 });
		assertEquals("The comparison is bad", +1, oid.compareTo(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 8, 0 })));
	}

	/**
	 * Tests the comparison with a smaller OID (case 2).
	 * <p>
	 * Test method for {@link SnmpOid#compareTo(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testCompareToWithSmallerOid2 () {
		this.logger.info("Tests the comparison with a smaller OID (case 2)...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 2 });
		assertEquals("The comparison is bad", +1, oid.compareTo(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9 })));
	}

	/**
	 * Tests the comparison with a smaller OID (case 3).
	 * <p>
	 * Test method for {@link SnmpOid#compareTo(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testCompareToWithSmallerOid3 () {
		this.logger.info("Tests the comparison with a smaller OID (case 3)...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 2 });
		assertEquals("The comparison is bad", +1, oid.compareTo(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 1, 0 })));
	}

	/**
	 * Tests the comparison with a largest OID (case 1).
	 * <p>
	 * Test method for {@link SnmpOid#compareTo(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testCompareToWithLargestOid1 () {
		this.logger.info("Tests the comparison with a largest OID (case 1)...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 2 });
		assertEquals("The comparison is bad", -1, oid.compareTo(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 3 })));
	}

	/**
	 * Tests the comparison with a largest OID (case 2).
	 * <p>
	 * Test method for {@link SnmpOid#compareTo(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testCompareToWithLargestOid2 () {
		this.logger.info("Tests the comparison with a largest OID (case 2)...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 2 });
		assertEquals("The comparison is bad", -1, oid.compareTo(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 10000, 0 })));
	}

	/**
	 * Tests the comparison with a largest OID (case 3).
	 * <p>
	 * Test method for {@link SnmpOid#compareTo(SnmpOid)}.
	 * </p>
	 */
	@Test
	public void testCompareToWithLargestOid3 () {
		this.logger.info("Tests the comparison with a largest OID (case 3)...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 2 });
		assertEquals("The comparison is bad", -1, oid.compareTo(SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 2, 0 })));
	}

	/**
	 * Test method for {@link SnmpOid#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests the toString...");
		final SnmpOid oid = SnmpOid.newInstance(new int[] { 1, 3, 6, 1, 4, 1, 9999, 9, 0 });
		assertEquals("The value returned by toString is bad", ".1.3.6.1.4.1.9999.9.0", oid.toString());
	}

}