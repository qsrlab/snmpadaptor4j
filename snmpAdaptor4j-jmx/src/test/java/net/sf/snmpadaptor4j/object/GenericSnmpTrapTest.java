package net.sf.snmpadaptor4j.object;

import static org.junit.Assert.*;
import java.util.HashMap;
import java.util.Map;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import net.sf.snmpadaptor4j.object.SnmpTrap;
import net.sf.snmpadaptor4j.object.SnmpTrapData;
import net.sf.snmpadaptor4j.object.GenericSnmpTrap;
import net.sf.snmpadaptor4j.object.GenericSnmpTrapType;
import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.object.GenericSnmpTrap}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class GenericSnmpTrapTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(GenericSnmpTrapTest.class);

	/**
	 * Tests the constructor.
	 * <p>
	 * Test method for {@link SnmpTrap#newInstance(long, SnmpOid, GenericSnmpTrapType, Map)}.
	 * </p>
	 */
	@Test
	public void testSnmpTrapGeneric () {
		this.logger.info("Tests the constructor...");
		final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final GenericSnmpTrap trap = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), GenericSnmpTrapType.linkDown, dataMap);
		assertEquals("timeStamp is bad", 48L, trap.getTimeStamp());
		assertEquals("source is bad", SnmpOid.newInstance("1.2"), trap.getSource());
		assertEquals("type is bad", GenericSnmpTrapType.linkDown, trap.getType());
		assertEquals("dataMap is bad", dataMap, trap.getDataMap());
	}

	/**
	 * Tests the tracing of the SNMP trap content to logs.
	 * <p>
	 * Test method for {@link SnmpTrap#traceTo(Logger)}.
	 * </p>
	 */
	@Test
	public void testTraceTo () {
		this.logger.info("Tests the tracing of the SNMP trap content to logs...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final Logger mockLogger = mocksControl.createMock(Logger.class);
		final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final GenericSnmpTrap trap = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), GenericSnmpTrapType.linkDown, dataMap);

		// Scenario
		mockLogger.trace(EasyMock.eq("enterprise         = .1.2"));
		mockLogger.trace(EasyMock.eq("generic-trap       = linkDown"));
		mockLogger.trace(EasyMock.eq("specific-trap      = 0"));
		mockLogger.trace(EasyMock.eq("time-stamp         = 48"));
		mockLogger.trace(EasyMock.eq("variable-bindings: .1.3.1.0 = (octetString) data 1"));
		mockLogger.trace(EasyMock.eq("variable-bindings: .1.3.2.0 = (octetString) data 2"));
		mocksControl.replay();

		// Test
		trap.traceTo(mockLogger);

		// Checking
		mocksControl.verify();

	}

	/**
	 * Test method for {@link GenericSnmpTrap#hashCode()}.
	 */
	@Test
	public void testHashCode () {
		this.logger.info("Tests the hashCode...");
		final Map<SnmpOid, SnmpTrapData> dataMap1 = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap1.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap1.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final Map<SnmpOid, SnmpTrapData> dataMap2 = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap2.put(SnmpOid.newInstance("1.3.3.0"), new SnmpTrapData(SnmpDataType.opaque, "data 3"));
		final GenericSnmpTrap trap1 = SnmpTrap.newInstance(0L, null, null, null);
		final GenericSnmpTrap trap2 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), GenericSnmpTrapType.linkDown, dataMap1);
		final GenericSnmpTrap trap3 = SnmpTrap.newInstance(55L, SnmpOid.newInstance("1.2"), GenericSnmpTrapType.linkDown, dataMap1);
		final GenericSnmpTrap trap4 = SnmpTrap.newInstance(55L, SnmpOid.newInstance("1.3"), GenericSnmpTrapType.linkDown, dataMap1);
		final GenericSnmpTrap trap5 = SnmpTrap.newInstance(55L, SnmpOid.newInstance("1.3"), GenericSnmpTrapType.linkUp, dataMap1);
		final GenericSnmpTrap trap6 = SnmpTrap.newInstance(55L, SnmpOid.newInstance("1.3"), GenericSnmpTrapType.linkUp, dataMap2);
		assertTrue("The value returned by hashCode is bad between trap1 and trap2", trap1.hashCode() != trap2.hashCode());
		assertTrue("The value returned by hashCode is bad between trap1 and trap3", trap1.hashCode() != trap3.hashCode());
		assertTrue("The value returned by hashCode is bad between trap1 and trap4", trap1.hashCode() != trap4.hashCode());
		assertTrue("The value returned by hashCode is bad between trap1 and trap5", trap1.hashCode() != trap5.hashCode());
		assertTrue("The value returned by hashCode is bad between trap1 and trap6", trap1.hashCode() != trap6.hashCode());
		assertTrue("The value returned by hashCode is bad between trap2 and trap3", trap2.hashCode() != trap3.hashCode());
		assertTrue("The value returned by hashCode is bad between trap2 and trap4", trap2.hashCode() != trap4.hashCode());
		assertTrue("The value returned by hashCode is bad between trap2 and trap5", trap2.hashCode() != trap5.hashCode());
		assertTrue("The value returned by hashCode is bad between trap2 and trap6", trap2.hashCode() != trap6.hashCode());
		assertTrue("The value returned by hashCode is bad between trap3 and trap4", trap3.hashCode() != trap4.hashCode());
		assertTrue("The value returned by hashCode is bad between trap3 and trap5", trap3.hashCode() != trap5.hashCode());
		assertTrue("The value returned by hashCode is bad between trap3 and trap6", trap3.hashCode() != trap6.hashCode());
		assertTrue("The value returned by hashCode is bad between trap4 and trap5", trap4.hashCode() != trap5.hashCode());
		assertTrue("The value returned by hashCode is bad between trap4 and trap6", trap4.hashCode() != trap6.hashCode());
		assertTrue("The value returned by hashCode is bad between trap5 and trap6", trap5.hashCode() != trap6.hashCode());
	}

	/**
	 * Tests the equality with <code>NULL</code>.
	 * <p>
	 * Test method for {@link GenericSnmpTrap#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithNull () {
		this.logger.info("Tests the equality with NULL...");
		final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final GenericSnmpTrap trap = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), GenericSnmpTrapType.linkDown, dataMap);
		assertFalse("The equality is bad", trap.equals(null));
	}

	/**
	 * Tests the equality with another object.
	 * <p>
	 * Test method for {@link GenericSnmpTrap#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithAnotherObject () {
		this.logger.info("Tests the equality with another object...");
		final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final GenericSnmpTrap trap = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), GenericSnmpTrapType.linkDown, dataMap);
		assertFalse("The equality is bad", trap.equals(new Object()));
	}

	/**
	 * Tests the equality with itself.
	 * <p>
	 * Test method for {@link GenericSnmpTrap#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithItself () {
		this.logger.info("Tests the equality with itself...");
		final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final GenericSnmpTrap trap = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), GenericSnmpTrapType.linkDown, dataMap);
		assertTrue("The equality is bad", trap.equals(trap));
	}

	/**
	 * Tests the equality with the same {@link GenericSnmpTrap}.
	 * <p>
	 * Test method for {@link GenericSnmpTrap#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithSameGenericSnmpTrap () {
		this.logger.info("Tests the equality with the same GenericSnmpTrap...");
		final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final GenericSnmpTrap trap1 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), GenericSnmpTrapType.linkDown, dataMap);
		final GenericSnmpTrap trap2 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), GenericSnmpTrapType.linkDown, dataMap);
		assertTrue("The equality is bad", trap1.equals(trap2));
		assertTrue("The equality with NULL values is bad", SnmpTrap.newInstance(0L, null, null, null).equals(SnmpTrap.newInstance(0L, null, null, null)));
	}

	/**
	 * Tests the equality with a different timeStamp.
	 * <p>
	 * Test method for {@link GenericSnmpTrap#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentTimeStamp () {
		this.logger.info("Tests the equality with a different timeStamp...");
		final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final GenericSnmpTrap trap1 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), GenericSnmpTrapType.linkDown, dataMap);
		final GenericSnmpTrap trap2 = SnmpTrap.newInstance(55L, SnmpOid.newInstance("1.2"), GenericSnmpTrapType.linkDown, dataMap);
		assertFalse("The equality is bad", trap1.equals(trap2));
	}

	/**
	 * Tests the equality with a different source.
	 * <p>
	 * Test method for {@link GenericSnmpTrap#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentSource () {
		this.logger.info("Tests the equality with a different source...");
		final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final GenericSnmpTrap trap1 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), GenericSnmpTrapType.linkDown, dataMap);
		final GenericSnmpTrap trap2 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.3"), GenericSnmpTrapType.linkDown, dataMap);
		assertFalse("The equality is bad", trap1.equals(trap2));
		assertFalse("The equality is bad (NULL value)", trap1.equals(SnmpTrap.newInstance(48L, null, GenericSnmpTrapType.linkDown, dataMap)));
		assertFalse("The equality is bad (NULL value)", SnmpTrap.newInstance(48L, null, GenericSnmpTrapType.linkDown, dataMap).equals(trap1));
	}

	/**
	 * Tests the equality with a different dataMap.
	 * <p>
	 * Test method for {@link GenericSnmpTrap#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentDataMap () {
		this.logger.info("Tests the equality with a different dataMap...");
		final Map<SnmpOid, SnmpTrapData> dataMap1 = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap1.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap1.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final Map<SnmpOid, SnmpTrapData> dataMap2 = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap1.put(SnmpOid.newInstance("1.3.3.0"), new SnmpTrapData(SnmpDataType.opaque, "data 3"));
		final GenericSnmpTrap trap1 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), GenericSnmpTrapType.linkDown, dataMap1);
		final GenericSnmpTrap trap2 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), GenericSnmpTrapType.linkDown, dataMap2);
		assertFalse("The equality is bad", trap1.equals(trap2));
		assertFalse("The equality is bad (NULL value)", trap1.equals(SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), GenericSnmpTrapType.linkDown, null)));
		assertFalse("The equality is bad (NULL value)", SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), GenericSnmpTrapType.linkDown, null).equals(trap1));
	}

	/**
	 * Tests the equality with a different type.
	 * <p>
	 * Test method for {@link GenericSnmpTrap#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentType () {
		this.logger.info("Tests the equality with a different type...");
		final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final GenericSnmpTrap trap1 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), GenericSnmpTrapType.linkDown, dataMap);
		final GenericSnmpTrap trap2 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), GenericSnmpTrapType.linkUp, dataMap);
		assertFalse("The equality is bad", trap1.equals(trap2));
		assertFalse("The equality is bad (NULL value)", trap1.equals(SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), null, dataMap)));
		assertFalse("The equality is bad (NULL value)", SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), null, dataMap).equals(trap1));
	}

	/**
	 * Test method for {@link GenericSnmpTrap#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests the toString...");
		final GenericSnmpTrap trap = SnmpTrap.newInstance(0, null, null, null);
		assertEquals("The value returned by toString is bad", "SNMP trap null - Type null/0", trap.toString());
	}

}