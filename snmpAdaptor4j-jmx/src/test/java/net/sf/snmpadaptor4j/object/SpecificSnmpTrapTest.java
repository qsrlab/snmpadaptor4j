package net.sf.snmpadaptor4j.object;

import static org.junit.Assert.*;
import java.util.HashMap;
import java.util.Map;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import net.sf.snmpadaptor4j.object.SnmpTrap;
import net.sf.snmpadaptor4j.object.SnmpTrapData;
import net.sf.snmpadaptor4j.object.SpecificSnmpTrap;
import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.object.SpecificSnmpTrap}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SpecificSnmpTrapTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(SpecificSnmpTrapTest.class);

	/**
	 * Tests the constructor.
	 * <p>
	 * Test method for {@link SnmpTrap#newInstance(long, SnmpOid, int, Map)}.
	 * </p>
	 */
	@Test
	public void testSnmpTrapSpecific () {
		this.logger.info("Tests the constructor...");
		final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final SpecificSnmpTrap trap = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), 3, dataMap);
		assertEquals("timeStamp is bad", 48L, trap.getTimeStamp());
		assertEquals("source is bad", SnmpOid.newInstance("1.2"), trap.getSource());
		assertEquals("type is bad", 3, trap.getType());
		assertEquals("dataMap is bad", dataMap, trap.getDataMap());
	}

	/**
	 * Tests the tracing of the SNMP trap content to logs.
	 * <p>
	 * Test method for {@link SnmpTrap#traceTo(Logger)}.
	 * </p>
	 */
	@Test
	public void testTraceTo () {
		this.logger.info("Tests the tracing of the SNMP trap content to logs...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final Logger mockLogger = mocksControl.createMock(Logger.class);
		final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final SpecificSnmpTrap trap = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), 3, dataMap);

		// Scenario
		mockLogger.trace(EasyMock.eq("enterprise         = .1.2"));
		mockLogger.trace(EasyMock.eq("generic-trap       = enterpriseSpecific"));
		mockLogger.trace(EasyMock.eq("specific-trap      = 3"));
		mockLogger.trace(EasyMock.eq("time-stamp         = 48"));
		mockLogger.trace(EasyMock.eq("variable-bindings: .1.3.1.0 = (octetString) data 1"));
		mockLogger.trace(EasyMock.eq("variable-bindings: .1.3.2.0 = (octetString) data 2"));
		mocksControl.replay();

		// Test
		trap.traceTo(mockLogger);

		// Checking
		mocksControl.verify();

	}

	/**
	 * Test method for {@link SpecificSnmpTrap#hashCode()}.
	 */
	@Test
	public void testHashCode () {
		this.logger.info("Tests the hashCode...");
		final Map<SnmpOid, SnmpTrapData> dataMap1 = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap1.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap1.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final Map<SnmpOid, SnmpTrapData> dataMap2 = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap2.put(SnmpOid.newInstance("1.3.3.0"), new SnmpTrapData(SnmpDataType.opaque, "data 3"));
		final SpecificSnmpTrap trap1 = SnmpTrap.newInstance(0L, null, 0, null);
		final SpecificSnmpTrap trap2 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), 0, dataMap1);
		final SpecificSnmpTrap trap3 = SnmpTrap.newInstance(55L, SnmpOid.newInstance("1.2"), 0, dataMap1);
		final SpecificSnmpTrap trap4 = SnmpTrap.newInstance(55L, SnmpOid.newInstance("1.3"), 0, dataMap1);
		final SpecificSnmpTrap trap5 = SnmpTrap.newInstance(55L, SnmpOid.newInstance("1.3"), 1, dataMap1);
		final SpecificSnmpTrap trap6 = SnmpTrap.newInstance(55L, SnmpOid.newInstance("1.3"), 1, dataMap2);
		assertTrue("The value returned by hashCode is bad between trap1 and trap2", trap1.hashCode() != trap2.hashCode());
		assertTrue("The value returned by hashCode is bad between trap1 and trap3", trap1.hashCode() != trap3.hashCode());
		assertTrue("The value returned by hashCode is bad between trap1 and trap4", trap1.hashCode() != trap4.hashCode());
		assertTrue("The value returned by hashCode is bad between trap1 and trap5", trap1.hashCode() != trap5.hashCode());
		assertTrue("The value returned by hashCode is bad between trap1 and trap6", trap1.hashCode() != trap6.hashCode());
		assertTrue("The value returned by hashCode is bad between trap2 and trap3", trap2.hashCode() != trap3.hashCode());
		assertTrue("The value returned by hashCode is bad between trap2 and trap4", trap2.hashCode() != trap4.hashCode());
		assertTrue("The value returned by hashCode is bad between trap2 and trap5", trap2.hashCode() != trap5.hashCode());
		assertTrue("The value returned by hashCode is bad between trap2 and trap6", trap2.hashCode() != trap6.hashCode());
		assertTrue("The value returned by hashCode is bad between trap3 and trap4", trap3.hashCode() != trap4.hashCode());
		assertTrue("The value returned by hashCode is bad between trap3 and trap5", trap3.hashCode() != trap5.hashCode());
		assertTrue("The value returned by hashCode is bad between trap3 and trap6", trap3.hashCode() != trap6.hashCode());
		assertTrue("The value returned by hashCode is bad between trap4 and trap5", trap4.hashCode() != trap5.hashCode());
		assertTrue("The value returned by hashCode is bad between trap4 and trap6", trap4.hashCode() != trap6.hashCode());
		assertTrue("The value returned by hashCode is bad between trap5 and trap6", trap5.hashCode() != trap6.hashCode());
	}

	/**
	 * Tests the equality with <code>NULL</code>.
	 * <p>
	 * Test method for {@link SpecificSnmpTrap#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithNull () {
		this.logger.info("Tests the equality with NULL...");
		final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final SpecificSnmpTrap trap = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), 0, dataMap);
		assertFalse("The equality is bad", trap.equals(null));
	}

	/**
	 * Tests the equality with another object.
	 * <p>
	 * Test method for {@link SpecificSnmpTrap#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithAnotherObject () {
		this.logger.info("Tests the equality with another object...");
		final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final SpecificSnmpTrap trap = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), 0, dataMap);
		assertFalse("The equality is bad", trap.equals(new Object()));
	}

	/**
	 * Tests the equality with itself.
	 * <p>
	 * Test method for {@link SpecificSnmpTrap#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithItself () {
		this.logger.info("Tests the equality with itself...");
		final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final SpecificSnmpTrap trap = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), 0, dataMap);
		assertTrue("The equality is bad", trap.equals(trap));
	}

	/**
	 * Tests the equality with the same {@link SpecificSnmpTrap}.
	 * <p>
	 * Test method for {@link SpecificSnmpTrap#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithSameSpecificSnmpTrap () {
		this.logger.info("Tests the equality with the same SpecificSnmpTrap...");
		final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final SpecificSnmpTrap trap1 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), 0, dataMap);
		final SpecificSnmpTrap trap2 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), 0, dataMap);
		assertTrue("The equality is bad", trap1.equals(trap2));
		assertTrue("The equality with NULL values is bad", SnmpTrap.newInstance(0L, null, null, null).equals(SnmpTrap.newInstance(0L, null, null, null)));
	}

	/**
	 * Tests the equality with a different timeStamp.
	 * <p>
	 * Test method for {@link SpecificSnmpTrap#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentTimeStamp () {
		this.logger.info("Tests the equality with a different timeStamp...");
		final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final SpecificSnmpTrap trap1 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), 0, dataMap);
		final SpecificSnmpTrap trap2 = SnmpTrap.newInstance(55L, SnmpOid.newInstance("1.2"), 0, dataMap);
		assertFalse("The equality is bad", trap1.equals(trap2));
	}

	/**
	 * Tests the equality with a different source.
	 * <p>
	 * Test method for {@link SpecificSnmpTrap#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentSource () {
		this.logger.info("Tests the equality with a different source...");
		final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final SpecificSnmpTrap trap1 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), 0, dataMap);
		final SpecificSnmpTrap trap2 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.3"), 0, dataMap);
		assertFalse("The equality is bad", trap1.equals(trap2));
		assertFalse("The equality is bad (NULL value)", trap1.equals(SnmpTrap.newInstance(48L, null, 0, dataMap)));
		assertFalse("The equality is bad (NULL value)", SnmpTrap.newInstance(48L, null, 0, dataMap).equals(trap1));
	}

	/**
	 * Tests the equality with a different dataMap.
	 * <p>
	 * Test method for {@link SpecificSnmpTrap#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentDataMap () {
		this.logger.info("Tests the equality with a different dataMap...");
		final Map<SnmpOid, SnmpTrapData> dataMap1 = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap1.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap1.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final Map<SnmpOid, SnmpTrapData> dataMap2 = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap1.put(SnmpOid.newInstance("1.3.3.0"), new SnmpTrapData(SnmpDataType.opaque, "data 3"));
		final SpecificSnmpTrap trap1 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), 0, dataMap1);
		final SpecificSnmpTrap trap2 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), 0, dataMap2);
		assertFalse("The equality is bad", trap1.equals(trap2));
		assertFalse("The equality is bad (NULL value)", trap1.equals(SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), 0, null)));
		assertFalse("The equality is bad (NULL value)", SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), 0, null).equals(trap1));
	}

	/**
	 * Tests the equality with a different type.
	 * <p>
	 * Test method for {@link SpecificSnmpTrap#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentType () {
		this.logger.info("Tests the equality with a different type...");
		final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
		dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
		dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
		final SpecificSnmpTrap trap1 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), 0, dataMap);
		final SpecificSnmpTrap trap2 = SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), 1, dataMap);
		assertFalse("The equality is bad", trap1.equals(trap2));
		assertFalse("The equality is bad (NULL value)", trap1.equals(SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), null, dataMap)));
		assertFalse("The equality is bad (NULL value)", SnmpTrap.newInstance(48L, SnmpOid.newInstance("1.2"), null, dataMap).equals(trap1));
	}

	/**
	 * Test method for {@link SpecificSnmpTrap#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests the toString...");
		final SpecificSnmpTrap trap = SnmpTrap.newInstance(0, null, 3, null);
		assertEquals("The value returned by toString is bad", "SNMP trap null - Type enterpriseSpecific/3", trap.toString());
	}

}