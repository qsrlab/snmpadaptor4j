package net.sf.snmpadaptor4j.object;

import static org.junit.Assert.*;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpTrapData;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.object.SnmpTrapData}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class SnmpTrapDataTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(SnmpTrapDataTest.class);

	/**
	 * Tests the constructor.
	 * <p>
	 * Test method for {@link SnmpTrapData#SnmpTrapData(SnmpDataType, Object)}.
	 * </p>
	 */
	@Test
	public void testSnmpTrapData () {
		this.logger.info("Tests the constructor...");
		final SnmpTrapData snmpTrapData = new SnmpTrapData(SnmpDataType.octetString, "VALUE");
		assertEquals("type is bad", SnmpDataType.octetString, snmpTrapData.getType());
		assertEquals("value is bad", "VALUE", snmpTrapData.getValue());
	}

	/**
	 * Test method for {@link SnmpTrapData#hashCode()}.
	 */
	@Test
	public void testHashCode () {
		this.logger.info("Tests the hashCode...");
		final SnmpTrapData snmpTrapData1 = new SnmpTrapData(null, null);
		final SnmpTrapData snmpTrapData2 = new SnmpTrapData(SnmpDataType.octetString, null);
		final SnmpTrapData snmpTrapData3 = new SnmpTrapData(null, "VALUE");
		final SnmpTrapData snmpTrapData4 = new SnmpTrapData(SnmpDataType.octetString, "VALUE");
		final SnmpTrapData snmpTrapData5 = new SnmpTrapData(SnmpDataType.timeTicks, new Long(10));
		final SnmpTrapData snmpTrapData6 = new SnmpTrapData(SnmpDataType.octetString, "OTHER_VALUE");
		assertTrue("The value returned by hashCode is bad between snmpTrapData1 and snmpTrapData2", snmpTrapData1.hashCode() != snmpTrapData2.hashCode());
		assertTrue("The value returned by hashCode is bad between snmpTrapData1 and snmpTrapData3", snmpTrapData1.hashCode() != snmpTrapData3.hashCode());
		assertTrue("The value returned by hashCode is bad between snmpTrapData1 and snmpTrapData4", snmpTrapData1.hashCode() != snmpTrapData4.hashCode());
		assertTrue("The value returned by hashCode is bad between snmpTrapData1 and snmpTrapData5", snmpTrapData1.hashCode() != snmpTrapData5.hashCode());
		assertTrue("The value returned by hashCode is bad between snmpTrapData1 and snmpTrapData6", snmpTrapData1.hashCode() != snmpTrapData6.hashCode());
		assertTrue("The value returned by hashCode is bad between snmpTrapData2 and snmpTrapData3", snmpTrapData2.hashCode() != snmpTrapData3.hashCode());
		assertTrue("The value returned by hashCode is bad between snmpTrapData2 and snmpTrapData4", snmpTrapData2.hashCode() != snmpTrapData4.hashCode());
		assertTrue("The value returned by hashCode is bad between snmpTrapData2 and snmpTrapData5", snmpTrapData2.hashCode() != snmpTrapData5.hashCode());
		assertTrue("The value returned by hashCode is bad between snmpTrapData2 and snmpTrapData6", snmpTrapData2.hashCode() != snmpTrapData6.hashCode());
		assertTrue("The value returned by hashCode is bad between snmpTrapData3 and snmpTrapData4", snmpTrapData3.hashCode() != snmpTrapData4.hashCode());
		assertTrue("The value returned by hashCode is bad between snmpTrapData3 and snmpTrapData5", snmpTrapData3.hashCode() != snmpTrapData5.hashCode());
		assertTrue("The value returned by hashCode is bad between snmpTrapData3 and snmpTrapData6", snmpTrapData3.hashCode() != snmpTrapData6.hashCode());
		assertTrue("The value returned by hashCode is bad between snmpTrapData4 and snmpTrapData5", snmpTrapData4.hashCode() != snmpTrapData5.hashCode());
		assertTrue("The value returned by hashCode is bad between snmpTrapData4 and snmpTrapData6", snmpTrapData4.hashCode() != snmpTrapData6.hashCode());
		assertTrue("The value returned by hashCode is bad between snmpTrapData5 and snmpTrapData6", snmpTrapData5.hashCode() != snmpTrapData6.hashCode());
	}

	/**
	 * Tests the equality with <code>NULL</code>.
	 * <p>
	 * Test method for {@link SnmpTrapData#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithNull () {
		this.logger.info("Tests the equality with NULL...");
		final SnmpTrapData snmpTrapData = new SnmpTrapData(SnmpDataType.octetString, "VALUE");
		assertFalse("The equality is bad", snmpTrapData.equals(null));
	}

	/**
	 * Tests the equality with another object.
	 * <p>
	 * Test method for {@link SnmpTrapData#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithAnotherObject () {
		this.logger.info("Tests the equality with another object...");
		final SnmpTrapData snmpTrapData = new SnmpTrapData(SnmpDataType.octetString, "VALUE");
		assertFalse("The equality is bad", snmpTrapData.equals(new Object()));
	}

	/**
	 * Tests the equality with itself.
	 * <p>
	 * Test method for {@link SnmpTrapData#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithItself () {
		this.logger.info("Tests the equality with itself...");
		final SnmpTrapData snmpTrapData = new SnmpTrapData(SnmpDataType.octetString, "VALUE");
		assertTrue("The equality is bad", snmpTrapData.equals(snmpTrapData));
	}

	/**
	 * Tests the equality with the same {@link SnmpTrapData}.
	 * <p>
	 * Test method for {@link SnmpTrapData#equals(java.lang.Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithSameSnmpTrapData () {
		this.logger.info("Tests the equality with the same SnmpTrapData...");
		assertTrue("The equality is bad", new SnmpTrapData(SnmpDataType.octetString, "VALUE").equals(new SnmpTrapData(SnmpDataType.octetString, "VALUE")));
		assertTrue("The equality with NULL values is bad", new SnmpTrapData(null, null).equals(new SnmpTrapData(null, null)));
	}

	/**
	 * Tests the equality with a different type.
	 * <p>
	 * Test method for {@link SnmpTrapData#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentType () {
		this.logger.info("Tests the equality with a different type...");
		assertFalse("The equality is bad", new SnmpTrapData(SnmpDataType.octetString, "VALUE").equals(new SnmpTrapData(SnmpDataType.opaque, "VALUE")));
		assertFalse("The equality is bad (NULL value)", new SnmpTrapData(SnmpDataType.octetString, "VALUE").equals(new SnmpTrapData(null, "VALUE")));
		assertFalse("The equality is bad (NULL value)", new SnmpTrapData(null, "VALUE").equals(new SnmpTrapData(SnmpDataType.octetString, "VALUE")));
	}

	/**
	 * Tests the equality with a different value.
	 * <p>
	 * Test method for {@link SnmpTrapData#equals(Object)}.
	 * </p>
	 */
	@Test
	public void testEqualsWithDifferentValue () {
		this.logger.info("Tests the equality with a different value...");
		assertFalse("The equality is bad", new SnmpTrapData(SnmpDataType.octetString, "VALUE").equals(new SnmpTrapData(SnmpDataType.octetString, "OTHER_VALUE")));
		assertFalse("The equality is bad (NULL value)", new SnmpTrapData(SnmpDataType.octetString, "VALUE").equals(new SnmpTrapData(SnmpDataType.octetString, null)));
		assertFalse("The equality is bad (NULL value)", new SnmpTrapData(SnmpDataType.octetString, null).equals(new SnmpTrapData(SnmpDataType.octetString, "VALUE")));
	}

	/**
	 * Test method for {@link SnmpTrapData#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests the toString...");
		final SnmpTrapData snmpTrapData = new SnmpTrapData(SnmpDataType.octetString, "VALUE");
		assertEquals("The value returned by toString is bad", "(octetString) VALUE", snmpTrapData.toString());
	}

}