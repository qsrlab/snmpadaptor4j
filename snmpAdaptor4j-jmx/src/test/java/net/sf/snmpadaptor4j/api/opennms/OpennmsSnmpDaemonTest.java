package net.sf.snmpadaptor4j.api.opennms;

import static org.junit.Assert.*;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import net.sf.snmpadaptor4j.api.SnmpException;
import net.sf.snmpadaptor4j.api.SnmpMib;
import net.sf.snmpadaptor4j.api.AttributeAccessor;
import net.sf.snmpadaptor4j.api.opennms.OpennmsSnmpDaemon;
import net.sf.snmpadaptor4j.api.opennms.OpennmsSupport;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import net.sf.snmpadaptor4j.test.mock.SnmpConfigurationMock;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.Test;
import org.opennms.protocols.snmp.SnmpAgentSession;
import org.opennms.protocols.snmp.SnmpEndOfMibView;
import org.opennms.protocols.snmp.SnmpInt32;
import org.opennms.protocols.snmp.SnmpNull;
import org.opennms.protocols.snmp.SnmpObjectId;
import org.opennms.protocols.snmp.SnmpOctetString;
import org.opennms.protocols.snmp.SnmpPduBulk;
import org.opennms.protocols.snmp.SnmpPduPacket;
import org.opennms.protocols.snmp.SnmpPduRequest;
import org.opennms.protocols.snmp.SnmpPeer;
import org.opennms.protocols.snmp.SnmpSyntax;
import org.opennms.protocols.snmp.SnmpVarBind;
import org.opennms.protocols.snmp.asn1.AsnEncodingException;

/**
 * Test class for {@link net.sf.snmpadaptor4j.api.opennms.OpennmsSnmpDaemon}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class OpennmsSnmpDaemonTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(OpennmsSnmpDaemonTest.class);

	/**
	 * Daemon to test with errors in sending.
	 */
	protected final class OpennmsSnmpDaemonToTest
			extends OpennmsSnmpDaemon {

		/**
		 * List of exceptions to throw.
		 */
		private final List<Exception> exceptionList = new ArrayList<Exception>();

		/**
		 * Destination.
		 */
		private SnmpPeer destination = null;

		/**
		 * Response.
		 */
		private SnmpPduPacket response = null;

		/**
		 * Constructor.
		 * @param mocksControl Mocks control.
		 */
		protected OpennmsSnmpDaemonToTest (final IMocksControl mocksControl) {
			super(null, mocksControl.createMock(SnmpMib.class));
		}

		/**
		 * Returns the list of exceptions to throw.
		 * @return List of exceptions to throw.
		 */
		protected List<Exception> getExceptionList () {
			return this.exceptionList;
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.api.opennms.OpennmsSnmpDaemon#sendPduPacket(org.opennms.protocols.snmp.SnmpAgentSession, org.opennms.protocols.snmp.SnmpPeer,
		 * org.opennms.protocols.snmp.SnmpPduPacket)
		 */
		@Override
		protected void sendPduPacket (final SnmpAgentSession session, final SnmpPeer peer, final SnmpPduPacket pdu) throws Exception {
			final Exception e = (this.exceptionList.size() > 0 ? this.exceptionList.remove(0) : null);
			if (e != null) {
				throw e;
			}
			this.response = pdu;
			this.destination = peer;
		}

		/**
		 * Returns the destination.
		 * @return Destination.
		 */
		public SnmpPeer getDestination () {
			return this.destination;
		}

		/**
		 * Returns the response.
		 * @return Response.
		 */
		protected SnmpPduPacket getResponse () {
			return this.response;
		}

	}

	/**
	 * Unhandled request.
	 */
	protected final class SnmpPduUnhandled
			extends SnmpPduPacket {

		/*
		 * {@inheritDoc}
		 * @see org.opennms.protocols.snmp.SnmpPduPacket#clone()
		 */
		@Override
		public Object clone () {
			return null;
		}

		/*
		 * {@inheritDoc}
		 * @see org.opennms.protocols.snmp.SnmpPduPacket#duplicate()
		 */
		@Override
		public SnmpSyntax duplicate () {
			return null;
		}

	}

	/**
	 * Tests starting and stopping of SNMP daemon for v1.
	 * <p>
	 * Test methods for:
	 * </p>
	 * <ul>
	 * <li>{@link OpennmsSnmpDaemon#start()}</li>,
	 * <li>{@link OpennmsSnmpDaemon#stop()}</li>,
	 * <li>{@link OpennmsSnmpDaemon#isStarted()}</li>.
	 * </ul>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testStartStopWithSNMPv1 () throws Exception {
		this.logger.info("Test starting and stopping of SNMP daemon for v1...");

		// Daemon creating
		final OpennmsSnmpDaemon daemon = new OpennmsSnmpDaemon(new SnmpConfigurationMock(1), null);
		daemon.logger.setLevel(Level.ALL);
		assertFalse("Daemon already started", daemon.isStarted());

		// Daemon starting
		daemon.start();
		assertTrue("Daemon not started", daemon.isStarted());

		// Daemon stopping
		daemon.stop();
		assertFalse("Daemon not stopped", daemon.isStarted());

	}

	/**
	 * Tests starting and stopping of SNMP daemon for v2.
	 * <p>
	 * Test methods for:
	 * </p>
	 * <ul>
	 * <li>{@link OpennmsSnmpDaemon#start()}</li>,
	 * <li>{@link OpennmsSnmpDaemon#stop()}</li>,
	 * <li>{@link OpennmsSnmpDaemon#isStarted()}</li>.
	 * </ul>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testStartStopWithSNMPv2 () throws Exception {
		this.logger.info("Test starting and stopping of SNMP daemon for v2...");

		// Daemon creating
		final OpennmsSnmpDaemon daemon = new OpennmsSnmpDaemon(new SnmpConfigurationMock(2), null);
		daemon.logger.setLevel(Level.ALL);
		assertFalse("Daemon already started", daemon.isStarted());

		// Daemon starting
		daemon.start();
		assertTrue("Daemon not started", daemon.isStarted());

		// Daemon stopping
		daemon.stop();
		assertFalse("Daemon not stopped", daemon.isStarted());

	}

	/**
	 * Tests starting and stopping of SNMP daemon for a default version.
	 * <p>
	 * Test methods for:
	 * </p>
	 * <ul>
	 * <li>{@link OpennmsSnmpDaemon#start()}</li>,
	 * <li>{@link OpennmsSnmpDaemon#stop()}</li>,
	 * <li>{@link OpennmsSnmpDaemon#isStarted()}</li>.
	 * </ul>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testStartStopWithDefaultVersion () throws Exception {
		this.logger.info("Test starting and stopping of SNMP daemon for a default version...");

		// Daemon creating
		final OpennmsSnmpDaemon daemon = new OpennmsSnmpDaemon(new SnmpConfigurationMock(0), null);
		daemon.logger.setLevel(Level.ALL);
		assertFalse("Daemon already started", daemon.isStarted());

		// Daemon starting
		daemon.start();
		assertTrue("Daemon not started", daemon.isStarted());

		// Daemon stopping
		daemon.stop();
		assertFalse("Daemon not stopped", daemon.isStarted());

	}

	/**
	 * Tests re-starting of SNMP daemon.
	 * <p>
	 * Test methods for:
	 * </p>
	 * <ul>
	 * <li>{@link OpennmsSnmpDaemon#start()}</li>,
	 * <li>{@link OpennmsSnmpDaemon#stop()}</li>,
	 * <li>{@link OpennmsSnmpDaemon#isStarted()}</li>.
	 * </ul>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testReStart () throws Exception {
		this.logger.info("Test re-starting of SNMP daemon...");

		// Daemon creating
		final OpennmsSnmpDaemon daemon = new OpennmsSnmpDaemon(new SnmpConfigurationMock(), null);
		daemon.logger.setLevel(Level.ALL);
		assertFalse("Daemon already started", daemon.isStarted());

		// Daemon starting
		daemon.start();
		assertTrue("Daemon not started", daemon.isStarted());

		// Daemon re-starting
		daemon.start();
		assertTrue("Daemon not started", daemon.isStarted());

		// Daemon stopping
		daemon.stop();
		assertFalse("Daemon not stopped", daemon.isStarted());

	}

	/**
	 * Tests re-stopping of SNMP daemon.
	 * <p>
	 * Test methods for:
	 * </p>
	 * <ul>
	 * <li>{@link OpennmsSnmpDaemon#start()}</li>,
	 * <li>{@link OpennmsSnmpDaemon#stop()}</li>,
	 * <li>{@link OpennmsSnmpDaemon#isStarted()}</li>.
	 * </ul>
	 */
	@Test
	public void testReStop () {
		this.logger.info("Test re-stopping of SNMP daemon...");

		// Daemon creating
		final OpennmsSnmpDaemon daemon = new OpennmsSnmpDaemon(new SnmpConfigurationMock(), null);
		daemon.logger.setLevel(Level.ALL);
		assertFalse("Daemon already started", daemon.isStarted());

		// Daemon re-stopping
		daemon.stop();
		assertFalse("Daemon not stopped", daemon.isStarted());

	}

	/**
	 * Tests of error handling.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#SnmpAgentSessionError(SnmpAgentSession, int, Object)}.
	 * </p>
	 */
	@Test
	public void testSnmpAgentSessionError () {
		this.logger.info("Tests of error handling...");
		final OpennmsSnmpDaemon daemon = new OpennmsSnmpDaemon(new SnmpConfigurationMock(), null);
		daemon.logger.setLevel(Level.ALL);
		try {
			daemon.SnmpAgentSessionError(null, 45, null);
			fail("None SnmpException has occurred");
		}
		catch (final SnmpException e) {
			this.logger.info(e);
			assertEquals("Error status is bad", 45, e.getErrorStatus());
			assertEquals("Error message is bad", "error 45", e.getMessage());
			assertEquals("Error status is bad", Level.ERROR, e.getLoggerLevel());
		}
	}

	/**
	 * Tests PDU receiving with unhandled PDU.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedPdu(SnmpAgentSession, InetAddress, int, SnmpOctetString, SnmpPduPacket)} .
	 * </p>
	 */
	@Test
	public void testSnmpReceivedPduUnhandled () {
		this.logger.info("Test PDU receiving with unhandled PDU...");
		final OpennmsSnmpDaemon daemon = new OpennmsSnmpDaemon(null, null);
		daemon.logger.setLevel(Level.OFF);
		final SnmpPduRequest pdu = new SnmpPduRequest();
		pdu.setRequestId(1);
		daemon.snmpReceivedPdu(null, null, 161, null, pdu);
	}

	/**
	 * Tests PDU receiving with unable to send any response.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedPdu(SnmpAgentSession, InetAddress, int, SnmpOctetString, SnmpPduPacket)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedPduWithUnableToSendAnyResponse () throws Exception {
		final InetAddress manager = InetAddress.getLocalHost();
		final int port = 999;
		this.logger.info("Test PDU receiving with unable to send any response...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		daemon.logger.setLevel(Level.ALL);

		// Scenario
		daemon.getExceptionList().add(new Exception("FIRST MOCK ERROR"));
		daemon.getExceptionList().add(new Exception("SECOND MOCK ERROR"));
		daemon.snmpMib.nextSet(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(new TreeMap<SnmpOid, AttributeAccessor>());
		mocksControl.replay();

		// Test
		SnmpPduBulk bulk = new SnmpPduBulk();
		bulk.setRequestId(2);
		bulk.addVarBind(new SnmpVarBind(oid1));
		daemon.snmpReceivedPdu(null, manager, port, null, bulk);

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests PDU receiving with ErrTooBig and unable to send the error.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedPdu(SnmpAgentSession, InetAddress, int, SnmpOctetString, SnmpPduPacket)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedPduWithErrTooBigAndUnableToSendError () throws Exception {
		final InetAddress manager = InetAddress.getLocalHost();
		final int port = 999;
		this.logger.info("Test PDU receiving with ErrTooBig and unable to send the error...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		daemon.logger.setLevel(Level.OFF);

		// Scenario
		daemon.getExceptionList().add(new AsnEncodingException("FIRST MOCK ERROR"));
		daemon.getExceptionList().add(new Exception("SECOND MOCK ERROR"));
		daemon.snmpMib.nextSet(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(new TreeMap<SnmpOid, AttributeAccessor>());
		mocksControl.replay();

		// Test
		SnmpPduBulk bulk = new SnmpPduBulk();
		bulk.setRequestId(3);
		bulk.addVarBind(new SnmpVarBind(oid1));
		daemon.snmpReceivedPdu(null, manager, port, null, bulk);

		// Checking
		mocksControl.verify();

	}

	/**
	 * Tests PDU receiving with unable to send the response, but succeeded to send the error.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedPdu(SnmpAgentSession, InetAddress, int, SnmpOctetString, SnmpPduPacket)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedPduWithUnableToSendTheResponse () throws Exception {
		final InetAddress manager = InetAddress.getLocalHost();
		final int port = 999;
		this.logger.info("Test PDU receiving with unable to send the response, but succeeded to send the error...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		daemon.logger.setLevel(Level.ALL);

		// Scenario
		daemon.getExceptionList().add(new Exception("FIRST MOCK ERROR"));
		daemon.snmpMib.next(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(null);
		mocksControl.replay();

		// Test
		SnmpPduBulk bulk = new SnmpPduBulk();
		bulk.setRequestId(4);
		bulk.addVarBind(new SnmpVarBind(oid1));
		bulk.setNonRepeaters(1);
		daemon.snmpReceivedPdu(null, manager, port, null, bulk);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", daemon.getDestination());
		assertEquals("Bad manager of destination", manager, daemon.getDestination().getPeer());
		assertEquals("Bad port of destination", port, daemon.getDestination().getPort());
		assertNotNull("NULL response", daemon.getResponse());
		assertEquals("Bad type of response", SnmpPduRequest.class, daemon.getResponse().getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, daemon.getResponse().getCommand());
		assertEquals("Bad requestId of response", bulk.getRequestId(), daemon.getResponse().getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrGenError, ((SnmpPduRequest) daemon.getResponse()).getErrorStatus());
		assertEquals("Bad errorIndex of response", 0, ((SnmpPduRequest) daemon.getResponse()).getErrorIndex());
		assertEquals("Bad length of response", 0, daemon.getResponse().getLength());

	}

	/**
	 * Tests PDU receiving with only ErrTooBig.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedPdu(SnmpAgentSession, InetAddress, int, SnmpOctetString, SnmpPduPacket)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedPduWithErrTooBig () throws Exception {
		final InetAddress manager = InetAddress.getLocalHost();
		final int port = 999;
		this.logger.info("Test PDU receiving with only ErrTooBig...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		daemon.logger.setLevel(Level.OFF);

		// Scenario
		daemon.getExceptionList().add(new AsnEncodingException("FIRST MOCK ERROR"));
		daemon.snmpMib.next(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(null);
		mocksControl.replay();

		// Test
		SnmpPduBulk bulk = new SnmpPduBulk();
		bulk.setRequestId(5);
		bulk.addVarBind(new SnmpVarBind(oid1));
		bulk.setNonRepeaters(1);
		daemon.snmpReceivedPdu(null, manager, port, null, bulk);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", daemon.getDestination());
		assertEquals("Bad manager of destination", manager, daemon.getDestination().getPeer());
		assertEquals("Bad port of destination", port, daemon.getDestination().getPort());
		assertNotNull("NULL response", daemon.getResponse());
		assertEquals("Bad type of response", SnmpPduRequest.class, daemon.getResponse().getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, daemon.getResponse().getCommand());
		assertEquals("Bad requestId of response", bulk.getRequestId(), daemon.getResponse().getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrTooBig, ((SnmpPduRequest) daemon.getResponse()).getErrorStatus());
		assertEquals("Bad errorIndex of response", 0, ((SnmpPduRequest) daemon.getResponse()).getErrorIndex());
		assertEquals("Bad length of response", 0, daemon.getResponse().getLength());

	}

	/**
	 * Tests PDU receiving with a {@link SnmpException}.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedPdu(SnmpAgentSession, InetAddress, int, SnmpOctetString, SnmpPduPacket)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedPduWithSnmpException () throws Exception {
		final InetAddress manager = InetAddress.getLocalHost();
		final int port = 999;
		this.logger.info("Test PDU receiving with a SnmpException...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		final int errorStatus = -99;
		daemon.logger.setLevel(Level.ALL);

		// Scenario
		daemon.snmpMib.nextSet(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andThrow(new SnmpException(errorStatus));
		mocksControl.replay();

		// Test
		SnmpPduBulk bulk = new SnmpPduBulk();
		bulk.setRequestId(6);
		bulk.addVarBind(new SnmpVarBind(oid1));
		daemon.snmpReceivedPdu(null, manager, port, null, bulk);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", daemon.getDestination());
		assertEquals("Bad manager of destination", manager, daemon.getDestination().getPeer());
		assertEquals("Bad port of destination", port, daemon.getDestination().getPort());
		assertNotNull("NULL response", daemon.getResponse());
		assertEquals("Bad type of response", SnmpPduRequest.class, daemon.getResponse().getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, daemon.getResponse().getCommand());
		assertEquals("Bad requestId of response", bulk.getRequestId(), daemon.getResponse().getRequestId());
		assertEquals("Bad errorStatus of response", errorStatus, ((SnmpPduRequest) daemon.getResponse()).getErrorStatus());
		assertEquals("Bad errorIndex of response", 1, ((SnmpPduRequest) daemon.getResponse()).getErrorIndex());
		assertEquals("Bad length of response", 0, daemon.getResponse().getLength());

	}

	/**
	 * Tests PDU receiving with an other exception.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedPdu(SnmpAgentSession, InetAddress, int, SnmpOctetString, SnmpPduPacket)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedPduWithOtherException () throws Exception {
		final InetAddress manager = InetAddress.getLocalHost();
		final int port = 999;
		this.logger.info("Test PDU receiving with an other exception...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		daemon.logger.setLevel(Level.ALL);

		// Scenario
		daemon.snmpMib.nextSet(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andThrow(new RuntimeException("MOCK EXCEPTION"));
		mocksControl.replay();

		// Test
		SnmpPduBulk bulk = new SnmpPduBulk();
		bulk.setRequestId(7);
		bulk.addVarBind(new SnmpVarBind(oid1));
		daemon.snmpReceivedPdu(null, manager, port, null, bulk);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", daemon.getDestination());
		assertEquals("Bad manager of destination", manager, daemon.getDestination().getPeer());
		assertEquals("Bad port of destination", port, daemon.getDestination().getPort());
		assertNotNull("NULL response", daemon.getResponse());
		assertEquals("Bad type of response", SnmpPduRequest.class, daemon.getResponse().getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, daemon.getResponse().getCommand());
		assertEquals("Bad requestId of response", bulk.getRequestId(), daemon.getResponse().getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrGenError, ((SnmpPduRequest) daemon.getResponse()).getErrorStatus());
		assertEquals("Bad errorIndex of response", 1, ((SnmpPduRequest) daemon.getResponse()).getErrorIndex());
		assertEquals("Bad length of response", 0, daemon.getResponse().getLength());

	}

	/**
	 * Tests PDU receiving with no result found.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedPdu(SnmpAgentSession, InetAddress, int, SnmpOctetString, SnmpPduPacket)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedPduWithNoResultFound () throws Exception {
		final InetAddress manager = InetAddress.getLocalHost();
		final int port = 999;
		this.logger.info("Test PDU receiving with no result found...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		daemon.logger.setLevel(Level.ALL);

		// Scenario
		daemon.snmpMib.next(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(null);
		mocksControl.replay();

		// Test
		SnmpPduBulk bulk = new SnmpPduBulk();
		bulk.setRequestId(8);
		bulk.addVarBind(new SnmpVarBind(oid1));
		bulk.setNonRepeaters(1);
		daemon.snmpReceivedPdu(null, manager, port, null, bulk);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", daemon.getDestination());
		assertEquals("Bad manager of destination", manager, daemon.getDestination().getPeer());
		assertEquals("Bad port of destination", port, daemon.getDestination().getPort());
		assertNotNull("NULL response", daemon.getResponse());
		assertEquals("Bad type of response", SnmpPduRequest.class, daemon.getResponse().getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, daemon.getResponse().getCommand());
		assertEquals("Bad requestId of response", bulk.getRequestId(), daemon.getResponse().getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrNoError, ((SnmpPduRequest) (daemon.getResponse())).getErrorStatus());
		assertEquals("Bad errorIndex of response", 0, ((SnmpPduRequest) (daemon.getResponse())).getErrorIndex());
		assertEquals("Bad length of response", 1, daemon.getResponse().getLength());
		assertNotNull("Bad varBind at 0 of response", daemon.getResponse().getVarBindAt(0));
		assertNotNull("Bad varBind.name at 0 of response", daemon.getResponse().getVarBindAt(0).getName());
		assertEquals("Bad varBind.name at 0 of response", ".1.3.6.1.4.1.99.12.8.1.1", daemon.getResponse().getVarBindAt(0).getName().toString());
		assertNotNull("Bad varBind.value at 0 of response", daemon.getResponse().getVarBindAt(0).getValue());
		assertEquals("Bad varBind.value at 0 of response", SnmpEndOfMibView.class, daemon.getResponse().getVarBindAt(0).getValue().getClass());

	}

	/**
	 * Tests PDU receiving with result found (non repeaters).
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedPdu(SnmpAgentSession, InetAddress, int, SnmpOctetString, SnmpPduPacket)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedPduWithResultNonRepeatersFound () throws Exception {
		final InetAddress manager = InetAddress.getLocalHost();
		final int port = 999;
		this.logger.info("Test PDU receiving with result found (non repeaters)...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		final AttributeAccessor mibNode = mocksControl.createMock(AttributeAccessor.class);
		daemon.logger.setLevel(Level.ALL);

		// Scenario
		daemon.snmpMib.next(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(mibNode);
		mibNode.toString();
		mibNode.getOid();
		EasyMock.expectLastCall().andReturn(SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.1"));
		mibNode.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode.getValue();
		EasyMock.expectLastCall().andReturn(null);
		mibNode.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		mocksControl.replay();

		// Test
		SnmpPduBulk bulk = new SnmpPduBulk();
		bulk.setRequestId(9);
		bulk.addVarBind(new SnmpVarBind(oid1));
		bulk.setNonRepeaters(1);
		daemon.snmpReceivedPdu(null, manager, port, null, bulk);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", daemon.getDestination());
		assertEquals("Bad manager of destination", manager, daemon.getDestination().getPeer());
		assertEquals("Bad port of destination", port, daemon.getDestination().getPort());
		assertNotNull("NULL response", daemon.getResponse());
		assertEquals("Bad type of response", SnmpPduRequest.class, daemon.getResponse().getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, daemon.getResponse().getCommand());
		assertEquals("Bad requestId of response", bulk.getRequestId(), daemon.getResponse().getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrNoError, ((SnmpPduRequest) (daemon.getResponse())).getErrorStatus());
		assertEquals("Bad errorIndex of response", 0, ((SnmpPduRequest) (daemon.getResponse())).getErrorIndex());
		assertEquals("Bad length of response", 1, daemon.getResponse().getLength());
		assertNotNull("Bad varBind at 0 of response", daemon.getResponse().getVarBindAt(0));
		assertNotNull("Bad varBind.name at 0 of response", daemon.getResponse().getVarBindAt(0).getName());
		assertEquals("Bad varBind.name at 0 of response", ".1.3.6.1.4.1.99.12.8.1.1.1", daemon.getResponse().getVarBindAt(0).getName().toString());
		assertNotNull("Bad varBind.value at 0 of response", daemon.getResponse().getVarBindAt(0).getValue());
		assertEquals("Bad varBind.value at 0 of response", SnmpInt32.class, daemon.getResponse().getVarBindAt(0).getValue().getClass());
		assertEquals("Bad varBind.value at 0 of response", 0, ((SnmpInt32) (daemon.getResponse().getVarBindAt(0).getValue())).getValue());

	}

	/**
	 * Tests PDU receiving with result found (100 repeaters).
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedPdu(SnmpAgentSession, InetAddress, int, SnmpOctetString, SnmpPduPacket)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedPduWithResult100RepeatersFound () throws Exception {
		final InetAddress manager = InetAddress.getLocalHost();
		final int port = 999;
		this.logger.info("Test PDU receiving with result found (100 repeaters)...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		final AttributeAccessor mibNode1 = mocksControl.createMock(AttributeAccessor.class);
		final AttributeAccessor mibNode2 = mocksControl.createMock(AttributeAccessor.class);
		final SortedMap<SnmpOid, AttributeAccessor> mibNodeMap = new TreeMap<SnmpOid, AttributeAccessor>();
		mibNodeMap.put(SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.1"), mibNode1);
		mibNodeMap.put(SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.2"), mibNode2);
		daemon.logger.setLevel(Level.ALL);

		// Scenario
		daemon.snmpMib.nextSet(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(mibNodeMap);
		mibNode1.toString();
		mibNode1.getOid();
		EasyMock.expectLastCall().andReturn(SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.1"));
		mibNode1.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode1.getValue();
		EasyMock.expectLastCall().andReturn(null);
		mibNode1.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		mibNode2.toString();
		mibNode2.getOid();
		EasyMock.expectLastCall().andReturn(SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.2"));
		mibNode2.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode2.getValue();
		EasyMock.expectLastCall().andReturn(null);
		mibNode2.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		mocksControl.replay();

		// Test
		SnmpPduBulk bulk = new SnmpPduBulk();
		bulk.setRequestId(10);
		bulk.addVarBind(new SnmpVarBind(oid1));
		bulk.setNonRepeaters(0);
		bulk.setMaxRepititions(100);
		daemon.snmpReceivedPdu(null, manager, port, null, bulk);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", daemon.getDestination());
		assertEquals("Bad manager of destination", manager, daemon.getDestination().getPeer());
		assertEquals("Bad port of destination", port, daemon.getDestination().getPort());
		assertNotNull("NULL response", daemon.getResponse());
		assertEquals("Bad type of response", SnmpPduRequest.class, daemon.getResponse().getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, daemon.getResponse().getCommand());
		assertEquals("Bad requestId of response", bulk.getRequestId(), daemon.getResponse().getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrNoError, ((SnmpPduRequest) (daemon.getResponse())).getErrorStatus());
		assertEquals("Bad errorIndex of response", 0, ((SnmpPduRequest) (daemon.getResponse())).getErrorIndex());
		assertEquals("Bad length of response", 3, daemon.getResponse().getLength());
		assertNotNull("Bad varBind at 0 of response", daemon.getResponse().getVarBindAt(0));
		assertNotNull("Bad varBind.name at 0 of response", daemon.getResponse().getVarBindAt(0).getName());
		assertEquals("Bad varBind.name at 0 of response", ".1.3.6.1.4.1.99.12.8.1.1.1", daemon.getResponse().getVarBindAt(0).getName().toString());
		assertNotNull("Bad varBind.value at 0 of response", daemon.getResponse().getVarBindAt(0).getValue());
		assertEquals("Bad varBind.value at 0 of response", SnmpInt32.class, daemon.getResponse().getVarBindAt(0).getValue().getClass());
		assertEquals("Bad varBind.value at 0 of response", 0, ((SnmpInt32) (daemon.getResponse().getVarBindAt(0).getValue())).getValue());
		assertNotNull("Bad varBind at 1 of response", daemon.getResponse().getVarBindAt(1));
		assertNotNull("Bad varBind.name at 1 of response", daemon.getResponse().getVarBindAt(1).getName());
		assertEquals("Bad varBind.name at 1 of response", ".1.3.6.1.4.1.99.12.8.1.1.2", daemon.getResponse().getVarBindAt(1).getName().toString());
		assertNotNull("Bad varBind.value at 1 of response", daemon.getResponse().getVarBindAt(1).getValue());
		assertEquals("Bad varBind.value at 1 of response", SnmpInt32.class, daemon.getResponse().getVarBindAt(1).getValue().getClass());
		assertEquals("Bad varBind.value at 1 of response", 0, ((SnmpInt32) (daemon.getResponse().getVarBindAt(1).getValue())).getValue());
		assertNotNull("Bad varBind at 2 of response", daemon.getResponse().getVarBindAt(2));
		assertNotNull("Bad varBind.name at 2 of response", daemon.getResponse().getVarBindAt(2).getName());
		assertEquals("Bad varBind.name at 2 of response", ".1.9", daemon.getResponse().getVarBindAt(2).getName().toString());
		assertNotNull("Bad varBind.value at 2 of response", daemon.getResponse().getVarBindAt(2).getValue());
		assertEquals("Bad varBind.value at 2 of response", SnmpEndOfMibView.class, daemon.getResponse().getVarBindAt(2).getValue().getClass());

	}

	/**
	 * Tests PDU receiving with result found (1 repeater).
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedPdu(SnmpAgentSession, InetAddress, int, SnmpOctetString, SnmpPduPacket)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedPduWithResult1RepeaterFound () throws Exception {
		final InetAddress manager = InetAddress.getLocalHost();
		final int port = 999;
		this.logger.info("Test PDU receiving with result found (1 repeater)...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		final AttributeAccessor mibNode1 = mocksControl.createMock(AttributeAccessor.class);
		final AttributeAccessor mibNode2 = mocksControl.createMock(AttributeAccessor.class);
		final SortedMap<SnmpOid, AttributeAccessor> mibNodeMap = new TreeMap<SnmpOid, AttributeAccessor>();
		mibNodeMap.put(SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.1"), mibNode1);
		mibNodeMap.put(SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.2"), mibNode2);
		daemon.logger.setLevel(Level.ALL);

		// Scenario
		daemon.snmpMib.nextSet(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(mibNodeMap);
		mibNode1.toString();
		mibNode1.getOid();
		EasyMock.expectLastCall().andReturn(SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.1"));
		mibNode1.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode1.getValue();
		EasyMock.expectLastCall().andReturn(null);
		mibNode1.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		mocksControl.replay();

		// Test
		SnmpPduBulk bulk = new SnmpPduBulk();
		bulk.setRequestId(11);
		bulk.addVarBind(new SnmpVarBind(oid1));
		bulk.setNonRepeaters(0);
		bulk.setMaxRepititions(1);
		daemon.snmpReceivedPdu(null, manager, port, null, bulk);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", daemon.getDestination());
		assertEquals("Bad manager of destination", manager, daemon.getDestination().getPeer());
		assertEquals("Bad port of destination", port, daemon.getDestination().getPort());
		assertNotNull("NULL response", daemon.getResponse());
		assertEquals("Bad type of response", SnmpPduRequest.class, daemon.getResponse().getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, daemon.getResponse().getCommand());
		assertEquals("Bad requestId of response", bulk.getRequestId(), daemon.getResponse().getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrNoError, ((SnmpPduRequest) (daemon.getResponse())).getErrorStatus());
		assertEquals("Bad errorIndex of response", 0, ((SnmpPduRequest) (daemon.getResponse())).getErrorIndex());
		assertEquals("Bad length of response", 1, daemon.getResponse().getLength());
		assertNotNull("Bad varBind at 0 of response", daemon.getResponse().getVarBindAt(0));
		assertNotNull("Bad varBind.name at 0 of response", daemon.getResponse().getVarBindAt(0).getName());
		assertEquals("Bad varBind.name at 0 of response", ".1.3.6.1.4.1.99.12.8.1.1.1", daemon.getResponse().getVarBindAt(0).getName().toString());
		assertNotNull("Bad varBind.value at 0 of response", daemon.getResponse().getVarBindAt(0).getValue());
		assertEquals("Bad varBind.value at 0 of response", SnmpInt32.class, daemon.getResponse().getVarBindAt(0).getValue().getClass());
		assertEquals("Bad varBind.value at 0 of response", 0, ((SnmpInt32) (daemon.getResponse().getVarBindAt(0).getValue())).getValue());

	}

	/**
	 * Tests GET receiving with unhandled PDU.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedGet(SnmpPduPacket, boolean)}.
	 * </p>
	 */
	@Test
	public void testSnmpReceivedGetUnhandled () {
		this.logger.info("Test GET receiving with unhandled PDU...");
		final OpennmsSnmpDaemon daemon = new OpennmsSnmpDaemon(null, null);
		daemon.logger.setLevel(Level.ALL);
		final SnmpPduPacket pdu = new SnmpPduUnhandled();
		pdu.setRequestId(12);
		final SnmpPduRequest response = daemon.snmpReceivedGet(pdu, true);

		// Checking
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", pdu.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrGenError, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 0, response.getErrorIndex());
		assertEquals("Bad length of response", 0, response.getLength());

	}

	/**
	 * Tests GET receiving with ErrTooBig.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedGet(SnmpPduPacket, boolean)}.
	 * </p>
	 */
	@Test
	public void testSnmpReceivedGetWithErrTooBig () {
		this.logger.info("Test GET receiving with ErrTooBig...");
		final OpennmsSnmpDaemon daemon = new OpennmsSnmpDaemon(null, null);
		daemon.logger.setLevel(Level.ALL);
		final SnmpPduPacket pdu = new SnmpPduRequest();
		pdu.setRequestId(13);
		for (int i = 1; i < 22; i++) {
			pdu.addVarBind(new SnmpVarBind(new SnmpObjectId(".1.3.6.1.4.1.99.12.8.1." + i)));
		}
		final SnmpPduRequest response = daemon.snmpReceivedGet(pdu, true);

		// Checking
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", pdu.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrTooBig, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 0, response.getErrorIndex());
		assertEquals("Bad length of response", 0, response.getLength());

	}

	/**
	 * Tests GET receiving with a {@link SnmpException}.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedGet(SnmpPduPacket, boolean)}.
	 * </p>
	 */
	@Test
	public void testSnmpReceivedGetV1WithSnmpException () {
		this.logger.info("Test GET receiving (SNMP v1) with a SnmpException...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		final int errorStatus = -99;
		daemon.logger.setLevel(Level.ALL);

		// Scenario
		daemon.snmpMib.next(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andThrow(new SnmpException(errorStatus));
		mocksControl.replay();

		// Test
		SnmpPduRequest pdu = new SnmpPduRequest();
		pdu.setRequestId(14);
		pdu.addVarBind(new SnmpVarBind(oid1));
		final SnmpPduRequest response = daemon.snmpReceivedGet(pdu, true);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", pdu.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", -99, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 1, response.getErrorIndex());
		assertEquals("Bad length of response", 0, response.getLength());

	}

	/**
	 * Tests GET receiving with an other exception.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedGet(SnmpPduPacket, boolean)}.
	 * </p>
	 */
	@Test
	public void testSnmpReceivedGetV1WithOtherException () {
		this.logger.info("Test GET receiving (SNMP v1) with an other exception...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		daemon.logger.setLevel(Level.ALL);

		// Scenario
		daemon.snmpMib.find(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andThrow(new RuntimeException("MOCK EXCEPTION"));
		mocksControl.replay();

		// Test
		SnmpPduRequest pdu = new SnmpPduRequest();
		pdu.setRequestId(15);
		pdu.addVarBind(new SnmpVarBind(oid1));
		final SnmpPduRequest response = daemon.snmpReceivedGet(pdu, false);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", pdu.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrGenError, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 1, response.getErrorIndex());
		assertEquals("Bad length of response", 0, response.getLength());

	}

	/**
	 * Tests GET receiving with getNext and no result found.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedGet(SnmpPduPacket, boolean)}.
	 * </p>
	 */
	@Test
	public void testSnmpReceivedGetV1WithGetNextAndNoResultFound () {
		this.logger.info("Test GET receiving (SNMP v1) with getNext and no result found...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		daemon.logger.setLevel(Level.ALL);

		// Scenario
		daemon.snmpMib.next(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(null);
		mocksControl.replay();

		// Test
		SnmpPduRequest pdu = new SnmpPduRequest();
		pdu.setRequestId(16);
		pdu.addVarBind(new SnmpVarBind(oid1));
		final SnmpPduRequest response = daemon.snmpReceivedGet(pdu, true);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", pdu.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrNoSuchName, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 1, response.getErrorIndex());
		assertEquals("Bad length of response", 1, response.getLength());
		assertNotNull("Bad varBind at 0 of response", response.getVarBindAt(0));
		assertNotNull("Bad varBind.name at 0 of response", response.getVarBindAt(0).getName());
		assertEquals("Bad varBind.name at 0 of response", ".1.3.6.1.4.1.99.12.8.1.1", response.getVarBindAt(0).getName().toString());
		assertNotNull("Bad varBind.value at 0 of response", response.getVarBindAt(0).getValue());
		assertEquals("Bad varBind.value at 0 of response", SnmpNull.class, response.getVarBindAt(0).getValue().getClass());

	}

	/**
	 * Tests GET receiving with getNext and 1 result found.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedGet(SnmpPduPacket, boolean)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedGetV1WithGetNextAnd1result () throws Exception {
		this.logger.info("Test GET receiving (SNMP v1) with getNext and 1 result found...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		final AttributeAccessor mibNode = mocksControl.createMock(AttributeAccessor.class);
		daemon.logger.setLevel(Level.ALL);

		// Scenario
		daemon.snmpMib.next(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(mibNode);
		mibNode.toString();
		mibNode.getOid();
		EasyMock.expectLastCall().andReturn(SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.1"));
		mibNode.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode.getValue();
		EasyMock.expectLastCall().andReturn(null);
		mibNode.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		mocksControl.replay();

		// Test
		SnmpPduRequest pdu = new SnmpPduRequest();
		pdu.setRequestId(17);
		pdu.addVarBind(new SnmpVarBind(oid1));
		final SnmpPduRequest response = daemon.snmpReceivedGet(pdu, true);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", pdu.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrNoError, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 0, response.getErrorIndex());
		assertEquals("Bad length of response", 1, response.getLength());
		assertNotNull("Bad varBind at 0 of response", response.getVarBindAt(0));
		assertNotNull("Bad varBind.name at 0 of response", response.getVarBindAt(0).getName());
		assertEquals("Bad varBind.name at 0 of response", ".1.3.6.1.4.1.99.12.8.1.1.1", response.getVarBindAt(0).getName().toString());
		assertNotNull("Bad varBind.value at 0 of response", response.getVarBindAt(0).getValue());
		assertEquals("Bad varBind.value at 0 of response", SnmpInt32.class, response.getVarBindAt(0).getValue().getClass());
		assertEquals("Bad varBind.value at 0 of response", 0, ((SnmpInt32) (response.getVarBindAt(0).getValue())).getValue());

	}

	/**
	 * Tests GET receiving with getNext and 2 results found.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedGet(SnmpPduPacket, boolean)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedGetV1WithGetNextAnd2results () throws Exception {
		this.logger.info("Test GET receiving (SNMP v1) with getNext and 2 results found...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		final String oid2 = "1.3.6.1.4.1.99.12.8.1.2";
		final AttributeAccessor mibNode1 = mocksControl.createMock(AttributeAccessor.class);
		final AttributeAccessor mibNode2 = mocksControl.createMock(AttributeAccessor.class);
		daemon.logger.setLevel(Level.OFF);

		// Scenario
		daemon.snmpMib.next(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(mibNode1);
		mibNode1.toString();
		mibNode1.getOid();
		EasyMock.expectLastCall().andReturn(SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.1"));
		mibNode1.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode1.getValue();
		EasyMock.expectLastCall().andReturn(null);
		mibNode1.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		daemon.snmpMib.next(EasyMock.eq(SnmpOid.newInstance(oid2)));
		EasyMock.expectLastCall().andReturn(mibNode2);
		mibNode2.toString();
		mibNode2.getOid();
		EasyMock.expectLastCall().andReturn(SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.2.1"));
		mibNode2.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode2.getValue();
		EasyMock.expectLastCall().andReturn(null);
		mibNode2.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		mocksControl.replay();

		// Test
		SnmpPduRequest pdu = new SnmpPduRequest();
		pdu.setRequestId(18);
		pdu.addVarBind(new SnmpVarBind(oid1));
		pdu.addVarBind(new SnmpVarBind(oid2));
		final SnmpPduRequest response = daemon.snmpReceivedGet(pdu, true);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", pdu.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrNoError, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 0, response.getErrorIndex());
		assertEquals("Bad length of response", 2, response.getLength());
		assertNotNull("Bad varBind at 0 of response", response.getVarBindAt(0));
		assertNotNull("Bad varBind.name at 0 of response", response.getVarBindAt(0).getName());
		assertEquals("Bad varBind.name at 0 of response", ".1.3.6.1.4.1.99.12.8.1.1.1", response.getVarBindAt(0).getName().toString());
		assertNotNull("Bad varBind.value at 0 of response", response.getVarBindAt(0).getValue());
		assertEquals("Bad varBind.value at 0 of response", SnmpInt32.class, response.getVarBindAt(0).getValue().getClass());
		assertEquals("Bad varBind.value at 0 of response", 0, ((SnmpInt32) (response.getVarBindAt(0).getValue())).getValue());
		assertNotNull("Bad varBind at 1 of response", response.getVarBindAt(1));
		assertNotNull("Bad varBind.name at 1 of response", response.getVarBindAt(1).getName());
		assertEquals("Bad varBind.name at 1 of response", ".1.3.6.1.4.1.99.12.8.1.2.1", response.getVarBindAt(1).getName().toString());
		assertNotNull("Bad varBind.value at 1 of response", response.getVarBindAt(1).getValue());
		assertEquals("Bad varBind.value at 1 of response", SnmpInt32.class, response.getVarBindAt(1).getValue().getClass());
		assertEquals("Bad varBind.value at 1 of response", 0, ((SnmpInt32) (response.getVarBindAt(1).getValue())).getValue());

	}

	/**
	 * Tests GET receiving with no getNext and no result found.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedGet(SnmpPduPacket, boolean)}.
	 * </p>
	 */
	@Test
	public void testSnmpReceivedGetV1WithNoGetNextAndNoResultFound () {
		this.logger.info("Test GET receiving (SNMP v1) with no getNext and no result found...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1.1";
		daemon.logger.setLevel(Level.ALL);

		// Scenario
		daemon.snmpMib.find(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(null);
		mocksControl.replay();

		// Test
		SnmpPduRequest pdu = new SnmpPduRequest();
		pdu.setRequestId(19);
		pdu.addVarBind(new SnmpVarBind(oid1));
		final SnmpPduRequest response = daemon.snmpReceivedGet(pdu, false);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", pdu.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrNoSuchName, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 1, response.getErrorIndex());
		assertEquals("Bad length of response", 1, response.getLength());
		assertNotNull("Bad varBind at 0 of response", response.getVarBindAt(0));
		assertNotNull("Bad varBind.name at 0 of response", response.getVarBindAt(0).getName());
		assertEquals("Bad varBind.name at 0 of response", ".1.3.6.1.4.1.99.12.8.1.1.1", response.getVarBindAt(0).getName().toString());
		assertNotNull("Bad varBind.value at 0 of response", response.getVarBindAt(0).getValue());
		assertEquals("Bad varBind.value at 0 of response", SnmpNull.class, response.getVarBindAt(0).getValue().getClass());

	}

	/**
	 * Tests GET receiving with no getNext and 1 result found.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedGet(SnmpPduPacket, boolean)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedGetV1WithNoGetNextAnd1result () throws Exception {
		this.logger.info("Test GET receiving (SNMP v1) with no getNext and 1 result found...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1.1";
		final AttributeAccessor mibNode = mocksControl.createMock(AttributeAccessor.class);
		daemon.logger.setLevel(Level.ALL);

		// Scenario
		daemon.snmpMib.find(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(mibNode);
		mibNode.toString();
		mibNode.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode.getValue();
		EasyMock.expectLastCall().andReturn(null);
		mibNode.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		mocksControl.replay();

		// Test
		SnmpPduRequest pdu = new SnmpPduRequest();
		pdu.setRequestId(20);
		pdu.addVarBind(new SnmpVarBind(oid1));
		final SnmpPduRequest response = daemon.snmpReceivedGet(pdu, false);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", pdu.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrNoError, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 0, response.getErrorIndex());
		assertEquals("Bad length of response", 1, response.getLength());
		assertNotNull("Bad varBind at 0 of response", response.getVarBindAt(0));
		assertNotNull("Bad varBind.name at 0 of response", response.getVarBindAt(0).getName());
		assertEquals("Bad varBind.name at 0 of response", ".1.3.6.1.4.1.99.12.8.1.1.1", response.getVarBindAt(0).getName().toString());
		assertNotNull("Bad varBind.value at 0 of response", response.getVarBindAt(0).getValue());
		assertEquals("Bad varBind.value at 0 of response", SnmpInt32.class, response.getVarBindAt(0).getValue().getClass());
		assertEquals("Bad varBind.value at 0 of response", 0, ((SnmpInt32) (response.getVarBindAt(0).getValue())).getValue());

	}

	/**
	 * Tests GET receiving with no getNext and 2 results found.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedGet(SnmpPduPacket, boolean)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedGetV1WithNoGetNextAnd2results () throws Exception {
		this.logger.info("Test GET receiving (SNMP v1) with no getNext and 2 results found...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1.1";
		final String oid2 = "1.3.6.1.4.1.99.12.8.1.2.1";
		final AttributeAccessor mibNode1 = mocksControl.createMock(AttributeAccessor.class);
		final AttributeAccessor mibNode2 = mocksControl.createMock(AttributeAccessor.class);
		daemon.logger.setLevel(Level.OFF);

		// Scenario
		daemon.snmpMib.find(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(mibNode1);
		mibNode1.toString();
		mibNode1.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode1.getValue();
		EasyMock.expectLastCall().andReturn(null);
		mibNode1.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		daemon.snmpMib.find(EasyMock.eq(SnmpOid.newInstance(oid2)));
		EasyMock.expectLastCall().andReturn(mibNode2);
		mibNode2.toString();
		mibNode2.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode2.getValue();
		EasyMock.expectLastCall().andReturn(null);
		mibNode2.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		mocksControl.replay();

		// Test
		SnmpPduRequest pdu = new SnmpPduRequest();
		pdu.setRequestId(21);
		pdu.addVarBind(new SnmpVarBind(oid1));
		pdu.addVarBind(new SnmpVarBind(oid2));
		final SnmpPduRequest response = daemon.snmpReceivedGet(pdu, false);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", pdu.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrNoError, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 0, response.getErrorIndex());
		assertEquals("Bad length of response", 2, response.getLength());
		assertNotNull("Bad varBind at 0 of response", response.getVarBindAt(0));
		assertNotNull("Bad varBind.name at 0 of response", response.getVarBindAt(0).getName());
		assertEquals("Bad varBind.name at 0 of response", ".1.3.6.1.4.1.99.12.8.1.1.1", response.getVarBindAt(0).getName().toString());
		assertNotNull("Bad varBind.value at 0 of response", response.getVarBindAt(0).getValue());
		assertEquals("Bad varBind.value at 0 of response", SnmpInt32.class, response.getVarBindAt(0).getValue().getClass());
		assertEquals("Bad varBind.value at 0 of response", 0, ((SnmpInt32) (response.getVarBindAt(0).getValue())).getValue());
		assertNotNull("Bad varBind at 1 of response", response.getVarBindAt(1));
		assertNotNull("Bad varBind.name at 1 of response", response.getVarBindAt(1).getName());
		assertEquals("Bad varBind.name at 1 of response", ".1.3.6.1.4.1.99.12.8.1.2.1", response.getVarBindAt(1).getName().toString());
		assertNotNull("Bad varBind.value at 1 of response", response.getVarBindAt(1).getValue());
		assertEquals("Bad varBind.value at 1 of response", SnmpInt32.class, response.getVarBindAt(1).getValue().getClass());
		assertEquals("Bad varBind.value at 1 of response", 0, ((SnmpInt32) (response.getVarBindAt(1).getValue())).getValue());

	}

	/**
	 * Tests GET receiving with no result found.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedGet(SnmpPduPacket, boolean)}.
	 * </p>
	 */
	@Test
	public void testSnmpReceivedGetV2WithNoResultFound () {
		this.logger.info("Test GET receiving with no result found...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		daemon.logger.setLevel(Level.OFF);

		// Scenario
		daemon.snmpMib.next(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(null);
		mocksControl.replay();

		// Test
		SnmpPduBulk bulk = new SnmpPduBulk();
		bulk.setRequestId(22);
		bulk.addVarBind(new SnmpVarBind(oid1));
		bulk.setNonRepeaters(1);
		final SnmpPduRequest response = daemon.snmpReceivedGet(bulk, true);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", bulk.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrNoError, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 0, response.getErrorIndex());
		assertEquals("Bad length of response", 1, response.getLength());
		assertNotNull("Bad varBind at 0 of response", response.getVarBindAt(0));
		assertNotNull("Bad varBind.name at 0 of response", response.getVarBindAt(0).getName());
		assertEquals("Bad varBind.name at 0 of response", ".1.3.6.1.4.1.99.12.8.1.1", response.getVarBindAt(0).getName().toString());
		assertNotNull("Bad varBind.value at 0 of response", response.getVarBindAt(0).getValue());
		assertEquals("Bad varBind.value at 0 of response", SnmpEndOfMibView.class, response.getVarBindAt(0).getValue().getClass());

	}

	/**
	 * Tests GET receiving with result found (non repeaters).
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedGet(SnmpPduPacket, boolean)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedGetV2WithResultNonRepeatersFound () throws Exception {
		this.logger.info("Test GET receiving with result found (non repeaters)...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		final AttributeAccessor mibNode = mocksControl.createMock(AttributeAccessor.class);
		daemon.logger.setLevel(Level.OFF);

		// Scenario
		daemon.snmpMib.next(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(mibNode);
		mibNode.toString();
		mibNode.getOid();
		EasyMock.expectLastCall().andReturn(SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.1"));
		mibNode.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode.getValue();
		EasyMock.expectLastCall().andReturn(null);
		mibNode.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		mocksControl.replay();

		// Test
		SnmpPduBulk bulk = new SnmpPduBulk();
		bulk.setRequestId(23);
		bulk.addVarBind(new SnmpVarBind(oid1));
		bulk.setNonRepeaters(1);
		final SnmpPduRequest response = daemon.snmpReceivedGet(bulk, true);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", bulk.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrNoError, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 0, response.getErrorIndex());
		assertEquals("Bad length of response", 1, response.getLength());
		assertNotNull("Bad varBind at 0 of response", response.getVarBindAt(0));
		assertNotNull("Bad varBind.name at 0 of response", response.getVarBindAt(0).getName());
		assertEquals("Bad varBind.name at 0 of response", ".1.3.6.1.4.1.99.12.8.1.1.1", response.getVarBindAt(0).getName().toString());
		assertNotNull("Bad varBind.value at 0 of response", response.getVarBindAt(0).getValue());
		assertEquals("Bad varBind.value at 0 of response", SnmpInt32.class, response.getVarBindAt(0).getValue().getClass());
		assertEquals("Bad varBind.value at 0 of response", 0, ((SnmpInt32) (response.getVarBindAt(0).getValue())).getValue());

	}

	/**
	 * Tests GET receiving with result found (100 repeaters).
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedGet(SnmpPduPacket, boolean)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedGetV2WithResult100RepeatersFound () throws Exception {
		this.logger.info("Test GET receiving with result found (100 repeaters)...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		final AttributeAccessor mibNode1 = mocksControl.createMock(AttributeAccessor.class);
		final AttributeAccessor mibNode2 = mocksControl.createMock(AttributeAccessor.class);
		final SortedMap<SnmpOid, AttributeAccessor> mibNodeMap = new TreeMap<SnmpOid, AttributeAccessor>();
		mibNodeMap.put(SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.1"), mibNode1);
		mibNodeMap.put(SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.2"), mibNode2);
		daemon.logger.setLevel(Level.OFF);

		// Scenario
		daemon.snmpMib.nextSet(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(mibNodeMap);
		mibNode1.toString();
		mibNode1.getOid();
		EasyMock.expectLastCall().andReturn(SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.1"));
		mibNode1.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode1.getValue();
		EasyMock.expectLastCall().andReturn(null);
		mibNode1.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		mibNode2.toString();
		mibNode2.getOid();
		EasyMock.expectLastCall().andReturn(SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.2"));
		mibNode2.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode2.getValue();
		EasyMock.expectLastCall().andReturn(null);
		mibNode2.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		mocksControl.replay();

		// Test
		SnmpPduBulk bulk = new SnmpPduBulk();
		bulk.setRequestId(24);
		bulk.addVarBind(new SnmpVarBind(oid1));
		bulk.setNonRepeaters(0);
		bulk.setMaxRepititions(100);
		final SnmpPduRequest response = daemon.snmpReceivedGet(bulk, true);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", bulk.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrNoError, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 0, response.getErrorIndex());
		assertEquals("Bad length of response", 3, response.getLength());
		assertNotNull("Bad varBind at 0 of response", response.getVarBindAt(0));
		assertNotNull("Bad varBind.name at 0 of response", response.getVarBindAt(0).getName());
		assertEquals("Bad varBind.name at 0 of response", ".1.3.6.1.4.1.99.12.8.1.1.1", response.getVarBindAt(0).getName().toString());
		assertNotNull("Bad varBind.value at 0 of response", response.getVarBindAt(0).getValue());
		assertEquals("Bad varBind.value at 0 of response", SnmpInt32.class, response.getVarBindAt(0).getValue().getClass());
		assertEquals("Bad varBind.value at 0 of response", 0, ((SnmpInt32) (response.getVarBindAt(0).getValue())).getValue());
		assertNotNull("Bad varBind at 1 of response", response.getVarBindAt(1));
		assertNotNull("Bad varBind.name at 1 of response", response.getVarBindAt(1).getName());
		assertEquals("Bad varBind.name at 1 of response", ".1.3.6.1.4.1.99.12.8.1.1.2", response.getVarBindAt(1).getName().toString());
		assertNotNull("Bad varBind.value at 1 of response", response.getVarBindAt(1).getValue());
		assertEquals("Bad varBind.value at 1 of response", SnmpInt32.class, response.getVarBindAt(1).getValue().getClass());
		assertEquals("Bad varBind.value at 1 of response", 0, ((SnmpInt32) (response.getVarBindAt(1).getValue())).getValue());
		assertNotNull("Bad varBind at 2 of response", response.getVarBindAt(2));
		assertNotNull("Bad varBind.name at 2 of response", response.getVarBindAt(2).getName());
		assertEquals("Bad varBind.name at 2 of response", ".1.9", response.getVarBindAt(2).getName().toString());
		assertNotNull("Bad varBind.value at 2 of response", response.getVarBindAt(2).getValue());
		assertEquals("Bad varBind.value at 2 of response", SnmpEndOfMibView.class, response.getVarBindAt(2).getValue().getClass());

	}

	/**
	 * Tests GET receiving with result found (1 repeater).
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedGet(SnmpPduPacket, boolean)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedGetV2WithResult1RepeaterFound () throws Exception {
		this.logger.info("Test GET receiving with result found (1 repeater)...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		final AttributeAccessor mibNode1 = mocksControl.createMock(AttributeAccessor.class);
		final AttributeAccessor mibNode2 = mocksControl.createMock(AttributeAccessor.class);
		final SortedMap<SnmpOid, AttributeAccessor> mibNodeMap = new TreeMap<SnmpOid, AttributeAccessor>();
		mibNodeMap.put(SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.1"), mibNode1);
		mibNodeMap.put(SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.2"), mibNode2);
		daemon.logger.setLevel(Level.OFF);

		// Scenario
		daemon.snmpMib.nextSet(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(mibNodeMap);
		mibNode1.toString();
		mibNode1.getOid();
		EasyMock.expectLastCall().andReturn(SnmpOid.newInstance("1.3.6.1.4.1.99.12.8.1.1.1"));
		mibNode1.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode1.getValue();
		EasyMock.expectLastCall().andReturn(null);
		mibNode1.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		mocksControl.replay();

		// Test
		SnmpPduBulk bulk = new SnmpPduBulk();
		bulk.setRequestId(25);
		bulk.addVarBind(new SnmpVarBind(oid1));
		bulk.setNonRepeaters(0);
		bulk.setMaxRepititions(1);
		final SnmpPduRequest response = daemon.snmpReceivedGet(bulk, true);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", bulk.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrNoError, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 0, response.getErrorIndex());
		assertEquals("Bad length of response", 1, response.getLength());
		assertNotNull("Bad varBind at 0 of response", response.getVarBindAt(0));
		assertNotNull("Bad varBind.name at 0 of response", response.getVarBindAt(0).getName());
		assertEquals("Bad varBind.name at 0 of response", ".1.3.6.1.4.1.99.12.8.1.1.1", response.getVarBindAt(0).getName().toString());
		assertNotNull("Bad varBind.value at 0 of response", response.getVarBindAt(0).getValue());
		assertEquals("Bad varBind.value at 0 of response", SnmpInt32.class, response.getVarBindAt(0).getValue().getClass());
		assertEquals("Bad varBind.value at 0 of response", 0, ((SnmpInt32) (response.getVarBindAt(0).getValue())).getValue());

	}

	/**
	 * Tests SET receiving with ErrTooBig.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedSet(SnmpPduPacket)}.
	 * </p>
	 */
	@Test
	public void testSnmpReceivedSetWithErrTooBig () {
		this.logger.info("Test SET receiving with ErrTooBig...");
		final OpennmsSnmpDaemon daemon = new OpennmsSnmpDaemon(null, null);
		daemon.logger.setLevel(Level.ALL);
		final SnmpPduPacket pdu = new SnmpPduRequest();
		pdu.setRequestId(26);
		for (int i = 1; i < 22; i++) {
			pdu.addVarBind(new SnmpVarBind(new SnmpObjectId(".1.3.6.1.4.1.99.12.8.1." + i)));
		}
		final SnmpPduRequest response = daemon.snmpReceivedSet(pdu);

		// Checking
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", pdu.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrTooBig, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 0, response.getErrorIndex());
		assertEquals("Bad length of response", 0, response.getLength());

	}

	/**
	 * Tests SET receiving with a {@link SnmpException}.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedSet(SnmpPduPacket)}.
	 * </p>
	 */
	@Test
	public void testSnmpReceivedSetWithSnmpException () {
		this.logger.info("Test SET receiving with a SnmpException...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		final int errorStatus = -99;
		daemon.logger.setLevel(Level.ALL);

		// Scenario
		daemon.snmpMib.find(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andThrow(new SnmpException(errorStatus));
		mocksControl.replay();

		// Test
		SnmpPduRequest pdu = new SnmpPduRequest();
		pdu.setRequestId(27);
		pdu.addVarBind(new SnmpVarBind(oid1));
		final SnmpPduRequest response = daemon.snmpReceivedSet(pdu);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", pdu.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", -99, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 1, response.getErrorIndex());
		assertEquals("Bad length of response", 0, response.getLength());

	}

	/**
	 * Tests SET receiving with an other exception.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedSet(SnmpPduPacket)}.
	 * </p>
	 */
	@Test
	public void testSnmpReceivedSetWithOtherException () {
		this.logger.info("Test SET receiving with an other exception...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		daemon.logger.setLevel(Level.ALL);

		// Scenario
		daemon.snmpMib.find(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andThrow(new RuntimeException("MOCK EXCEPTION"));
		mocksControl.replay();

		// Test
		SnmpPduRequest pdu = new SnmpPduRequest();
		pdu.setRequestId(28);
		pdu.addVarBind(new SnmpVarBind(oid1));
		final SnmpPduRequest response = daemon.snmpReceivedSet(pdu);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", pdu.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrGenError, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 1, response.getErrorIndex());
		assertEquals("Bad length of response", 0, response.getLength());

	}

	/**
	 * Tests SET receiving with no result found.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedSet(SnmpPduPacket)}.
	 * </p>
	 */
	@Test
	public void testSnmpReceivedSetWithNoResultFound () {
		this.logger.info("Test SET receiving with no result found...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		daemon.logger.setLevel(Level.ALL);

		// Scenario
		daemon.snmpMib.find(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(null);
		mocksControl.replay();

		// Test
		SnmpPduRequest pdu = new SnmpPduRequest();
		pdu.setRequestId(29);
		pdu.addVarBind(new SnmpVarBind(oid1));
		final SnmpPduRequest response = daemon.snmpReceivedSet(pdu);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", pdu.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrNoSuchName, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 1, response.getErrorIndex());
		assertEquals("Bad length of response", 1, response.getLength());
		assertNotNull("Bad varBind at 0 of response", response.getVarBindAt(0));
		assertNotNull("Bad varBind.name at 0 of response", response.getVarBindAt(0).getName());
		assertEquals("Bad varBind.name at 0 of response", ".1.3.6.1.4.1.99.12.8.1.1", response.getVarBindAt(0).getName().toString());
		assertNotNull("Bad varBind.value at 0 of response", response.getVarBindAt(0).getValue());
		assertEquals("Bad varBind.value at 0 of response", SnmpNull.class, response.getVarBindAt(0).getValue().getClass());

	}

	/**
	 * Tests SET receiving with 1 result found.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedSet(SnmpPduPacket)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedSetWith1ResultFound () throws Exception {
		this.logger.info("Test SET receiving with 1 result found...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		final AttributeAccessor mibNode = mocksControl.createMock(AttributeAccessor.class);
		daemon.logger.setLevel(Level.ALL);

		// Scenario
		daemon.snmpMib.find(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(mibNode);
		mibNode.toString();
		mibNode.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode.isWritable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode.getJmxDataType();
		EasyMock.expectLastCall().andReturn(int.class);
		mibNode.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		mibNode.setValue(EasyMock.eq(new Integer(45)));
		mibNode.getOid();
		EasyMock.expectLastCall().andReturn(SnmpOid.newInstance(oid1));
		mibNode.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode.getValue();
		EasyMock.expectLastCall().andReturn(new Integer(45));
		mibNode.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		mibNode.toString();
		mocksControl.replay();

		// Test
		SnmpPduRequest pdu = new SnmpPduRequest();
		pdu.setRequestId(30);
		pdu.addVarBind(new SnmpVarBind(oid1, new SnmpInt32(45)));
		final SnmpPduRequest response = daemon.snmpReceivedSet(pdu);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", pdu.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrNoError, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 0, response.getErrorIndex());
		assertEquals("Bad length of response", 1, response.getLength());
		assertNotNull("Bad varBind at 0 of response", response.getVarBindAt(0));
		assertNotNull("Bad varBind.name at 0 of response", response.getVarBindAt(0).getName());
		assertEquals("Bad varBind.name at 0 of response", ".1.3.6.1.4.1.99.12.8.1.1", response.getVarBindAt(0).getName().toString());
		assertNotNull("Bad varBind.value at 0 of response", response.getVarBindAt(0).getValue());
		assertEquals("Bad varBind.value at 0 of response", SnmpInt32.class, response.getVarBindAt(0).getValue().getClass());
		assertEquals("Bad varBind.value at 0 of response", 45, ((SnmpInt32) (response.getVarBindAt(0).getValue())).getValue());

	}

	/**
	 * Tests SET receiving with 2 results found.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#snmpReceivedSet(SnmpPduPacket)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedSetWith2ResultsFound () throws Exception {
		this.logger.info("Test SET receiving with 2 results found...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final OpennmsSnmpDaemonToTest daemon = new OpennmsSnmpDaemonToTest(mocksControl);
		final String oid1 = "1.3.6.1.4.1.99.12.8.1.1";
		final String oid2 = "1.3.6.1.4.1.99.12.8.1.2";
		final AttributeAccessor mibNode1 = mocksControl.createMock(AttributeAccessor.class);
		final AttributeAccessor mibNode2 = mocksControl.createMock(AttributeAccessor.class);
		daemon.logger.setLevel(Level.OFF);

		// Scenario
		daemon.snmpMib.find(EasyMock.eq(SnmpOid.newInstance(oid1)));
		EasyMock.expectLastCall().andReturn(mibNode1);
		mibNode1.toString();
		mibNode1.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode1.isWritable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode1.getJmxDataType();
		EasyMock.expectLastCall().andReturn(int.class);
		mibNode1.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		mibNode1.setValue(EasyMock.eq(new Integer(45)));
		mibNode1.getOid();
		EasyMock.expectLastCall().andReturn(SnmpOid.newInstance(oid1));
		mibNode1.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode1.getValue();
		EasyMock.expectLastCall().andReturn(new Integer(45));
		mibNode1.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		mibNode1.toString();
		daemon.snmpMib.find(EasyMock.eq(SnmpOid.newInstance(oid2)));
		EasyMock.expectLastCall().andReturn(mibNode2);
		mibNode2.toString();
		mibNode2.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode2.isWritable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode2.getJmxDataType();
		EasyMock.expectLastCall().andReturn(int.class);
		mibNode2.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		mibNode2.setValue(EasyMock.eq(new Integer(52)));
		mibNode2.getOid();
		EasyMock.expectLastCall().andReturn(SnmpOid.newInstance(oid2));
		mibNode2.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode2.getValue();
		EasyMock.expectLastCall().andReturn(new Integer(52));
		mibNode2.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		mibNode2.toString();
		mocksControl.replay();

		// Test
		SnmpPduRequest pdu = new SnmpPduRequest();
		pdu.setRequestId(31);
		pdu.addVarBind(new SnmpVarBind(oid1, new SnmpInt32(45)));
		pdu.addVarBind(new SnmpVarBind(oid2, new SnmpInt32(52)));
		final SnmpPduRequest response = daemon.snmpReceivedSet(pdu);

		// Checking
		mocksControl.verify();
		assertNotNull("NULL response", response);
		assertEquals("Bad type of response", SnmpPduRequest.class, response.getClass());
		assertEquals("Bad commande of response", SnmpPduPacket.RESPONSE, response.getCommand());
		assertEquals("Bad requestId of response", pdu.getRequestId(), response.getRequestId());
		assertEquals("Bad errorStatus of response", SnmpPduPacket.ErrNoError, response.getErrorStatus());
		assertEquals("Bad errorIndex of response", 0, response.getErrorIndex());
		assertEquals("Bad length of response", 2, response.getLength());
		assertNotNull("Bad varBind at 0 of response", response.getVarBindAt(0));
		assertNotNull("Bad varBind.name at 0 of response", response.getVarBindAt(0).getName());
		assertEquals("Bad varBind.name at 0 of response", ".1.3.6.1.4.1.99.12.8.1.1", response.getVarBindAt(0).getName().toString());
		assertNotNull("Bad varBind.value at 0 of response", response.getVarBindAt(0).getValue());
		assertEquals("Bad varBind.value at 0 of response", SnmpInt32.class, response.getVarBindAt(0).getValue().getClass());
		assertEquals("Bad varBind.value at 0 of response", 45, ((SnmpInt32) (response.getVarBindAt(0).getValue())).getValue());
		assertNotNull("Bad varBind at 1 of response", response.getVarBindAt(1));
		assertNotNull("Bad varBind.name at 1 of response", response.getVarBindAt(1).getName());
		assertEquals("Bad varBind.name at 1 of response", ".1.3.6.1.4.1.99.12.8.1.2", response.getVarBindAt(1).getName().toString());
		assertNotNull("Bad varBind.value at 1 of response", response.getVarBindAt(1).getValue());
		assertEquals("Bad varBind.value at 1 of response", SnmpInt32.class, response.getVarBindAt(1).getValue().getClass());
		assertEquals("Bad varBind.value at 1 of response", 52, ((SnmpInt32) (response.getVarBindAt(1).getValue())).getValue());

	}

	/**
	 * Tests a {@link SnmpSyntax} creation with a not readable attribute.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#newSnmpValue(AttributeAccessor)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSnmpValueWithNotReadable () throws Exception {
		this.logger.info("Test a SnmpSyntax creation with a not readable attribute...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final AttributeAccessor mibNode = mocksControl.createMock(AttributeAccessor.class);

		// Scenario
		mibNode.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.FALSE);
		mocksControl.replay();

		// Test
		try {
			OpennmsSnmpDaemon.newSnmpValue(mibNode);
			fail("None SnmpException has occurred");
		}
		catch (final SnmpException e) {
			mocksControl.verify();
			assertEquals("Bad errorStatus of SnmpException", SnmpPduPacket.ErrNoAccess, e.getErrorStatus());
			assertEquals("Bad message of SnmpException", "error 6 (MBean attribute is not readable)", e.getMessage());
			assertEquals("Bad loggerLevel of SnmpException", Level.ERROR, e.getLoggerLevel());
			assertNull("Bad cause of SnmpException", e.getCause());
		}

	}

	/**
	 * Tests a {@link SnmpSyntax} creation with an exception when getValue.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#newSnmpValue(AttributeAccessor)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSnmpValueWithGetValueException () throws Exception {
		this.logger.info("Test a SnmpSyntax creation with an exception when getValue...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final AttributeAccessor mibNode = mocksControl.createMock(AttributeAccessor.class);

		// Scenario
		mibNode.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode.getValue();
		EasyMock.expectLastCall().andThrow(new Exception("MOCK EXCEPTION"));
		mocksControl.replay();

		// Test
		try {
			OpennmsSnmpDaemon.newSnmpValue(mibNode);
			fail("None Exception has occurred");
		}
		catch (final SnmpException e) {
			mocksControl.verify();
			assertEquals("Bad errorStatus of SnmpException", SnmpPduPacket.ErrNoAccess, e.getErrorStatus());
			assertEquals("Bad message of SnmpException", "error 6 (Impossible to read the attribute due to error)", e.getMessage());
			assertEquals("Bad loggerLevel of SnmpException", Level.ERROR, e.getLoggerLevel());
			assertNotNull("Bad cause of SnmpException", e.getCause());
			assertEquals("Bad cause message of SnmpException", "MOCK EXCEPTION", e.getCause().getMessage());
		}

	}

	/**
	 * Tests an update of a new value with a not readable attribute.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#setValue(AttributeAccessor, SnmpSyntax)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetValueWithNotReadable () throws Exception {
		this.logger.info("Test an update of a new value with a not readable attribute...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final AttributeAccessor mibNode = mocksControl.createMock(AttributeAccessor.class);

		// Scenario
		mibNode.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.FALSE);
		mocksControl.replay();

		// Test
		try {
			OpennmsSnmpDaemon.setValue(mibNode, new SnmpInt32(45));
			fail("None SnmpException has occurred");
		}
		catch (final SnmpException e) {
			mocksControl.verify();
			assertEquals("Bad errorStatus of SnmpException", SnmpPduPacket.ErrNoAccess, e.getErrorStatus());
			assertEquals("Bad message of SnmpException", "error 6 (MBean attribute is not readable)", e.getMessage());
			assertEquals("Bad loggerLevel of SnmpException", Level.ERROR, e.getLoggerLevel());
			assertNull("Bad cause of SnmpException", e.getCause());
		}

	}

	/**
	 * Tests an update of a new value with a not writable attribute.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#setValue(AttributeAccessor, SnmpSyntax)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetValueWithNotWritable () throws Exception {
		this.logger.info("Test an update of a new value with a not writable attribute...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final AttributeAccessor mibNode = mocksControl.createMock(AttributeAccessor.class);

		// Scenario
		mibNode.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode.isWritable();
		EasyMock.expectLastCall().andReturn(Boolean.FALSE);
		mocksControl.replay();

		// Test
		try {
			OpennmsSnmpDaemon.setValue(mibNode, new SnmpInt32(45));
			fail("None SnmpException has occurred");
		}
		catch (final SnmpException e) {
			mocksControl.verify();
			assertEquals("Bad errorStatus of SnmpException", SnmpPduPacket.ErrNotWritable, e.getErrorStatus());
			assertEquals("Bad message of SnmpException", "error 17 (MBean attribute is not writable)", e.getMessage());
			assertEquals("Bad loggerLevel of SnmpException", Level.ERROR, e.getLoggerLevel());
			assertNull("Bad cause of SnmpException", e.getCause());
		}

	}

	/**
	 * Tests an update of a new value with an exception when getValue.
	 * <p>
	 * Test method for {@link OpennmsSnmpDaemon#setValue(AttributeAccessor, SnmpSyntax)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSetValueWithSetValueException () throws Exception {
		this.logger.info("Test an update of a new value with an exception when getValue...");
		final IMocksControl mocksControl = EasyMock.createStrictControl();
		final AttributeAccessor mibNode = mocksControl.createMock(AttributeAccessor.class);

		// Scenario
		mibNode.isReadable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode.isWritable();
		EasyMock.expectLastCall().andReturn(Boolean.TRUE);
		mibNode.getJmxDataType();
		EasyMock.expectLastCall().andReturn(int.class);
		mibNode.getSnmpDataType();
		EasyMock.expectLastCall().andReturn(SnmpDataType.integer32);
		mibNode.setValue(EasyMock.eq(new Integer(45)));
		EasyMock.expectLastCall().andThrow(new Exception("MOCK EXCEPTION"));
		mocksControl.replay();

		// Test
		try {
			OpennmsSnmpDaemon.setValue(mibNode, new SnmpInt32(45));
			fail("None Exception has occurred");
		}
		catch (final SnmpException e) {
			mocksControl.verify();
			assertEquals("Bad errorStatus of SnmpException", SnmpPduPacket.ErrNotWritable, e.getErrorStatus());
			assertEquals("Bad message of SnmpException", "error 17 (Impossible to write in the attribute due to error)", e.getMessage());
			assertEquals("Bad loggerLevel of SnmpException", Level.ERROR, e.getLoggerLevel());
			assertNotNull("Bad cause of SnmpException", e.getCause());
			assertEquals("Bad cause message of SnmpException", "MOCK EXCEPTION", e.getCause().getMessage());
		}

	}

	/**
	 * Tests method for {@link OpennmsSnmpDaemon#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests of toString...");
		OpennmsSupport daemon = new OpennmsSnmpDaemon(null, null);
		assertEquals("toString is bad", "SnmpDaemon:opennms[null]", daemon.toString());
		daemon = new OpennmsSnmpDaemon(new SnmpConfigurationMock(), null);
		assertEquals("toString is bad", "SnmpDaemon:opennms[127.0.0.1:161]", daemon.toString());
	}

}