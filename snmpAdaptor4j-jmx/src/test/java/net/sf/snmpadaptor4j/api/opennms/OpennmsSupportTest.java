package net.sf.snmpadaptor4j.api.opennms;

import static org.junit.Assert.*;
import java.math.BigInteger;
import java.net.InetAddress;
import net.sf.snmpadaptor4j.api.SnmpException;
import net.sf.snmpadaptor4j.api.opennms.OpennmsSupport;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.opennms.protocols.snmp.SnmpCounter32;
import org.opennms.protocols.snmp.SnmpCounter64;
import org.opennms.protocols.snmp.SnmpGauge32;
import org.opennms.protocols.snmp.SnmpIPAddress;
import org.opennms.protocols.snmp.SnmpInt32;
import org.opennms.protocols.snmp.SnmpNull;
import org.opennms.protocols.snmp.SnmpObjectId;
import org.opennms.protocols.snmp.SnmpOctetString;
import org.opennms.protocols.snmp.SnmpOpaque;
import org.opennms.protocols.snmp.SnmpPduPacket;
import org.opennms.protocols.snmp.SnmpSyntax;
import org.opennms.protocols.snmp.SnmpTimeTicks;
import org.opennms.protocols.snmp.SnmpUInt32;

/**
 * Test class for {@link net.sf.snmpadaptor4j.api.opennms.OpennmsSupport}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class OpennmsSupportTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(OpennmsSupportTest.class);

	/**
	 * Tests a {@link SnmpSyntax} creation with <code>integer32</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newSnmpValue(SnmpDataType, Object)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSnmpValueWithInteger32 () throws Exception {
		SnmpInt32 snmpValue = testNewSnmpValue(SnmpInt32.class, "NULL=>integer32", null, SnmpDataType.integer32);
		assertEquals("Bad value of result", 0, snmpValue.getValue());
		snmpValue = testNewSnmpValue(SnmpInt32.class, "Short=>integer32", new Short((short) 103), SnmpDataType.integer32);
		assertEquals("Bad value of result", 103, snmpValue.getValue());
		snmpValue = testNewSnmpValue(SnmpInt32.class, "Byte=>integer32", new Byte((byte) 102), SnmpDataType.integer32);
		assertEquals("Bad value of result", 102, snmpValue.getValue());
		snmpValue = testNewSnmpValue(SnmpInt32.class, "Integer=>integer32", new Integer(101), SnmpDataType.integer32);
		assertEquals("Bad value of result", 101, snmpValue.getValue());
		snmpValue = testNewSnmpValue(SnmpInt32.class, "Boolean(false)=>integer32", Boolean.FALSE, SnmpDataType.integer32);
		assertEquals("Bad value of result", 2, snmpValue.getValue());
		snmpValue = testNewSnmpValue(SnmpInt32.class, "Boolean(true)=>integer32", Boolean.TRUE, SnmpDataType.integer32);
		assertEquals("Bad value of result", 1, snmpValue.getValue());
		String message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Long=>integer32", new Long(104), SnmpDataType.integer32);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Long is inconsistent with integer32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "BigInteger=>integer32", new BigInteger("105"), SnmpDataType.integer32);
		assertEquals("Bad message of SnmpException", "error 7 (class java.math.BigInteger is inconsistent with integer32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "String=>integer32", "106", SnmpDataType.integer32);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.String is inconsistent with integer32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "byte[]=>integer32", new byte[] { 107, 108 }, SnmpDataType.integer32);
		assertEquals("Bad message of SnmpException", "error 7 (class [B is inconsistent with integer32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "InetAddress=>integer32", InetAddress.getByName("127.0.0.1"), SnmpDataType.integer32);
		assertEquals("Bad message of SnmpException", "error 7 (class java.net.Inet4Address is inconsistent with integer32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "SnmpOid=>integer32", SnmpOid.newInstance(".1"), SnmpDataType.integer32);
		assertEquals("Bad message of SnmpException", "error 7 (class net.sf.snmpadaptor4j.object.SnmpOid is inconsistent with integer32)", message);
	}

	/**
	 * Tests a {@link SnmpSyntax} creation with <code>unsigned32</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newSnmpValue(SnmpDataType, Object)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSnmpValueWithUnsigned32 () throws Exception {
		SnmpUInt32 snmpValue = testNewSnmpValue(SnmpUInt32.class, "NULL=>unsigned32", null, SnmpDataType.unsigned32);
		assertEquals("Bad value of result", 0, snmpValue.getValue());
		snmpValue = testNewSnmpValue(SnmpUInt32.class, "Short=>unsigned32", new Short((short) 103), SnmpDataType.unsigned32);
		assertEquals("Bad value of result", 103, snmpValue.getValue());
		snmpValue = testNewSnmpValue(SnmpUInt32.class, "Byte=>unsigned32", new Byte((byte) 102), SnmpDataType.unsigned32);
		assertEquals("Bad value of result", 102, snmpValue.getValue());
		snmpValue = testNewSnmpValue(SnmpUInt32.class, "Integer=>unsigned32", new Integer(101), SnmpDataType.unsigned32);
		assertEquals("Bad value of result", 101, snmpValue.getValue());
		String message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Boolean(false)=>integer32", Boolean.FALSE, SnmpDataType.unsigned32);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Boolean is inconsistent with unsigned32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Boolean(true)=>integer32", Boolean.TRUE, SnmpDataType.unsigned32);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Boolean is inconsistent with unsigned32)", message);
		snmpValue = testNewSnmpValue(SnmpUInt32.class, "Long=>unsigned32", new Long(104), SnmpDataType.unsigned32);
		assertEquals("Bad value of result", 104, snmpValue.getValue());
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Long=>unsigned32", new Long(-1), SnmpDataType.unsigned32);
		assertEquals("Bad message of SnmpException", "error 7 (the value must be between 0 and 4294967295)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Long=>unsigned32", new Long(4294967296L), SnmpDataType.unsigned32);
		assertEquals("Bad message of SnmpException", "error 7 (the value must be between 0 and 4294967295)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "BigInteger=>unsigned32", new BigInteger("105"), SnmpDataType.unsigned32);
		assertEquals("Bad message of SnmpException", "error 7 (class java.math.BigInteger is inconsistent with unsigned32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "String=>unsigned32", "106", SnmpDataType.unsigned32);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.String is inconsistent with unsigned32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "byte[]=>unsigned32", new byte[] { 107, 108 }, SnmpDataType.unsigned32);
		assertEquals("Bad message of SnmpException", "error 7 (class [B is inconsistent with unsigned32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "InetAddress=>unsigned32", InetAddress.getByName("127.0.0.1"), SnmpDataType.unsigned32);
		assertEquals("Bad message of SnmpException", "error 7 (class java.net.Inet4Address is inconsistent with unsigned32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "SnmpOid=>unsigned32", SnmpOid.newInstance(".1"), SnmpDataType.unsigned32);
		assertEquals("Bad message of SnmpException", "error 7 (class net.sf.snmpadaptor4j.object.SnmpOid is inconsistent with unsigned32)", message);
	}

	/**
	 * Tests a {@link SnmpSyntax} creation with <code>gauge32</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newSnmpValue(SnmpDataType, Object)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSnmpValueWithGauge32 () throws Exception {
		SnmpGauge32 snmpValue = testNewSnmpValue(SnmpGauge32.class, "NULL=>gauge32", null, SnmpDataType.gauge32);
		assertEquals("Bad value of result", 0, snmpValue.getValue());
		snmpValue = testNewSnmpValue(SnmpGauge32.class, "Short=>gauge32", new Short((short) 103), SnmpDataType.gauge32);
		assertEquals("Bad value of result", 103, snmpValue.getValue());
		snmpValue = testNewSnmpValue(SnmpGauge32.class, "Byte=>gauge32", new Byte((byte) 102), SnmpDataType.gauge32);
		assertEquals("Bad value of result", 102, snmpValue.getValue());
		snmpValue = testNewSnmpValue(SnmpGauge32.class, "Integer=>gauge32", new Integer(101), SnmpDataType.gauge32);
		assertEquals("Bad value of result", 101, snmpValue.getValue());
		String message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Boolean(false)=>gauge32", Boolean.FALSE, SnmpDataType.gauge32);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Boolean is inconsistent with unsigned32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Boolean(true)=>gauge32", Boolean.TRUE, SnmpDataType.gauge32);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Boolean is inconsistent with unsigned32)", message);
		snmpValue = testNewSnmpValue(SnmpGauge32.class, "Long=>gauge32", new Long(104), SnmpDataType.gauge32);
		assertEquals("Bad value of result", 104, snmpValue.getValue());
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Long=>gauge32", new Long(-1), SnmpDataType.gauge32);
		assertEquals("Bad message of SnmpException", "error 7 (the value must be between 0 and 4294967295)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Long=>gauge32", new Long(4294967296L), SnmpDataType.gauge32);
		assertEquals("Bad message of SnmpException", "error 7 (the value must be between 0 and 4294967295)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "BigInteger=>gauge32", new BigInteger("105"), SnmpDataType.gauge32);
		assertEquals("Bad message of SnmpException", "error 7 (class java.math.BigInteger is inconsistent with unsigned32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "String=>gauge32", "106", SnmpDataType.gauge32);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.String is inconsistent with unsigned32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "byte[]=>gauge32", new byte[] { 107, 108 }, SnmpDataType.gauge32);
		assertEquals("Bad message of SnmpException", "error 7 (class [B is inconsistent with unsigned32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "InetAddress=>gauge32", InetAddress.getByName("127.0.0.1"), SnmpDataType.gauge32);
		assertEquals("Bad message of SnmpException", "error 7 (class java.net.Inet4Address is inconsistent with unsigned32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "SnmpOid=>gauge32", SnmpOid.newInstance(".1"), SnmpDataType.gauge32);
		assertEquals("Bad message of SnmpException", "error 7 (class net.sf.snmpadaptor4j.object.SnmpOid is inconsistent with unsigned32)", message);
	}

	/**
	 * Tests a {@link SnmpSyntax} creation with <code>counter32</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newSnmpValue(SnmpDataType, Object)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSnmpValueWithCounter32 () throws Exception {
		SnmpCounter32 snmpValue = testNewSnmpValue(SnmpCounter32.class, "NULL=>counter32", null, SnmpDataType.counter32);
		assertEquals("Bad value of result", 0, snmpValue.getValue());
		snmpValue = testNewSnmpValue(SnmpCounter32.class, "Short=>counter32", new Short((short) 103), SnmpDataType.counter32);
		assertEquals("Bad value of result", 103, snmpValue.getValue());
		snmpValue = testNewSnmpValue(SnmpCounter32.class, "Byte=>counter32", new Byte((byte) 102), SnmpDataType.counter32);
		assertEquals("Bad value of result", 102, snmpValue.getValue());
		snmpValue = testNewSnmpValue(SnmpCounter32.class, "Integer=>counter32", new Integer(101), SnmpDataType.counter32);
		assertEquals("Bad value of result", 101, snmpValue.getValue());
		String message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Boolean(false)=>counter32", Boolean.FALSE, SnmpDataType.counter32);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Boolean is inconsistent with unsigned32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Boolean(true)=>counter32", Boolean.TRUE, SnmpDataType.counter32);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Boolean is inconsistent with unsigned32)", message);
		snmpValue = testNewSnmpValue(SnmpCounter32.class, "Long=>counter32", new Long(104), SnmpDataType.counter32);
		assertEquals("Bad value of result", 104, snmpValue.getValue());
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Long=>counter32", new Long(-1), SnmpDataType.counter32);
		assertEquals("Bad message of SnmpException", "error 7 (the value must be between 0 and 4294967295)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Long=>counter32", new Long(4294967296L), SnmpDataType.counter32);
		assertEquals("Bad message of SnmpException", "error 7 (the value must be between 0 and 4294967295)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "BigInteger=>counter32", new BigInteger("105"), SnmpDataType.counter32);
		assertEquals("Bad message of SnmpException", "error 7 (class java.math.BigInteger is inconsistent with unsigned32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "String=>counter32", "106", SnmpDataType.counter32);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.String is inconsistent with unsigned32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "byte[]=>counter32", new byte[] { 107, 108 }, SnmpDataType.counter32);
		assertEquals("Bad message of SnmpException", "error 7 (class [B is inconsistent with unsigned32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "InetAddress=>counter32", InetAddress.getByName("127.0.0.1"), SnmpDataType.counter32);
		assertEquals("Bad message of SnmpException", "error 7 (class java.net.Inet4Address is inconsistent with unsigned32)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "SnmpOid=>counter32", SnmpOid.newInstance(".1"), SnmpDataType.counter32);
		assertEquals("Bad message of SnmpException", "error 7 (class net.sf.snmpadaptor4j.object.SnmpOid is inconsistent with unsigned32)", message);
	}

	/**
	 * Tests a {@link SnmpSyntax} creation with <code>counter64</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newSnmpValue(SnmpDataType, Object)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSnmpValueWithCounter64 () throws Exception {
		SnmpCounter64 snmpValue = testNewSnmpValue(SnmpCounter64.class, "NULL=>counter64", null, SnmpDataType.counter64);
		assertEquals("Bad value of result", new BigInteger("0"), snmpValue.getValue());
		String message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Short=>counter64", new Short((short) 103), SnmpDataType.counter64);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Short is inconsistent with unsigned64)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Byte=>counter64", new Byte((byte) 102), SnmpDataType.counter64);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Byte is inconsistent with unsigned64)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Integer=>counter64", new Integer(101), SnmpDataType.counter64);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Integer is inconsistent with unsigned64)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Boolean(false)=>counter64", Boolean.FALSE, SnmpDataType.counter64);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Boolean is inconsistent with unsigned64)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Boolean(true)=>counter64", Boolean.TRUE, SnmpDataType.counter64);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Boolean is inconsistent with unsigned64)", message);
		snmpValue = testNewSnmpValue(SnmpCounter64.class, "Long=>counter64", new Long(104), SnmpDataType.counter64);
		assertEquals("Bad value of result", new BigInteger("104"), snmpValue.getValue());
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Long=>counter64", new Long(-1), SnmpDataType.counter64);
		assertEquals("Bad message of SnmpException", "error 7 (the value must be between 0 and 18446744073709551615)", message);
		snmpValue = testNewSnmpValue(SnmpCounter64.class, "BigInteger=>counter64", new BigInteger("105"), SnmpDataType.counter64);
		assertEquals("Bad value of result", new BigInteger("105"), snmpValue.getValue());
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "BigInteger=>counter64", new BigInteger("18446744073709551616"), SnmpDataType.counter64);
		assertEquals("Bad message of SnmpException", "error 7 (the value must be between 0 and 18446744073709551615)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "String=>counter64", "106", SnmpDataType.counter64);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.String is inconsistent with unsigned64)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "byte[]=>counter64", new byte[] { 107, 108 }, SnmpDataType.counter64);
		assertEquals("Bad message of SnmpException", "error 7 (class [B is inconsistent with unsigned64)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "InetAddress=>counter64", InetAddress.getByName("127.0.0.1"), SnmpDataType.counter64);
		assertEquals("Bad message of SnmpException", "error 7 (class java.net.Inet4Address is inconsistent with unsigned64)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "SnmpOid=>counter64", SnmpOid.newInstance(".1"), SnmpDataType.counter64);
		assertEquals("Bad message of SnmpException", "error 7 (class net.sf.snmpadaptor4j.object.SnmpOid is inconsistent with unsigned64)", message);
	}

	/**
	 * Tests a {@link SnmpSyntax} creation with <code>timeTicks</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newSnmpValue(SnmpDataType, Object)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSnmpValueWithTimeTicks () throws Exception {
		SnmpTimeTicks snmpValue = testNewSnmpValue(SnmpTimeTicks.class, "NULL=>timeTicks", null, SnmpDataType.timeTicks);
		assertEquals("Bad value of result", 0, snmpValue.getValue());
		String message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Short=>timeTicks", new Short((short) 103), SnmpDataType.timeTicks);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Short is inconsistent with timeTicks)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Byte=>timeTicks", new Byte((byte) 102), SnmpDataType.timeTicks);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Byte is inconsistent with timeTicks)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Integer=>timeTicks", new Integer(101), SnmpDataType.timeTicks);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Integer is inconsistent with timeTicks)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Boolean(false)=>timeTicks", Boolean.FALSE, SnmpDataType.timeTicks);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Boolean is inconsistent with timeTicks)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Boolean(true)=>timeTicks", Boolean.TRUE, SnmpDataType.timeTicks);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Boolean is inconsistent with timeTicks)", message);
		snmpValue = testNewSnmpValue(SnmpTimeTicks.class, "Long=>timeTicks", new Long(2500), SnmpDataType.timeTicks);
		assertEquals("Bad value of result", 250, snmpValue.getValue());
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Long=>timeTicks", new Long(-20), SnmpDataType.timeTicks);
		assertEquals("Bad message of SnmpException", "error 7 (the value must be between 0 and 4294967295)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Long=>timeTicks", new Long(42949672960L), SnmpDataType.timeTicks);
		assertEquals("Bad message of SnmpException", "error 7 (the value must be between 0 and 4294967295)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "BigInteger=>timeTicks", new BigInteger("105"), SnmpDataType.timeTicks);
		assertEquals("Bad message of SnmpException", "error 7 (class java.math.BigInteger is inconsistent with timeTicks)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "String=>timeTicks", "106", SnmpDataType.timeTicks);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.String is inconsistent with timeTicks)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "byte[]=>timeTicks", new byte[] { 107, 108 }, SnmpDataType.timeTicks);
		assertEquals("Bad message of SnmpException", "error 7 (class [B is inconsistent with timeTicks)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "InetAddress=>timeTicks", InetAddress.getByName("127.0.0.1"), SnmpDataType.timeTicks);
		assertEquals("Bad message of SnmpException", "error 7 (class java.net.Inet4Address is inconsistent with timeTicks)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "SnmpOid=>timeTicks", SnmpOid.newInstance(".1"), SnmpDataType.timeTicks);
		assertEquals("Bad message of SnmpException", "error 7 (class net.sf.snmpadaptor4j.object.SnmpOid is inconsistent with timeTicks)", message);
	}

	/**
	 * Tests a {@link SnmpSyntax} creation with <code>octetString</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newSnmpValue(SnmpDataType, Object)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSnmpValueWithOctetString () throws Exception {
		SnmpOctetString snmpValue = testNewSnmpValue(SnmpOctetString.class, "NULL=>octetString", null, SnmpDataType.octetString);
		assertArrayEquals("Bad value of result", "".getBytes(), snmpValue.getString());
		snmpValue = testNewSnmpValue(SnmpOctetString.class, "Short=>octetString", new Short((short) 103), SnmpDataType.octetString);
		assertEquals("Bad value of result", "103", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOctetString.class, "Byte=>octetString", new Byte((byte) 102), SnmpDataType.octetString);
		assertEquals("Bad value of result", "102", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOctetString.class, "Integer=>octetString", new Integer(101), SnmpDataType.octetString);
		assertEquals("Bad value of result", "101", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOctetString.class, "Boolean(false)=>octetString", Boolean.FALSE, SnmpDataType.octetString);
		assertEquals("Bad value of result", "false", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOctetString.class, "Boolean(true)=>octetString", Boolean.TRUE, SnmpDataType.octetString);
		assertEquals("Bad value of result", "true", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOctetString.class, "Long=>octetString", new Long(104), SnmpDataType.octetString);
		assertEquals("Bad value of result", "104", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOctetString.class, "BigInteger=>octetString", new BigInteger("105"), SnmpDataType.octetString);
		assertEquals("Bad value of result", "105", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOctetString.class, "String=>octetString", "106", SnmpDataType.octetString);
		assertEquals("Bad value of result", "106", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOctetString.class, "byte[]=>octetString", "107".getBytes(), SnmpDataType.octetString);
		assertEquals("Bad value of result", "107", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOctetString.class, "InetAddress=>octetString", InetAddress.getByName("127.0.0.1"), SnmpDataType.octetString);
		assertEquals("Bad value of result", "/127.0.0.1", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOctetString.class, "SnmpOid=>octetString", SnmpOid.newInstance(".1"), SnmpDataType.octetString);
		assertEquals("Bad value of result", ".1", new String(snmpValue.getString()));
	}

	/**
	 * Tests a {@link SnmpSyntax} creation with <code>ipAddress</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newSnmpValue(SnmpDataType, Object)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSnmpValueWithIpAddress () throws Exception {
		SnmpIPAddress snmpValue = testNewSnmpValue(SnmpIPAddress.class, "NULL=>ipAddress", null, SnmpDataType.ipAddress);
		assertEquals("Bad value of result", InetAddress.getByName("0.0.0.0"), snmpValue.convertToIpAddress());
		String message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Short=>ipAddress", new Short((short) 103), SnmpDataType.ipAddress);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Short is inconsistent with ipAddress)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Byte=>ipAddress", new Byte((byte) 102), SnmpDataType.ipAddress);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Byte is inconsistent with ipAddress)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Integer=>ipAddress", new Integer(101), SnmpDataType.ipAddress);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Integer is inconsistent with ipAddress)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Boolean(false)=>ipAddress", Boolean.FALSE, SnmpDataType.ipAddress);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Boolean is inconsistent with ipAddress)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Boolean(true)=>ipAddress", Boolean.TRUE, SnmpDataType.ipAddress);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Boolean is inconsistent with ipAddress)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Long=>ipAddress", new Long(104), SnmpDataType.ipAddress);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Long is inconsistent with ipAddress)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "BigInteger=>ipAddress", new BigInteger("105"), SnmpDataType.ipAddress);
		assertEquals("Bad message of SnmpException", "error 7 (class java.math.BigInteger is inconsistent with ipAddress)", message);
		snmpValue = testNewSnmpValue(SnmpIPAddress.class, "String=>ipAddress", "127.0.0.1", SnmpDataType.ipAddress);
		assertEquals("Bad value of result", InetAddress.getByName("127.0.0.1"), snmpValue.convertToIpAddress());
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongValue, "String=>ipAddress", "XXX", SnmpDataType.ipAddress);
		assertEquals("Bad message of SnmpException", "error 10 (IP address is malformed)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "byte[]=>ipAddress", new byte[] { 107, 108 }, SnmpDataType.ipAddress);
		assertEquals("Bad message of SnmpException", "error 7 (class [B is inconsistent with ipAddress)", message);
		snmpValue = testNewSnmpValue(SnmpIPAddress.class, "InetAddress=>ipAddress", InetAddress.getByName("127.0.0.1"), SnmpDataType.ipAddress);
		assertEquals("Bad value of result", InetAddress.getByName("127.0.0.1"), snmpValue.convertToIpAddress());
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "SnmpOid=>ipAddress", SnmpOid.newInstance(".1"), SnmpDataType.ipAddress);
		assertEquals("Bad message of SnmpException", "error 7 (class net.sf.snmpadaptor4j.object.SnmpOid is inconsistent with ipAddress)", message);
	}

	/**
	 * Tests a {@link SnmpSyntax} creation with <code>objectIdentifier</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newSnmpValue(SnmpDataType, Object)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSnmpValueWithObjectIdentifier () throws Exception {
		SnmpObjectId snmpValue = testNewSnmpValue(SnmpObjectId.class, "NULL=>objectIdentifier", null, SnmpDataType.objectIdentifier);
		assertEquals("Bad value of result", ".1", snmpValue.toString());
		String message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Short=>objectIdentifier", new Short((short) 103), SnmpDataType.objectIdentifier);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Short is inconsistent with objectIdentifier)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Byte=>objectIdentifier", new Byte((byte) 102), SnmpDataType.objectIdentifier);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Byte is inconsistent with objectIdentifier)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Integer=>objectIdentifier", new Integer(101), SnmpDataType.objectIdentifier);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Integer is inconsistent with objectIdentifier)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Boolean(false)=>objectIdentifier", Boolean.FALSE, SnmpDataType.objectIdentifier);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Boolean is inconsistent with objectIdentifier)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Boolean(true)=>objectIdentifier", Boolean.TRUE, SnmpDataType.objectIdentifier);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Boolean is inconsistent with objectIdentifier)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "Long=>objectIdentifier", new Long(104), SnmpDataType.objectIdentifier);
		assertEquals("Bad message of SnmpException", "error 7 (class java.lang.Long is inconsistent with objectIdentifier)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "BigInteger=>objectIdentifier", new BigInteger("105"), SnmpDataType.objectIdentifier);
		assertEquals("Bad message of SnmpException", "error 7 (class java.math.BigInteger is inconsistent with objectIdentifier)", message);
		snmpValue = testNewSnmpValue(SnmpObjectId.class, "String=>objectIdentifier", ".1.1.2", SnmpDataType.objectIdentifier);
		assertEquals("Bad value of result", ".1.1.2", snmpValue.toString());
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "byte[]=>objectIdentifier", new byte[] { 107, 108 }, SnmpDataType.objectIdentifier);
		assertEquals("Bad message of SnmpException", "error 7 (class [B is inconsistent with objectIdentifier)", message);
		message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "InetAddress=>objectIdentifier", InetAddress.getByName("127.0.0.1"), SnmpDataType.objectIdentifier);
		assertEquals("Bad message of SnmpException", "error 7 (class java.net.Inet4Address is inconsistent with objectIdentifier)", message);
		snmpValue = testNewSnmpValue(SnmpObjectId.class, "SnmpOid=>objectIdentifier", SnmpOid.newInstance(".1"), SnmpDataType.objectIdentifier);
		assertEquals("Bad value of result", ".1", snmpValue.toString());
	}

	/**
	 * Tests a {@link SnmpSyntax} creation with <code>opaque</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newSnmpValue(SnmpDataType, Object)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSnmpValueWithOpaque () throws Exception {
		SnmpOpaque snmpValue = testNewSnmpValue(SnmpOpaque.class, "NULL=>opaque", null, SnmpDataType.opaque);
		assertArrayEquals("Bad value of result", "".getBytes(), snmpValue.getString());
		snmpValue = testNewSnmpValue(SnmpOpaque.class, "Short=>opaque", new Short((short) 103), SnmpDataType.opaque);
		assertEquals("Bad value of result", "103", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOpaque.class, "Byte=>opaque", new Byte((byte) 102), SnmpDataType.opaque);
		assertEquals("Bad value of result", "102", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOpaque.class, "Integer=>opaque", new Integer(101), SnmpDataType.opaque);
		assertEquals("Bad value of result", "101", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOpaque.class, "Boolean(false)=>opaque", Boolean.FALSE, SnmpDataType.opaque);
		assertEquals("Bad value of result", "false", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOpaque.class, "Boolean(true)=>opaque", Boolean.TRUE, SnmpDataType.opaque);
		assertEquals("Bad value of result", "true", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOpaque.class, "Long=>opaque", new Long(104), SnmpDataType.opaque);
		assertEquals("Bad value of result", "104", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOpaque.class, "BigInteger=>opaque", new BigInteger("105"), SnmpDataType.opaque);
		assertEquals("Bad value of result", "105", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOpaque.class, "String=>opaque", "106", SnmpDataType.opaque);
		assertEquals("Bad value of result", "106", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOpaque.class, "byte[]=>opaque", "107".getBytes(), SnmpDataType.opaque);
		assertEquals("Bad value of result", "107", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOpaque.class, "InetAddress=>opaque", InetAddress.getByName("127.0.0.1"), SnmpDataType.opaque);
		assertEquals("Bad value of result", "/127.0.0.1", new String(snmpValue.getString()));
		snmpValue = testNewSnmpValue(SnmpOpaque.class, "SnmpOid=>opaque", SnmpOid.newInstance(".1"), SnmpDataType.opaque);
		assertEquals("Bad value of result", ".1", new String(snmpValue.getString()));
	}

	/**
	 * Tests a {@link SnmpSyntax} creation with an unhandled type.
	 * <p>
	 * Test method for {@link OpennmsSupport#newSnmpValue(SnmpDataType, Object)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSnmpValueWithUnhandledType () throws Exception {
		String message = testNewSnmpValue(SnmpPduPacket.ErrWrongType, "an unhandled type", new Integer(45), null);
		assertEquals("Bad message of SnmpException", "error 7 (null unhandled)", message);
	}

	/**
	 * Tests a {@link SnmpSyntax} creation.
	 * @param snmpClass Class of SNMP value to return.
	 * @param label Equality label.
	 * @param jmxValue Value.
	 * @param type SNMP data type.
	 * @return SNMP value to check.
	 * @throws Exception Exception if an error has occurred.
	 */
	@SuppressWarnings("unchecked")
	private <T extends SnmpSyntax> T testNewSnmpValue (final Class<T> snmpClass, final String label, final Object jmxValue, final SnmpDataType type)
			throws Exception {
		this.logger.info("Test a SnmpSyntax creation with " + label + "...");

		// Test
		final SnmpSyntax snmpValue = OpennmsSupport.newSnmpValue(type, jmxValue);

		// Checking
		assertNotNull("Bad result", snmpValue);
		assertEquals("Bad type of result", snmpClass, snmpValue.getClass());

		return (T) snmpValue;
	}

	/**
	 * Tests a {@link SnmpSyntax} creation for wrong type error.
	 * @param errorStatus Error status.
	 * @param label Equality label.
	 * @param jmxValue Value.
	 * @param type SNMP data type.
	 * @return Error message.
	 * @throws Exception Exception if an error has occurred.
	 */
	private String testNewSnmpValue (final int errorStatus, final String label, final Object jmxValue, final SnmpDataType type) throws Exception {
		this.logger.info("Test a SnmpSyntax creation with " + label + "...");

		// Test
		String message = null;
		try {
			OpennmsSupport.newSnmpValue(type, jmxValue);
			fail("None SnmpException has occurred");
		}
		catch (final SnmpException e) {
			assertEquals("Bad errorStatus of SnmpException", errorStatus, e.getErrorStatus());
			assertEquals("Bad loggerLevel of SnmpException", Level.ERROR, e.getLoggerLevel());
			assertNull("Bad cause of SnmpException", e.getCause());
			message = e.getMessage();
		}

		return message;
	}

	/**
	 * Tests an update of a new value with <code>NULL</code> value.
	 * <p>
	 * Test method for {@link OpennmsSupport#newJmxValue(Class, SnmpDataType, SnmpSyntax)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewJmxValueWithNull () throws Exception {
		this.logger.info("Test an update of a new value with NULL value...");

		// Test
		final Object result = OpennmsSupport.newJmxValue(long.class, SnmpDataType.counter64, new SnmpNull());

		// Checks
		assertNull("Result not NULL", result);

	}

	/**
	 * Tests an update of a new value with <code>integer32</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newJmxValue(Class, SnmpDataType, SnmpSyntax)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewJmxValueWithInteger32 () throws Exception {

		// SnmpInt32=>short
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpInt32(-32769)=>integer32=>short", new SnmpInt32(-32769), short.class, SnmpPduPacket.ErrBadValue,
				"error 3 (the value must be between -32768 and 32767)");
		testNewJmxValue(SnmpDataType.integer32, "SnmpInt32(-32768)=>integer32=>short", new SnmpInt32(-32768), short.class, new Short((short) -32768));
		testNewJmxValue(SnmpDataType.integer32, "SnmpInt32(+32767)=>integer32=>Short", new SnmpInt32(32767), Short.class, new Short((short) 32767));
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpInt32(+32768)=>integer32=>Short", new SnmpInt32(32768), Short.class, SnmpPduPacket.ErrBadValue,
				"error 3 (the value must be between -32768 and 32767)");

		// SnmpInt32=>byte
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpInt32(-129)=>integer32=>byte", new SnmpInt32(-129), byte.class, SnmpPduPacket.ErrBadValue,
				"error 3 (the value must be between -128 and 127)");
		testNewJmxValue(SnmpDataType.integer32, "SnmpInt32(-128)=>integer32=>byte", new SnmpInt32(-128), byte.class, new Byte((byte) -128));
		testNewJmxValue(SnmpDataType.integer32, "SnmpInt32(+127)=>integer32=>Byte", new SnmpInt32(127), Byte.class, new Byte((byte) 127));
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpInt32(+128)=>integer32=>Byte", new SnmpInt32(128), Byte.class, SnmpPduPacket.ErrBadValue,
				"error 3 (the value must be between -128 and 127)");

		// SnmpInt32=>int
		testNewJmxValue(SnmpDataType.integer32, "SnmpInt32(-32769)=>integer32=>int", new SnmpInt32(-32769), int.class, new Integer(-32769));
		testNewJmxValue(SnmpDataType.integer32, "SnmpInt32(+32768)=>integer32=>Integer", new SnmpInt32(32768), Integer.class, new Integer(32768));

		// SnmpInt32=>boolean
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpInt32(0)=>integer32=>boolean", new SnmpInt32(0), boolean.class, SnmpPduPacket.ErrBadValue,
				"error 3 (the value must be between true(1) and false(2))");
		testNewJmxValue(SnmpDataType.integer32, "SnmpInt32(+1)=>integer32=>boolean(true)", new SnmpInt32(1), boolean.class, Boolean.TRUE);
		testNewJmxValue(SnmpDataType.integer32, "SnmpInt32(+2)=>integer32=>Boolean(false)", new SnmpInt32(2), Boolean.class, Boolean.FALSE);
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpInt32(+3)=>integer32=>boolean", new SnmpInt32(3), Boolean.class, SnmpPduPacket.ErrBadValue,
				"error 3 (the value must be between true(1) and false(2))");

		// SnmpInt32=>long
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpInt32=>integer32=>long", new SnmpInt32(48), long.class, SnmpPduPacket.ErrWrongType,
				"error 7 (long is inconsistent with integer32)");

		// SnmpInt32=>BigInteger
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpInt32=>integer32=>BigInteger", new SnmpInt32(48), BigInteger.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.math.BigInteger is inconsistent with integer32)");

		// SnmpInt32=>String
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpInt32=>integer32=>String", new SnmpInt32(48), String.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.String is inconsistent with integer32)");

		// SnmpInt32=>byte[]
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpInt32=>integer32=>byte[]", new SnmpInt32(48), byte[].class, SnmpPduPacket.ErrWrongType,
				"error 7 (class [B is inconsistent with integer32)");

		// SnmpInt32=>InetAddress
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpInt32=>integer32=>InetAddress", new SnmpInt32(48), InetAddress.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.net.InetAddress is inconsistent with integer32)");

		// SnmpInt32=>SnmpOid
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpInt32=>integer32=>SnmpOid", new SnmpInt32(48), SnmpOid.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class net.sf.snmpadaptor4j.object.SnmpOid is inconsistent with integer32)");

		// SnmpUInt32
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpUInt32=>integer32", new SnmpUInt32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an integer32)");

		// SnmpGauge32
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpGauge32=>integer32", new SnmpGauge32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an integer32)");

		// SnmpCounter32
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpCounter32=>integer32", new SnmpCounter32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an integer32)");

		// SnmpCounter64
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpCounter64=>integer32", new SnmpCounter64(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an integer32)");

		// SnmpTimeTicks
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpTimeTicks=>integer32", new SnmpTimeTicks(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an integer32)");

		// SnmpOctetString
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpOctetString=>integer32", new SnmpOctetString(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an integer32)");

		// SnmpIPAddress
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpIPAddress=>integer32", new SnmpIPAddress(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an integer32)");

		// SnmpObjectId
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpObjectId=>integer32", new SnmpObjectId(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an integer32)");

		// SnmpOpaque
		testNewJmxBadValue(SnmpDataType.integer32, "SnmpOpaque=>integer32", new SnmpOpaque(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an integer32)");

	}

	/**
	 * Tests an update of a new value with <code>unsigned32</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newJmxValue(Class, SnmpDataType, SnmpSyntax)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewJmxValueWithUnsigned32 () throws Exception {

		// SnmpInt32
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpInt32=>unsigned32", new SnmpInt32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an unsigned32)");

		// SnmpUInt32=>short
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpUInt32(-1)=>unsigned32=>short", new SnmpUInt32(-1), short.class, SnmpPduPacket.ErrBadValue,
				"error 3 (the value must be between 0 and 32767)");
		testNewJmxValue(SnmpDataType.unsigned32, "SnmpUInt32(0)=>unsigned32=>short", new SnmpUInt32(0), short.class, new Short((short) 0));
		testNewJmxValue(SnmpDataType.unsigned32, "SnmpUInt32(+32767)=>unsigned32=>Short", new SnmpUInt32(32767), Short.class, new Short((short) 32767));
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpUInt32(+32768)=>unsigned32=>Short", new SnmpUInt32(32768), Short.class, SnmpPduPacket.ErrBadValue,
				"error 3 (the value must be between 0 and 32767)");

		// SnmpUInt32=>byte
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpUInt32(-1)=>unsigned32=>byte", new SnmpUInt32(-1), byte.class, SnmpPduPacket.ErrBadValue,
				"error 3 (the value must be between 0 and 127)");
		testNewJmxValue(SnmpDataType.unsigned32, "SnmpUInt32(0)=>unsigned32=>byte", new SnmpUInt32(0), byte.class, new Byte((byte) 0));
		testNewJmxValue(SnmpDataType.unsigned32, "SnmpUInt32(+127)=>unsigned32=>Byte", new SnmpUInt32(127), Byte.class, new Byte((byte) 127));
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpUInt32(+128)=>unsigned32=>Byte", new SnmpUInt32(128), Byte.class, SnmpPduPacket.ErrBadValue,
				"error 3 (the value must be between 0 and 127)");

		// SnmpUInt32=>int
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpUInt32(-1)=>unsigned32=>int", new SnmpUInt32(-1), int.class, SnmpPduPacket.ErrBadValue,
				"error 3 (the value must be between 0 and 2147483647)");
		testNewJmxValue(SnmpDataType.unsigned32, "SnmpUInt32(0)=>unsigned32=>int", new SnmpUInt32(0), int.class, new Integer(0));
		testNewJmxValue(SnmpDataType.unsigned32, "SnmpUInt32(+2147483647)=>unsigned32=>Integer", new SnmpUInt32(2147483647), Integer.class, new Integer(2147483647));
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpUInt32(+2147483648)=>unsigned32=>Integer", new SnmpUInt32(2147483648L), Integer.class,
				SnmpPduPacket.ErrBadValue, "error 3 (the value must be between 0 and 2147483647)");

		// SnmpUInt32=>boolean
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpUInt32(0)=>unsigned32=>boolean", new SnmpUInt32(0), boolean.class, SnmpPduPacket.ErrWrongType,
				"error 7 (boolean is inconsistent with unsigned32)");
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpUInt32(+1)=>unsigned32=>boolean(true)", new SnmpUInt32(1), boolean.class, SnmpPduPacket.ErrWrongType,
				"error 7 (boolean is inconsistent with unsigned32)");
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpUInt32(+2)=>unsigned32=>Boolean(false)", new SnmpUInt32(2), Boolean.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Boolean is inconsistent with unsigned32)");
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpUInt32(+3)=>unsigned32=>boolean", new SnmpUInt32(3), Boolean.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Boolean is inconsistent with unsigned32)");

		// SnmpUInt32=>long
		testNewJmxValue(SnmpDataType.unsigned32, "SnmpUInt32(0)=>unsigned32=>long", new SnmpUInt32(0), long.class, new Long(0));
		testNewJmxValue(SnmpDataType.unsigned32, "SnmpUInt32(+4294967295)=>unsigned32=>Long", new SnmpUInt32(4294967295L), Long.class, new Long(4294967295L));

		// SnmpUInt32=>BigInteger
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpUInt32=>unsigned32=>BigInteger", new SnmpUInt32(48), BigInteger.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.math.BigInteger is inconsistent with unsigned32)");

		// SnmpUInt32=>String
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpUInt32=>unsigned32=>String", new SnmpUInt32(48), String.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.String is inconsistent with unsigned32)");

		// SnmpUInt32=>byte[]
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpUInt32=>unsigned32=>byte[]", new SnmpUInt32(48), byte[].class, SnmpPduPacket.ErrWrongType,
				"error 7 (class [B is inconsistent with unsigned32)");

		// SnmpUInt32=>InetAddress
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpUInt32=>unsigned32=>InetAddress", new SnmpUInt32(48), InetAddress.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.net.InetAddress is inconsistent with unsigned32)");

		// SnmpUInt32=>SnmpOid
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpUInt32=>unsigned32=>SnmpOid", new SnmpUInt32(48), SnmpOid.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class net.sf.snmpadaptor4j.object.SnmpOid is inconsistent with unsigned32)");

		// SnmpGauge32
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpGauge32=>unsigned32", new SnmpGauge32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an unsigned32)");

		// SnmpCounter32
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpCounter32=>unsigned32", new SnmpCounter32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an unsigned32)");

		// SnmpCounter64
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpCounter64=>unsigned32", new SnmpCounter64(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an unsigned32)");

		// SnmpTimeTicks
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpTimeTicks=>unsigned32", new SnmpTimeTicks(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an unsigned32)");

		// SnmpOctetString
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpOctetString=>unsigned32", new SnmpOctetString(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an unsigned32)");

		// SnmpIPAddress
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpIPAddress=>unsigned32", new SnmpIPAddress(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an unsigned32)");

		// SnmpObjectId
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpObjectId=>unsigned32", new SnmpObjectId(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an unsigned32)");

		// SnmpOpaque
		testNewJmxBadValue(SnmpDataType.unsigned32, "SnmpOpaque=>unsigned32", new SnmpOpaque(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an unsigned32)");

	}

	/**
	 * Tests an update of a new value with <code>gauge32</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newJmxValue(Class, SnmpDataType, SnmpSyntax)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewJmxValueWithGauge32 () throws Exception {

		// SnmpInt32
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpInt32=>gauge32", new SnmpInt32(48), null, SnmpPduPacket.ErrBadValue, "error 3 (New value is not a gauge32)");

		// SnmpUInt32
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpUInt32=>gauge32", new SnmpUInt32(48), null, SnmpPduPacket.ErrBadValue, "error 3 (New value is not a gauge32)");

		// SnmpGauge32=>short
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpGauge32(-1)=>gauge32=>short", new SnmpGauge32(-1), short.class, SnmpPduPacket.ErrBadValue,
				"error 3 (the value must be between 0 and 32767)");
		testNewJmxValue(SnmpDataType.gauge32, "SnmpGauge32(0)=>gauge32=>short", new SnmpGauge32(0), short.class, new Short((short) 0));
		testNewJmxValue(SnmpDataType.gauge32, "SnmpGauge32(+32767)=>gauge32=>Short", new SnmpGauge32(32767), Short.class, new Short((short) 32767));
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpGauge32(+32768)=>gauge32=>Short", new SnmpGauge32(32768), Short.class, SnmpPduPacket.ErrBadValue,
				"error 3 (the value must be between 0 and 32767)");

		// SnmpGauge32=>byte
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpGauge32(-1)=>gauge32=>byte", new SnmpGauge32(-1), byte.class, SnmpPduPacket.ErrBadValue,
				"error 3 (the value must be between 0 and 127)");
		testNewJmxValue(SnmpDataType.gauge32, "SnmpGauge32(0)=>gauge32=>byte", new SnmpGauge32(0), byte.class, new Byte((byte) 0));
		testNewJmxValue(SnmpDataType.gauge32, "SnmpGauge32(+127)=>gauge32=>Byte", new SnmpGauge32(127), Byte.class, new Byte((byte) 127));
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpGauge32(+128)=>gauge32=>Byte", new SnmpGauge32(128), Byte.class, SnmpPduPacket.ErrBadValue,
				"error 3 (the value must be between 0 and 127)");

		// SnmpGauge32=>int
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpGauge32(-1)=>gauge32=>int", new SnmpGauge32(-1), int.class, SnmpPduPacket.ErrBadValue,
				"error 3 (the value must be between 0 and 2147483647)");
		testNewJmxValue(SnmpDataType.gauge32, "SnmpGauge32(0)=>gauge32=>int", new SnmpGauge32(0), int.class, new Integer(0));
		testNewJmxValue(SnmpDataType.gauge32, "SnmpGauge32(+2147483647)=>gauge32=>Integer", new SnmpGauge32(2147483647), Integer.class, new Integer(2147483647));
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpGauge32(+2147483648)=>gauge32=>Integer", new SnmpGauge32(2147483648L), Integer.class,
				SnmpPduPacket.ErrBadValue, "error 3 (the value must be between 0 and 2147483647)");

		// SnmpGauge32=>boolean
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpGauge32(0)=>gauge32=>boolean", new SnmpGauge32(0), boolean.class, SnmpPduPacket.ErrWrongType,
				"error 7 (boolean is inconsistent with gauge32)");
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpGauge32(+1)=>gauge32=>boolean(true)", new SnmpGauge32(1), boolean.class, SnmpPduPacket.ErrWrongType,
				"error 7 (boolean is inconsistent with gauge32)");
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpGauge32(+2)=>gauge32=>Boolean(false)", new SnmpGauge32(2), Boolean.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Boolean is inconsistent with gauge32)");
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpGauge32(+3)=>gauge32=>boolean", new SnmpGauge32(3), Boolean.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Boolean is inconsistent with gauge32)");

		// SnmpGauge32=>long
		testNewJmxValue(SnmpDataType.gauge32, "SnmpGauge32(0)=>gauge32=>long", new SnmpGauge32(0), long.class, new Long(0));
		testNewJmxValue(SnmpDataType.gauge32, "SnmpGauge32(+4294967295)=>gauge32=>Long", new SnmpGauge32(4294967295L), Long.class, new Long(4294967295L));

		// SnmpGauge32=>BigInteger
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpGauge32=>gauge32=>BigInteger", new SnmpGauge32(48), BigInteger.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.math.BigInteger is inconsistent with gauge32)");

		// SnmpGauge32=>String
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpGauge32=>gauge32=>String", new SnmpGauge32(48), String.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.String is inconsistent with gauge32)");

		// SnmpGauge32=>byte[]
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpGauge32=>gauge32=>byte[]", new SnmpGauge32(48), byte[].class, SnmpPduPacket.ErrWrongType,
				"error 7 (class [B is inconsistent with gauge32)");

		// SnmpGauge32=>InetAddress
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpGauge32=>gauge32=>InetAddress", new SnmpGauge32(48), InetAddress.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.net.InetAddress is inconsistent with gauge32)");

		// SnmpGauge32=>SnmpOid
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpGauge32=>gauge32=>SnmpOid", new SnmpGauge32(48), SnmpOid.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class net.sf.snmpadaptor4j.object.SnmpOid is inconsistent with gauge32)");

		// SnmpCounter32
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpCounter32=>gauge32", new SnmpCounter32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not a gauge32)");

		// SnmpCounter64
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpCounter64=>gauge32", new SnmpCounter64(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not a gauge32)");

		// SnmpTimeTicks
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpTimeTicks=>gauge32", new SnmpTimeTicks(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not a gauge32)");

		// SnmpOctetString
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpOctetString=>gauge32", new SnmpOctetString(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not a gauge32)");

		// SnmpIPAddress
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpIPAddress=>gauge32", new SnmpIPAddress(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not a gauge32)");

		// SnmpObjectId
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpObjectId=>gauge32", new SnmpObjectId(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not a gauge32)");

		// SnmpOpaque
		testNewJmxBadValue(SnmpDataType.gauge32, "SnmpOpaque=>gauge32", new SnmpOpaque(), null, SnmpPduPacket.ErrBadValue, "error 3 (New value is not a gauge32)");

	}

	/**
	 * Tests an update of a new value with <code>counter32</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newJmxValue(Class, SnmpDataType, SnmpSyntax)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewJmxValueWithCounter32 () throws Exception {

		// SnmpInt32
		testNewJmxBadValue(SnmpDataType.counter32, "SnmpInt32=>counter32", new SnmpInt32(48), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter32 are not writable)");

		// SnmpUInt32
		testNewJmxBadValue(SnmpDataType.counter32, "SnmpUInt32=>counter32", new SnmpUInt32(48), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter32 are not writable)");

		// SnmpGauge32
		testNewJmxBadValue(SnmpDataType.counter32, "SnmpGauge32=>counter32", new SnmpGauge32(48), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter32 are not writable)");

		// SnmpCounter32
		testNewJmxBadValue(SnmpDataType.counter32, "SnmpCounter32=>counter32", new SnmpCounter32(48), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter32 are not writable)");

		// SnmpCounter64
		testNewJmxBadValue(SnmpDataType.counter32, "SnmpCounter64=>counter32", new SnmpCounter64(48), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter32 are not writable)");

		// SnmpTimeTicks
		testNewJmxBadValue(SnmpDataType.counter32, "SnmpTimeTicks=>counter32", new SnmpTimeTicks(48), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter32 are not writable)");

		// SnmpOctetString
		testNewJmxBadValue(SnmpDataType.counter32, "SnmpOctetString=>counter32", new SnmpOctetString(), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter32 are not writable)");

		// SnmpIPAddress
		testNewJmxBadValue(SnmpDataType.counter32, "SnmpIPAddress=>counter32", new SnmpIPAddress(), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter32 are not writable)");

		// SnmpObjectId
		testNewJmxBadValue(SnmpDataType.counter32, "SnmpObjectId=>counter32", new SnmpObjectId(), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter32 are not writable)");

		// SnmpOpaque
		testNewJmxBadValue(SnmpDataType.counter32, "SnmpOpaque=>counter32", new SnmpOpaque(), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter32 are not writable)");

	}

	/**
	 * Tests an update of a new value with <code>counter64</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newJmxValue(Class, SnmpDataType, SnmpSyntax)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewJmxValueWithCounter64 () throws Exception {

		// SnmpInt32
		testNewJmxBadValue(SnmpDataType.counter64, "SnmpInt32=>counter64", new SnmpInt32(48), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter64 are not writable)");

		// SnmpUInt32
		testNewJmxBadValue(SnmpDataType.counter64, "SnmpUInt32=>counter64", new SnmpUInt32(48), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter64 are not writable)");

		// SnmpGauge32
		testNewJmxBadValue(SnmpDataType.counter64, "SnmpGauge32=>counter64", new SnmpGauge32(48), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter64 are not writable)");

		// SnmpCounter32
		testNewJmxBadValue(SnmpDataType.counter64, "SnmpCounter32=>counter64", new SnmpCounter32(48), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter64 are not writable)");

		// SnmpCounter64
		testNewJmxBadValue(SnmpDataType.counter64, "SnmpCounter64=>counter64", new SnmpCounter64(48), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter64 are not writable)");

		// SnmpTimeTicks
		testNewJmxBadValue(SnmpDataType.counter64, "SnmpTimeTicks=>counter64", new SnmpTimeTicks(48), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter64 are not writable)");

		// SnmpOctetString
		testNewJmxBadValue(SnmpDataType.counter64, "SnmpOctetString=>counter64", new SnmpOctetString(), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter64 are not writable)");

		// SnmpIPAddress
		testNewJmxBadValue(SnmpDataType.counter64, "SnmpIPAddress=>counter64", new SnmpIPAddress(), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter64 are not writable)");

		// SnmpObjectId
		testNewJmxBadValue(SnmpDataType.counter64, "SnmpObjectId=>counter64", new SnmpObjectId(), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter64 are not writable)");

		// SnmpOpaque
		testNewJmxBadValue(SnmpDataType.counter64, "SnmpOpaque=>counter64", new SnmpOpaque(), null, SnmpPduPacket.ErrNotWritable,
				"error 17 (counter64 are not writable)");

	}

	/**
	 * Tests an update of a new value with <code>timeTicks</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newJmxValue(Class, SnmpDataType, SnmpSyntax)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewJmxValueWithTimeTicks () throws Exception {

		// SnmpInt32
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpInt32=>timeTicks", new SnmpInt32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not a timeTicks)");

		// SnmpUInt32
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpUInt32=>timeTicks", new SnmpUInt32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not a timeTicks)");

		// SnmpGauge32
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpGauge32=>timeTicks", new SnmpGauge32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not a timeTicks)");

		// SnmpCounter32
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpCounter32=>timeTicks", new SnmpCounter32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not a timeTicks)");

		// SnmpCounter64
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpCounter64=>timeTicks", new SnmpCounter64(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not a timeTicks)");

		// SnmpTimeTicks=>short
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpTimeTicks=>timeTicks=>short", new SnmpTimeTicks(48), short.class, SnmpPduPacket.ErrWrongType,
				"error 7 (short is inconsistent with timeTicks)");
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpTimeTicks=>timeTicks=>Short", new SnmpTimeTicks(48), Short.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Short is inconsistent with timeTicks)");

		// SnmpTimeTicks=>byte
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpTimeTicks=>timeTicks=>byte", new SnmpTimeTicks(48), byte.class, SnmpPduPacket.ErrWrongType,
				"error 7 (byte is inconsistent with timeTicks)");
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpTimeTicks=>timeTicks=>Byte", new SnmpTimeTicks(48), Byte.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Byte is inconsistent with timeTicks)");

		// SnmpTimeTicks=>int
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpTimeTicks=>timeTicks=>int", new SnmpTimeTicks(48), int.class, SnmpPduPacket.ErrWrongType,
				"error 7 (int is inconsistent with timeTicks)");
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpTimeTicks=>timeTicks=>Integer", new SnmpTimeTicks(48), Integer.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Integer is inconsistent with timeTicks)");

		// SnmpTimeTicks=>boolean
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpTimeTicks(0)=>timeTicks=>boolean", new SnmpTimeTicks(0), boolean.class, SnmpPduPacket.ErrWrongType,
				"error 7 (boolean is inconsistent with timeTicks)");
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpTimeTicks(+1)=>timeTicks=>boolean(true)", new SnmpTimeTicks(1), boolean.class, SnmpPduPacket.ErrWrongType,
				"error 7 (boolean is inconsistent with timeTicks)");
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpTimeTicks(+2)=>timeTicks=>Boolean(false)", new SnmpTimeTicks(2), Boolean.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Boolean is inconsistent with timeTicks)");
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpTimeTicks(+3)=>timeTicks=>boolean", new SnmpTimeTicks(3), Boolean.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Boolean is inconsistent with timeTicks)");

		// SnmpTimeTicks=>long
		testNewJmxValue(SnmpDataType.timeTicks, "SnmpTimeTicks(0)=>timeTicks=>long", new SnmpTimeTicks(0), long.class, new Long(0));
		testNewJmxValue(SnmpDataType.timeTicks, "SnmpTimeTicks(+4294967295)=>timeTicks=>Long", new SnmpTimeTicks(4294967295L), Long.class, new Long(42949672950L));

		// SnmpTimeTicks=>BigInteger
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpTimeTicks=>timeTicks=>BigInteger", new SnmpTimeTicks(48), BigInteger.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.math.BigInteger is inconsistent with timeTicks)");

		// SnmpTimeTicks=>String
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpTimeTicks=>timeTicks=>String", new SnmpTimeTicks(48), String.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.String is inconsistent with timeTicks)");

		// SnmpTimeTicks=>byte[]
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpTimeTicks=>timeTicks=>byte[]", new SnmpTimeTicks(48), byte[].class, SnmpPduPacket.ErrWrongType,
				"error 7 (class [B is inconsistent with timeTicks)");

		// SnmpTimeTicks=>InetAddress
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpTimeTicks=>timeTicks=>InetAddress", new SnmpTimeTicks(48), InetAddress.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.net.InetAddress is inconsistent with timeTicks)");

		// SnmpTimeTicks=>SnmpOid
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpTimeTicks=>timeTicks=>SnmpOid", new SnmpTimeTicks(48), SnmpOid.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class net.sf.snmpadaptor4j.object.SnmpOid is inconsistent with timeTicks)");

		// SnmpOctetString
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpOctetString=>timeTicks", new SnmpOctetString(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not a timeTicks)");

		// SnmpIPAddress
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpIPAddress=>timeTicks", new SnmpIPAddress(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not a timeTicks)");

		// SnmpObjectId
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpObjectId=>timeTicks", new SnmpObjectId(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not a timeTicks)");

		// SnmpOpaque
		testNewJmxBadValue(SnmpDataType.timeTicks, "SnmpOpaque=>timeTicks", new SnmpOpaque(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not a timeTicks)");

	}

	/**
	 * Tests an update of a new value with <code>octetString</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newJmxValue(Class, SnmpDataType, SnmpSyntax)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewJmxValueWithOctetString () throws Exception {

		// SnmpInt32
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpInt32=>octetString", new SnmpInt32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an octetString)");

		// SnmpUInt32
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpUInt32=>octetString", new SnmpUInt32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an octetString)");

		// SnmpGauge32
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpGauge32=>octetString", new SnmpGauge32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an octetString)");

		// SnmpCounter32
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpCounter32=>octetString", new SnmpCounter32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an octetString)");

		// SnmpCounter64
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpCounter64=>octetString", new SnmpCounter64(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an octetString)");

		// SnmpTimeTicks
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpTimeTicks=>octetString", new SnmpTimeTicks(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an octetString)");

		// SnmpOctetString=>short
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpOctetString=>octetString=>short", new SnmpOctetString(), short.class, SnmpPduPacket.ErrWrongType,
				"error 7 (short is inconsistent with octetString)");
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpOctetString=>octetString=>Short", new SnmpOctetString(), Short.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Short is inconsistent with octetString)");

		// SnmpOctetString=>byte
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpOctetString=>octetString=>byte", new SnmpOctetString(), byte.class, SnmpPduPacket.ErrWrongType,
				"error 7 (byte is inconsistent with octetString)");
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpOctetString=>octetString=>Byte", new SnmpOctetString(), Byte.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Byte is inconsistent with octetString)");

		// SnmpOctetString=>int
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpOctetString=>octetString=>int", new SnmpOctetString(), int.class, SnmpPduPacket.ErrWrongType,
				"error 7 (int is inconsistent with octetString)");
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpOctetString=>octetString=>Integer", new SnmpOctetString(), Integer.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Integer is inconsistent with octetString)");

		// SnmpOctetString=>boolean
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpOctetString=>octetString=>boolean(true)", new SnmpOctetString(), boolean.class,
				SnmpPduPacket.ErrWrongType, "error 7 (boolean is inconsistent with octetString)");
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpOctetString=>octetString=>Boolean(false)", new SnmpOctetString(), Boolean.class,
				SnmpPduPacket.ErrWrongType, "error 7 (class java.lang.Boolean is inconsistent with octetString)");

		// SnmpOctetString=>long
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpOctetString=>octetString=>long", new SnmpOctetString(), long.class, SnmpPduPacket.ErrWrongType,
				"error 7 (long is inconsistent with octetString)");
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpOctetString=>octetString=>Long", new SnmpOctetString(), Long.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Long is inconsistent with octetString)");

		// SnmpOctetString=>BigInteger
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpOctetString=>octetString=>BigInteger", new SnmpOctetString(), BigInteger.class,
				SnmpPduPacket.ErrWrongType, "error 7 (class java.math.BigInteger is inconsistent with octetString)");

		// SnmpOctetString=>String
		testNewJmxValue(SnmpDataType.octetString, "SnmpOctetString(\"TEST\")=>octetString=>String", new SnmpOctetString("TEST".getBytes()), String.class, "TEST");

		// SnmpOctetString=>byte[]
		testNewJmxValue(SnmpDataType.octetString, "SnmpOctetString(\"TEST\")=>octetString=>byte[]", new SnmpOctetString("TEST".getBytes()), byte[].class,
				"TEST".getBytes());

		// SnmpOctetString=>InetAddress
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpOctetString=>octetString=>InetAddress", new SnmpOctetString(), InetAddress.class,
				SnmpPduPacket.ErrWrongType, "error 7 (class java.net.InetAddress is inconsistent with octetString)");

		// SnmpOctetString=>SnmpOid
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpOctetString=>octetString=>SnmpOid", new SnmpOctetString(), SnmpOid.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class net.sf.snmpadaptor4j.object.SnmpOid is inconsistent with octetString)");

		// SnmpIPAddress
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpIPAddress=>octetString", new SnmpIPAddress(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an octetString)");

		// SnmpObjectId
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpObjectId=>octetString", new SnmpObjectId(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an octetString)");

		// SnmpOpaque
		testNewJmxBadValue(SnmpDataType.octetString, "SnmpOpaque=>octetString", new SnmpOpaque(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an octetString)");

	}

	/**
	 * Tests an update of a new value with <code>ipAddress</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newJmxValue(Class, SnmpDataType, SnmpSyntax)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewJmxValueWithIpAddress () throws Exception {

		// SnmpInt32
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpInt32=>ipAddress", new SnmpInt32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an ipAddress)");

		// SnmpUInt32
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpUInt32=>ipAddress", new SnmpUInt32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an ipAddress)");

		// SnmpGauge32
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpGauge32=>ipAddress", new SnmpGauge32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an ipAddress)");

		// SnmpCounter32
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpCounter32=>ipAddress", new SnmpCounter32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an ipAddress)");

		// SnmpCounter64
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpCounter64=>ipAddress", new SnmpCounter64(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an ipAddress)");

		// SnmpTimeTicks
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpTimeTicks=>ipAddress", new SnmpTimeTicks(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an ipAddress)");

		// SnmpOctetString
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpOctetString=>ipAddress", new SnmpOctetString(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an ipAddress)");

		// SnmpIPAddress=>short
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpIPAddress=>ipAddress=>short", new SnmpIPAddress(), short.class, SnmpPduPacket.ErrWrongType,
				"error 7 (short is inconsistent with ipAddress)");
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpIPAddress=>ipAddress=>Short", new SnmpIPAddress(), Short.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Short is inconsistent with ipAddress)");

		// SnmpIPAddress=>byte
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpIPAddress=>ipAddress=>byte", new SnmpIPAddress(), byte.class, SnmpPduPacket.ErrWrongType,
				"error 7 (byte is inconsistent with ipAddress)");
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpIPAddress=>ipAddress=>Byte", new SnmpIPAddress(), Byte.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Byte is inconsistent with ipAddress)");

		// SnmpIPAddress=>int
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpIPAddress=>ipAddress=>int", new SnmpIPAddress(), int.class, SnmpPduPacket.ErrWrongType,
				"error 7 (int is inconsistent with ipAddress)");
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpIPAddress=>ipAddress=>Integer", new SnmpIPAddress(), Integer.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Integer is inconsistent with ipAddress)");

		// SnmpIPAddress=>boolean
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpIPAddress=>ipAddress=>boolean(true)", new SnmpIPAddress(), boolean.class, SnmpPduPacket.ErrWrongType,
				"error 7 (boolean is inconsistent with ipAddress)");
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpIPAddress=>ipAddress=>Boolean(false)", new SnmpIPAddress(), Boolean.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Boolean is inconsistent with ipAddress)");

		// SnmpIPAddress=>long
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpIPAddress=>ipAddress=>long", new SnmpIPAddress(), long.class, SnmpPduPacket.ErrWrongType,
				"error 7 (long is inconsistent with ipAddress)");
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpIPAddress=>ipAddress=>Long", new SnmpIPAddress(), Long.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Long is inconsistent with ipAddress)");

		// SnmpIPAddress=>BigInteger
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpIPAddress=>ipAddress=>BigInteger", new SnmpIPAddress(), BigInteger.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.math.BigInteger is inconsistent with ipAddress)");

		// SnmpIPAddress=>String
		testNewJmxValue(SnmpDataType.ipAddress, "SnmpIPAddress(\"127.0.0.1\")=>ipAddress=>String", new SnmpIPAddress("127.0.0.1"), String.class, "127.0.0.1");

		// SnmpIPAddress=>byte[]
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpIPAddress=>ipAddress=>byte[]", new SnmpIPAddress(), byte[].class, SnmpPduPacket.ErrWrongType,
				"error 7 (class [B is inconsistent with ipAddress)");

		// SnmpIPAddress=>InetAddress
		testNewJmxValue(SnmpDataType.ipAddress, "SnmpIPAddress(\"127.0.0.1\")=>ipAddress=>InetAddress", new SnmpIPAddress("127.0.0.1"), InetAddress.class,
				InetAddress.getByName("127.0.0.1"));

		// SnmpIPAddress=>SnmpOid
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpIPAddress=>ipAddress=>SnmpOid", new SnmpIPAddress(), SnmpOid.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class net.sf.snmpadaptor4j.object.SnmpOid is inconsistent with ipAddress)");

		// SnmpObjectId
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpObjectId=>ipAddress", new SnmpObjectId(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an ipAddress)");

		// SnmpOpaque
		testNewJmxBadValue(SnmpDataType.ipAddress, "SnmpOpaque=>ipAddress", new SnmpOpaque(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an ipAddress)");

	}

	/**
	 * Tests an update of a new value with <code>objectIdentifier</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newJmxValue(Class, SnmpDataType, SnmpSyntax)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewJmxValueWithObjectIdentifier () throws Exception {

		// SnmpInt32
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpInt32=>objectIdentifier", new SnmpInt32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an objectIdentifier)");

		// SnmpUInt32
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpUInt32=>objectIdentifier", new SnmpUInt32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an objectIdentifier)");

		// SnmpGauge32
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpGauge32=>objectIdentifier", new SnmpGauge32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an objectIdentifier)");

		// SnmpCounter32
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpCounter32=>objectIdentifier", new SnmpCounter32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an objectIdentifier)");

		// SnmpCounter64
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpCounter64=>objectIdentifier", new SnmpCounter64(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an objectIdentifier)");

		// SnmpTimeTicks
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpTimeTicks=>objectIdentifier", new SnmpTimeTicks(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an objectIdentifier)");

		// SnmpOctetString
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpOctetString=>objectIdentifier", new SnmpOctetString(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an objectIdentifier)");

		// SnmpIPAddress
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpIPAddress=>objectIdentifier", new SnmpIPAddress(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an objectIdentifier)");

		// SnmpObjectId=>short
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpObjectId=>objectIdentifier=>short", new SnmpObjectId(), short.class, SnmpPduPacket.ErrWrongType,
				"error 7 (short is inconsistent with objectIdentifier)");
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpObjectId=>objectIdentifier=>Short", new SnmpObjectId(), Short.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Short is inconsistent with objectIdentifier)");

		// SnmpObjectId=>byte
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpObjectId=>objectIdentifier=>byte", new SnmpObjectId(), byte.class, SnmpPduPacket.ErrWrongType,
				"error 7 (byte is inconsistent with objectIdentifier)");
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpObjectId=>objectIdentifier=>Byte", new SnmpObjectId(), Byte.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Byte is inconsistent with objectIdentifier)");

		// SnmpObjectId=>int
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpObjectId=>objectIdentifier=>int", new SnmpObjectId(), int.class, SnmpPduPacket.ErrWrongType,
				"error 7 (int is inconsistent with objectIdentifier)");
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpObjectId=>objectIdentifier=>Integer", new SnmpObjectId(), Integer.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Integer is inconsistent with objectIdentifier)");

		// SnmpObjectId=>boolean
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpObjectId=>objectIdentifier=>boolean(true)", new SnmpObjectId(), boolean.class,
				SnmpPduPacket.ErrWrongType, "error 7 (boolean is inconsistent with objectIdentifier)");
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpObjectId=>objectIdentifier=>Boolean(false)", new SnmpObjectId(), Boolean.class,
				SnmpPduPacket.ErrWrongType, "error 7 (class java.lang.Boolean is inconsistent with objectIdentifier)");

		// SnmpObjectId=>long
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpObjectId=>objectIdentifier=>long", new SnmpObjectId(), long.class, SnmpPduPacket.ErrWrongType,
				"error 7 (long is inconsistent with objectIdentifier)");
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpObjectId=>objectIdentifier=>Long", new SnmpObjectId(), Long.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Long is inconsistent with objectIdentifier)");

		// SnmpObjectId=>BigInteger
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpObjectId=>objectIdentifier=>BigInteger", new SnmpObjectId(), BigInteger.class,
				SnmpPduPacket.ErrWrongType, "error 7 (class java.math.BigInteger is inconsistent with objectIdentifier)");

		// SnmpObjectId=>String
		testNewJmxValue(SnmpDataType.objectIdentifier, "SnmpObjectId(\"1\")=>objectIdentifier=>String", new SnmpObjectId("1"), String.class, ".1");

		// SnmpObjectId=>byte[]
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpObjectId=>objectIdentifier=>byte[]", new SnmpObjectId(), byte[].class, SnmpPduPacket.ErrWrongType,
				"error 7 (class [B is inconsistent with objectIdentifier)");

		// SnmpObjectId=>InetAddress
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpObjectId=>objectIdentifier=>byte[]", new SnmpObjectId(), InetAddress.class,
				SnmpPduPacket.ErrWrongType, "error 7 (class java.net.InetAddress is inconsistent with objectIdentifier)");

		// SnmpObjectId=>SnmpOid
		testNewJmxValue(SnmpDataType.objectIdentifier, "SnmpObjectId(\"1\")=>objectIdentifier=>SnmpOid", new SnmpObjectId("1"), SnmpOid.class,
				SnmpOid.newInstance("1"));

		// SnmpOpaque
		testNewJmxBadValue(SnmpDataType.objectIdentifier, "SnmpOpaque=>objectIdentifier", new SnmpOpaque(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an objectIdentifier)");

	}

	/**
	 * Tests an update of a new value with <code>opaque</code>.
	 * <p>
	 * Test method for {@link OpennmsSupport#newJmxValue(Class, SnmpDataType, SnmpSyntax)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewJmxValueWithOpaque () throws Exception {

		// SnmpInt32
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpInt32=>opaque", new SnmpInt32(48), null, SnmpPduPacket.ErrBadValue, "error 3 (New value is not an opaque)");

		// SnmpUInt32
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpUInt32=>opaque", new SnmpUInt32(48), null, SnmpPduPacket.ErrBadValue, "error 3 (New value is not an opaque)");

		// SnmpGauge32
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpGauge32=>opaque", new SnmpGauge32(48), null, SnmpPduPacket.ErrBadValue, "error 3 (New value is not an opaque)");

		// SnmpCounter32
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpCounter32=>opaque", new SnmpCounter32(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an opaque)");

		// SnmpCounter64
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpCounter64=>opaque", new SnmpCounter64(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an opaque)");

		// SnmpTimeTicks
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpTimeTicks=>opaque", new SnmpTimeTicks(48), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an opaque)");

		// SnmpOctetString
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpOctetString=>opaque", new SnmpOctetString(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an opaque)");

		// SnmpIPAddress
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpIPAddress=>opaque", new SnmpIPAddress(), null, SnmpPduPacket.ErrBadValue,
				"error 3 (New value is not an opaque)");

		// SnmpObjectId
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpObjectId=>opaque", new SnmpObjectId(), null, SnmpPduPacket.ErrBadValue, "error 3 (New value is not an opaque)");

		// SnmpOpaque=>short
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpOpaque=>opaque=>short", new SnmpOpaque(), short.class, SnmpPduPacket.ErrWrongType,
				"error 7 (short is inconsistent with opaque)");
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpOpaque=>opaque=>Short", new SnmpOpaque(), Short.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Short is inconsistent with opaque)");

		// SnmpOpaque=>byte
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpOpaque=>opaque=>byte", new SnmpOpaque(), byte.class, SnmpPduPacket.ErrWrongType,
				"error 7 (byte is inconsistent with opaque)");
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpOpaque=>opaque=>Byte", new SnmpOpaque(), Byte.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Byte is inconsistent with opaque)");

		// SnmpOpaque=>int
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpOpaque=>opaque=>int", new SnmpOpaque(), int.class, SnmpPduPacket.ErrWrongType,
				"error 7 (int is inconsistent with opaque)");
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpOpaque=>opaque=>Integer", new SnmpOpaque(), Integer.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Integer is inconsistent with opaque)");

		// SnmpOpaque=>boolean
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpOpaque=>opaque=>boolean(true)", new SnmpOpaque(), boolean.class, SnmpPduPacket.ErrWrongType,
				"error 7 (boolean is inconsistent with opaque)");
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpOpaque=>opaque=>Boolean(false)", new SnmpOpaque(), Boolean.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Boolean is inconsistent with opaque)");

		// SnmpOpaque=>long
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpOpaque=>opaque=>long", new SnmpOpaque(), long.class, SnmpPduPacket.ErrWrongType,
				"error 7 (long is inconsistent with opaque)");
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpOpaque=>opaque=>Long", new SnmpOpaque(), Long.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.lang.Long is inconsistent with opaque)");

		// SnmpOpaque=>BigInteger
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpOpaque=>opaque=>BigInteger", new SnmpOpaque(), BigInteger.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.math.BigInteger is inconsistent with opaque)");

		// SnmpOpaque=>String
		testNewJmxValue(SnmpDataType.opaque, "SnmpOpaque(\"TEST\")=>opaque=>String", new SnmpOpaque("TEST".getBytes()), String.class, "TEST");

		// SnmpOpaque=>byte[]
		testNewJmxValue(SnmpDataType.opaque, "SnmpOpaque(\"TEST\")=>opaque=>byte[]", new SnmpOpaque("TEST".getBytes()), byte[].class, "TEST".getBytes());

		// SnmpOpaque=>InetAddress
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpOpaque=>opaque=>InetAddress", new SnmpOpaque(), InetAddress.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class java.net.InetAddress is inconsistent with opaque)");

		// SnmpOpaque=>SnmpOid
		testNewJmxBadValue(SnmpDataType.opaque, "SnmpOpaque=>opaque=>SnmpOid", new SnmpOpaque(), SnmpOid.class, SnmpPduPacket.ErrWrongType,
				"error 7 (class net.sf.snmpadaptor4j.object.SnmpOid is inconsistent with opaque)");

	}

	/**
	 * Tests an update of a new value with an unhandled type.
	 * <p>
	 * Test method for {@link OpennmsSupport#newJmxValue(Class, SnmpDataType, SnmpSyntax)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewJmxValueWithUnhandledType () throws Exception {
		testNewJmxBadValue(null, "an unhandled type", new SnmpObjectId(), null, SnmpPduPacket.ErrWrongType, "error 7 (null unhandled)");
	}

	/**
	 * Tests an update of a new value.
	 * @param type SNMP data type.
	 * @param label Equality label.
	 * @param newValue New SNMP value to set.
	 * @param jmxDataType Data type of JMX attribute.
	 * @param expectedValue Expected value to set.
	 * @throws Exception Exception if an error has occurred.
	 */
	private void testNewJmxValue (final SnmpDataType type, final String label, final SnmpSyntax newValue, final Class<?> jmxDataType, final Object expectedValue)
			throws Exception {
		this.logger.info("Test an update of a new value with " + label + "...");

		// Test
		final Object result = OpennmsSupport.newJmxValue(jmxDataType, type, newValue);

		// Checks
		if (expectedValue instanceof byte[]) {
			assertArrayEquals("Bad value of result", (byte[]) expectedValue, (byte[]) result);
		}
		else {
			assertEquals("Bad value of result", expectedValue, result);
		}

	}

	/**
	 * Tests an update of a new value for an exception.
	 * @param type SNMP data type.
	 * @param label Equality label.
	 * @param newValue New SNMP value to set.
	 * @param jmxDataType Data type of JMX attribute.
	 * @param errorStatus Expected error status.
	 * @param errorMessage Expected error message.
	 * @throws Exception Exception if an error has occurred.
	 */
	private void testNewJmxBadValue (final SnmpDataType type, final String label, final SnmpSyntax newValue, final Class<?> jmxDataType, final int errorStatus,
			final String errorMessage) throws Exception {
		this.logger.info("Test an update of a new value with " + label + "...");

		// Test
		try {
			OpennmsSupport.newJmxValue(jmxDataType, type, newValue);
			fail("None Exception has occurred");
		}
		catch (final SnmpException e) {
			assertEquals("Bad errorStatus of SnmpException", errorStatus, e.getErrorStatus());
			assertEquals("Bad message of SnmpException", errorMessage, e.getMessage());
			assertEquals("Bad loggerLevel of SnmpException", Level.ERROR, e.getLoggerLevel());
			assertNull("Bad cause of SnmpException", e.getCause());
		}

	}

}