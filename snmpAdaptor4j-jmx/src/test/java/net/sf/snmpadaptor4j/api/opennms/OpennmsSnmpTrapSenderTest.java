package net.sf.snmpadaptor4j.api.opennms;

import static org.junit.Assert.*;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import net.sf.snmpadaptor4j.SnmpManagerConfiguration;
import net.sf.snmpadaptor4j.api.opennms.OpennmsSnmpTrapSender;
import net.sf.snmpadaptor4j.api.opennms.OpennmsSupport;
import net.sf.snmpadaptor4j.object.SnmpDataType;
import net.sf.snmpadaptor4j.object.SnmpOid;
import net.sf.snmpadaptor4j.object.SnmpTrap;
import net.sf.snmpadaptor4j.object.SnmpTrapData;
import net.sf.snmpadaptor4j.object.GenericSnmpTrap;
import net.sf.snmpadaptor4j.object.SpecificSnmpTrap;
import net.sf.snmpadaptor4j.object.GenericSnmpTrapType;
import net.sf.snmpadaptor4j.test.SnmpTrapManager;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.opennms.protocols.snmp.SnmpPduPacket;
import org.opennms.protocols.snmp.SnmpSession;
import org.opennms.protocols.snmp.SnmpSyntax;

/**
 * Test class for {@link net.sf.snmpadaptor4j.api.opennms.OpennmsSnmpTrapSender}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class OpennmsSnmpTrapSenderTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(OpennmsSnmpTrapSenderTest.class);

	/**
	 * Connection parameters to the mock SNMP manager V1.
	 */
	private final SnmpManagerConfiguration managerV1 = new SnmpManagerConfiguration("127.0.0.1", 1162, 1, "public");

	/**
	 * Connection parameters to the mock SNMP manager V2.
	 */
	private final SnmpManagerConfiguration managerV2 = new SnmpManagerConfiguration("127.0.0.1", 1162, 2, "public");

	/**
	 * Object representing an unknown SNMP trap.
	 */
	protected final class SnmpTrapUnknown
			extends SnmpTrap {

		/**
		 * Serial number.
		 */
		private static final long serialVersionUID = -8621815907938719112L;

		/**
		 * Constructor.
		 * @param timeStamp Elapsed time in <b>100th of second</b> since the application launch.
		 * @param source OID indexing the source of the trap.
		 * @param dataMap Map of "interesting" information.
		 */
		protected SnmpTrapUnknown (final long timeStamp, final SnmpOid source, final Map<SnmpOid, SnmpTrapData> dataMap) {
			super(timeStamp, source, dataMap);
		}

		/*
		 * {@inheritDoc}
		 * @see net.sf.snmpadaptor4j.object.SnmpTrap#traceTo(org.apache.log4j.Logger)
		 */
		@Override
		public void traceTo (final Logger extLogger) {
			// NOP
		}

	}

	/**
	 * Tests opening and closing of SNMP trap sender.
	 * <p>
	 * Test methods for:
	 * </p>
	 * <ul>
	 * <li>{@link OpennmsSnmpTrapSender#open()}</li>,
	 * <li>{@link OpennmsSnmpTrapSender#close()}</li>,
	 * <li>{@link OpennmsSnmpTrapSender#isConnected()}</li>.
	 * </ul>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testOpenClose () throws Exception {
		this.logger.info("Tests openning and closing of SNMP trap sender...");

		// SNMP trap sender creating
		final OpennmsSnmpTrapSender sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), this.managerV1.getAddress(), this.managerV1.getPort(),
				this.managerV1.getVersion(), this.managerV1.getCommunity());
		sender.logger.setLevel(Level.ALL);
		assertFalse("SNMP trap sender already started", sender.isConnected());

		// SNMP trap sender opening
		sender.open();
		assertTrue("SNMP trap sender not connected", sender.isConnected());

		// SNMP trap sender closing
		sender.close();
		assertFalse("SNMP trap sender not disconnected", sender.isConnected());

	}

	/**
	 * Tests re-opening of SNMP trap sender.
	 * <p>
	 * Test methods for:
	 * </p>
	 * <ul>
	 * <li>{@link OpennmsSnmpTrapSender#open()}</li>,
	 * <li>{@link OpennmsSnmpTrapSender#close()}</li>,
	 * <li>{@link OpennmsSnmpTrapSender#isConnected()}</li>.
	 * </ul>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testReOpening () throws Exception {
		this.logger.info("Tests re-opening of SNMP trap sender...");

		// SNMP trap sender creating
		final OpennmsSnmpTrapSender sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), this.managerV1.getAddress(), this.managerV1.getPort(),
				this.managerV1.getVersion(), this.managerV1.getCommunity());
		sender.logger.setLevel(Level.OFF);
		assertFalse("SNMP trap sender already started", sender.isConnected());

		// SNMP trap sender opening
		sender.open();
		assertTrue("SNMP trap sender not connected", sender.isConnected());

		// SNMP trap sender re-opening
		sender.open();
		assertTrue("SNMP trap sender not connected", sender.isConnected());
		sender.logger.setLevel(Level.ALL);
		sender.open();
		assertTrue("SNMP trap sender not connected", sender.isConnected());

		// SNMP trap sender closing
		sender.logger.setLevel(Level.OFF);
		sender.close();
		assertFalse("SNMP trap sender not disconnected", sender.isConnected());

	}

	/**
	 * Tests re-closing of SNMP trap sender.
	 * <p>
	 * Test methods for:
	 * </p>
	 * <ul>
	 * <li>{@link OpennmsSnmpTrapSender#open()}</li>,
	 * <li>{@link OpennmsSnmpTrapSender#close()}</li>,
	 * <li>{@link OpennmsSnmpTrapSender#isConnected()}</li>.
	 * </ul>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testReClosing () throws Exception {
		this.logger.info("Tests re-closing of SNMP trap sender...");

		// SNMP trap sender creating
		final OpennmsSnmpTrapSender sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), this.managerV1.getAddress(), this.managerV1.getPort(),
				this.managerV1.getVersion(), this.managerV1.getCommunity());
		sender.logger.setLevel(Level.OFF);
		assertFalse("SNMP trap sender already started", sender.isConnected());

		// SNMP trap sender re-closing
		sender.close();
		assertFalse("SNMP trap sender not disconnected", sender.isConnected());
		sender.logger.setLevel(Level.ALL);
		sender.close();
		assertFalse("SNMP trap sender not disconnected", sender.isConnected());

	}

	/**
	 * Tests SNMP trap v1 sending with no session.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV1WithNoSession () throws Exception {
		this.logger.info("Tests SNMP trap v1 sending with no session...");

		// SNMP trap sender creating
		final OpennmsSnmpTrapSender sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), this.managerV1.getAddress(), this.managerV1.getPort(),
				this.managerV1.getVersion(), this.managerV1.getCommunity());
		sender.logger.setLevel(Level.OFF);
		assertFalse("SNMP trap sender already started", sender.isConnected());
		try {

			// Sending test
			final SpecificSnmpTrap trap = SnmpTrap.newInstance(0, SnmpOid.newInstance("1.2"), 3, null);
			sender.send(trap);
			fail("None exception has occurred");

		}
		catch (final Exception e) {

			// Checking
			assertSame("Bad type of exception", Exception.class, e.getClass());
			assertEquals("Bad message of exception", "The connection to the SNMP manager is closed", e.getMessage());
			assertNull("Bad cause of exception", e.getCause());

		}
	}

	/**
	 * Tests SNMP trap v1 sending with no source.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV1WithNoSource () throws Exception {
		this.logger.info("Tests SNMP trap v1 sending with no source...");

		// SNMP trap sender creating
		final OpennmsSnmpTrapSender sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), this.managerV1.getAddress(), this.managerV1.getPort(),
				this.managerV1.getVersion(), this.managerV1.getCommunity());
		sender.logger.setLevel(Level.ALL);
		assertFalse("SNMP trap sender already started", sender.isConnected());
		try {

			// Sending test
			final SpecificSnmpTrap trap = SnmpTrap.newInstance(0, null, 3, null);
			sender.send(trap);
			fail("None exception has occurred");

		}
		catch (final Exception e) {

			// Checking
			assertSame("Bad type of exception", Exception.class, e.getClass());
			assertEquals("Bad message of exception", "The source of trap is missing", e.getMessage());
			assertNull("Bad cause of exception", e.getCause());

		}
	}

	/**
	 * Tests SNMP specific trap v1 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV1Specific () throws Exception {
		this.logger.info("Tests SNMP specific trap v1 sending...");
		final SnmpTrapManager trapManager = new SnmpTrapManager();
		trapManager.start();
		try {

			// SNMP trap sender creating
			final OpennmsSnmpTrapSender sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), this.managerV1.getAddress(), this.managerV1.getPort(),
					this.managerV1.getVersion(), this.managerV1.getCommunity());
			sender.logger.setLevel(Level.ALL);

			// SNMP trap sender opening
			sender.open();
			try {

				// Sending test
				final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
				dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
				dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
				final SpecificSnmpTrap trap = SnmpTrap.newInstance(0, SnmpOid.newInstance("1.2"), 3, dataMap);
				sender.send(trap);

				// Checking
				Thread.sleep(50);
				trapManager.assertTrap(InetAddress.getLocalHost(), "public", trap);
				trapManager.assertNoTrap();

			}
			finally {

				// SNMP trap sender closing
				sender.close();

			}
		}
		finally {
			trapManager.stop();
		}
	}

	/**
	 * Tests SNMP <b>coldStart</b> trap v1 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV1ColdStart () throws Exception {
		this.logger.info("Tests SNMP coldStart trap v1 sending...");
		testSendV1Generic(GenericSnmpTrapType.coldStart);
	}

	/**
	 * Tests SNMP <b>warmStart</b> trap v1 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV1WarmStart () throws Exception {
		this.logger.info("Tests SNMP warmStart trap v1 sending...");
		testSendV1Generic(GenericSnmpTrapType.warmStart);
	}

	/**
	 * Tests SNMP <b>linkDown</b> trap v1 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV1LinkDown () throws Exception {
		this.logger.info("Tests SNMP linkDown trap v1 sending...");
		testSendV1Generic(GenericSnmpTrapType.linkDown);
	}

	/**
	 * Tests SNMP <b>linkUp</b> trap v1 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV1LinkUp () throws Exception {
		this.logger.info("Tests SNMP linkUp trap v1 sending...");
		testSendV1Generic(GenericSnmpTrapType.linkUp);
	}

	/**
	 * Tests SNMP <b>authenticationFailure</b> trap v1 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV1AuthenticationFailure () throws Exception {
		this.logger.info("Tests SNMP authenticationFailure trap v1 sending...");
		testSendV1Generic(GenericSnmpTrapType.authenticationFailure);
	}

	/**
	 * Tests SNMP <b>egpNeighborLoss</b> trap v1 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV1EgpNeighborLoss () throws Exception {
		this.logger.info("Tests SNMP egpNeighborLoss trap v1 sending...");
		testSendV1Generic(GenericSnmpTrapType.egpNeighborLoss);
	}

	/**
	 * Tests SNMP generic trap v1 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @param trapType SNMP trap type.
	 * @throws Exception Exception if an error has occurred.
	 */
	private void testSendV1Generic (final GenericSnmpTrapType trapType) throws Exception {
		final SnmpTrapManager trapManager = new SnmpTrapManager();
		trapManager.start();
		try {

			// SNMP trap sender creating
			final OpennmsSnmpTrapSender sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), this.managerV1.getAddress(), this.managerV1.getPort(),
					this.managerV1.getVersion(), this.managerV1.getCommunity());
			sender.logger.setLevel(Level.OFF);

			// SNMP trap sender opening
			sender.open();
			try {

				// Sending test
				final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
				dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
				dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
				final GenericSnmpTrap trap = SnmpTrap.newInstance(0, SnmpOid.newInstance("1.2"), trapType, dataMap);
				sender.send(trap);

				// Checking
				Thread.sleep(50);
				trapManager.assertTrap(InetAddress.getLocalHost(), "public", trap);
				trapManager.assertNoTrap();

			}
			finally {

				// SNMP trap sender closing
				sender.close();

			}
		}
		finally {
			trapManager.stop();
		}
	}

	/**
	 * Tests SNMP unknown generic trap v1 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV1UnknownGeneric () throws Exception {
		this.logger.info("Tests SNMP unknown generic trap v1 sending...");
		final SnmpTrapManager trapManager = new SnmpTrapManager();
		trapManager.start();
		try {

			// SNMP trap sender creating
			final OpennmsSnmpTrapSender sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), this.managerV1.getAddress(), this.managerV1.getPort(),
					this.managerV1.getVersion(), this.managerV1.getCommunity());
			sender.logger.setLevel(Level.OFF);

			// SNMP trap sender opening
			sender.open();
			try {

				// Sending test
				final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
				dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
				dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
				final GenericSnmpTrap trap = SnmpTrap.newInstance(0, SnmpOid.newInstance("1.2"), null, dataMap);
				try {
					sender.send(trap);
					fail("None exception has occurred");
				}
				catch (final Exception e) {

					// Checking
					assertSame("Bad type of exception", Exception.class, e.getClass());
					assertEquals("Bad message of exception", "Generic trap type unknown", e.getMessage());
					assertNull("Bad cause of exception", e.getCause());

				}

			}
			finally {

				// SNMP trap sender closing
				sender.close();

			}
		}
		finally {
			trapManager.stop();
		}
	}

	/**
	 * Tests SNMP unknown trap v1 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV1Unknown () throws Exception {
		this.logger.info("Tests SNMP unknown trap v1 sending...");
		final SnmpTrapManager trapManager = new SnmpTrapManager();
		trapManager.start();
		try {

			// SNMP trap sender creating
			final OpennmsSnmpTrapSender sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), this.managerV1.getAddress(), this.managerV1.getPort(),
					this.managerV1.getVersion(), this.managerV1.getCommunity());
			sender.logger.setLevel(Level.OFF);

			// SNMP trap sender opening
			sender.open();
			try {

				// Sending test
				final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
				dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
				dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
				final SnmpTrapUnknown trap = new SnmpTrapUnknown(0, SnmpOid.newInstance("1.2"), dataMap);
				try {
					sender.send(trap);
					fail("None exception has occurred");
				}
				catch (final Exception e) {

					// Checking
					assertSame("Bad type of exception", Exception.class, e.getClass());
					assertEquals("Bad message of exception", "Trap type unknown", e.getMessage());
					assertNull("Bad cause of exception", e.getCause());

				}

			}
			finally {

				// SNMP trap sender closing
				sender.close();

			}
		}
		finally {
			trapManager.stop();
		}
	}

	/**
	 * Tests SNMP trap v2 sending with no session.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV2WithNoSession () throws Exception {
		this.logger.info("Tests SNMP trap v2 sending with no session...");

		// SNMP trap sender creating
		final OpennmsSnmpTrapSender sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), this.managerV2.getAddress(), this.managerV2.getPort(),
				this.managerV2.getVersion(), this.managerV2.getCommunity());
		sender.logger.setLevel(Level.OFF);
		assertFalse("SNMP trap sender already started", sender.isConnected());
		try {

			// Sending test
			final SpecificSnmpTrap trap = SnmpTrap.newInstance(0, SnmpOid.newInstance("1.2"), 3, null);
			sender.send(trap);
			fail("None exception has occurred");

		}
		catch (final Exception e) {

			// Checking
			assertSame("Bad type of exception", Exception.class, e.getClass());
			assertEquals("Bad message of exception", "The connection to the SNMP manager is closed", e.getMessage());
			assertNull("Bad cause of exception", e.getCause());

		}
	}

	/**
	 * Tests SNMP trap v2 sending with no source.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV2WithNoSource () throws Exception {
		this.logger.info("Tests SNMP trap v2 sending with no source...");

		// SNMP trap sender creating
		final OpennmsSnmpTrapSender sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), this.managerV2.getAddress(), this.managerV2.getPort(),
				this.managerV2.getVersion(), this.managerV2.getCommunity());
		sender.logger.setLevel(Level.ALL);
		assertFalse("SNMP trap sender already started", sender.isConnected());
		try {

			// Sending test
			final SpecificSnmpTrap trap = SnmpTrap.newInstance(0, null, 3, null);
			sender.send(trap);
			fail("None exception has occurred");

		}
		catch (final Exception e) {

			// Checking
			assertSame("Bad type of exception", Exception.class, e.getClass());
			assertEquals("Bad message of exception", "The source of trap is missing", e.getMessage());
			assertNull("Bad cause of exception", e.getCause());

		}
	}

	/**
	 * Tests SNMP specific trap v2 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV2Specific () throws Exception {
		this.logger.info("Tests SNMP specific trap v2 sending...");
		final SnmpTrapManager trapManager = new SnmpTrapManager();
		trapManager.start();
		try {

			// SNMP trap sender creating
			final OpennmsSnmpTrapSender sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), this.managerV2.getAddress(), this.managerV2.getPort(),
					this.managerV2.getVersion(), this.managerV2.getCommunity());
			sender.logger.setLevel(Level.ALL);

			// SNMP trap sender opening
			sender.open();
			try {

				// Sending test
				final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
				dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
				dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
				final SpecificSnmpTrap trap = SnmpTrap.newInstance(0, SnmpOid.newInstance("1.2"), 3, dataMap);
				sender.send(trap);

				// Checking
				Thread.sleep(50);
				trapManager.assertTrap(InetAddress.getLocalHost(), "public", trap);
				trapManager.assertNoTrap();

			}
			finally {

				// SNMP trap sender closing
				sender.close();

			}
		}
		finally {
			trapManager.stop();
		}
	}

	/**
	 * Tests SNMP <b>coldStart</b> trap v2 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV2ColdStart () throws Exception {
		this.logger.info("Tests SNMP coldStart trap v2 sending...");
		testSendV2Generic(GenericSnmpTrapType.coldStart);
	}

	/**
	 * Tests SNMP <b>warmStart</b> trap v2 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV2WarmStart () throws Exception {
		this.logger.info("Tests SNMP warmStart trap v2 sending...");
		testSendV2Generic(GenericSnmpTrapType.warmStart);
	}

	/**
	 * Tests SNMP <b>linkDown</b> trap v2 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV2LinkDown () throws Exception {
		this.logger.info("Tests SNMP linkDown trap v2 sending...");
		testSendV2Generic(GenericSnmpTrapType.linkDown);
	}

	/**
	 * Tests SNMP <b>linkUp</b> trap v2 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV2LinkUp () throws Exception {
		this.logger.info("Tests SNMP linkUp trap v2 sending...");
		testSendV2Generic(GenericSnmpTrapType.linkUp);
	}

	/**
	 * Tests SNMP <b>authenticationFailure</b> trap v2 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV2AuthenticationFailure () throws Exception {
		this.logger.info("Tests SNMP authenticationFailure trap v2 sending...");
		testSendV2Generic(GenericSnmpTrapType.authenticationFailure);
	}

	/**
	 * Tests SNMP <b>egpNeighborLoss</b> trap v2 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV2EgpNeighborLoss () throws Exception {
		this.logger.info("Tests SNMP egpNeighborLoss trap v2 sending...");
		testSendV2Generic(GenericSnmpTrapType.egpNeighborLoss);
	}

	/**
	 * Tests SNMP generic trap v2 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @param trapType SNMP trap type.
	 * @throws Exception Exception if an error has occurred.
	 */
	private void testSendV2Generic (final GenericSnmpTrapType trapType) throws Exception {
		final SnmpTrapManager trapManager = new SnmpTrapManager();
		trapManager.start();
		try {

			// SNMP trap sender creating
			final OpennmsSnmpTrapSender sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), this.managerV2.getAddress(), this.managerV2.getPort(),
					this.managerV2.getVersion(), this.managerV2.getCommunity());
			sender.logger.setLevel(Level.OFF);

			// SNMP trap sender opening
			sender.open();
			try {

				// Sending test
				final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
				dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
				dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
				final GenericSnmpTrap trap = SnmpTrap.newInstance(0, SnmpOid.newInstance("1.2"), trapType, dataMap);
				sender.send(trap);

				// Checking
				Thread.sleep(50);
				trapManager.assertTrap(InetAddress.getLocalHost(), "public", trap);
				trapManager.assertNoTrap();

			}
			finally {

				// SNMP trap sender closing
				sender.close();

			}
		}
		finally {
			trapManager.stop();
		}
	}

	/**
	 * Tests SNMP unknown generic trap v2 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV2UnknownGeneric () throws Exception {
		this.logger.info("Tests SNMP unknown generic trap v2 sending...");
		final SnmpTrapManager trapManager = new SnmpTrapManager();
		trapManager.start();
		try {

			// SNMP trap sender creating
			final OpennmsSnmpTrapSender sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), this.managerV2.getAddress(), this.managerV2.getPort(),
					this.managerV2.getVersion(), this.managerV2.getCommunity());
			sender.logger.setLevel(Level.OFF);

			// SNMP trap sender opening
			sender.open();
			try {

				// Sending test
				final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
				dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
				dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
				final GenericSnmpTrap trap = SnmpTrap.newInstance(0, SnmpOid.newInstance("1.2"), null, dataMap);
				try {
					sender.send(trap);
					fail("None exception has occurred");
				}
				catch (final Exception e) {

					// Checking
					assertSame("Bad type of exception", Exception.class, e.getClass());
					assertEquals("Bad message of exception", "Generic trap type unknown", e.getMessage());
					assertNull("Bad cause of exception", e.getCause());

				}

			}
			finally {

				// SNMP trap sender closing
				sender.close();

			}
		}
		finally {
			trapManager.stop();
		}
	}

	/**
	 * Tests SNMP unknown generic trap v2 sending.
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender#send(SnmpTrap)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSendV2Unknown () throws Exception {
		this.logger.info("Tests SNMP unknown trap v2 sending...");
		final SnmpTrapManager trapManager = new SnmpTrapManager();
		trapManager.start();
		try {

			// SNMP trap sender creating
			final OpennmsSnmpTrapSender sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), this.managerV2.getAddress(), this.managerV2.getPort(),
					this.managerV2.getVersion(), this.managerV2.getCommunity());
			sender.logger.setLevel(Level.OFF);

			// SNMP trap sender opening
			sender.open();
			try {

				// Sending test
				final Map<SnmpOid, SnmpTrapData> dataMap = new HashMap<SnmpOid, SnmpTrapData>();
				dataMap.put(SnmpOid.newInstance("1.3.1.0"), new SnmpTrapData(SnmpDataType.octetString, "data 1"));
				dataMap.put(SnmpOid.newInstance("1.3.2.0"), new SnmpTrapData(SnmpDataType.octetString, "data 2"));
				final SnmpTrapUnknown trap = new SnmpTrapUnknown(0, SnmpOid.newInstance("1.2"), dataMap);
				try {
					sender.send(trap);
					fail("None exception has occurred");
				}
				catch (final Exception e) {

					// Checking
					assertSame("Bad type of exception", Exception.class, e.getClass());
					assertEquals("Bad message of exception", "Trap type unknown", e.getMessage());
					assertNull("Bad cause of exception", e.getCause());

				}

			}
			finally {

				// SNMP trap sender closing
				sender.close();

			}
		}
		finally {
			trapManager.stop();
		}
	}

	/**
	 * Tests the handler (snmpReceivedPdu).
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender.NullHandler#snmpReceivedPdu(SnmpSession, int, SnmpPduPacket)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpReceivedPdu () throws Exception {
		this.logger.info("Tests the handler (snmpReceivedPdu)...");
		final OpennmsSnmpTrapSender sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), null, 162, 1, null);
		final OpennmsSnmpTrapSender.NullHandler handler = sender.new NullHandler();
		handler.snmpReceivedPdu(null, -1, null);
	}

	/**
	 * Tests the handler (snmpInternalError).
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender.NullHandler#snmpInternalError(SnmpSession, int, SnmpSyntax)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpInternalError () throws Exception {
		this.logger.info("Tests the handler (snmpInternalError)...");
		final OpennmsSnmpTrapSender sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), null, 162, 1, null);
		final OpennmsSnmpTrapSender.NullHandler handler = sender.new NullHandler();
		handler.snmpInternalError(null, -1, null);
	}

	/**
	 * Tests the handler (snmpTimeoutError).
	 * <p>
	 * Test method for {@link OpennmsSnmpTrapSender.NullHandler#snmpTimeoutError(SnmpSession, SnmpSyntax)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testSnmpTimeoutError () throws Exception {
		this.logger.info("Tests the handler (snmpTimeoutError)...");
		final OpennmsSnmpTrapSender sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), null, 162, 1, null);
		final OpennmsSnmpTrapSender.NullHandler handler = sender.new NullHandler();
		handler.snmpTimeoutError(null, null);
	}

	/**
	 * Tests method for {@link OpennmsSnmpTrapSender#getName()}.
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testGetName () throws Exception {
		this.logger.info("Tests of name...");
		final OpennmsSnmpTrapSender sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), "localhost", 162, 1, "public");
		assertEquals("name is bad", "localhost:162/public", sender.getName());
	}

	/**
	 * Tests method for {@link OpennmsSnmpTrapSender#toString()}.
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testToString () throws Exception {
		this.logger.info("Tests of toString...");
		final OpennmsSupport sender = new OpennmsSnmpTrapSender(InetAddress.getLocalHost(), null, 162, 1, null);
		assertEquals("toString is bad", "SnmpTrapSender:opennms[null:162/null]", sender.toString());
	}

}