package net.sf.snmpadaptor4j.api.opennms;

import static org.junit.Assert.*;
import java.net.InetAddress;
import net.sf.snmpadaptor4j.api.SnmpDaemon;
import net.sf.snmpadaptor4j.api.SnmpDaemonConfiguration;
import net.sf.snmpadaptor4j.api.SnmpMib;
import net.sf.snmpadaptor4j.api.SnmpTrapSender;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Test class for {@link net.sf.snmpadaptor4j.api.opennms.OpennmsSnmpApiFactory}.
 * @author <a href="http://fr.linkedin.com/in/jpminetti/">Jean-Philippe MINETTI</a>
 */
public final class OpennmsSnmpApiFactoryTest {

	/**
	 * Logger.
	 */
	private final Logger logger = Logger.getLogger(OpennmsSnmpApiFactoryTest.class);

	/**
	 * Tests the creation of new SNMP daemon.
	 * <p>
	 * Test method for {@link OpennmsSnmpApiFactory#newSnmpDaemon(SnmpDaemonConfiguration, SnmpMib)}.
	 * </p>
	 */
	@Test
	public void testNewSnmpDaemon () {
		this.logger.info("Tests the creation of new SNMP daemon...");
		final OpennmsSnmpApiFactory factory = new OpennmsSnmpApiFactory();
		final SnmpDaemon daemon = factory.newSnmpDaemon(null, null);
		assertNotNull("The instance is NULL", daemon);
		assertEquals("The instance class is bad", OpennmsSnmpDaemon.class, daemon.getClass());
	}

	/**
	 * Tests the creation of new SNMP trap sender.
	 * <p>
	 * Test method for {@link OpennmsSnmpApiFactory#newSnmpTrapSender(InetAddress, String, int, int, String)}.
	 * </p>
	 * @throws Exception Exception if an error has occurred.
	 */
	@Test
	public void testNewSnmpTrapSender () throws Exception {
		this.logger.info("Tests the creation of new SNMP trap sender...");
		final OpennmsSnmpApiFactory factory = new OpennmsSnmpApiFactory();
		final SnmpTrapSender sender = factory.newSnmpTrapSender(InetAddress.getLocalHost(), null, 162, 1, null);
		assertNotNull("The instance is NULL", sender);
		assertEquals("The instance class is bad", OpennmsSnmpTrapSender.class, sender.getClass());
	}

	/**
	 * Test method for {@link OpennmsSnmpApiFactory#toString()}.
	 */
	@Test
	public void testToString () {
		this.logger.info("Tests of toString...");
		final OpennmsSnmpApiFactory factory = new OpennmsSnmpApiFactory();
		assertEquals("toString is bad", "SnmpApiFactory", factory.toString());
	}

}